{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit selparam;

  {$MODE Delphi}

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, CheckLst, compile;

type
  TselParamDlg = class(TForm)
    ParamListBox: TCheckListBox;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  private
  public
  end;

var
  selParamDlg: TselParamDlg;

implementation

  {$R *.lfm}

procedure TselParamDlg.FormActivate(Sender: TObject);
var p : integer;
    index : integer;
    hauteur : integer;
begin
     inherited;
     ParamListBox.clear;
     for p := 0 to pred(NbreConst) do begin
         index := indexConst[p];
         ParamListBox.Items.Add(grandeurs[index].nom);
         ParamListBox.checked[p] := ListeConstAff.indexOf(grandeurs[index].nom)>=0;
     end;
     hauteur := ParamListBox.Count*(paramListBox.itemHeight+1)+30;
     if hauteur>180 then height := hauteur;

end;

procedure TselParamDlg.OKBtnClick(Sender: TObject);
var p : integer;
begin
     ListeConstAff.clear;
     for p := 0 to pred(ParamListBox.items.count) do begin
         if ParamListBox.checked[p] then
             ListeConstAff.add(ParamListBox.items[p])
     end;
end;

end.
