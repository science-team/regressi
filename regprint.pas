{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit regprint;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons, inifiles,
  StdCtrls, ExtCtrls, Spin, Dialogs, grids, printers, sysutils,
  constreg, regutil, compile, graphker, graphvar, valeurs, aidekey;

type
  TPrintDlg = class(TForm)
    PanelOption: TPanel;
    PrintBtn: TBitBtn;
    CancelBtn: TBitBtn;
    HelpBtn: TBitBtn;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DateCB: TCheckBox;
    NomFichierCB: TCheckBox;
    GrandeursCB: TCheckBox;
    VariabGB: TGroupBox;
    TableauVariabCB: TCheckBox;
    ModeleVariabCB: TCheckBox;
    GrapheVariabCB: TCheckBox;
    TangenteCB: TCheckBox;
    ParamGB: TGroupBox;
    GrapheParamCB: TCheckBox;
    TableauParamCB: TCheckBox;
    ModeleParamCB: TCheckBox;
    FourierGB: TGroupBox;
    TableauFourierCB: TCheckBox;
    GrapheFourierCB: TCheckBox;
    StatistiqueGB: TGroupBox;
    TableauStatCB: TCheckBox;
    GrapheStatCB: TCheckBox;
    GroupBox2: TGroupBox;
    EnteteEdit: TEdit;
    PanelPages: TPanel;
    GraphePageRG: TRadioGroup;
    PagesBtn: TSpeedButton;
    PrintCB: TComboBox;
    ImprimanteGB: TGroupBox;
    PrinterBtn: TSpeedButton;
    GridPrintCB: TCheckBox;
    procedure PrintBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure PagesBtnClick(Sender: TObject);
    procedure GraphePageRGClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PrintCBChange(Sender: TObject);
    procedure PrinterBtnClick(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
  private
  public
  end;

var
  PrintDlg: TPrintDlg;

implementation

uses  Graphpar, Graphfft, Statisti, Curmodel, Selpage;

  {$R *.lfm}

const
    titreReg = 'Impression';

procedure TPrintDlg.PrintBtnClick(Sender: TObject);
var
  debutImp : integer;
  page,sauvePageCourante : codePage;

Procedure ImprimeParam;
var i : integer;
begin
    if GrapheParamCB.visible and GrapheParamCB.checked then
         FgrapheParam.ImprimerGraphe(debutImp);
    if ModeleParamCB.checked then begin
         VerifOrpheline(FgrapheParam.MemoModele.Lines.Count+FgrapheParam.MemoResultat.Lines.Count+1,debutImp);
         ImprimerLigne(stModelisation,debutImp);
         for i := 0 to pred(FgrapheParam.MemoModele.Lines.count) do
             ImprimerLigne(FgrapheParam.MemoModele.Lines[i],debutImp);
         for i := 0 to pred(FgrapheParam.MemoResultat.Lines.Count) do
             ImprimerLigne(FgrapheParam.MemoResultat.Lines[i],debutImp);
    end;
    if TableauParamCB.checked then begin
        Fvaleurs.TraceGridParam;
        VerifOrpheline(Fvaleurs.GridParam.RowCount,debutImp);
        ImprimerGrid(Fvaleurs.GridParam,debutImp);
        if Fvaleurs.GridParamGlb.visible then begin
           VerifOrpheline(Fvaleurs.GridParamGlb.RowCount,debutImp);
           ImprimerGrid(Fvaleurs.GridParamGlb,debutImp);
        end;
    end;
end;

Procedure ImprimeStat;
begin
      if GrapheStatCB.checked then
          FgrapheStat.ImprimerGraphe(debutImp);
      if TableauStatCB.checked then begin
         ImprimerGrid(FgrapheStat.StatGrid,debutImp);
         ImprimerGrid(FgrapheStat.DistGrid,debutImp)
      end;
end;

Procedure ImprimeFourier;
begin
        if GrapheFourierCB.checked then
             FgrapheFFT.ImprimerGraphe(debutImp);
end;

Procedure ImprimerTableauVariab;
var i,index : integer;
    texte : string;
begin
      Fvaleurs.MajGridVariab := true;
      Fvaleurs.TraceGridVariab;
      ImprimerLigne(pages[pageCourante].TitrePage,debutImp);
      if ListeConstAff.count>0 then begin
         texte := '';
         for i := 0 to pred(ListeConstAff.count) do begin
           index := indexNom(ListeConstAff[i]);
           texte := texte+grandeurs[index].FormatNomEtUnite(pages[pageCourante].valeurConst[index]);
         end;
         ImprimerLigne(texte,debutImp);
      end;   
      ImprimerGrid(Fvaleurs.GridVariab,debutImp);
end;

Procedure ImprimerModele(page : codePage);
var i : integer;
begin with pages[page] do begin
      DebutImpressionTexte(debutImp);
      ImprimerLigne(TitrePage,debutImp);
      for i := 0 to pred(TexteResultatModele.Count) do
          ImprimerLigne(TexteResultatModele[i],debutImp);
end end;

Procedure ImprimerModeles;
var page : integer;
begin
   if ModeleVariabCB.checked then
      if (GraphePageRG.itemIndex=0)
           then ImprimerModele(pageCourante)
           else for page := 1 to NbrePages do
                    if pages[page].active then
                          ImprimerModele(Page);
end;

Procedure ImprimerGrapheVariab;
begin
      FgrapheVariab.ImprimerGraphe(debutImp);
      if not(FgrapheVariab.graphes[1].superposePage) and
          ModeleVariabCB.checked then
            ImprimerModele(pageCourante);
end;

var i : integer;
    titre : string;
begin
          GridPrint := GridPrintCB.checked;
          screen.cursor := crHourGlass;
          enabled := false;
          Printer.title := 'Regressi '+NomFichierData;
try
          Printer.BeginDoc;
          debutImp := 0;
          DebutImpressionTexte(debutImp);
          userName := enTeteEdit.text;
          titre := enTeteEdit.text;
          if dateCB.checked then
             titre := titre+' '+DateToStr(Now);
          if titre<>'' then ImprimerLigne(Titre,debutImp);
          if NomFichierCB.checked then ImprimerLigne(NomFichierData,debutImp);
// Grandeurs
          if GrandeursCB.checked then
             for i := 0 to pred(Fvaleurs.memo.lines.count) do
                 ImprimerLigne(Fvaleurs.Memo.lines[i],debutImp);
// Valeurs des variables
           if (modeAcquisition=AcqSimulation) and
              not(Fvaleurs.PagesIndependantesCB.checked) then with grandeurs[0] do
                ImprimerLigne(nom+'='+
                    formatValeurEtUnite(pages[1].MiniSimulation)+'..'+
                    formatValeurEtUnite(pages[1].MaxiSimulation),debutImp);
          if TableauVariabCB.visible and
             TableauVariabCB.checked then if GraphePageRG.itemIndex=0
             then ImprimerTableauVariab
             else begin
                 SauvePageCourante := pageCourante;
                 for page := 1 to NbrePages do if pages[page].active then begin
                     pageCourante := page;
                     if (modeAcquisition=AcqSimulation) and
                        (Fvaleurs.PagesIndependantesCB.checked) then with grandeurs[0] do
                              ImprimerLigne(nom+'='+
                                   formatValeurEtUnite(pages[page].MiniSimulation)+'..'+
                                   formatValeurEtUnite(pages[page].MaxiSimulation),debutImp);
                     if pages[page].commentaireP<>''
                         then ImprimerLigne(pages[page].commentaireP,debutImp);
                     ImprimerTableauVariab;
                 end;
                 PageCourante := SauvePageCourante;
                 Fvaleurs.MajGridVariab := true;
             end;
// Modélisation
          if ModeleVariabCB.checked then begin
             ImprimerLigne(stModelisation,debutImp);
             for i := 0 to pred(TexteModele.count) do
                   ImprimerLigne(TexteModele[i],debutImp);
          end;
// Graphe et valeurs modélisation
      if GrapheVariabCB.checked
         then if GraphePageRG.itemIndex=0
              then ImprimerGrapheVariab
              else if FgrapheVariab.graphes[1].superposePage
                  then begin
                     ImprimerModeles;
                     ImprimerGrapheVariab;
                  end
                  else begin
                      SauvePageCourante := pageCourante;
                      for page := 1 to NbrePages do with pages[page] do
                          if active then begin
                             pageCourante := page;
                             for i := 1 to 3 do with FgrapheVariab.Graphes[i] do
                                 if paintBox.Visible then begin
                                    Modif := [gmPage];
                                    PaintBox.Refresh;
                                 end;
                             if commentaireP<>'' then ImprimerLigne(titrePage,debutImp);
                             ImprimerGrapheVariab;
                          end;
                      PageCourante := SauvePageCourante;
                      for i := 1 to 3 do with FgrapheVariab.Graphes[i] do
                          if paintBox.visible then begin
                             Modif := [gmPage];
                             PaintBox.Refresh;
                          end;
              end // GrapheVariabCB.checked
         else ImprimerModeles;
      if TangenteCB.visible and TangenteCB.checked then begin
           FgrapheVariab.graphes[1].RemplitTableauEquivalence;
           if curseurModeleDlg<>nil then
               ImprimerGrid(curseurModeleDlg.tableau,debutImp);
      end;
      if FourierGB.visible then ImprimeFourier;
      if StatistiqueGB.visible then ImprimeStat;
      if ParamGB.visible then ImprimeParam;
      Printer.EndDoc;
      except
             on E: Exception do begin
                showMessage(E.message);
                try
                Printer.Abort;
                except
                end;
             end;
      end;
      screen.cursor := crDefault;
      enabled := true;
      printer.copies := 1;
      Fvaleurs.MajGridVariab := true;
end;

procedure TPrintDlg.FormActivate(Sender: TObject);
begin
    inherited;
    printCB.visible := (printer.printers.Count>1);
    if printCB.visible then begin
       printCB.items := printer.printers;
       printCB.itemIndex := printer.printerIndex;
    end;
    GridPrintCB.checked := GridPrint;                     
    TableauParamCB.visible := (NbreConst+NbreParam[paramNormal])>0;
    GrapheParamCB.visible := FgrapheParam<>nil;
    ModeleParamCB.visible := (FgrapheParam<>nil) and
                             (FgrapheParam.PanelModele.visible);
    if not ModeleParamCB.visible then ModeleParamCB.checked := false;
    ParamGB.visible := TableauParamCB.visible;
    TangenteCB.caption := NomLigneRappel[LigneRappelCourante];
    TangenteCB.visible := FgrapheVariab.graphes[1].equivalences[pageCourante].count>0;
    ModeleVariabCB.visible := ModeleDefini in FgrapheVariab.etatModele;
    if not ModeleVariabCB.visible then ModeleVariabCB.checked := false;
    PanelPages.Visible := NbrePages>1;
    GraphePageRG.itemIndex := 0;
    enTeteEdit.text := UserName;
end;

procedure TPrintDlg.HelpBtnClick(Sender: TObject);
begin
    Application.HelpContext(HELP_Impression)
end;

procedure TPrintDlg.PagesBtnClick(Sender: TObject);
begin
    if selectPageDlg=nil then Application.CreateForm(TselectPageDlg, selectPageDlg);
    SelectPageDlg.caption := 'Choix des pages à imprimer';
    SelectPageDlg.appelPrint := true;
    SelectPageDlg.showModal
end;

procedure TPrintDlg.GraphePageRGClick(Sender: TObject);
begin
    PagesBtn.visible := graphePageRG.itemIndex=1
end;

procedure TPrintDlg.FormCreate(Sender: TObject);
var Rini : TIniFile;
begin
  inherited;
  Rini := TIniFile.create(NomFichierIni);
  try
  with Rini do begin
       grandeursCB.checked := readBool(TitreReg,'grandeurs',true);
       tableauVariabCB.checked := readBool(TitreReg,'tabVar',false);
       grapheVariabCB.checked := readBool(TitreReg,'grVar',true);
       grapheParamCB.checked := readBool(TitreReg,'grPar',false);
       modeleVariabCB.checked := readBool(TitreReg,'modVar',true);
       modeleParamCB.checked := readBool(TitreReg,'modPar',false);
       tangenteCB.checked := readBool(TitreReg,'tangente',true);
       tableauParamCB.checked := readBool(TitreReg,'tabPar',false);
       grapheFourierCB.checked := readBool(TitreReg,'grFFT',true);
       tableauFourierCB.checked := readBool(TitreReg,'tabFFT',false);
       grapheStatCB.checked := readBool(TitreReg,'grStat',true);
       tableauStatCB.checked := readBool(TitreReg,'tabStat',false);
       dateCB.checked := readBool(TitreReg,'date',true);
       nomFichierCB.checked := readBool(TitreReg,'nomFile',false);
  end;
  finally
  Rini.free;
  end;
  enTeteEdit.text := userName;
end;

procedure TPrintDlg.PrintCBChange(Sender: TObject);
begin
   printer.PrinterIndex := printCB.itemIndex
end;

procedure TPrintDlg.PrinterBtnClick(Sender: TObject);
begin
end;

procedure TPrintDlg.FormDeactivate(Sender: TObject);
var Rini : TIniFile;
begin
  try
  Rini := TIniFile.create(NomFichierIni);
  with Rini do begin
       writeBool(TitreReg,'grandeurs',grandeursCB.checked);
       writeBool(TitreReg,'tabVar',tableauVariabCB.checked);
       writeBool(TitreReg,'grVar',grapheVariabCB.checked);
       writeBool(TitreReg,'grPar',grapheParamCB.checked);
       writeBool(TitreReg,'modVar',modeleVariabCB.checked);
       writeBool(TitreReg,'modPar',modeleParamCB.checked);
       writeBool(TitreReg,'tangente',tangenteCB.checked);
       writeBool(TitreReg,'tabPar',tableauParamCB.checked);
       writeBool(TitreReg,'grFFT',grapheFourierCB.checked);
       writeBool(TitreReg,'tabFFT',tableauFourierCB.checked);
       writeBool(TitreReg,'grStat',grapheStatCB.checked);
       writeBool(TitreReg,'tabStat',tableauStatCB.checked);
       writeBool(TitreReg,'date',dateCB.checked);
       writeBool(TitreReg,'nomFile',nomFichierCB.checked);
  end;
  except
  end;
  inherited;
end;

end.

