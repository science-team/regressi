{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit saveparam;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, ExtCtrls, CheckLst,
  math, regutil, compile, aideKey;

type
  TSaveParamDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    GroupBox1: TGroupBox;
    HelpBtn: TBitBtn;
    ParamListBox: TCheckListBox;
    Memo1: TMemo;
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
  private
  public
  end;

var
  SaveParamDlg: TSaveParamDlg;

implementation

  {$R *.lfm}

procedure TSaveParamDlg.FormActivate(Sender: TObject);
var i,j,page : integer;
begin
     inherited;
     with ParamListBox do begin
        Clear;
        for i := 1 to NbreParam[paramNormal] do
            Items.add(Parametres[paramNormal,i].nom);
        if modelePagesIndependantes then
           for page := 1 to NbrePages do with pages[page] do
              for j := 1 to MaxParametres do
                  if (nomParam[j]<>'') and
                     (Items.indexOf(nomParam[j])<0) then
                      Items.add(nomParam[j]);
        for i := 0 to pred(items.count) do
            checked[i] := true;
     end;
end;

procedure TSaveParamDlg.OKBtnClick(Sender: TObject);
var page : integer;
    indexC : integer;
    i,j,k : integer;
begin
     for i := 0 to pred(ParamListBox.count) do
         if ParamListBox.checked[i] then begin
          indexC := indexNomVariab(ParamListBox.items[i]);
          if indexC=grandeurInconnue then
             indexC := AjouteExperimentale(ParamListBox.items[i],constante);
          for page := 1 to NbrePages do with pages[page] do begin
              if modelePagesIndependantes then begin
                 j := 1;
                 k := 0;
                 repeat
                    if nomParam[j]=ParamListBox.items[i]
                       then k := j
                       else inc(j);
                 until (nomParam[j]='') or (k>0) or (j>MaxParametres);
              end
              else k := i+1;
              if k=0 then begin
                 valeurConst[indexC] := Nan;
                 incertConst[indexC] := Nan;
              end
              else begin
                 valeurConst[indexC] := valeurParam[paramNormal,k];
                 incertConst[indexC] := incertParam[k];
                 grandeurs[indexC].nomUnite := parametres[paramNormal,k].nomUnite;
                 grandeurs[indexC].fonct.expression := parametres[paramNormal,k].fonct.expression;
              end;
          end;
     end;
end;

procedure TSaveParamDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(HELP_SauvegardedelaModelisation)
end;

end.
