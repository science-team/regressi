{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit optfft;

  {$MODE Delphi}

interface

uses
  SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Spin, ComCtrls, Math,
  regutil,  fft, compile, graphker, selpage, aideKey;

type
  TOptionsFFTDlg = class(TForm)
    PanelBoutons: TPanel;
    BtnOK: TBitBtn;
    BtnCancel: TBitBtn;
    BtnHelp: TBitBtn;
    PanelOptions: TPanel;
    PageControl: TPageControl;
    AbscisseTS: TTabSheet;
    CalculTS: TTabSheet;
    FenetreRadioG: TRadioGroup;
    OrdonneeTS: TTabSheet;
    SpinMindB: TSpinEdit;
    EditMindB: TEdit;
    LabeldB: TLabel;
    OrdonneeRG: TRadioGroup;
    SuperpositionTS: TTabSheet;
    SuperPagesCB: TCheckBox;
    AnalyseurCB: TCheckBox;
    PagesBtn: TSpeedButton;
    FreqReduiteCB: TCheckBox;
    LabelNomTemps: TLabel;
    OptimiseNbreHarmCB: TCheckBox;
    HarmMinSE: TSpinEdit;
    Label4: TLabel;
    DecalageSE: TSpinEdit;
    Label2: TLabel;
    DecibelCB: TCheckBox;
    EnveloppeCB: TCheckBox;
    ContinuCB: TCheckBox;
    NbreRaiesEdit: TLabeledEdit;
    NbreRaiesUD: TUpDown;
    HarmoniqueAffCB: TCheckBox;
    LabelWidth: TLabel;
    WidthEcranSE: TSpinEdit;
    ReglePeriodeCB: TCheckBox;
    ListeVariableBox: TListBox;
    GrandeurBtn: TBitBtn;
    AjusteRectangleRG: TRadioGroup;
    procedure FormActivate(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure SpinMindBDownClick(Sender: TObject);
    procedure SpinMindBUpClick(Sender: TObject);
    procedure dBCheckBoxClick(Sender: TObject);
    procedure PagesBtnClick(Sender: TObject);
    procedure SuperPagesCBClick(Sender: TObject);
    procedure EditHarmoniqueKeyPress(Sender: TObject; var Key: Char);
    procedure GrandeurFreqBtn(Sender: TObject);
  private
    procedure MajdB(down  : boolean);
  public
    MiniDecibel : double;
    OrdonneeFFT : TordonneeFFT;
  end;

var
  OptionsFFTDlg : TOptionsFFTDlg;

implementation

uses Graphfft, modif;

  {$R *.lfm}

procedure TOptionsFFTDlg.FormActivate(Sender: TObject);
var i : integer;
begin
     inherited;
     FenetreRadioG.itemIndex := ord(Fenetre);
     HarmMinSE.value := PrecisionFFT;
     DecalageSE.value := decalageFFT;
     OptimiseNbreHarmCB.Checked := NbreHarmoniqueOptimise;
     NbreRaiesUD.Position := NbreHarmoniqueAff;
     HarmoniqueAffCB.checked := HarmoniqueAff;
     i := round(20*log10(MiniDecibel));
     EditMindB.text := IntToStr(i);
     LabelNomTemps.caption := 'Grandeur inverse de '+grandeurs[indexTri].nom+' : ' ;
     GrandeurBtn.caption := grandeurs[cFrequence].nom;
     EnveloppeCB.checked := FgrapheFFT.enveloppeSpectre;
     ordonneeRG.itemIndex := ord(ordonneeFFT);
     LabeldB.visible := DecibelCB.checked;
     EditMindB.visible := LabeldB.visible;
     SpinMindB.visible := LabeldB.visible;
     widthEcranSE.value := penWidthVGA;
     reglePeriodeCB.checked := avecReglagePeriode;
     AnalyseurCB.checked := OgAnalyseurLogique in FgrapheFFT.GrapheFrequence.OptionGraphe;
     ajusteRectangleRG.itemIndex := ord(zeroPaddingPermis);
//     sincCB.checked := sincPermis;
     ListeVariableBox.upDate;
end;

procedure TOptionsFFTDlg.BtnOKClick(Sender: TObject);
begin with FgrapheFFT,grapheFrequence do begin
    NbreHarmoniqueAff := NbreRaiesUD.Position;
    HarmoniqueAff := HarmoniqueAffCB.checked;
    NbreHarmoniqueOptimise := OptimiseNbreHarmCB.Checked;
    PrecisionFFT := HarmMinSE.value;
    Fenetre := Tfenetre(FenetreRadioG.itemIndex);
    zeroPaddingPermis := ajusteRectangleRG.itemIndex=1;
//    Grandeurs[cFrequence].nom := editNomFrequence.text;
//    Grandeurs[cFrequence].formatU := grandeurs[indexTri].formatU;
//    Grandeurs[cFrequence].precisionU := grandeurs[indexTri].precisionU;
    EnveloppeSpectre := enveloppeCB.checked;
    OrdonneeFFT := TordonneeFFT(ordonneeRG.itemIndex);
    DecalageFFT := decalageSE.value;
    penWidthVGA := widthEcranSE.value;
    avecReglagePeriode := reglePeriodeCB.checked;
//    sincPermis := sincCB.checked;
    if decibelCB.checked
        then DecadeDB := DecadeSE.value
        else DecadeDB := 0;
    if AnalyseurCB.checked
         then begin
            include(OptionGraphe,OgAnalyseurLogique);
            include(grapheTemps.OptionGraphe,OgAnalyseurLogique)
         end
         else begin
            exclude(OptionGraphe,OgAnalyseurLogique);
            exclude(grapheTemps.OptionGraphe,OgAnalyseurLogique);
         end;
end end;

procedure TOptionsFFTDlg.BtnHelpClick(Sender: TObject);
begin
     Application.HelpContext(HELP_OptionsFourier)
end;

procedure TOptionsFFTDlg.MajdB(down  : boolean);
var N : integer;
begin
     N := StrToInt(EditMindB.Text);
     if down
        then begin if N>-200 then dec(N,10) end
        else begin if N<200 then inc(N,10) end;
     EditMindB.Text := IntToStr(N);
     MiniDecibel := power(10,N/20.0);
end;

procedure TOptionsFFTDlg.GrandeurFreqBtn(Sender: TObject);
begin  // modif du nom, de l'unité et du format
     modifDlg := TmodifDlg.create(self);
     modifDlg.index := cFrequence;
     if modifDlg.showModal=mrOK then
end;

procedure TOptionsFFTDlg.SpinMindBDownClick(Sender: TObject);
begin
    MajdB(true)
end;

procedure TOptionsFFTDlg.SpinMindBUpClick(Sender: TObject);
begin
     MajdB(false)
end;

procedure TOptionsFFTDlg.dBCheckBoxClick(Sender: TObject);
begin
     LabeldB.visible := decibelCB.checked;
     EditMindB.visible := LabeldB.visible;
     SpinMindB.visible := LabeldB.visible;
end;

procedure TOptionsFFTDlg.PagesBtnClick(Sender: TObject);
begin
     if selectPageDlg=nil then Application.CreateForm(TselectPageDlg, selectPageDlg);
     SelectPageDlg.appelPrint := false;
     SelectPageDlg.caption := 'Choix des pages affichées sur le graphe';
     SelectPageDlg.showModal
end;

procedure TOptionsFFTDlg.SuperPagesCBClick(Sender: TObject);
begin
   PagesBtn.visible := SuperPagesCB.checked
end;

procedure TOptionsFFTDlg.EditHarmoniqueKeyPress(Sender: TObject;
  var Key: Char);
begin
   verifKeyGetInt(key)
end;

end.
