{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit FusionU;

  {$MODE Delphi}

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, regutil, compile;

type
  TFusionDlg = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    HelpBtn: TBitBtn;
    Label1: TLabel;
    NomCB: TComboBox;
    procedure FormActivate(Sender: TObject);
  private
  public
    nom : string;
  end;

var
  FusionDlg: TFusionDlg;

implementation

  {$R *.lfm}

procedure TFusionDlg.FormActivate(Sender: TObject);
var i : integer;
begin
     label1.caption := nom+' n''existe pas';
     nomCB.Clear;
     for i := 0 to pred(NbreGrandeurs) do with grandeurs[i] do
         if (genreG=variable) and (fonct.genreC=g_experimentale) then nomCB.items.add(nom);
     nomCB.ItemIndex := 0;
end;

end.
