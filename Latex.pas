{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit Latex;

interface

uses sysutils, classes, Clipbrd, grids, regutil, compile;

procedure AjouteGrandeur(index : integer);
procedure AjouteModele(amodele : Tmodele);
procedure DebutDoc(titre,auteur,date : string);
procedure FinDoc;

function TranslateNomTexte(anom : String) : String;
function TranslateNomMath(anom : String) : String;
Function TranslateExpression(F:Pelement) : String;

Procedure AjouteLigne(aligne : string);
Procedure AjouteLigneItem(aligne : string);
Procedure AjouteGrid(agrid : TstringGrid);
Procedure AjouteSection(titre : String);
Procedure AjouteParagraphe(titre : String);
Procedure AjouteLigneLatex(aligne : String);

Procedure SaveFile(nomFichier : string);

implementation

var latexStr : TstringList;
    avecExposant : boolean;

Function UnicodeToLatexMath(carac : string) : String;
var j : integer;
begin
     avecExposant := false;
     if carac=alphaMin then result := '\alpha '
     else if carac=betaMin then result := '\beta '
     else if carac=gammaMin then result := '\gamma '
     else if carac=gammaMaj then result := '\Gamma '
     else if carac=deltaMin then result := '\delta '
     else if carac=deltaMaj then result := '\Delta '
     else if carac=epsilonMin then result := '\epsilon '
     else if carac=zetaMin then result := '\zeta '
     else if carac=etaMin then result := '\eta '
     else if carac=kappaMin then result := '\kappa '
     else if carac=lambdaMin then result := '\lambda '
     else if carac=lambdaMaj then result := '\Lambda '
     else if carac=muMin then result := '\mu '
     else if carac=nuMin then result := '\nu '
     else if carac=xiMin then result := '\xi '
     else if carac=xiMaj then result := '\Xi '
     else if carac=pimin then result := '\pi '
     else if carac=piMaj then result := '\Pi '
     else if carac=rhoMin then result := '\rho '
     else if carac=sigmaMin then result := '\sigma '
     else if carac=sigmaMaj then result := '\Sigma '
     else if carac=tauMin then result := '\tau '
     else if carac=thetaMin then result := '\theta '
     else if carac=thetaMaj then result := '\Theta '
     else if carac=phiMin then result := '\phi '
     else if carac=phiMaj then result := '\Phi '
     else if carac=chiMin then result := '\chi '
     else if carac=psiMin then result := '\psi '
     else if carac=psiMaj then  result := '\Psi '
     else if carac=omegaMin then result := '\omega '
     else if carac=omegaMaj then result := '\Omega '
     else if carac='�' then result := '\S'
     else if carac='&' then result := '\&'
     else if carac='#' then result := '\#'
     else if carac='�' then result := '\degres'
     else if carac='�' then result := '\pm'
     else if carac='%' then result := '\%'
     else if carac=InfEgal then result := '\leq'
     else if carac=SupEgal then result := '\geq'
     else begin
                         result := carac;
                         for j := 0 to 9 do
                            if carac=ChiffreExp[j] then begin
                               result := intToStr(j);
                               break;
                            end;
                      end;
end;

Function UnicodeToLatexTexte(carac : UnicodeChar) : String;
var j : integer;
begin
     avecExposant := false;
     if carac=alphaMin then result := '$\alpha$'
     else if carac=betaMin then result := '$\beta$'
     else if carac=gammaMin then result := '$\gamma$'
     else if carac=gammaMaj then result := '$\Gamma$'
     else if carac=deltaMin then result := '$\delta$'
     else if carac=deltaMaj then result := '$\Delta$'
     else if carac=epsilonMin then result := '$\epsilon$'
     else if carac=zetaMin then result := '$\zeta$'
     else if carac=etaMin then result := '$\eta$'
     else if carac=kappaMin then result := '$\kappa$'
     else if carac=lambdaMin then result := '$\lambda$'
     else if carac=lambdaMaj then result := '$\Lambda$'
     else if carac=muMin then result := '$\mu$'
     else if carac=nuMin then result := '$\nu$'
     else if carac=xiMin then result := '$\xi$'
     else if carac=xiMaj then result := '$\Xi$'
     else if carac=pimin then result := '$\pi$'
     else if carac=piMaj then result := '$\Pi$'
     else if carac=rhomin then result := '$\rho$'
     else if carac=sigmaMin then result := '$\sigma$'
     else if carac=sigmaMaj then result := '$\Sigma$'
     else if carac=tauMin then result := '$\tau$'
     else if carac=thetaMin then result := '$\theta$'
     else if carac=thetaMaj then result := '$\Theta$'
     else if carac=phiMin then result := '$\phi$'
     else if carac=phiMaj then result := '$\Phi$'
     else if carac=chiMin then result := '$\chi$'
     else if carac=psiMin then result := '$\psi$'
     else if carac=psiMaj then result := '$\Psi$'
     else if carac=omegaMin then result := '$\omega$'
     else if carac=omegaMaj then result := '$\Omega$'
     else if carac='%' then result := '\%'
     else if carac='�' then result := '\S'
     else if carac='&' then result := '\&'
     else if carac='#' then result := '\#'
     else if carac='�' then result := '\degres'
     else if carac=InfEgal then result := '$\leq$'
     else if carac=SupEgal then result := '$\geq$'
     else if carac='�' then result := '$\pm$'
                      else begin
                         result := carac;
                         for j := 0 to 9 do
                            if carac=ChiffreExp[j] then begin
                               avecExposant := true;
                               result := intToStr(j);
                               break;
                            end;
                      end;
end;

function TranslateNomMath(anom : String) : String;
var i : integer;
begin
    result := '';
    for i := 1 to length(anom) do
        result := result+unicodeToLatexMath(anom[i]);
end;

function TranslateNomTexte(anom : String) : String;
var i : integer;
    tr : String;
    exposantEnCours : boolean;
begin
    result := '';
    exposantEnCours := false;
    for i := 1 to length(anom) do begin
       // tr := unicodeToLatexTexte(anom[i]);
        if exposantEnCours
           then if avecExposant
              then
              else begin
                 exposantEnCours := false;
                 result := result+'}$';
              end
           else if avecExposant
              then begin
               result := result+'${^';
               exposantEnCours := true;
              end;
        result := result+tr;
    end;
    if exposantEnCours then
        result := result+'}$';
end;

Function TranslateExpression(F:Pelement) : String;
// renvoie le code Tex d'un arbre point� par F

Function calcIf(ff : Pelement): String;
begin with ff^ do begin
   result := result+' \textrm{if } '+TranslateExpression(test)+' \newline';
   result := result+' \textrm{ then } '+TranslateExpression(positif)+' \newline';
   result := result+' \textrm{ else } '+TranslateExpression(negatif);
end end;

Function calcPW(ff : Pelement): string;
begin
   result := '\begin{dcases} ';
   repeat
       result := result + translateExpression(ff.PWtest)+'& '+ translateExpression(ff.PWthen)+'\newline';
       ff := ff.PWtestElse;
       if ff.typeElement<>Piecewise then begin
          result := result + '\text{else} & '+ translateExpression(ff)+'\newline';
          ff := nil;
       end;
   until ff=nil;
   result := result+'\end{dcases}';
end;

Function calculFonctionGlb(F : Pelement): String;

Function CalcIntegrale : String; // int�grale operandGlb / varMuette de a � b
var a,b,sigma,vm : string;
begin with F^ do begin
        a := translateExpression(operandDebut);
        b := translateExpression(operandFin);
        vm := translateNomMath(Grandeurs[cMuette].nom);
        sigma := translateExpression(operandGlb);
        result := '\int'+'_{'+a+'}'+'^{'+b+'}'+sigma+'\, \mathrm{d} '+vm;
end end; // CalcIntegrale

var freqLoc,rapportCyclique,anom,anomY : string;
begin with F^ do begin // calculFonctionGlb
    result := '';
    aNom := translateNomMath(varX.nom);
    if operandGlb<>nil then freqLoc := translateExpression(OperandGlb);
    if OperandDebut<>nil then rapportCyclique := translateExpression(OperandDebut);
    case CodeFglb of
        maximum,position,minimum,moyenneAll,somme,efficace,frequence,
                    ecartType,surface,phase,initial :
                    result := LowerCase(nomFonctionGlb[CodeFglb])+'('+aNom+')';
        pente,origine :  begin
           aNomY := translateNomMath(varY.nom);
           result := LowerCase(nomFonctionGlb[CodeFglb])+'('+aNom+','+anomY+')';
        end;
        derivee : begin
           aNomY := translateNomMath(varY.nom);
           result := '\frac{ \mathrm{d} '+anomY+'}{ \mathrm{d} '+aNom+'}';
        end;
        equation : result := freqLoc;
        integraleDefinie,integraleMuette : result := CalcIntegrale;
        fonctionPage : result := 'page';
        cCreneau : result := 'Creneau('+freqLoc+','+rapportCyclique+')';
        cCnp : result := 'C_{'+freqLoc+'}{'+rapportCyclique+'}';
        cTriangle : result := 'Triangle('+freqLoc+','+rapportCyclique+')';
        cGauss : CalculFonctionGlb := 'Gauss('+freqLoc+','+rapportCyclique+')';
        cPhaseModulo : result := 'PhaseModulo('+anom+freqLoc+','+rapportCyclique+')';
        cPoisson : result := 'Poisson('+anom+','+freqLoc+')';
        cBessel : CalculFonctionGlb := 'BesselJ('+rapportCyclique+','+freqLoc+')';
        cPeigne : result := 'Peigne('+rapportCyclique+','+freqLoc+')';
   end; // case
end end; // calculFonctionGlb

Var X,Y : String;
    parX,parY : boolean; // parenth�se X Y
Begin // TranslateExpression
  result := '';
  if F=nil then exit;
  With F^ do Case TypeElement of
       Operateur: Begin
            X := translateExpression(OperG);
            parX := (Operg.TypeElement=Operateur) and
                    charInSet(OperG.CodeOp,['+','-']) and
                    charInSet(CodeOp,['*','^','&','#','@']);
            if parX then X := '('+X+')';
            Y := translateExpression(OperD);
            parY := (OperD.TypeElement=Operateur) and
                    charInSet(OperD.CodeOp,['+','-']) and
                    charInSet(CodeOp,['*','&','#','@']);
            if parY then Y := '('+Y+')';
            Case CodeOp of
                 '+','<','-','>','=' : result := X+CodeOp+Y;
                 '*' : result := X+'\cdot '+Y; // ou times ?
                 '/' : result := '\frac{'+X+'}{'+Y+'}';
                 '@' : result := X+'+'+Y;
                 '#' : result := X+'-'+Y;
                 '^' : result := X+'^{'+Y+'}';
                 '&' : result := '\arg('+X+'+\jmath'+Y+')';
             end;{case codeOp}
       end;{operateur}
       Fonction: Begin
             X := translateExpression(Operand);
              case codef of
                racine : result := '\sqrt{'+X+'}';
                oppose: result := '-'+X;
                absolue: result := '\| '+X+' \|';
                inverse: result := '\frac{1}{'+X+'}';
                carre : result := X+'^{2}';
                lognep: result := '\ln ('+X+')';
                logdec: result := '\log ('+X+')';
                sinus: result := '\sin ('+X+')';
                cosinus: result := '\cos ('+X+')';
                tangente: result := '\tan ('+X+')';
                arcSinus: result := '\arcsin ('+X+')';
                arcCosinus: result := '\arccos ('+X+')';
                arcTangente: result := '\arctan ('+X+')';
                cosHyper: result := '\cosh ('+X+')';
                sinHyper: result := '\sinh ('+X+')';
                tanHyper: result := '\tanh ('+X+')';
                NotFunction,FonctInconnue: result := '?';
                CodeGamma: result := '\Gamma ('+X+')';
                CodeFact: result := X+'!';
                reelle: result := '\Re ('+X+')';
                imaginaire: result := '\Im ('+X+')';
                argument: result := '\arg ('+X+')';
                echelon: result := '\Upsilon ('+X+')';
                entierSup: result := '\lceil {'+X+'} \rceil';
                entier,entierInf: result := '\lfloor {'+X+'} \rfloor';
                exponentielle: result := '\exp('+X+')';
                sinusCardinal: result := '\rm {sinc}('+X+')' ;
                errorFunction: result := '\rm {erf}('+X+')' ;
             else result := LowerCase(nomFonction[codeF])+'('+X+')';
           end;// case codeF
       End;{Fonction}
       FonctionGlb : result := calculFonctionGlb(F);
       IfThenElse : result := calcIf(F);
       PieceWise: result := calcPW(F);
       grandeur : result := translateNomMath(Pvar.nom);
       grandeurIndicee,IncertIndicee : result := translateNomMath(Pvariab.nom)+'['+intToStr(round(numero))+']';
       Nombre: if F=pointeurPi then result := '\pi' else result := floatToStrPoint(valeur);
       RacineMoinsUn : result := '\jmath';
     	 Incert : result := '';
       BoucleFor: result := '';
       else result := '';
  End; // case
End; //  TranslateExpression

procedure AjouteGrandeur(index : integer);
var sigma,vm : string;
    debutLigne,nomL,nomX : String;
begin with grandeurs[index] do begin
     nomL := translateNomMath(nom);
     debutLigne := '\[ '+nomL+'=';
     nomX := translateNomMath(grandeurs[0].nom);
     case fonct.genreC of
          g_fonction,g_derivee,g_equation,g_filtrage, g_definitionFiltre :
              ajouteLigneLatex(debutLigne+translateExpression(fonct.calcul)+' \]');
          g_experimentale,g_texte : if fonct.expression<>'' then
              ajouteLigne(debutLigne+fonct.expression);
          g_diff1 : ajouteLigneLatex('\[ \frac{ \mathrm{d} '+NomL+'}{ \mathrm{d} '+nomX+'}='+
              translateExpression(fonct.calcul.operandGlb)+' \]');
          g_diff2 : ajouteLigneLatex('\[ \frac{ \mathrm{d} ^2'+nomL+'}{ \mathrm{d} '+nomX+'^2}='+
              translateExpression(fonct.calcul.operandGlb)+' \]');
          g_integrale : begin
             vm := translateNomMath(fonct.calcul^.varX.nom);
             sigma := translateExpression(fonct.calcul.OperandGlb);
             ajouteLigneLatex(debutLigne+'\int'+sigma+'\, \mathrm{d} '+vm+' \]');
          end;
          g_a_affecter, g_forward : ;
          g_deriveeSeconde : begin
            sigma := translateNomMath(fonct.calcul.varY.nom);
            vm := translateNomMath(fonct.calcul.varX.nom);
            ajouteLigneLatex(debutLigne+'\frac{ \mathrm{d} ^2'+sigma+'}{ \mathrm{d} '+vm+'^2}');
          end;
          g_euler : ;
    // g_lissage, g_FreqInst, g_phaseContinue,
    // g_retardCorr, g_enveloppe, g_harmonique, g_correlation, g_decalage
          else if fonct.expression<>'' then
              ajouteLigne(debutLigne+fonct.expression+' \]');
     end;
end end;

procedure DebutDoc(titre,auteur,date : string);
begin
     latexStr.clear;
     latexStr.add('%!TEX encoding = UTF-8 Unicode');
     latexStr.add('\documentclass[french]{article}');
     latexStr.add('\usepackage[utf8]{inputenc}');
     latexStr.add('\usepackage[T1]{fontenc}');
//     latexStr.add('\usepackage{lmodern}');
     latexStr.add('\usepackage[french]{babel}');
     latexStr.add('\usepackage{tikz}');
     latexStr.add('\usepackage{mathtools}');
     latexStr.add('\usepackage{amsmath}');
     latexStr.add('\usepackage{pgfplots}');
     latexStr.add('\usetikzlibrary{plotmarks}');
     latexStr.add('\title{'+titre+'}');
     latexStr.add('\author{'+auteur+'}');
     if date<>'' then latexStr.add('\date{'+date+'}');
     latexStr.add('\begin{document}');
     latexStr.add('\maketitle');
end;

procedure FinDoc;
begin
     latexStr.add('\end{document}');
end;

Procedure AjouteLigne(aligne : string);
begin
     if aligne<>'' then begin
        aligne := translateNomTexte(aligne);
        latexStr.add(aligne);
     end;
end;

Procedure AjouteLigneItem(aligne : string);
begin
     if aligne<>'' then begin
        aligne := translateNomTexte(aligne);
        aligne := '\item '+aligne;
        latexStr.add(aligne);
     end;
end;

Procedure AjouteGrid(agrid : TstringGrid);

Function LigneDeChiffres(row : integer) : boolean;
var j,col : integer;
    s : string;
begin
    result := true;
    for col := 0 to pred(agrid.colCount) do begin
        s := agrid.cells[col,row];
        for j := 1 to length(s) do
            if not charInSet(s[j],caracNombre)
               then begin
                   result := false;
                   exit;
               end;
   end;            
end;

var col,row : integer;
    ligne : string;
begin
    latexStr.add('');
    ligne := '\begin{tabular}{';
    for col := 0 to pred(agrid.colCount) do
        ligne := ligne+'|c';
    ligne := ligne+'|}';
    latexStr.add(ligne);
    for row := 0 to pred(agrid.rowCount) do
        if ligneDeChiffres(row)
           then begin
              ligne := agrid.cells[0,row];
              for col := 1 to pred(agrid.colCount) do
                  ligne := ligne + ' & ' + agrid.cells[col,row];
              ligne := ligne+'\newline';
              latexStr.add(ligne);
           end
           else begin
              ligne := translateNomTexte(agrid.cells[0,row]);
              for col := 1 to pred(agrid.colCount) do
                  ligne := ligne + ' & ' + translateNomTexte(agrid.cells[col,row]);
              ligne := ligne+'\cr';
              latexStr.add(ligne);
           end;
    latexStr.add('\end{tabular}');
end;

Procedure AjouteModele(amodele : Tmodele);
begin
     with amodele do
          latexStr.add('\[ '+translateNomMath(addrY.nom)+'='+translateExpression(calcul)+' \]')
end;


Procedure SaveFile(nomFichier : string);
var fichierDest : TextFile;
   i: Integer;
begin
     FileMode := fmOpenWrite;
     AssignFile(fichierDest,NomFichier);
     Rewrite(fichierDest);
     for i := 0 to latexStr.Count-1 do
         writeln(fichierDest,latexStr[i]);
     closeFile(fichierDest);
     FileMode := fmOpenReadWrite;
end;

Procedure AjouteSection(titre : String);
begin
  titre := translateNomTexte(titre);
  latexStr.add('\section{'+titre+'}');
end;

Procedure AjouteParagraphe(titre : String);
begin
  titre := translateNomTexte(titre);
  latexStr.add('\paragraph{'+titre+'}');
end;

Procedure AjouteLigneLatex(aligne : String); // tel quel sans traduction
begin
    latexStr.add(aligne);
end;

initialization
     latexStr := TstringList.create

finalization
     latexStr.free

end.
