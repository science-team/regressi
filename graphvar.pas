{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit graphvar;

  {$MODE Delphi}

interface

uses
     LCLIntf, LCLType,
     Classes, Graphics, Forms, Controls, Menus, ExtCtrls,
     Dialogs, ComCtrls, math, sysutils,
     Types,  StdCtrls, Grids, Messages, printers, actnList,
     constreg, maths, regutil, uniteKer, statCalc,
     compile, graphKer, modeleGr, aideKey,
     laz2_DOM, laz2_XMLRead, laz2_XMLUtils,
     Buttons, TDIClass;

type
  TnbreGraphe = (UnGr,DeuxGrVert,DeuxGrHoriz);

  { TFgrapheVariab }

  TFgrapheVariab = class(TForm)
    MemoResultat: TMemo;
    MemoModele: TMemo;
    PanelModele: TPanel;
    PaintBox2: TPaintBox;
    PanelAjuste: TPanel;
    MenuAxes: TPopupMenu;
    CoordonneesItem: TMenuItem;
    identifierPagesItemA: TMenuItem;
    PopupMenuModele: TPopupMenu;
    FermerItem: TMenuItem;
    SplitterModele: TSplitter;
    TableauXYItem: TMenuItem;
    TDINoteBook1: TTDINoteBook;
    ViderXYItem: TMenuItem;
    MemoModeleGB: TGroupBox;
    ResultatGB: TGroupBox;
    HeaderXY: TStatusBar;
    TitreModeleItem: TMenuItem;
    PanelCentral: TPanel;
    BornesMenu: TPopupMenu;
    Inter1: TMenuItem;
    Inter2: TMenuItem;
    Inter3: TMenuItem;
    Inter4: TMenuItem;
    RazBornes: TMenuItem;
    ResidusItem: TMenuItem;
    SaveDialog: TSaveDialog;
    MenuDessin: TPopupMenu;
    DessinSupprimerItem: TMenuItem;
    ProprietesDessin: TMenuItem;
    PanelPrinc: TPanel;
    PanelBis: TPanel;
    RaZModele: TMenuItem;
    SaveParamItem: TMenuItem;
    ParamScrollBox: TScrollBox;
    Panel1: TPanel;
    AjusteBtn: TSpeedButton;
    Panel10: TPanel;
    MoinsRapideBtn: TSpeedButton;
    MoinsBtn: TSpeedButton;
    PlusRapideBtn: TSpeedButton;
    PlusBtn: TSpeedButton;
    SigneBtn: TSpeedButton;
    EditValeur10: TEdit;
    Panel3: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    Edit1: TEdit;
    Panel4: TPanel;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    Edit2: TEdit;
    Panel5: TPanel;
    SpeedButton11: TSpeedButton;
    SpeedButton12: TSpeedButton;
    SpeedButton13: TSpeedButton;
    SpeedButton14: TSpeedButton;
    SpeedButton15: TSpeedButton;
    Edit3: TEdit;
    Panel6: TPanel;
    SpeedButton16: TSpeedButton;
    SpeedButton17: TSpeedButton;
    SpeedButton18: TSpeedButton;
    SpeedButton19: TSpeedButton;
    SpeedButton20: TSpeedButton;
    Edit4: TEdit;
    Panel7: TPanel;
    SpeedButton21: TSpeedButton;
    SpeedButton22: TSpeedButton;
    SpeedButton23: TSpeedButton;
    SpeedButton24: TSpeedButton;
    SpeedButton25: TSpeedButton;
    Edit5: TEdit;
    Panel8: TPanel;
    SpeedButton26: TSpeedButton;
    SpeedButton27: TSpeedButton;
    SpeedButton28: TSpeedButton;
    SpeedButton29: TSpeedButton;
    SpeedButton30: TSpeedButton;
    Edit6: TEdit;
    Panel9: TPanel;
    SpeedButton31: TSpeedButton;
    SpeedButton32: TSpeedButton;
    SpeedButton33: TSpeedButton;
    SpeedButton34: TSpeedButton;
    SpeedButton35: TSpeedButton;
    Edit7: TEdit;
    Panel11: TPanel;
    SpeedButton36: TSpeedButton;
    SpeedButton37: TSpeedButton;
    SpeedButton38: TSpeedButton;
    SpeedButton39: TSpeedButton;
    SpeedButton40: TSpeedButton;
    Edit8: TEdit;
    PanelParam1: TPanel;
    SpeedButton41: TSpeedButton;
    SpeedButton42: TSpeedButton;
    SpeedButton43: TSpeedButton;
    SpeedButton44: TSpeedButton;
    SpeedButton45: TSpeedButton;
    EditValeur1: TEdit;
    TraceAutoCB: TCheckBox;
    RepeatTimer: TTimer;
    AffIncertBis: TMenuItem;
    Optionsmodle1: TMenuItem;
    ImageList1: TImageList;
    ModeleToolBar: TToolBar;
    OptionsModeleBtn0: TToolButton;
    ModeleGrBtn: TToolButton;
    BornesBtn: TToolButton;
    InitEquaDiffBtn: TToolButton;
    ToolBarGraphe: TToolBar;
    CurseurBtn: TToolButton;
    CoordonneesBtn: TToolButton;
    NGraphesBtn: TToolButton;
    CopierBtn: TToolButton;
    VecteursBtn: TToolButton;
    CurseurMenu: TPopupMenu;
    SelectItem: TMenuItem;
    CurTexteItem: TMenuItem;
    CurLigneItem: TMenuItem;
    CurGommesItem: TMenuItem;
    CurReticuleItem: TMenuItem;
    curDataItem: TMenuItem;
    CurTangenteItem: TMenuItem;
    CurOrigineItem: TMenuItem;
    CurModeleItem: TMenuItem;
    NgraphesMenu: TPopupMenu;
    UngrapheItem: TMenuItem;
    DeuxGraphesVert: TMenuItem;
    DeuxGraphesHoriz: TMenuItem;
    PaintBox3: TPaintBox;
    Bevel1: TBevel;
    LabelY: TLabel;
    LabelX: TLabel;
    ModelePagesIndependantesMenu: TMenuItem;
    SavePosItem: TMenuItem;
    ProprieteCourbe: TMenuItem;
    TimerModele: TTimer;
    IntersectionItem: TMenuItem;
    ToolBarGrandeurs: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    VecteursMenu: TPopupMenu;
    VitesseItem: TMenuItem;
    AccelerItem: TMenuItem;
    VitesseAccItem: TMenuItem;
    NoVecteursItem: TMenuItem;
    IdentifierPagesItemN: TMenuItem;
    CurReticuleModeleItem: TMenuItem;
    Options1: TMenuItem;
    FildeferItem: TMenuItem;
    TrigoBtn: TToolButton;
    Enregistrergraphe1: TMenuItem;
    Imprimergraphe1: TMenuItem;
    Imprimermodlisation1: TMenuItem;
    MajBtn: TSpeedButton;
    ZoomAutoBtn: TToolButton;
    ZoomAvBtn: TToolButton;
    ZoomArBtn: TToolButton;
    ZoomManuelBtn: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    LabelDeltaX: TLabel;
    LabelDeltaY: TLabel;
    MenuReticule: TPopupMenu;
    tableauXYItemBis: TMenuItem;
    ViderXYItemBis: TMenuItem;
    SavePosItemBis: TMenuItem;
    FinReticuleItem: TMenuItem;
    intersectionTer: TMenuItem;
    TimerSizing: TTimer;
    CopyMenu: TPopupMenu;
    metafile2: TMenuItem;
    bitmap2: TMenuItem;
    Jpeg2: TMenuItem;
    Png1: TMenuItem;
    addModeleItem: TMenuItem;
    TrigoLabel: TLabel;
    ModeleItem: TMenuItem;
    ResidusItemBis: TMenuItem;
    N1: TMenuItem;
    ModeleSepare: TMenuItem;
    N3: TMenuItem;
    clipEPSitem: TMenuItem;
    PaintBox1: TPaintBox;
    IncertChi2Item: TMenuItem;
    ResidusStudentCB: TCheckBox;
    AffIncert: TMenuItem;
    IncertBtn: TToolButton;
    IdentifierPagesItemC: TMenuItem;
    ActionList1: TActionList;
    IdentAction: TAction;
    CurseurGrid: TStringGrid;
    ModeleBtn: TToolButton;
    ModeleMenu: TPopupMenu;
    LineaireItem: TMenuItem;
    AffineItem: TMenuItem;
    EchelonItem: TMenuItem;
    RadioItem: TMenuItem;
    ParaboleItem: TMenuItem;
    ModelesAutresItem: TMenuItem;
    Sauvermodle1: TMenuItem;
    AbscisseCB: TComboBox;
    LabelToolBar: TLabel;
    procedure FormHide(Sender: TObject);
    procedure MemoResultatChange(Sender: TObject);
    procedure PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure tShowmeAgainTimer(Sender: TObject);
    procedure ToolButton1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ZoomAvantItemClick(Sender: TObject);
    procedure PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CoordonneesItemClick(Sender: TObject);
    procedure ZoomAutoItemClick(Sender: TObject);
    procedure CopierItemClick(Sender: TObject);
    procedure PaintBoxPaint(Sender: TObject);
    procedure AjusteBtnClick(Sender: TObject);
    procedure MemoModeleChange(Sender: TObject);
    procedure ZoomArriereItemClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    Procedure ValideParam(paramCourant : integer;maj : boolean);
    procedure ValeursParamKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure ZoomManuelItemClick(Sender: TObject);
    procedure CopierModeleItemClick(Sender: TObject);
    procedure InitEquaDiffBtnClick(Sender: TObject);
    procedure MemoResultatKeyPress(Sender: TObject; var Key: Char);
    procedure MenuAxesPopup(Sender: TObject);
    procedure TableauXYItemClick(Sender: TObject);
    procedure ModelGrClick(Sender: TObject);
    procedure ViderXYItemClick(Sender: TObject);
    procedure TraceAutoBtnClick(Sender: TObject);
    procedure MemoModeleClick(Sender: TObject);
    procedure MajBtnClick(Sender: TObject);
    procedure ImprimeGrItemClick(Sender: TObject);
    procedure MemoModeleKeyPress(Sender: TObject; var Key: Char);
    procedure BornesClick(Sender: TObject);
    procedure BornesMenuPopup(Sender: TObject);
    procedure RazBornesClick(Sender: TObject);
    procedure ResidusItemClick(Sender: TObject);
    procedure ImprModeleBtnClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DessinDeleteItemClick(Sender: TObject);
    procedure DessinOptionsItemClick(Sender: TObject);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure identifierPages(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure RaZModeleClick(Sender: TObject);
    procedure SaveParamItemClick(Sender: TObject);
    procedure EditValeurExit(Sender: TObject);
    procedure EditValeurKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EditValeurKeyPress(Sender: TObject; var Key: Char);
    procedure MoinsRapideBtnClick(Sender: TObject);
    procedure MoinsBtnClick(Sender: TObject);
    procedure PlusBtnClick(Sender: TObject);
    procedure PlusRapideBtnClick(Sender: TObject);
    procedure SigneBtnClick(Sender: TObject);
    procedure ModifParam(paramCourant : integer;coeff : double);
    procedure ParamBtnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ParamBtnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RepeatTimerTimer(Sender: TObject);
    procedure OrigineBtnClick(Sender: TObject);
    procedure VecteursItemClick(Sender: TObject);
    procedure OptionsModeleBtnClick(Sender: TObject);
    procedure AffIncertBisClick(Sender: TObject);
    procedure ChoixCurseurClick(Sender: TObject);
    procedure CurseurBtnClick(Sender: TObject);
    procedure LabelYMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure DeuxGraphesVertClick(Sender: TObject);
    procedure PaintBoxClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure ModelisationItemClick(Sender: TObject);
    procedure FermerItemClick(Sender: TObject);
    procedure SavePosItemClick(Sender: TObject);
    procedure ProprieteCourbeClick(Sender: TObject);
    procedure PopupMenuModelePopup(Sender: TObject);
    procedure TimerModeleTimer(Sender: TObject);
    procedure PaintBox2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure CurseurMenuPopup(Sender: TObject);
    procedure Options1Click(Sender: TObject);
    procedure VecteursMenuPopup(Sender: TObject);
    procedure FildeferItemClick(Sender: TObject);
    procedure TrigoBtnClick(Sender: TObject);
    procedure Enregistrergraphe1Click(Sender: TObject);
    procedure FinReticuleItemClick(Sender: TObject);
    procedure MenuReticulePopup(Sender: TObject);
    procedure IntersectionItemClick(Sender: TObject);
    procedure TimerSizingTimer(Sender: TObject);
    procedure metafile2Click(Sender: TObject);
    procedure addModeleItemClick(Sender: TObject);
    procedure ModeleItemClick(Sender: TObject);
    procedure PanelModeleResize(Sender: TObject);
    procedure IncertChi2ItemClick(Sender: TObject);
    procedure ResidusStudentCBClick(Sender: TObject);
    procedure AffIncertClick(Sender: TObject);
    procedure IncertBtnClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ModeleBtnClick(Sender: TObject);
    procedure LineaireItemClick(Sender: TObject);
    procedure AffineItemClick(Sender: TObject);
    procedure EchelonItemClick(Sender: TObject);
    procedure ParaboleItemClick(Sender: TObject);
    procedure RadioItemClick(Sender: TObject);
    procedure Sauvermodle1Click(Sender: TObject);
    procedure AbscisseCBClick(Sender: TObject);
    procedure ToolButton1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
      Sizing : boolean;
      grandeurBoutonDroit : boolean; // clic sur TButton avec bouton droit
      oldCursorPos : Tpoint;
      mouseMoving : boolean;
      indexPointCourant : integer;
      iPropCourbe : integer;
      courbePointCourant : integer;
      ValeursParamChange : boolean;
      InitiationAfaire : boolean;
      GrapheCourant : TgrapheReg;
      indexGrCourant : integer;
      IndexPointModeleGr : integer;
      IndexModeleGr : integer;
      Curseur : Tcurseur;
      zoneZoom : Trect;
      EntreeValidee : boolean;
      nbreGraphe : TnbreGraphe;
      derf : matriceFonction; // Dérivées premières / aux paramètres
      erreurCalcul,erreurParam : boolean;
      BorneSelect : Tborne;
      verifGraphe : boolean;
      xOrigine : double;
      dXresidu,dYresidu : vecteur;
      BorneActive : codeIntervalle;
      ValeurDeriveeX,ValeurYDiff,ValeurYpDiff : TvecteurExtrapole;
      ModeleCalc : array[codePage] of boolean;
      ValeurXDiff : vecteur;
      IsModeleSysteme : boolean;
      IsModeleMagnum : boolean;
      DeltaXcadre,DeltaYcadre : integer; // écart Pointeur souris centre du cadre
      MajModelePermis : boolean;
      editValeur : array[codeParam] of Tedit;
      panelParam : array[codeParam] of Tpanel;
      TimerButton : TSpeedButton;
      CurMovePermis : boolean;
      Ydrag : integer;
      orthoAverifier : boolean;
      xCurseur,yCurseur : integer;
      Effaces : boolean;
      XorigineInt : integer;
      Procedure VerifCoordonnee(indexGr : integer);
      procedure SetPenBrush(c : Tcurseur);
      procedure MajParametres;
      procedure MajResultat;
      Procedure LanceModele(ajuste : boolean);
      procedure CompileModele(var PosErreur,LongErreur : integer);
      procedure ConstruitModele;
      procedure SetUniteParametre;
      Procedure CalcIntersection;
      Procedure EffectueModele(Ajuste,WithMessage : boolean);
      Procedure EffectueModeleMono(m : integer);
      Procedure LanceCompile;
      Procedure Compile;
      Procedure MajModeleGr(NumeroGr : integer);
      Procedure InitCurseurModele;
      procedure TraceCurseurCourant(indexGr : integer);
      procedure setPointCourant(i,c : integer);
      procedure SetBoutonsModele;
      Procedure ResetHeaderXY;
      procedure ModeleMagnum(indiceCoord : integer;direct : boolean);
      procedure MajIdentPage(indexGr : integer);
      procedure MajIdentCoord(indexGr : integer);
      procedure verifParametres;
      procedure AjustePanelModele;
      procedure SetCoordonneeResidu;
      procedure AffecteModele(indexGr : integer);
      procedure ZoomSuperpose(indexGr : integer);
      procedure ZoomModele(indexGr : integer);
      procedure SetTaillePolice;
      procedure SetPanelModele(avecModele : boolean);
      procedure SetPanelParam;
      procedure SetParamEdit;
      procedure setModeleGR(index : TgenreModeleGr);
      procedure identPages;
      Procedure ModifGraphe(indexGr : integer);
  protected
      procedure WMRegMaj(var Msg : TWMRegMessage); message WM_Reg_Maj;
      procedure WMRegCalcul(var Msg : TWMRegMessage); message WM_Reg_Calcul;
      procedure WMRegModele(var Msg : TWMRegMessage); message WM_Reg_Modele;
      procedure WMRegOrigine(var Msg : TWMRegMessage); message WM_Reg_Origine;
      procedure WMRegInitMagnum(var Msg : TWMRegMessage); message WM_Reg_InitMagnum;
  public
      Graphes : array[1..3] of TgrapheReg;
      configCharge,majFichierEnCours : boolean;
      etatModele : setEtatModele;
      varYPos,varXPos : Tgrandeur;
      mesureDelta : boolean;
      withModele : boolean;

      Procedure SetCoordonnee(indexGr : integer);
      procedure LitConfig;
      procedure EcritConfigXML(ANode : TDomNOde);
      procedure LitConfigXML(ANode : TDomNOde);
      procedure CalculCurseurModele;
      procedure ImprimerGraphe(var bas : integer);
      procedure VersLatex(NomFichier : string);
  end;

var FgrapheVariab : TFgrapheVariab;

implementation

uses
     zoomMan, ClipBrd, fspec,
     choixModele, curModel, regmain, cursData, options,
     systDiff, identPages, Regdde,
     SaveParam,
     CoordPhys, ChoixTang, Valeurs,
     OrigineU, optModele, savePosition, PropCourbe,
     optionsvitesse, Latex, OptionsAffModeleU, savemodele;

  {$R *.lfm}

{$I modele.pas}
{$I modeleMono.pas}

const
   CoeffParam = 1.023;
   CoeffParamRapide = 2;
   InitRepeatPause = 400; // pause before repeat timer (ms)
   RepeatPause     = 100; // pause before hint window displays (ms)
   IncertNobitmap = 54;
   IncertYesbitmap = 52;

var
   courbeModele : Tcourbe;
   indexCourbeModele : integer;

 procedure TFgrapheVariab.VersLatex(NomFichier : string);
 var i : integer;
 begin
     for i := 1 to 3 do
         if graphes[i].paintBox.visible and graphes[i].grapheOK then
            graphes[i].versLatex(NomFichier,char(ord('0')+i));
end;

 Procedure TfgrapheVariab.InitCurseurModele;
begin
   pages[pageCourante].affecteConstParam;
   ligneRappelCourante := lrXdeY;
   graphes[1].RemplitTableauEquivalence;
   if curseurModeleDlg=nil then
      curseurModeleDlg := TcurseurModeleDlg.create(self);
   curseurModeleDlg.show;
end;

Procedure TfgrapheVariab.CalculCurseurModele;
var i : integer;
    xr,yr : double;
    ecartY,deltaY,deltaX : double;

Procedure AffecteCurseurModele;
var NewEquivalence : Tequivalence;
    i : integer;
begin with graphes[1],CurseurModeleDlg do
  if (LigneXdeY-2)>=equivalences[pageCourante].count
    then begin
       NewEquivalence := Tequivalence.Create(0,0,0,0,xr,yr,0,graphes[1]);
       NewEquivalence.ligneRappel := lrXdeY;
       equivalences[pageCourante].Add(NewEquivalence);
       with Tableau do
            if ligneXdeY=pred(rowCount) then begin
                rowCount := rowCount+1;
                for i := 0 to 2 do cells[i,pred(rowCount)] := '';
                row := rowCount-1;
            end;
    end
    else begin
       NewEquivalence := equivalences[pageCourante].items[LigneXdeY-2];
       NewEquivalence.Draw; // efface
       NewEquivalence.ve := xr;
       NewEquivalence.ligneRappel := lrXdeY;
       NewEquivalence.pHe := yr;
    end;
  NewEquivalence.Draw;
end;

Function CalculExp(exp : string) : double;
var posErreur,longErreur : integer;
begin
   if exp=''
     then result := Nan
     else begin
        PrevenirFonctionDeParam := false;
        grandeurImmediate.fonct.expression := exp;
        if grandeurImmediate.fonct.compileF(posErreur,longErreur,false,paramNormal,0)
           then begin
              try
              result := calcule(grandeurImmediate.fonct.calcul);
              except
              result := Nan;
              end;
           end
           else result := Nan
     end;
end;

var i1 : integer;
begin
if curseurModeleDlg=nil then
      curseurModeleDlg := TcurseurModeleDlg.create(self);
with graphes[1],pages[pageCourante],
           curseurModeleDlg,courbeModele do begin
    xr := calculExp(tableau.cells[0,ligneXdeY]);
    yr := calculExp(tableau.cells[1,ligneXdeY]);
    if equation
       then begin // interpolation
            i := pointProcheReal(xr,yr,courbeModele,mondeY);
            if i<debutC then exit;
            ecartY := yr-valY[i];
            if i<finC then i1 := succ(i) else i1 := pred(i);
            deltaY := valY[i1]-valY[i];
            if (i>debutC) and (deltaY*ecartY<0) then begin
               i1 := pred(i);
               deltaY := valY[i1]-valY[i];
            end;
            deltaX := valX[i1]-valX[i];
            try
                xr := valX[i]+deltaX*ecartY/deltaY;
            except
                xr := valX[i];
            end;
            tableau.cells[0,ligneXdeY] := FormatReg(xr);
       end
       else begin
            grandeurs[fonctionTheorique[1].indexX].valeurCourante := xr;
            yr := calcule(fonctionTheorique[1].calcul);
            tableau.cells[1,ligneXdeY] := FormatReg(yr);
       end;
    affecteCurseurModele;
end end; // CalculCurseurModele 

procedure TFgrapheVariab.PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
var GrapheCourantMove : TgrapheReg;

procedure AffichePosition;
var Xr,Yr : double;
    YNom,YVal,XNom,XVal : string;
    icourbe : integer;
begin with GrapheCourantMove do begin
    if not(monde[mondeX].defini) then exit;
    iCourbe := courbeProche(x,y);
    if iCourbe<0 then
       if courbes.count>0
          then iCourbe := 0
          else exit;
    with courbes[iCourbe] do begin
       MondeRT(x,y,iMondeC,xr,yr);
       ValeurCurseur[coX] := xr;
       ValeurCurseur[coY] := yr;
       varYPos := varY;
       varXPos := varX;
       if varY<>nil then YNom := varY.nom else YNom := monde[imondeC].axe.nom;
       if varX<>nil then XNom := varX.nom else XNom := monde[mondeX].axe.nom;
       if monde[iMondeC].graduation=gdB
         then begin
            yr := 20*log10(yr);
            if (varY<>nil) and (curseur=curReticule) then begin
               curseurOsc[1].grandeurCurseur := varY;
               curseurOsc[1].mondeC := iMondeC;
            end;
            YVal := FloatToStrF(Yr,ffGeneral,3,0)+' dB';
         end
         else if varY<>nil
            then YVal := varY.formatValeurEtUnite(Yr)
            else YVal := monde[iMondeC].axe.formatValeurEtUnite(Yr);
       if varX<>nil
          then XVal := varX.formatValeurEtUnite(Xr)
          else XVal := monde[mondeX].axe.formatValeurEtUnite(Xr);
    end;
    if curseur=curReticule then begin
        labelX.transparent := false;
        labelY.transparent := false;
        labelX.caption := XNom+'='+XVal+' ';
        labelY.caption := YNom+'='+YVal+' ';
        labelX.top := limiteFenetre.top+5+hautCarac;
        labelY.top := y-8;
        labelY.left := limiteFenetre.left+5;
        labelX.left := x-(labelX.width div 2);
     end
     else begin
        ResetHeaderXY;
        AffecteStatusPanel(HeaderXY,1,XNom+'='+XVal);
        AffecteStatusPanel(HeaderXY,2,YNom+'='+YVal);
     end;
     if curseurGrid.visible then begin
           CurseurGrid.cells[0,0] := XNom;
           CurseurGrid.cells[0,1] := YNom;
           CurseurGrid.cells[1,0] := XVal;
           CurseurGrid.cells[1,1] := YVal;
           CurseurGrid.RowCount := 4;
           CurseurGrid.Height := 4*CurseurGrid.DefaultRowHeight+4;
      end;
end end; // AffichePosition

procedure AfficheResidu;
var i : integer;
    ecart,ref95,residu : double;
    XX,YY : string;
begin with pages[pageCourante],GrapheCourantMove do begin
    if not(monde[mondeX].defini) then exit;
    i := pointProche(x,y,indexCourbeModele,true,false);
    if (i<debut[1]) or (i>fin[1]) then exit;
    XX := '';
try
    if courbes[indexCourbeModele].IncertY<>nil
       then begin
          ecart := courbes[indexCourbeModele].incertY[i];
          ecart := (valeurVar[fonctionTheorique[1].indexY,i]-valeurTheorique[1,i])/ecart;
          YY := stEcart+FloatToStrF(ecart,ffGeneral,3,1)+' * '+stIncertitude;
       end
       else if modeleCalcule and fonctionTheorique[1].residuStat.statOK
          then begin
             ref95 := fonctionTheorique[1].residuStat.t95;
             residu := fonctionTheorique[1].residuStat.residusStudent[i-debut[1]];
             XX := stResiduStudent+FloatToStrF(residu,ffGeneral,3,1)+
                   stLimiteStudent95+FloatToStrF(ref95,ffGeneral,3,1)+')';
             ecart := valeurVar[fonctionTheorique[1].indexY,i]-valeurTheorique[1,i];
             YY := stEcart+FloatToStrF(ecart,ffGeneral,3,1);
          end
          else begin
             ecart := valeurVar[fonctionTheorique[1].indexY,i]-valeurTheorique[1,i];
             YY := stEcart+FloatToStrF(ecart,ffGeneral,3,1);
          end;
except
      YY := '';
end;
    AffecteStatusPanel(HeaderXY,0,YY);
    AffecteStatusPanel(HeaderXY,1,XX);
end end; // AfficheResidu

procedure AfficheResiduGr;
const inegaliteResidu : array[boolean] of string = ('<','>');
var i : integer;
    ecart,ref95,residu : double;
    XX,YY : string;
begin with pages[pageCourante],GrapheCourantMove do begin
    if not(monde[mondeX].defini) then exit;
    i := pointProche(x,y,0,true,false);
    if (i<0) then exit;
    XX := '';
try
    if modeleCalcule and fonctionTheorique[1].residuStat.statOK
          then begin
             ref95 := fonctionTheorique[1].residuStat.t95;
             residu := fonctionTheorique[1].residuStat.residusStudent[i];
             XX := stResiduStudent+FloatToStrF(residu,ffGeneral,3,1)+
                   inegaliteResidu[residu<ref95]+
                   FloatToStrF(ref95,ffGeneral,3,1)+stLimiteStudent95;
             ecart := fonctionTheorique[1].residuStat.residus[i];
             YY := stEcart+FloatToStrF(ecart,ffGeneral,3,1);
          end
          else YY := ''
except
      YY := '';
end;
    AffecteStatusPanel(HeaderXY,0,YY);
    AffecteStatusPanel(HeaderXY,1,XX);
end end; // AfficheResiduGr

procedure AfficheDelta;
var Xval,Yval,Xnom,Ynom : string;
    dX : double;
begin with GrapheCourantMove,curseurOsc[1] do begin
    if not(monde[mondeX].defini) or not(monde[mondeC].defini) then exit;
    case monde[mondeC].graduation of
         gdB : begin
            dX := 20*log10(curseurOsc[2].yr/yr);
            YNom := deltaMaj+monde[curseurOsc[1].mondeC].Axe.nom;
            YVal := floatToStrF(dX,ffGeneral,3,0)+' dB';
         end;
         gLog : begin
            dX := curseurOsc[2].yr/yr;
            YNom := '÷'+monde[mondeC].Axe.nom;
            YVal := floatToStrF(dX,ffGeneral,3,0);
         end;
         else begin
             dX := curseurOsc[2].yr-yr;
             YNom := deltaMaj+monde[mondeC].Axe.nom;
             YVal := monde[mondeC].Axe.formatValeurEtUnite(dX);
         end;
    end;
    valeurCurseur[coDeltay] := dX;
    if monde[mondeX].graduation=gLog
       then begin
            dX := curseurOsc[2].xr-xr;
            XNom := '÷'+monde[mondeX].Axe.nom;
            XVal := floatToStrF(dX,ffGeneral,3,0);
       end
       else begin
           dX := curseurOsc[2].xr-xr;
           XNom := deltaMaj+monde[mondeX].Axe.nom;
           XVal := monde[mondeX].Axe.formatValeurEtUnite(dX);
       end;
    ValeurCurseur[coDeltax] := dX;
    if curseur=curReticule then begin
        LabelDeltaX.visible := true;
        LabelDeltaY.visible := true;
        labelDeltaX.transparent := false;
        labelDeltaY.transparent := false;
        labelDeltaX.caption := XNom+'='+XVal+' ';
        labelDeltaY.caption := YNom+'='+YVal+' ';
        labelDeltaX.top := limiteFenetre.top+5;
        labelDeltaY.top := (yc+curseurOsc[2].yc) div 2;
        if abs(labelDeltaY.top-labelY.Top)<hautCarac then
            if labelDeltaY.top>labelY.top
               then labelDeltaY.top := labelY.top+hautCarac
               else labelDeltaY.top := labelY.top-hautCarac;
        labelDeltaY.left := limiteFenetre.left+4;
        labelDeltaX.left := (xc+curseurOsc[2].xc-labelX.width) div 2;
    end;
    if curseurGrid.visible then begin
       CurseurGrid.cells[0,2] := XNom;
       CurseurGrid.cells[0,3] := YNom;
       CurseurGrid.cells[1,2] := XVal;
       CurseurGrid.cells[1,3] := YVal;
    end;
    mesureDelta := true;
end end; // AfficheDelta

procedure EffaceDelta;
begin
    if curseurGrid.visible then begin
       CurseurGrid.cells[0,2] := '';
       CurseurGrid.cells[0,3] := '';
       CurseurGrid.cells[1,2] := '';
       CurseurGrid.cells[1,3] := '';
    end;
    LabelDeltaX.visible := false;
    LabelDeltaY.visible := false;
end;

Procedure DeplaceDessin;
begin with grapheCourantMove,dessinCourant do
                 if posDessinCourant=sdCadre
                    then begin
                       RectangleGr(zoneZoom); { efface }
                       AffecteCentreRect(x+deltaXcadre,y+deltaYcadre,zoneZoom);
                       RectangleGr(zoneZoom);
                    end
                    else begin
                       LineGr(zoneZoom); { efface }
                       affectePosition(x+deltaXcadre,y+deltaYcadre,posDessinCourant,shift);
                       case posDessinCourant of
                            sdVert : zoneZoom := rect(x1i,LigneMin,x1i,ligneMax);
                            sdHoriz : zoneZoom := rect(ligneMin,y1i,ligneMax,y1i);
                            else zoneZoom := rect(x1i,y1i,x2i,y2i);
                       end; {case}
                       LineGr(zoneZoom);
                    end
end; // DeplaceDessin

var deplacement : boolean;
    borneLoc : Tborne;
    selectEqLoc : TselectEquivalence;
    indexLoc : integer;
    iProche : integer;
    indexGr : integer;
    hintLoc : string;
    deltaM : double;
begin // PaintBoxMouseMove
   indexGr := (sender as tcontrol).tag;
   if indexGr<1 then indexGr := 1;
   grapheCourantMove := Graphes[indexGr];
   if not grapheCourantMove.grapheOK then exit;
   if ssRight in Shift then exit; // clic droit
   with grapheCourantMove do begin
   hintLoc := '';
   deplacement := ssLeft in Shift;
   mesureDelta := false;
   case curseur of
        curModele : ;
        curXMaxi,curXMini : if abs(x-xCurseur)>8 then begin
             changeEchelleX(x,xCurseur,curseur=curXMaxi);
             xCurseur := x;
             (sender as TPaintBox).invalidate;
        end;
        curYMaxi,curYMini : if abs(y-yCurseur)>8 then begin
             changeEchelleY(y,yCurseur,curseur=curYMaxi);
             yCurseur := y;
             (sender as TPaintBox).invalidate;
        end;
        curZoomAv,curBornes,curEfface : if deplacement
           then with canvas,zoneZoom do begin
               Rectangle(left,top,right,bottom);// efface l'ancien
               right := X;
               bottom := Y;
               Rectangle(left,top,right,bottom);
               hintLoc := stFinZoom;
          end
          else hintLoc := stDebutZoom;
        curSelect : if deplacement
              then if dessinCourant<>nil
                 then deplaceDessin
                 else if borneSelect.style<>bsAucune
                    then SetBorne(x,y,BorneSelect)
                    else begin

                    end
                else begin // pas déplacement
                  SetDessinCourant(x,y);
                  if (posDessinCourant=sdNone) then selectEqLoc := seNone;
                  if (posDessinCourant<>sdNone) or
                      GetBorne(x,y,BorneLoc) or
                     (selectEqLoc<>seNone)
                  then begin
                     TgraphicControl(sender).cursor := crHandPoint;
                     hintLoc := hGlisser+' ; ';
                     if posDessinCourant=sdCadre
                        then hintLoc := hintLoc+hDbleTexte
                        else if posDessinCourant<>sdNone
                           then hintLoc := hintLoc+hDbleCouleur
                           else if selectEqLoc=seNone
                             then with borneLoc do begin
                                case style of
                                    bsDebut : hintLoc := hintLoc+stDebutDe;
                                    bsFin : hintLoc := hintLoc+stFinDe;
                                    bsDebutVert : hintLoc := hintLoc+stDebutDe;
                                    bsFinVert : hintLoc := hintLoc+stFinDe;
                                end; // case style
                                if indexModele>0 then
                                   hintLoc := hintLoc+FonctionTheorique[indexModele].expression;
                              end; // with borne
                  end
                  else begin // pas borne
                     if UseSelect then hintLoc := stClicVecteur;
                     TgraphicControl(sender).cursor := crDefault;
                     if indexPointCourant>=0
                        then if Pages[pageCourante].PointActif[indexPointCourant]
                            then hintLoc := hSupprPointActif
                            else  hintLoc := hSupprPointNonActif
                        else if curMovePermis then hintLoc := hMove;
                  end;
             end;
        curLigne,curTexte : if deplacement then with canvas,dessinCourant do begin
                MoveTo(x1i,y1i);
                LineTo(x2i,y2i); // efface l'ancienne 
                AffectePosition(x,y,sdPoint2,shift);
                MoveTo(x1i,y1i);
                LineTo(x2i,y2i);
        end;
        curReticule : begin
           TraceReticule(cCourant); // efface
           if cCourant>1 then traceEcart;
           curseurOsc[cCourant].xc := x;
           curseurOsc[cCourant].yc := y;
           TraceReticule(cCourant); // retrace
           if cCourant>1 then traceEcart;
           AffichePosition;
           case cCourant of
                1 : hintLoc := stBarreReticule1;
                2 : hintLoc := stBarreReticule2;
                3 : hintLoc := stBarreReticule3;
           end;
           if cCourant>1
              then afficheDelta
              else effaceDelta;
        end;
        curMove : if deplacement then
        with grapheCourantMove do begin
            with monde[mondeX] do begin
                 deltaM := (x-zoneZoom.left)/a;
                 mini := mini+deltaM;
                 maxi := maxi+deltaM;
                 defini := true;
            end;
            with monde[mondeY] do begin
                 deltaM := (y-zoneZoom.top)/a;
                 mini := mini+deltaM;
                 maxi := maxi+deltaM;
                 defini := true;
            end;
            with monde[mondeDroit] do begin
                 deltaM := (y-zoneZoom.top)/a;
                 mini := mini+deltaM;
                 maxi := maxi+deltaM;
                 defini := true;
            end;
            if (abs(x-zoneZoom.left)+abs(y-zoneZoom.Top)>8) then begin
               paintBox.invalidate;
               zoneZoom := rect(X,Y,X,Y);
            end;
        end
        else setPenBrush(curSelect);
        curOrigine : begin
           TraceCurseurCourant(indexGr); // efface précédent
           XorigineInt := X;
           TraceCurseurCourant(indexGr);
           hintLoc := hOrigine;
        end;
        curReticuleData : if deplacement and (cCourant>0)
           then begin
               setSegmentInt(x,y);
               setPointCourant(curseurOsc[cCourant].Ic,0);
               if curseurGrid.visible then setStatusReticuleData(curseurGrid);
           end
           else begin
                GetCurseurProche(x,y,false);
                if cCourant=0
                   then TgraphicControl(sender).cursor := crDefault
                   else TgraphicControl(sender).cursor := crHandPoint;
                hintLoc := hSegment;
           end;
        curReticuleModele : begin
              if GetReticuleModeleProche(x,y)
                 then TgraphicControl(sender).cursor := crHandPoint
                 else TgraphicControl(sender).cursor := crCross;
              hintLoc := hReticuleModele;
        end;
        curEquivalence : if (equivalenceCourante<>nil) then begin
           equivalenceCourante.drawFugitif; // efface
           equivalenceCourante.mondepH := mondeDerivee;
           iProche := PointProche(x,y,indexCourbeEquivalence,true,ligneRappelCourante=lrTangente);
           if iProche>=0 then with equivalenceCourante do begin
              SetValeurEquivalence(iProche);
              ResetHeaderXY;
              if curseurGrid.visible then begin
                 CurseurGrid.cells[0,0] := 'n°';
                 CurseurGrid.cells[0,1] := monde[mondeX].Axe.Nom;
                 CurseurGrid.cells[0,2] := monde[mondeY].Axe.Nom;
                 CurseurGrid.cells[0,3] := unitePente.Nom;
                 CurseurGrid.cells[1,0] := IntToStr(iProche);
                 CurseurGrid.cells[1,1] := monde[mondeX].Axe.formatValeurEtUnite(ve);
                 CurseurGrid.cells[1,2] := monde[mondeY].Axe.FormatValeurEtUnite(phe);
                 CurseurGrid.cells[1,3] := unitePente.formatValeurPente(pente);
                 CurseurGrid.RowCount := 4;
                 CurseurGrid.Height := 4*CurseurGrid.DefaultRowHeight;
              end;
           end;
           equivalenceCourante.drawFugitif; // trace
        end;
        curModeleGr :
           if deplacement
              then if dessinCourant<>nil
                 then deplaceDessin
                 else if (indexPointModeleGr>0)
                   then with modeleGraphique[IndexModeleGr] do begin
                      DessineUnPoint(indexPointModeleGr); // efface
                      AffecteUnPoint(indexPointModeleGr,x,y);
                      DessineUnPoint(indexPointModeleGr);
                      mouseMoving := true;
              //  La mise à jour du modele se fait par timerModele
                      exit;
                   end
                   else if borneSelect.style<>bsAucune
                       then SetBorne(x,y,BorneSelect)
                       else
              else with modeleGraphique[1] do begin // pas de déplacement
                 indexLoc := GetIndex(x,y);
                 if indexLoc=0
                    then if GetBorne(x,y,BorneLoc)
                        then begin
                           TgraphicControl(sender).cursor := crHandPoint;
                           hintLoc := hGlisser;
                        end
                        else TgraphicControl(sender).cursor := crDefault
                    else begin
                        TgraphicControl(sender).cursor := crHandPoint;
                        hintLoc := GetHint(indexLoc);
                    end;
             end;
   end;
   if hintLoc=''
      then TgraphicControl(sender).hint := hClicDroit
      else TgraphicControl(sender).hint := hintLoc;
   AffecteStatusPanel(headerXY,3,hintLoc);
   if curseur in [curBornes,curSelect]
      then for iProche := 0 to 2 do videStatusPanel(HeaderXY,iProche)
      else if not (curseur in [curEquivalence,curReticuleData,curReticule,curReticuleModele]) then
         affichePosition;
   if (curseur in [curModeleGr,curSelect]) and
      (withModele) and
      (indexCourbeModele>=0) and
      (indexGr=1) then afficheResidu;
   if (curseur=curSelect) and (indexGr=3) then afficheResiduGr;
end end; // PaintBoxMouseMove

procedure TFgrapheVariab.tShowmeAgainTimer(Sender: TObject);
begin
end;

procedure TFgrapheVariab.ToolButton1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  GrandeurBoutonDroit := (button=mbRight);
end;

procedure TFgrapheVariab.MemoResultatChange(Sender: TObject);
begin
end;

procedure TFgrapheVariab.FormHide(Sender: TObject);
begin
end;

procedure TFgrapheVariab.ZoomAvantItemClick(Sender: TObject);
begin
    if grapheCourant.grapheOK then setPenBrush(curZoomAv)
end; // zoomAvant

procedure TFgrapheVariab.SetPenBrush(c : Tcurseur);

Procedure SetPenLoc(indexGr : integer);
begin
  if graphes[indexGr].paintBox.Visible then
with graphes[indexGr],canvas,paintBox do begin
  Pen.Color := PColorReticule;
  Pen.style := PstyleReticule; 
  Pen.mode := pmNotXor;
  Brush.style := bsClear;
  Brush.color := ColorToRGB(clWindow);
  cursor := crDefault;
  case c of
       curZoomAv : begin
          Brush.color := BrushCouleurZoom;
          cursor := crDefault;//Zoom;
          Brush.style := bsSolid;
       end;
       curBornes : begin
          Brush.color := BrushCouleurZoom;
          cursor := crDefault;//Rectangle;
          Brush.style := bsSolid;
       end;
       curTexte : cursor := crDefault;//Lettre;
       curLigne : cursor := crDefault;//Pencil;
       curReticule,curOrigine : cursor := crCross;//crNone;
       curEfface : cursor := crDefault;//Gomme;
       curEquivalence : begin
          if ((indexCourbeEquivalence<0) or
              (indexCourbeEquivalence>=courbes.count))
              then setCourbeDerivee;
          cursor := crDefault;
          Pen.style := PstyleTangente;
       end;
       curModeleGr : begin
            cursor := crCross;
            Brush.color := BrushCouleurSelect;
            Brush.style := bsSolid;
       end;
       curModele : if (pages[pageCourante].modeleCalcule) and
       (ModeleConstruit in etatModele) and
       (monde[mondeX].axe=grandeurs[fonctionTheorique[1].indexX]) and
       (monde[mondeY].axe=grandeurs[fonctionTheorique[1].indexY])
          then cursor := crCross
          else begin
             afficheErreur(ErCurseurModele,0);
             curseur := curSelect
          end;
       curMove : cursor := crSizeAll;
       curXmaxi,curXmini : cursor := crSizeWE;
       curYmaxi,curYMini : cursor := crSizeNS;
       else cursor := crCross;
    end;{case}
    panelPrinc.Cursor := cursor;
end end;

var i : integer;
begin // SetPenBrush
    if c=curOrigine then begin
       if graphes[1].monde[mondeX].axe.fonct.genreC<>g_experimentale then begin
          AfficheErreur(erOrigineExp,0);
          c := curSelect;
       end;
       if graphes[1].superposePage then begin
          AfficheErreur(erOriginePage,0);
          c := curSelect;
       end;
     end;
     if (c=curModeleGr) and (NbreModele>2) then c := curSelect;
     if (c=curOrigine) and (curseur<>curOrigine) then
        showMessage(hCurseurOrigine);
     if (curseur=curReticuleData) and (c<>curReticuleData) then
         graphes[1].paintBox.invalidate;
     curseur := c;
     CurseurGrid.visible := (c in [curReticuleData, curEquivalence]) and avecTableau;
     for i := 1 to 3 do SetPenLoc(i);
     CurseurBtn.ImageIndex := iconCurseur[curseur];
     HeaderXY.visible := c in
          [curSelect,curTexte,curLigne,curOrigine,curModele];
     LabelX.visible := c=curReticule;
     LabelY.visible := c=curReticule;
     LabelX.Color := fondReticule;
     LabelY.Color := fondReticule;
     LabelDeltaX.visible := (c=curReticule) and
                            (graphes[1].cCourant>1);
     LabelDeltaY.visible := (c=curReticule) and
                            (graphes[1].cCourant>1);
     LabelDeltaX.Color := fondReticule;
     LabelDeltaY.Color := fondReticule;
     if curseur=curSelect then SelectItem.checked := true;
end; // SetPenBrush 

procedure TFgrapheVariab.PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);

Procedure GetDessinLoc;
begin with grapheCourant do begin
          SetDessinCourant(x,y);
          canvas.Brush.color := colorToRGB(clWindow);
          if dessinCourant<>nil
              then with dessinCourant do begin
                  case posDessinCourant of
                       sdCadre : zoneZoom := cadre;
                       sdVert : zoneZoom := rect(x1i,ligneMin,x1i,ligneMax);
                       sdHoriz : zoneZoom := rect(ligneMin,y1i,ligneMax,y1i);
                       else zoneZoom := rect(x1i,y1i,x2i,y2i);
                  end; // case posDessinCourant
         // Pour éviter déplacement du dessin par le clic
                  case posDessinCourant of
                       sdCadre : begin
                          RectangleGr(zoneZoom);
                          with cadre do begin
                               DeltaXcadre := (right+left) div 2-X;
                               DeltaYcadre := (bottom+top) div 2-Y;
                          end;
                       end;// cadre
                       sdPoint1 : begin
                             DeltaXcadre := X1i-X;
                             DeltaYcadre := Y1i-Y;
                       end;
                       sdPoint2 : begin
                             DeltaXcadre := X2i-X;
                             DeltaYcadre := Y2i-Y;
                       end;
                       sdCentre : begin
                             DeltaXcadre := (X2i+X1i) div 2 -X;
                             DeltaYcadre := (Y2i+Y1i) div 2 -Y;
                       end;
                       else begin
                             DeltaXcadre := 0;
                             DeltaYcadre := 0;
                       end;
                  end; // case posdessin
                  if posDessinCourant<>sdCadre then LineGr(zoneZoom);
               end // with dessinCourant
end end;

var j,iProche : integer;
    icourbe : integer;
    grandeurRef : Tgrandeur;
    indexCoord,newIndex : integer;
    isMaxi : boolean;
begin // MouseDown
   indexGrCourant := (sender as Tcontrol).tag;
   grapheCourant := Graphes[indexGrCourant];
   if not grapheCourant.grapheOK then exit;
   if (Button=mbRight) then begin
      grapheCourant.SetDessinCourant(x,y);
      exit; // clic droit donc menu contextuel
   end;
   Ydrag := 0;
   if DeuxGraphesVert.checked or
      DeuxGraphesHoriz.checked
        then for j := 1 to 3 do
             if Graphes[j].paintBox.Visible then
                graphes[j].EncadreTitre(indexGrCourant=j);
   if (curseur=curSelect) and
      (ssDouble in shift) then
       if grapheCourant.DessinCourant<>nil
          then begin
             grapheCourant.DessinCourant.litOption(grapheCourant);
             grapheCourant.DessinCourant := nil;
             grapheCourant.paintBox.invalidate;
             exit;
          end
          else if grapheCourant.isAxe(x,y)>0
              then CoordonneesItemClick(nil)
              else if grapheCourant.isAxeX(x,y,isMaxi) or 
                      grapheCourant.isAxeY(x,y,isMaxi)
                 then zoomManuelItemClick(sender);
   if (curseur=curSelect) and (grapheCourant.DessinCourant=nil) then begin
           indexCoord := grapheCourant.isAxe(x,y);
           if indexCoord>0 then
              if grapheCourant.Coordonnee[indexCoord].iMondeC>=mondeSans
               then begin
                    grandeurRef := grandeurs[grapheCourant.Coordonnee[indexCoord].codeY];
                    for j := 1 to maxOrdonnee do with grapheCourant.Coordonnee[j] do
                        if (codeY=grandeurInconnue) or
                           (j=indexCoord) or
                           grandeurs[codeY].uniteEgale(grandeurRef)
                              then iMondeC := mondeY
                              else iMondeC := mondeSans;
                    include(grapheCourant.modif,gmXY);
                    ModifGraphe(indexGrCourant);
              end
          end;
   if (curseur=curEquivalence) and
      (ligneRappelCourante=lrTangente) then with grapheCourant do begin
           indexCoord := isAxe(x,y);
           if indexCoord>0 then begin
               newIndex := Coordonnee[indexCoord].iCourbe;
               if indexCourbeEquivalence<>newIndex then begin
                  indexCourbeEquivalence := newIndex;
//                  equivalenceCourante := nil;
                  resetCourbeDerivee;
                  exit;
               end;
           end;
  end;
  with grapheCourant do begin
  case curseur of
       curModele,curXmaxi,curXMini,curYmaxi,curYMini : ;
       curZoomAv,curBornes : zoneZoom := rect(X,Y,X,Y);
       curLigne,curTexte : with dessinCourant do  begin
           if (ogAnalyseurLogique in optionGraphe) then
               iMonde := mondeProche(x,y);
           affectePosition(x,y,sdPoint1,shift);
           x2i := x1i;
           y2i := y1i;
       end;
       curReticule : if not(ssDouble in shift) then begin
       // active deuxième curseur pour différence
                for j := 1 to cCourant do traceReticule(j);
                if cCourant>1 then traceEcart;
                curseurOsc[cCourant+1].xc := curseurOsc[cCourant].xc;
                curseurOsc[cCourant+1].xr := curseurOsc[cCourant].xr;
                curseurOsc[cCourant+1].yc := curseurOsc[cCourant].yc;
                curseurOsc[cCourant+1].yr := curseurOsc[cCourant].yr;
                curseurOsc[2].mondeC := curseurOsc[1].mondeC;
                inc(cCourant);
                if cCourant>3 then cCourant := 1;
                if cCourant>1 then traceEcart;
                for j := 1 to cCourant do traceReticule(j);
       end;
       curSelect : begin
          GetDessinLoc;
//          iProche := -1;
          if dessinCourant=nil then if not getBorne(x,y,BorneSelect)
                    then
                    else begin
                        if isModeleMagnum then begin
                           iCourbe := CourbeProche(x,y);
                           if (iCourbe>=0) and
                            (courbes[iCourbe].indexModele>0) and
                             not courbes[iCourbe].courbeExp then begin
                            // réactiver CurModeleGr
                             modeleGraphique[1].setParametres(pages[pageCourante].valeurParam[paramNormal]);
                             setPenBrush(CurModeleGr);
                             paintBox.invalidate;
                             exit;
                         end;
                       end;
                       for j := 0 to pred(courbes.count) do begin
                           if ((courbes[j].indexModele=0) or courbes[j].courbeExp) and
                              (courbes[j].page=pageCourante) then begin
                              iProche := PointProche(x,y,j,true,false);
                              if iProche>=0 then begin
                                 if Pages[pageCourante].PointActif[iProche]
                                    then TgraphicControl(sender).hint := hSupprPointActif
                                    else TgraphicControl(sender).hint := hSupprPointNonActif;
                                 setPointCourant(iProche,j);
                                 break;
                              end;
                           end;
                    end;
                    end;
           if curMovePermis and
              (dessinCourant=nil) and
         //     (iProche<0) and
              (BorneSelect.style=bsAucune) then begin
                  setPenBrush(curMove);
                  zoneZoom := rect(X,Y,X,Y);
              end;
            if (curSelect=curseur) and isAxeX(x,y,isMaxi) then begin
                 xCurseur := x;
                 if ismaxi
                    then SetPenBrush(curXmaxi)
                    else SetPenBrush(curXmini);
            end;
            if (curSelect=curseur) and isAxeY(x,y,isMaxi) then begin
                 yCurseur := y;
                 if ismaxi
                    then SetPenBrush(curYmaxi)
                    else SetPenBrush(curYmini);
            end;
       end; // curSelect
       curEfface : begin
           SetDessinCourant(x,y);
           if dessinCourant<>nil then with dessins do begin
                  Remove(DessinCourant);
                  curseur := curSelect;
                  paintBox.invalidate;
           end;
           zoneZoom := rect(X,Y,X,Y);
       end;
       curEquivalence : begin
           iproche := PointProche(x,y,indexCourbeEquivalence,true,ligneRappelCourante=lrTangente);
           if iProche>=0 then begin
              AjouteEquivalence(iProche,true);
              TgraphicControl(sender).invalidate;
           end;
           setPointCourant(iProche,indexCourbeEquivalence);
           ResetHeaderXY;
           if curseurGrid.visible then begin
              for j := 0 to pred(statusSegment[1].count) do
                 CurseurGrid.cells[1,j] := statusSegment[1].strings[j];
              for j := 0 to pred(statusSegment[0].count) do
                 CurseurGrid.cells[0,j] := statusSegment[0].strings[j];
              CurseurGrid.RowCount := statusSegment[0].count;
              CurseurGrid.Height := CurseurGrid.RowCount*CurseurGrid.DefaultRowHeight+4;
           end;
       end;
       curReticuleData : begin
          GetCurseurProche(x,y,true);
          setSegmentInt(x,y);
          if curseurGrid.visible then setStatusReticuleData(curseurGrid);
          SetPointCourant(curseurOsc[cCourant].Ic,0);
       end;
       curReticuleModele : setReticuleModele(x,y,true,true);
       curModeleGr : begin
            indexModeleGR := 0;
            for j := 1 to NbreModele do begin
                indexPointModeleGr := modeleGraphique[j].GetIndex(x,y);
                if indexPointModeleGr>0 then begin
                   indexModeleGR := j;
                   break;
                end;
            end;
            timerModele.enabled := true;
            if indexPointModeleGr=0
               then getBorne(x,y,BorneSelect)
               else BorneSelect.style := bsAucune;
            if (indexPointModeleGr=0) and (BorneSelect.style=bsAucune)
                then getDessinLoc
                else dessinCourant := nil;
            if dessinCourant=nil then
               canvas.Brush.color := BrushCouleurSelect
       end;
  end { case }
end end; // PaintBoxMouseDown

procedure TFgrapheVariab.PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var Agraphe : TgrapheReg;

Procedure affecteZoomAv;
begin
  with zoneZoom do begin
        Agraphe.canvas.Rectangle(left,top,right,bottom);// efface
        right := X;
        bottom := Y;
        setPenBrush(curSelect);
        if (left=right) or (bottom=top) then exit;
        Agraphe.affecteZoomAvant(zoneZoom,true);
   end;
   curMovePermis := true;
   include(Agraphe.modif,gmEchelle);
   Agraphe.paintBox.invalidate;
end; // affecteZoomAvant

Procedure affecteBornes;
var x1,x2,y1,y2 : double;
    i : integer;
    indexX,indexY : integer;
    mSelect,m : indiceMonde;
begin with zoneZoom,Agraphe do begin
        VideStatusPanel(HeaderXY,0);
        canvas.Rectangle(left,top,right,bottom);// efface
        right := X;
        bottom := Y;
        setPenBrush(curSelect);
        if (left=right) or (bottom=top) then exit;
        with pages[pageCourante] do begin
             debut[BorneActive] := pred(nmes);
             fin[BorneActive] := 0;
             indexX := indexNom(monde[mondeX].axe.nom);
             indexY := grandeurInconnue;
             mSelect := mondeY;
             if BorneActive<=NbreModele then
                for m := mondeY to mondeSans do
                  if (monde[m].axe<>nil) and
                     (fonctionTheorique[BorneActive].indexY=
                      indexNom(monde[m].axe.nom))
                  then begin
                     indexY := indexNom(monde[m].axe.nom);
                     mSelect := m;
                     break;
                  end;
             if indexY=grandeurInconnue then
                for m := mondeY to mondeSans do
                    if monde[m].axe<>nil then begin
                       indexY := indexNom(monde[m].axe.nom);
                       mSelect := m;
                       break;
                  end;
             MondeRT(right,top,mSelect,x1,y1);
             MondeRT(left,bottom,mSelect,x2,y2);
             for i := 0 to pred(nmes) do
                 if ((valeurVar[indexX,i]-x1)*(valeurVar[indexX,i]-x2)<0) and
                    ((valeurVar[indexY,i]-y1)*(valeurVar[indexY,i]-y2)<0) then begin
                    if debut[BorneActive]>i then debut[BorneActive] := i;
                    if fin[BorneActive]<i then fin[BorneActive] := i;
                 end;
             borneVisible := true;
             if (nmes<MaxVecteurDefaut) and (NbreModele<=1) and (borneActive=1)
               then begin
                  for i := 0 to debut[borneActive]-1 do
                      PointActif[i] := false;
                  for i := debut[borneActive] to fin[borneActive] do begin
                      PointActif[i] :=
                     ((valeurVar[indexX,i]-x1)*(valeurVar[indexX,i]-x2)<0) and
                     ((valeurVar[indexY,i]-y1)*(valeurVar[indexY,i]-y2)<0);
                     if not PointActif[i] then borneVisible := false;
                  end;
                  for i := fin[borneActive]+1 to pred(nmes) do
                     PointActif[i] := false;
               end
               else begin
                    for i := 0 to pred(MaxVecteurDefaut) do PointActif[i] := true;
               end;
             if BorneActive=succ(NbreModele)
                then begin
                   FonctionTheorique[succ(NbreModele)].indexX := indexX;
                   FonctionTheorique[succ(NbreModele)].indexY := indexY;
                   ChoixModeleDlg.coordonnee := coordonnee;
                   ChoixModeleDlg.appelMagnum := false;
                   ChoixModeleDlg.PageControl.ActivePage := ChoixModeleDlg.ModManuel;
                   if ChoixModeleDlg.showModal=mrOk then
                      if ChoixModeleDlg.modeleChoisi=mgManuel
                         then begin
                           if ChoixModeleDlg.effaceModele then MemoModele.clear;
                           with FonctionTheorique[NbreModele] do
                           if enTete<>'' then begin
                              MemoModele.lines.Add(enTete+'='+expression);
                              lanceModele(genreC=g_fonction);
                              TgraphicControl(sender).invalidate;
                              exit;
                           end
                        end
                        else ModeleMagnum(ChoixModeleDlg.CoordRG.itemIndex+1,false);
               end
               else if (curseur=curModeleGr) and (indexModeleGr>0) then begin
                   ModeleGraphique[indexModeleGr].initialiseParametre(
                       Pages[pageCourante].debut[indexModeleGr],
                       Pages[pageCourante].fin[indexModeleGr],false);
               end;
        end;
        if ModeleDefini in etatModele then
           if [ModeleConstruit,ModeleLineaire] <= etatModele
               then lanceModele(true)
               else begin
                  pages[pageCourante].ModeleCalcule := false;
                  pages[pageCourante].ModeleErrone := false;
                  include(Agraphe.modif,gmModele);
                  if (withModele) then setParamEdit;
               end;
       paintbox.invalidate;
end end; // affecteBornes

Procedure affecteEfface;
var xfin,xdebut,yfin,ydebut : double;
    indexPoint : integer;
    modifNbre : boolean;
    p : codePage;
begin with zoneZoom,Agraphe do begin
    canvas.Rectangle(left,top,right,bottom);// efface
    right := X;
    bottom := Y;
    if (left=right) or (bottom=top)
        then begin
            indexPoint := PointProche(x,y,0,true,false);
            modifNbre := indexPoint>=0;
            if modifNbre then Pages[pageCourante].SupprimeLignes(indexPoint,indexPoint);
        end
        else begin
            MondeRT(right,top,mondeY,xfin,yfin);
            MondeRT(left,bottom,mondeY,xdebut,ydebut);
            with coordonnee[1] do
                 ModifNbre := pages[pageCourante].EffacePage(xdebut,ydebut,xfin,yfin,codeX,codeY);
            if superposePage and (NbrePages>1) and OKreg(okSupprAutresPages,0) then
                 for p := 1 to NbrePages do if p<>pageCourante then with pages[p] do
                     if active then with coordonnee[1] do
                        EffacePage(xdebut,ydebut,xfin,yfin,codeX,codeY);
        end;
   if modifNbre
      then Application.MainForm.Perform(WM_Reg_Maj,MajSupprPoints,0)
      else if SupprReticule(x,y) then begin
          paintBox.invalidate;
      end;
   if not effaces then setPenBrush(curSelect);
end end; // affecteEfface

Procedure affecteOrigine;
var y0 : double;
begin
   Agraphe.canvas.MoveTo(XorigineInt,0);
   Agraphe.canvas.LineTo(XorigineInt,height);// efface
   Agraphe.MondeRT(x,0,mondeY,xOrigine,y0);
   setPenBrush(curSelect);
   PostMessage(handle,Wm_Reg_Origine,0,0);
end; // affecteOrigine

procedure AffecteDessin;
label fin;
begin
if Agraphe.dessinCourant<>nil then
with Agraphe.dessinCourant do begin
   with Agraphe.canvas do begin
        MoveTo(x1i,y1i);
        LineTo(x2i,y2i); // efface
   end;
   if isTexte
      then begin
          if not litOption(Agraphe) then begin
              Agraphe.dessinCourant.free;
              goto fin;
          end;
      end
      else begin
           if (curseur=curLigne) and
              ((abs(x1i-x2i)+abs(y1i-y2i))<3) then begin
              afficheErreur(erLigneNulle,0);
              Agraphe.dessinCourant.free;
              goto fin;
           end;
      end;
   AffectePosition(x,y,sdPoint2,shift);
   Agraphe.dessins.Add(Agraphe.DessinCourant);
   ModifFichier := true;
   draw;
end;
   fin :
   Agraphe.dessinCourant := nil;
   setPenBrush(curSelect);
end; // AffecteDessin

procedure AffecteBorne;
begin with borneSelect do
   if (Agraphe.courbes[indexCourbe].debutC<
       Agraphe.courbes[indexCourbe].finC)
   then begin
      case style of
         bsDebut,bsDebutVert : pages[pageCourante].debut[indexModele] :=
             Agraphe.courbes[indexCourbe].debutC;
         bsFin,bsFinVert : pages[pageCourante].fin[indexModele] :=
             Agraphe.courbes[indexCourbe].finC;
      end;
      if [ModeleConstruit,ModeleLineaire] <= etatModele
           then lanceModele(true)
   end;
end;// AffecteBorne

procedure AffecteVecteur;
var j,iProche : integer;
    ib : integer;
begin
    for j := 0 to pred(Agraphe.courbes.count) do with Agraphe.courbes[j] do
        if (trVitesse in trace) or (trAcceleration in trace) then begin
             iProche := Agraphe.PointProche(x,y,j,true,false);
             if (iProche>=0) and (iProche<256) then begin
                ib := byte(iProche);
                if ib in PointSelect
                   then exclude(PointSelect,ib)
                   else include(PointSelect,ib);
                Agraphe.paintBox.invalidate;
                break;
             end;
    end;
end; // AffecteVecteur

procedure finDeplaceDessin;
begin with Agraphe do begin
     if posDessinCourant=sdCadre
           then begin
                RectangleGr(zoneZoom);
                dessinCourant.AffectePosition(x+deltaXcadre,y+deltaYcadre,posDessinCourant,shift);
           end
           else dessinCourant.AffectePosition(x+deltaXcadre,y+deltaYcadre,posDessinCourant,shift);
     paintBox.invalidate;
     dessinCourant := nil;
end end;

var P : Tpoint;
    j : integer;
begin // MouseUp
  Agraphe := Graphes[(sender as Tcontrol).tag];
  mouseMoving := false;
  if (x<0) or (y<0) or not(Agraphe.grapheOK) then exit;
  if Button=mbRight then begin
     if grapheCourant.DessinCourant=nil then
        grapheCourant.setDessinCourant(x,y);
     P := (sender as TgraphicControl).ClientToScreen(point(X,Y));
     if grapheCourant.dessinCourant<>nil
         then MenuDessin.popUp(P.x,P.y)
         else
             if (y<Agraphe.limiteCourbe.top) or
                     (y>Agraphe.limiteCourbe.bottom)
                    then CoordonneesItemClick(nil)
                    else begin
      iPropCourbe := Agraphe.CourbeProche(x,y);
      if (iPropCourbe>=0) and
         ((Agraphe.courbes[iPropCourbe].indexModele=0) or
           Agraphe.courbes[iPropCourbe].courbeExp) and
         (Agraphe.courbes[iPropCourbe].page=pageCourante) then begin
            grapheCourant := Agraphe;
            ProprieteCourbeClick(nil);
         end// caractéristiques de la courbe
         else
         if curseur in [curReticule,curReticuleData,curEquivalence,curReticuleModele]
             then MenuReticule.popUp(P.x,P.y)
             else MenuAxes.popUp(P.x,P.y);
         end;
     exit;
  end; // bouton droit
  case curseur of
       curZoomAv : affecteZoomAv;
       curBornes : affecteBornes;
       curEfface : affecteEfface;
       curXmaxi,curXMini : begin
           aGraphe.changeEchelleX(x,xCurseur,curseur=curXmaxi);
           setPenBrush(curSelect);
           (sender as TPaintBox).invalidate;
       end;
       curYmaxi,curYmini : begin
           aGraphe.changeEchelleY(y,yCurseur,curseur=curYmaxi);
           setPenBrush(curSelect);
           (sender as TPaintBox).invalidate;
       end;
       curTexte,curLigne : affecteDessin;
       curSelect : if grapheCourant.dessinCourant<>nil
            then finDeplaceDessin
            else if borneSelect.style=bsAucune
               then if UseSelect
                  then affecteVecteur
                  else
               else AffecteBorne;
       curReticuleData,curReticuleModele : Agraphe.paintbox.invalidate;
       curReticule : ;
       curEquivalence : if Ydrag>0 then begin
             (sender as Tcontrol).endDrag(false);
             if (ligneRappelCourante=lrTangente) then with Agraphe do begin
               j := Coordonnee[Ydrag].iCourbe;
               if indexCourbeEquivalence<>j then begin
                  indexCourbeEquivalence := j;
                  resetCourbeDerivee;
               end;
             end;
             Ydrag := 0;
       end;
       curModele : ;
       curMove : setPenBrush(curSelect);
       curOrigine : affecteOrigine;
       curModeleGr : if indexPointModeleGr>0 then
          with modeleGraphique[indexModeleGr] do begin
               DessineUnPoint(indexPointModeleGr);
               AffecteUnPoint(indexPointModeleGr,x,y);
               DessineUnPoint(indexPointModeleGr);
               MajModeleGr(indexModeleGr);
               TimerModele.enabled := false;
               indexPointModeleGr := 0;
               indexModeleGr := 0;
          end
          else if grapheCourant.dessinCourant<>nil
            then finDeplaceDessin
            else AffecteBorne;
  end;
//  grapheCourant.dessinCourant := nil;
end; // MouseUp

procedure TFgrapheVariab.FormCreate(Sender: TObject);
var m : codeIntervalle;
    i,j,k : integer;
begin
  MajFichierEnCours := true;
  PanelAjuste.enabled := true;
  CurseurGrid.ColWidths[0] := 60;
  CurseurGrid.ColWidths[1] := 120;
  CurseurGrid.Width := 184;
  InitiationAfaire := true;
  for i := 1 to 3 do Graphes[i] := TgrapheReg.create;
  with Graphes[2] do begin
     paintBox := paintBox2;
     panel := panelBis;
     canvas := paintBox2.canvas;
     labelYcurseur := labelY;
  end;
  with Graphes[1] do begin
     paintBox := paintBox1;
     panel := panelPrinc;
     canvas := paintBox1.canvas;
     labelYcurseur := labelY;
  end;
  with Graphes[3] do begin
     paintBox := paintBox3;
     panel := panelPrinc;
     canvas := paintBox3.canvas;
     labelYcurseur := labelY;
  end;
  indexGrCourant := 1;
  GrapheCourant := graphes[1];
  curseur := curSelect;
  selectItem.Checked := true;
  entreeValidee := false;
  for m := 1 to maxIntervalles do
      valeurDeriveeX[m] := nil;
  VerifGraphe := true;
  with ParamScrollBox do
        for i := 0 to pred(controlCount) do begin
           if controls[i] is Tpanel then begin
              j := TPanel(controls[i]).tag;
              PanelParam[j] := TPanel(controls[i]);
              with PanelParam[j] do
                   for k := 0 to pred(controlCount) do begin
                       if controls[k] is Tedit then
                          EditValeur[j] := TEdit(controls[k]);
                   end;
           end;
  end;
  setTaillePolice;
  splitterModele.visible := false;
  panelModele.visible := false;
end; // formCreate

procedure TFgrapheVariab.SetCoordonnee(indexGr : integer);

var CourbeExpAffectee,SimulationAffectee,withModeleLoc : boolean;

Procedure AjouteVariab(m : indiceMonde;i : indiceOrdonnee;page : codePage);
var xV,yV : vecteur;
    c : shortInt;
    avecModele : boolean;
    courbeVariab,courbeAdd : Tcourbe;
    iX,iY : integer;
    traceV : TsetTrace;
    ligneV : Tligne;
    couleurModeleCourant : Tcolor;

Procedure AddIncert(Acourbe : Tcourbe);
var avecIncertX,avecIncertY : boolean;
begin
      with pages[page] do begin
         avecIncertX := isCorrect(incertVar[iX,acourbe.debutC]);
         if avecIncertX
            then Acourbe.IncertX := incertVar[iX]
            else Acourbe.IncertX := nil;
         avecIncertY := not isNan(incertVar[iY,acourbe.debutC]);
         if avecIncertY
            then Acourbe.IncertY := incertVar[iY]
            else Acourbe.IncertY := nil;
         if avecIncertY and not avecIncertX then Acourbe.IncertX := incertNulle;
         if avecIncertX and not avecIncertY then Acourbe.IncertY := incertNulle;
      end;
end;

const NbandeDefault = 256;

var j,cc,b : integer;
    CouleurModeleActive : boolean;
begin with graphes[indexGr],pages[page] do begin
      if m>=MaxMonde then exit;
      with Coordonnee[i] do begin
           iX := CodeX;
           iY := CodeY;
           exclude(trace,trSonagramme);
           TraceV := trace;
           LigneV := ligne;
           if (LigneV=LiModele) and (NbreModele=0) then LigneV := LiDroite;
           couleurModeleCourant := couleur;
           if motif=mIncert then
              if not grandeurs[iY].incertDefinie or
                 not avecEllipse
                    then motif := TMotif(i);
      end;
      if grandeurs[iY].fonct.genreC=g_texte then begin
            for c := 0 to pred(courbes.count) do
                if courbes[c].page=page then begin
                   include(courbes[c].trace,trTexte);
                   couleurTexte := couleurModeleCourant;
                   courbes[c].texteC := TstringList.create;
                   for j := 0 to pred(pages[page].nmes) do
                       courbes[c].texteC.add(texteVar[iY,j]);
                   exit;
                end;
     end;
     if (grandeurs[iY].genreG in [constante,constanteGlb]) or
        (grandeurs[iX].genreG in [constante,constanteGlb]) then begin
              setLength(yV,nmes);
              case grandeurs[iY].genreG of
                   constante : for j := 0 to pred(nmes) do yV[j] := valeurConst[iY];
                   variable : copyVecteur(yV,valeurVar[iY]);
                   constanteGlb : for j := 0 to pred(nmes) do yV[j] := grandeurs[iY].valeurCourante;
              end;
              setLength(xV,nmes);
              case grandeurs[iX].genreG of
                   constante : for j := 0 to pred(nmes) do xV[j] := valeurConst[iX];
                   variable : copyVecteur(xV,valeurVar[iX]);
                   constanteGlb : for j := 0 to pred(nmes) do xV[j] := grandeurs[iX].valeurCourante;
              end;
              courbeVariab := AjouteCourbe(xV,yV,m,nmes,
                   grandeurs[iX],grandeurs[iY],page);
              if (grandeurs[iY].genreG in [constante,constanteGlb]) and
                 (grandeurs[iX].genreG in [constante,constanteGlb])
                   then courbeVariab.Trace := [trPoint]
                   else courbeVariab.Trace := [trLigne];
              courbeVariab.Adetruire := true;
              with Coordonnee[i] do
                   courbeVariab.setStyle(couleur,styleT,motif);
              exit;
      end;
      courbeVariab := AjouteCourbe(valeurVar[iX],valeurVar[iY],m,nmes,
              grandeurs[iX],grandeurs[iY],page);
      with coordonnee[i] do
           courbeVariab.setStyle(couleur,styleT,motif);
      AddIncert(courbeVariab);
      avecModele := false;
      if traceV=[] then if modeAcquisition=AcqSimulation
      then begin
          TraceV := [trLigne];
          LigneV := LiDroite
      end
      else case TraceDefaut of
          tdPoint : TraceV := [trPoint];
          tdLigne : begin
                 TraceV := [trLigne];
                 LigneV := LiDroite
          end;
          tdLissage : begin
                 TraceV := [trLigne,trPoint];
                 LigneV := LiSpline;
          end;
      end;
      courbeVariab.Trace := TraceV;
      CouleurModeleActive := false;
// 2 modeles différents => on force des couleurs différentes
      for c := 2 to NbreModele do
          for cc := 1 to pred(c) do
              if (fonctionTheorique[c].indexY)=
                 (fonctionTheorique[cc].indexY) then begin
                     CouleurModeleActive := true;
                     break;
               end;
      if TrLigne in TraceV then case LigneV of
           LiModele : begin
                exclude(courbeVariab.Trace,trLigne);
                exclude(traceV,trPoint);
                exclude(traceV,trLigne);
                if ModeleDefini in etatModele then begin
                   for c := 1 to NbreModele do with fonctionTheorique[c] do
                      if (iY=indexY) and (iX=indexX) and not(implicite) then begin
                            if (debut[c]>0) or
                               (fin[c]<pred(nmes)) or
                               CouleurModeleActive then begin
                                courbeAdd := AjouteCourbe(valeurVar[iX],valeurVar[iY],
                                   m,nmes,grandeurs[iX],grandeurs[iY],page);
                                courbeAdd.debutC := debut[c];
                                courbeAdd.finC := fin[c];
                                courbeAdd.trace := [trPoint];
                                courbeAdd.courbeExp := true;
                                courbeAdd.IndexModele := c;
                                AddIncert(courbeAdd);
                                CourbeExpAffectee := true;
                                couleurModeleCourant := couleurModele[c];
                                with coordonnee[i] do
                                     courbeAdd.setStyle(couleurModeleCourant,styleT,motif);
                            end
                            else begin // UN modele sur toute les donnees
                               courbeVariab.courbeExp := true;
                               courbeVariab.IndexModele := c;
                               courbeExpAffectee := true; { ?? }
//                               with coordonnee[i] do courbeVariab.setStyle(couleur,styleT,motif);
                               if NbreModele>1 then couleurModeleCourant := couleurModele[c];
                            end; // modele sur toute les donnees
                            if modeleCalcule then begin
                               setlength(xV,NmesMax+1);
                               setLength(yV,NmesMax+1);
                               if c=1 then begin
                                  indexCourbeModele := courbes.count-1;
                                  indexReticuleModele := courbes.count;
                               end;
                               courbeAdd := AjouteCourbe(xV,yV,m,nmes,
                                  grandeurs[iX],grandeurs[iY],page);
                               courbeAdd.IndexModele := c;
                               AvecModele := true;
                               if coordonnee[i].motif=mHisto
                                  then begin
                                      courbeAdd.Trace := [trPoint];
                                      courbeAdd.setStyle(couleurModele[c],psSolid,mHisto);
                                      couleurModeleCourant := courbeAdd.couleur;
                                  end
                                  else begin
                                      courbeAdd.Trace := [trLigne];
                                      with coordonnee[i] do
                                           courbeAdd.setStyle(couleurModeleCourant,styleT,motif);
                                  end;
                               courbeAdd.Adetruire := true;
                               if c=1 then courbeModele := courbeAdd;
                               // lineaireVar
                               if lineaire and withBandeConfiance then for b := 1 to 2 do begin // min et max
                                  xV := nil;yV := nil; // nouveaux vecteurs
                                  setlength(xV,NbandeDefault);
                                  setlength(yV,NbandeDefault);
                                  courbeAdd := AjouteCourbe(xV,yV,m,NbandeDefault,
                                     grandeurs[iX],grandeurs[iY],page);
                                  courbeAdd.IndexModele := c;
                                  courbeAdd.IndexBande := b;
                                  courbeAdd.courbeExp := false;
                                  courbeAdd.Trace := [trLigne];
                                  courbeAdd.Adetruire := true;
                                  courbeAdd.setStyle(couleurBande,psDot,mCroix);
                               end;
                            end;
           end; // for c de ModeleDefini
           if not(avecModele) and
                  (ModeleDefini in etatModele) and
                   modeleCalcule and
                   (NbreModele=2) and
                   (fonctionTheorique[1].genreC=g_fonction) and
                   (fonctionTheorique[1].indexX=0) and
                   (fonctionTheorique[2].indexX=0) and
                   (((fonctionTheorique[1].indexY=iY) and
                     (fonctionTheorique[2].indexY=iX)) or
                    ((fonctionTheorique[1].indexY=iX) and
                     (fonctionTheorique[2].indexY=iY))) then begin
                               setLength(xV,NmesMax+1);
                               setLength(yV,NmesMax+1);
                               courbeAdd := AjouteCourbe(xV,yV,m,nmes,
                                  grandeurs[iX],grandeurs[iY],page);
                               AvecModele := true;
                               courbeAdd.ModeleParametrique := true;
                               CourbeAdd.ModeleParamX1 := fonctionTheorique[1].indexY=iX;
                               courbeAdd.Trace := [trLigne];
                               courbeAdd.Adetruire := true;
                               with coordonnee[i] do
                                    courbeAdd.setStyle(couleurModeleCourant,styleT,motif);
                end; // Modele parametrique
           if not(avecModele) and
                  (ModeleDefini in etatModele) and
                   modeleCalcule and
                   (NbreModele=1) and
                   (fonctionTheorique[1].genreC=g_fonction) and
                   (fonctionTheorique[1].indexX=0) and
                   (((fonctionTheorique[1].indexY=iY) and
                     (fonctionTheorique[1].indexYp=iX)) or
                    ((fonctionTheorique[1].indexY=iX) and
                     (fonctionTheorique[1].indexYp=iY))) then begin
                               setlength(xV,NmesMax+1);
                               setlength(yV,NmesMax+1);
                               courbeAdd := AjouteCourbe(xV,yV,m,nmes,
                                  grandeurs[iX],grandeurs[iY],page);
                               AvecModele := true;
                               courbeAdd.PortraitDePhase := true;
                               CourbeAdd.ModeleParamX1 := fonctionTheorique[1].indexY=iX;
                               courbeAdd.Trace := [trLigne];
                               with coordonnee[i] do
                                    courbeAdd.setStyle(couleurModeleCourant,styleT,motif);
                               courbeAdd.Adetruire := true;
                end; // Portrait de phase avec fonction
          if not(avecModele) and
                (ModeleDefini in etatModele) and
                   modeleCalcule and
                   (NbreModele=1) and
                   (fonctionTheorique[1].genreC=g_diff2) and
                   (fonctionTheorique[1].indexX=0) and
                   (((fonctionTheorique[1].indexYp=iY) and
                     (fonctionTheorique[1].indexY=iX)) or
                    ((fonctionTheorique[1].indexYp=iX) and
                     (fonctionTheorique[1].indexY=iY))) then begin
                               setlength(xV,nmesMax+1);
                               setlength(yV,nmesMax+1);
                               courbeAdd := AjouteCourbe(xV,yV,m,nmes,
                                  grandeurs[iX],grandeurs[iY],page);
                               AvecModele := true;
                               courbeAdd.PortraitDePhase := true;
                               CourbeAdd.ModeleParamX1 := fonctionTheorique[1].indexY=iX;
                               courbeAdd.Trace := [trLigne];
                               with coordonnee[i] do
                                   courbeAdd.setStyle(couleurModeleCourant,styleT,motif);
                               courbeAdd.Adetruire := true;
                         end; // Portrait de phase équa diff 
                end; // modeleDefini
                if not(avecModele) and
                   (NbreModele>0) and
                   fonctionTheorique[1].implicite then begin
                   setlength(xV,NmesMax+1);
                   setLength(yV,NmesMax+1);
                   courbeAdd := AjouteCourbe(xV,yV,m,nmes,
                      grandeurs[iX],grandeurs[iY],page);
                   courbeAdd.IndexModele := 1;
                   AvecModele := true;
                   courbeAdd.Trace := [trLigne];
                   courbeAdd.Adetruire := true;
                   with coordonnee[i] do
                        courbeAdd.setStyle(couleurModele[1],styleT,motif);
                   courbeModele := courbeAdd;
//                   indexCourbeModele := courbes.count-1;
                end;
                if not(avecModele) and
                   ((valeurLisse[iX]<>valeurVar[iX]) or
                    (valeurLisse[iY]<>valeurVar[iY]))
                       then begin
                            courbeAdd := AjouteCourbe(valeurLisse[iX],valeurLisse[iY],
                                m,nmes,grandeurs[iX],grandeurs[iY],page);
                            with coordonnee[i] do
                                courbeAdd.setStyle(couleur,styleT,motif);
                            courbeAdd.Trace := [trLigne];
                            courbeAdd.IndexModele := indexLisse;
                        end;
           if not(avecModele) and not(pasDeModele in etatModele) then
                   if (valeurLisse[iX]<>valeurVar[iX]) or
                      (valeurLisse[iY]<>valeurVar[iY])
                       then begin
                           courbeAdd := AjouteCourbe(valeurLisse[iX],valeurLisse[iY],
                                m,nmes,grandeurs[iX],grandeurs[iY],page);
                            with coordonnee[i] do
                                courbeAdd.setStyle(couleur,styleT,motif);
                            courbeAdd.Trace := [trLigne];
                            courbeAdd.IndexModele := indexLisse;
                        end;
        end;
        LiSpline :
        //if (pages[page].nmes<maxVecteurDefaut) then
         begin
              exclude(courbeVariab.trace,trLigne);
              courbeVariab.courbeExp := true;
              exclude(traceV,trPoint);
              courbeAdd := AjouteCourbe(valeurVar[iX],valeurVar[iY],m,nmes,grandeurs[iX],grandeurs[iY],page);
              with coordonnee[i] do
                   courbeAdd.setStyle(couleur,styleT,motif);
              courbeAdd.Adetruire := true;
              courbeAdd.Trace := [trLigne];
              courbeAdd.IndexModele := indexSpline;
              exclude(traceV,trLigne);
        end;
        LiSinc :
        //if (pages[page].nmes<maxVecteurDefaut) then
         begin
              exclude(courbeVariab.trace,trLigne);
              courbeVariab.courbeExp := true;
              exclude(traceV,trPoint);
              courbeAdd := AjouteCourbe(valeurVar[iX],valeurVar[iY],m,nmes,grandeurs[iX],grandeurs[iY],page);
              with coordonnee[i] do
                   courbeAdd.setStyle(couleur,styleT,motif);
              courbeAdd.Adetruire := true;
              courbeAdd.Trace := [trLigne];
              courbeAdd.IndexModele := indexSinc;
              exclude(traceV,trLigne);
        end;
    end; // case ligneV
    if (SimulationDefinie in etatModele) and (withModele)
        then
       for c := 1 to NbreFonctionSuper do with fonctionSuperposee[c] do
           if (iY=indexY) and (iX=indexX) then begin
                 setLength(xV,nmesMax+1);
                 setLength(yV,nmesMax+1);
                 courbeAdd := AjouteCourbe(xV,yV,m,nmes,
                    grandeurs[iX],grandeurs[iY],page);
                 courbeAdd.IndexModele := -c;
                 with coordonnee[i] do
                      courbeAdd.setStyle(couleurModele[-c],styleT,motif);
                 courbeAdd.Trace := [trLigne];
                 courbeAdd.Adetruire := true;
                 SimulationAffectee := true;
           end; // for c de SimulationDefini
    if TraceV<>[] then begin
       courbeVariab.trace := courbeVariab.trace+traceV;
       if avecModele and ([trVitesse,trAcceleration]*traceV<>[]) then begin
          courbeAdd := AjouteCourbe(valeurLisse[iX],valeurLisse[iY],
                         m,nmes,grandeurs[iX],grandeurs[iY],page);
          with coordonnee[i] do
               courbeAdd.setStyle(couleur,styleT,motif);
          courbeAdd.Trace := [];
          if trVitesse in traceV then begin
             include(courbeAdd.Trace,trVitesse);
             exclude(courbeVariab.Trace,trVitesse);
          end;
          if trAcceleration in traceV then begin
             include(courbeAdd.Trace,trAcceleration);
             exclude(courbeVariab.Trace,trAcceleration);             
          end;
       end;
    end;
    withModeleLoc := withModeleLoc or avecModele;
end end; // AjouteVariab

Function indexVariable(const nomX : string) : integer;
// Renvoie le code de la variable de nom : nomX
var i : integer;
begin
   result := -1;
   for i := 0 to pred(NbreVariab) do
       if grandeurs[indexVariab[i]].nom=nomX then begin
         result := i;
         exit;
       end;
end;

var
  i : integer;
  p : codePage;
  courbeAdd : Tcourbe;
  x,y : vecteur;
  mondeLoc : indiceMonde;
begin // SetCoordonnee
with graphes[indexGr] do begin
   indexReticuleModele := -1;
   withResidusStudent := false;
   orthoAverifier := true;
   VerifCoordonnee(indexGr);
   courbeModele := nil;
   indexCourbeModele := -1;
   indexCourbeEquivalence := -1;
   CourbeExpAffectee := false;
   SimulationAffectee := false;
   mondeLoc := mondeY;
   withModeleLoc := withModele;
   for p := 1 to NbrePages do Pages[p].tri;
   for i := 1 to maxOrdonnee do with Coordonnee[i] do
     if codeX<>grandeurInconnue then begin
        if superposePage and
           (NbrePages>1) and
           (OgAnalyseurLogique in OptionGraphe) then mondeLoc := mondeY;
        if OgPolaire in OptionGraphe then begin
            iMondeC := mondeY;
            if (i=1) then if codeY=grandeurInconnue
                  then monde[mondeY].Axe := nil
                  else monde[iMondeC].Axe := grandeurs[codeY];
        end;
        if (withModele) then begin
              include(trace,trLigne);
              ligne := liModele;
              include(trace,trPoint);
        end;
        if superposePage and (NbrePages>1)
           then for p := 1 to NbrePages do begin
              if pages[p].active then 
                 if OgAnalyseurLogique in OptionGraphe
                      then begin
                          AjouteVariab(mondeLoc,i,p);
                          inc(mondeLoc);
                      end
                      else AjouteVariab(iMondeC,i,p)
           end
           else AjouteVariab(iMondeC,i,pageCourante);
     end;
   if (ModeleDefini in etatModele) and
      withModeleLoc and
      not(CourbeExpAffectee) then with courbes[0] do begin
              courbeAdd := AjouteCourbe(
                   valX,valY,iMondeC,pages[page].nmes,
                   varX,varY,page);
              courbeAdd.debutC := pages[page].debut[1];
              courbeAdd.finC := pages[page].fin[1];
              courbeAdd.trace := [trPoint];
              with pages[page] do begin
                 courbeAdd.IncertX := incertVar[varX.indexG];
                 courbeAdd.IncertY := incertVar[varY.indexG];
              end;
              with Coordonnee[1] do
                   courbeAdd.setStyle(couleur,styleT,motif);
              courbeAdd.courbeExp := true;
              courbeAdd.indexModele := 1;
   end;
   if (SimulationDefinie in etatModele) and
       (withModele) and
       not(SimulationAffectee) and
       (courbes.count>0) then begin
                 i := courbes.count;
                 repeat dec(i)
                 until (i<0) or
                       (fonctionSuperposee[1].indexY=courbes[i].varY.indexG);
                 if i>=0 then begin
                    setLength(x,pages[pageCourante].nmesMax+1);
                    setLength(y,pages[pageCourante].nmesMax+1);
                    courbeAdd := AjouteCourbe(x,y,
                         courbes[i].iMondeC,pages[pageCourante].nmes,
                         courbes[i].varX,courbes[i].varY,pageCourante);
                    courbeAdd.IndexModele := -1;
                    fonctionSuperposee[1].indexX := courbes[i].varX.indexG;
                    with coordonnee[i] do
                        courbeAdd.setStyle(couleurModele[-1],styleT,motif);
                    courbeAdd.Trace := [trLigne];
                    courbeAdd.Adetruire := true;
                 end;   
   end;
   modif := [gmEchelle];
   grapheOK := true;
   if indexGr=1 then
      AbscisseCB.itemIndex := indexVariable(graphes[1].coordonnee[1].nomX);
end end; // SetCoordonnee 

procedure TFgrapheVariab.VerifCoordonnee(indexGr : integer);

procedure VerifVotable;
var i : integer;
begin
    for i := 0 to NbreVariab do begin
        if grandeurs[indexVariab[i]].nom = 'Longitude' then begin
            graphes[1].optionGraphe := [OgOrthonorme,OgPolaire];
            graphes[1].optionModele := [];
            graphes[1].coordonnee[1].Trace := [trPoint];
            ordreLissage := 3;
            dimPointVGA := 4;
            graphes[1].coordonnee[1].nomX := 'Longitude';
            graphes[1].coordonnee[1].codeX := indexNom('Longitude');
            graphes[1].coordonnee[1].nomY := 'Distance';
            graphes[1].coordonnee[1].codeY := indexNom('Distance');
            graphes[1].nbreOrdonnee := 1;
            break;
        end;
    end;
end;

var i : integer;
    avecAxe : boolean;
    avecIncert : boolean;
begin with graphes[indexGr] do begin
    avecIncert := false;
    for i := 1 to maxOrdonnee do with coordonnee[i] do begin
        codeY := indexNom(nomY);
        if (codeY>maxGrandeurs) or
           not (grandeurs[codeY].genreG in [variable,constante,constanteGlb])
           then codeY := grandeurInconnue;
        codeX := indexNom(nomX);
        if (codeX<>grandeurInconnue) and
           not (grandeurs[codeX].genreG in [variable,constante,constanteGlb])
           then codeX := grandeurInconnue;
    end;
    nbreOrdonnee := 0;
    for i := 1 to maxOrdonnee do with coordonnee[i] do
        if (codeX<>grandeurInconnue) and
           (codeY<>grandeurInconnue)
           then begin
              inc(nbreOrdonnee);
              coordonnee[nbreOrdonnee] := coordonnee[i];
              avecIncert := avecIncert or
                 grandeurs[codeY].incertDefinie or
                 grandeurs[codeX].incertDefinie;
           end;
    for i := succ(NbreOrdonnee) to maxOrdonnee do with coordonnee[i] do begin
           codeX := grandeurInconnue;
           codeY := grandeurInconnue;
           iMondeC := mondeY;
           nomX := '';
           nomY := '';
           ligne := liDroite;
    end;
    if nbreOrdonnee=0 then verifVotable;
    if nbreOrdonnee=0 then begin
       Dessins.clear;
       for i := mondeX to MaxOrdonnee do Monde[i].graduation := gLin;
       with coordonnee[1] do begin
          codeX := indexVariab[0];
          nomX := grandeurs[codeX].nom;
          curseur := curSelect;
          codeY := indexVariab[1];
          nomY := grandeurs[codeY].nom;
          iMondeC := mondeY;
          nbreOrdonnee := 1;
       end;
       with coordonnee[2] do begin
          codeY := indexVariab[2];
          if (codeY<>grandeurInconnue) and
             (grandeurs[codeY].fonct.genreC=g_experimentale) then begin
             codeX := indexVariab[0];
             nomX := grandeurs[codeX].nom;
             nomY := grandeurs[codeY].nom;
             if unitesEgales(grandeurs[codeY],grandeurs[coordonnee[1].codeY])
                then iMondeC := mondeY
                else iMondeC := mondeDroit;
             nbreOrdonnee := 2;
          end;
       end;
    end // nbreOrdonnee=0
    else begin
        AvecAxe := false;
        for i := 1 to NbreOrdonnee do with coordonnee[i] do
            if iMondeC<>mondeSans then avecAxe := true;
        if not AvecAxe then coordonnee[1].iMondeC := mondeY;
    end;
    incertBtn.enabled := avecIncert;
    AffIncert.enabled := avecIncert;
    if not avecIncert then begin
       avecEllipse := false;
       incertBtn.down := false;
       incertBtn.ImageIndex := incertNobitmap;
       AffIncert.checked := false;
    end;
end end; // VerifCoordonnee(indexGr : integer);

procedure TFgrapheVariab.FormResize(Sender: TObject);
begin  // arrive après le dimensionnement
    if (withModele) then AjustePanelModele;
    if PanelBis.visible
        then if residusItem.checked
            then PanelBis.Width := PanelCentral.width div 3
            else PanelBis.Width := PanelCentral.width div 2
        else PanelBis.Width := 0;
    if PaintBox3.visible
        then if (withModele) and residusItem.checked
            then PaintBox3.height := panelPrinc.height div 3
            else PaintBox3.height := panelPrinc.height div 2
        else PaintBox3.height := 0;
    if CopierBtn.visible then
       ToolBarGraphe.List := (CopierBtn.Left+CopierBtn.width)<PanelCentral.width
end;

procedure TFgrapheVariab.CoordonneesItemClick(Sender: TObject);
var oldnomx,oldnomy : string;
begin
if FcoordonneesPhys=nil then
   Application.CreateForm(TFcoordonneesPhys, FcoordonneesPhys);
with FcoordonneesPhys,grapheCourant do begin
        if not grapheOK then exit;
        Agraphe := grapheCourant;
        ListeConst := false;
        oldNomX := coordonnee[1].nomX;
        oldNomY := coordonnee[1].nomY;
        modeleEnCours := (withModele) and (NbreModele>0);
        modelePermis := NbreModele>0;
        Transfert.AssignEntree(grapheCourant);
        if FcoordonneesPhys.ShowModal=mrOK then begin
          Curseur := curSelect;
          Transfert.AssignSortie(grapheCourant);
          include(modif,gmXY);
          if (coordonnee[1].nomX<>oldNomX) or
             (coordonnee[1].nomY<>oldNomY) then begin
              if not UseDefaut then grapheCourant.Dessins.clear;
              if curseur in [curReticuleData,curReticuleModele] then curseur := curSelect;
          end;
          ModifGraphe(indexGrCourant);
          graphes[3-indexGrCourant].pasPoint := graphes[indexGrCourant].pasPoint;
          OptionGrapheDefault := graphes[indexGrCourant].OptionGraphe;
          if (withModele) and not modeleEnCours then begin
                nbreModele := 0;
                etatModele := [];
                setPanelModele(false);
          end;
     end;{OK}
end end; // CoordonneesItemClick

procedure TFgrapheVariab.ZoomAutoItemClick(Sender: TObject);
begin
     curMovePermis := false;
     if not grapheCourant.grapheOK then exit;
     include(grapheCourant.modif,gmEchelle);
     grapheCourant.monde[mondeX].defini := false;
     grapheCourant.useDefaut := false;
     grapheCourant.useDefautX := false;
     grapheCourant.autoTick := true;
     modifGraphe(indexGrCourant);
end; // ZoomAutoItemClick

procedure TFgrapheVariab.CopierItemClick(Sender: TObject);
begin
     GrapheCourant.VersPressePapier(grapheClip)
end;

procedure TFgrapheVariab.WMRegMaj(var Msg : TWMRegMessage);

procedure RazCoord;
var i,j,m : integer;
    UnitePrec : string;
begin
    Curseur := curSelect;
    IsModeleMagnum := false;
    j := 1;
    m := mondeY;
    UnitePrec := '';
    IntersectionItem.visible := false;
    IntersectionTer.visible := false;
    for i := 1 to pred(NbreVariab) do with graphes[1].Coordonnee[j] do begin
        codeY := indexVariab[i];
        ligne := LiDroite;
        if grandeurs[codeY].fonct.genreC=g_experimentale then begin
             codeX := indexVariab[0];
             nomX := grandeurs[codeX].nom;
             nomY := grandeurs[codeY].nom;
             if (j>1) and (UnitePrec<>grandeurs[codeY].nomUnite) then inc(m);
             UnitePrec := grandeurs[codeY].nomUnite;
             iMondeC := m;
             graphes[1].monde[iMondeC].graduation := gLin;
             inc(j);
             if j>MaxOrdonnee then exit;
        end
    end;
    for i := 1 to 3 do with graphes[i] do if paintBox.visible then begin
        useDefaut := false;
        Dessins.clear;
    end;
    IdentAction.checked := false;
    for i := 1 to MaxPages do ModeleCalc[i] := false;
end;

Procedure FaireMajFichier;
var i : integer;
    ouvrirModele : boolean;
begin
       etatModele := [];
       Fvaleurs.prevenirPi := true;

       MajFichierEnCours := false;
       MajModelePermis := false;
       OuvrirModele := false;
       MemoModele.Clear;
//       MemoModele.Text := TexteModele.text;
       for i := 0 to pred(TexteModele.count) do begin
           MemoModele.Lines.Add(TexteModele[i]);
           if TexteModele[i]<>'' then OuvrirModele := true;
       end;
       MajModelePermis := true;
       MemoModeleChange(nil);
       if configCharge
         then begin
              for i := 1 to NbrePages do begin
                    pages[i].modeleCalcule := ModeleCalc[i];
                    pages[i].modeleErrone := false;
              end
         end
         else RazCoord;
       if choixIdentPagesDlg<>nil then choixIdentPagesDlg.initParam;
       case nbreGraphe of
            UnGr : DeuxGraphesVertClick(UnGrapheItem);
            DeuxGrVert : DeuxGraphesVertClick(DeuxGraphesVert);
            DeuxGrHoriz : DeuxGraphesVertClick(DeuxGraphesHoriz);
       end;
       if (ModeleCalc[1] or OuvrirModele)
          then setPanelModele(true);
       if ModeleCalc[1] then PostMessage(handle,WM_Reg_Calcul,CalculAjuste,0);
end;

procedure ResetForme;
var i,j : integer;
begin
    for i := 1 to MaxParametres do
        PanelParam[i].visible := false;
    majFichierEnCours := true;
    configCharge := false;
    curseur := curSelect;
    for i := 1 to 3 do with graphes[i] do if paintBox.Visible then begin
        Modif := [gmXY];
        Dessins.clear;
        grapheOK := false;
        curReticuleDataActif := false;
        curReticuleModeleActif := false;
        for j := mondeX to mondeSans do monde[j].axe := nil;
        ResetEquivalence;
        OptionGraphe := OptionGrapheDefault;(***)
    end;
    IntersectionItem.visible := false;
    IntersectionTer.visible := false;
    Fvaleurs.prevenirPi := true;
    UnGrapheItem.checked := true;
    MemoModele.Clear;
    MemoResultat.Clear;
    etatModele := [];
    VerifGraphe := true;
    if withModele then setPanelModele(false);
    nbreGraphe := UnGr;
    PanelBis.visible := false;
    PaintBox2.visible := false;
    PaintBox3.visible := false;
    residusStudentCB.visible := false;
    ModelePagesIndependantesMenu.checked := false;
    ModelePagesIndependantes := false;
    RepeatTimer.enabled := false;
    IsModeleMagnum := false;

end;

var i,j,iVar : integer;
    m : shortInt;
    modifParam : boolean;
begin with msg do begin // WMRegMaj
      Graphes[1].Modif := [gmXY]; // le + général
      case TypeMaj of
          MajNom : for i := 1 to 3 do
              graphes[i].MajNomGr(codeMaj);
          MajSauvePage : begin
              if (withModele) and
                 ModelePagesIndependantesMenu.checked then begin
                 ModelePagesIndependantes := true;
                 Pages[pageCourante].TexteModeleP.text := texteModele.text;
              end;
              Graphes[1].Modif := [];
          end;
          MajChangePage : begin
              if (curseur in [curReticuleData,curReticuleModele]) or
                 not graphes[1].superposePage
                    then include(graphes[1].Modif,gmPage)
                    else graphes[1].Modif := [];

              if (withModele) then begin
                 MajParametres;
                 include(graphes[1].Modif,gmValeurModele);
                 if ([ModeleDefini,ModeleConstruit] <= etatModele) and
                    (pages[pageCourante].ModeleCalcule) then MajResultat;
              end;
              if (withModele) and
                 ModelePagesIndependantesMenu.checked and
                 (Pages[pageCourante].TexteModeleP.count>0) then begin
                    MajModelePermis := false;
                    ModelePagesIndependantes := true;
                    MemoModele.clear;
                    MemoModele.Lines := pages[pageCourante].TexteModeleP;
                    MajModelePermis := true;
                    etatModele := [];
                    LanceModele(true); // RàZ Modéle
              end;
              if ([ModeleDefini,ModeleConstruit] <= etatModele)
                 and not(pages[pageCourante].ModeleCalcule)
                 and not(pages[pageCourante].ModeleErrone)
                 and not ModelePagesIndependantes
                 and (withModele)
                 then if isModeleMagnum
                     then PostMessage(handle,Wm_Reg_InitMagnum,0,0)
                     else if pages[1].ModeleCalcule
                          then PostMessage(handle,Wm_Reg_Calcul,CalculAjuste,0)
          end;
          MajAjoutPage : begin
              if (curseur in [curReticuleData,curReticuleModele]) or
                  not graphes[1].superposePage
                    then include(graphes[1].Modif,gmPage)
                    else graphes[1].Modif := [];
              pages[PageCourante].modeleErrone := pages[1].modeleErrone;
              if (withModele) then begin
                 MajParametres;
                 include(graphes[1].Modif,gmValeurModele);
              end;
              if ModelePagesIndependantes then etatModele := [];
              if ([ModeleDefini,ModeleConstruit] <= etatModele) and
                 not(pages[1].ModeleErrone)
                 and (withModele)
                 then if isModeleMagnum
                     then PostMessage(handle,Wm_Reg_InitMagnum,0,0)
                     else if pages[1].ModeleCalcule
                         then PostMessage(handle,Wm_Reg_Calcul,CalculAjuste,0)
          end;
          MajSupprPage : if graphes[1].superposePage then ;
          MajSelectPage : with Graphes[1] do begin
                modif := [gmXY,gmOptions];
                if codeMaj<>0 then SuperposePage := codeMaj>1;
          end;
          MajGroupePage : Graphes[1].modif := [gmXY,gmOptions];
          MajValeurAcq,MajValeur,MajIncertitude : begin
              Graphes[1].Modif := [gmValeurs];
              MemoModeleChange(nil);
          end;
          MajValeurConst : begin
              Graphes[1].Modif := [gmValeurs];
          end;
          MajSupprPoints : Graphes[1].Modif := [gmValeurs];
          MajValeurGr : begin
              Graphes[1].Modif := [gmValeurs];
              MemoModeleChange(nil);
          end;
          MajAjoutValeur : begin
              Graphes[1].Modif := [gmValeurs];
              MemoModeleChange(nil);
              for m := 1 to NbreModele do with pages[pageCourante] do
                  if (debut[m]=0) and
                     ((fin[m]=-1) or (fin[m]=nmes-2)) then
                     fin[m] := pred(nmes)
          end;
          MajTri : if active then exit;
          MajPolice : begin
             setTaillePolice;
             Graphes[1].Modif := [];
             if panelModele.width<20*Font.Size then
                panelModele.width := 20*Font.Size
          end;
          MajPreferences : begin
             Graphes[1].Modif := [];
          end;
          MajModele : if (withModele) and
                         residusItem.checked and
                         UnGrapheItem.checked then
             setCoordonneeResidu;
          MajVide : ResetForme;
          MajGrandeur : begin
              if majFichierEnCours
                 then FaireMajFichier
                 else if (etatModele<>[]) and not ajoutGrandeurNonModele then begin
                     etatModele := [];
                     majBtn.Enabled := true;
                  end;
              if NbreVariab>0 then begin
                    abscisseCB.Items.Clear;
                    for i := 0 to pred(NbreVariab) do
                        abscisseCB.Items.Add(grandeurs[indexVariab[i]].nom)
              end;
              for i := 0 to (ToolBarGrandeurs.ButtonCount-1) do begin
                  ToolBarGrandeurs.Buttons[i].visible := i<NbreVariab;
                  if i<NbreVariab then begin
                     iVar :=  indexVariab[i];
                     ToolBarGrandeurs.Buttons[i].caption := grandeurs[iVar].nom;
                     ToolBarGrandeurs.Buttons[i].tag := iVar;
                     ToolBarGrandeurs.Buttons[i].Hint := 'Clic '+grandeurs[iVar].nom+'=ordonnée ; clic droit : à droite'
                  end;
              end;
              LabelToolBar.Visible := true;
          end;
          MajUnites : exclude(etatModele,UniteParamDefinie);
          MajUnitesParam : MajResultat;
          MajFichier : if majFichierEnCours then FaireMajFichier else exit;
          MajNumeroMesure : exit;
          MajOptionsGraphe : for i := 1 to 3 do begin
                if OgQuadrillage in optionGrapheDefault
                     then include(graphes[i].optionGraphe,OgQuadrillage)
                     else exclude(graphes[i].optionGraphe,OgQuadrillage);
                if OgZeroGradue in optionGrapheDefault
                     then include(graphes[i].optionGraphe,OgZeroGradue)
                     else exclude(graphes[i].optionGraphe,OgZeroGradue);
          end;
      end;
      ModifParam := TypeMaj in [MajGroupePage,MajSupprPage,MajModele];
      if TypeMaj in [MajIncertitude,MajValeur,MajValeurAcq]
          then begin
             pages[PageCourante].modeleCalcule := false;
             pages[PageCourante].modeleErrone := false;
          end;
      if ModifParam and (withModele)
                     then begin
           MajParametres;
           MajResultat;
           include(graphes[1].Modif,gmValeurModele);
      end;
      if (Graphes[1].Modif<>[]) and not MajFichierEnCours then begin
         if (curseur=curModeleGr) and
            (PageCourante>0) then
               for j := 1 to NbreModele do
                   modeleGraphique[j].setParametres(pages[pageCourante].valeurParam[paramNormal]);
         setBoutonsModele;
         for i := 2 to 3 do graphes[i].Modif := graphes[1].Modif;
         if residusItem.checked then begin
            graphes[3].Modif := [];
            graphes[2].Modif := [];
         end;
         for i := 1 to 3 do modifGraphe(i);
      end;
      case typeMaj of
           MajSupprPoints : if [ModeleConstruit,ModeleLineaire] <= etatModele
               then LanceModele(true);
           MajChangePage,MajAjoutPage,MajModele,MajSauvePage : ; // déjà fait
           else if not ajoutGrandeurNonModele then begin
                pages[pageCourante].ModeleCalcule := false;
                etatModele := [];
          end;
      end;
end end; // WMRegMaj 

procedure TFgrapheVariab.ImprimerGraphe(var bas : integer);
var i : integer;
begin
    for i := 1 to 3 do if graphes[i].paintBox.Visible then
        if printer.orientation=poLandscape
           then graphes[i].versImprimante(HautGraphePaysage,bas)
           else graphes[i].versImprimante(HautGrapheTxt,bas);
end;

procedure TFgrapheVariab.PaintBoxPaint(Sender: TObject);
var Agraphe : TgrapheReg;

procedure BandeConfiance;
var i,j,im,p,Nbande : integer;
    courbeMax,courbeMin : Tcourbe;
    valeurXmax,valeurYmax,valeurXmin,valeurYmin : vecteur;
begin
  Agraphe.ChercheMinMax;
  for i := 0 to pred(Agraphe.courbes.count) do 
      if Agraphe.courbes[i].indexBande=MaxConfiance then begin
          CourbeMax := Agraphe.courbes[i];
          CourbeMin := Agraphe.courbes[i]; // pour le compilateur
          im := Agraphe.courbes[i].indexModele;
          p := Agraphe.courbes[i].page;
          for j := succ(i) to pred(Agraphe.courbes.count) do begin
                  if (Agraphe.courbes[j].indexBande=MinConfiance) and
                     (Agraphe.courbes[j].page=p) and
                     (Agraphe.courbes[j].indexModele=im) then
                        CourbeMin := Agraphe.courbes[j];
          end;
          with fonctionTheorique[im],pages[p] do begin
                    valeurXmax := courbeMax.valX;
                    valeurYmax := courbeMax.valY;
                    valeurXmin := courbeMin.valX;
                    valeurYmin := courbeMin.valY;
                    GenereM(valeurXmax,valeurYmax,Agraphe.miniMod,Agraphe.maxiMod,false,Nbande);
                    stat2.init(valeurVar[indexX],valeurVar[indexY],debut[im],fin[im]);
                    stat2.genereBande(valeurXmin,valeurYmin,valeurXmax,valeurYMax,Nbande);
                    CourbeMax.FinC := Nbande;
                    CourbeMin.FinC := Nbande;
         end;// fonctionTheorique
  end;
end; // BandeConfiance

Procedure TestModif(indexGr : integer);
var i : integer;
begin
        if (([gmXY,gmModele,gmPage,gmValeurs]*Agraphe.modif)<>[]) or
           (Agraphe.courbes.count=0)
            then begin
               if ([gmValeurs]=Agraphe.modif) or Agraphe.UseDefaut
                   then Agraphe.courbes.clear
                   else begin
                       Agraphe.reset;
                       if not (gmPage in Agraphe.modif) and
                          not (gmValeurs in Agraphe.modif) and
                          not configCharge then
                             Agraphe.resetEquivalence;
                   end;
              if (indexGr<>1) and residusItem.checked
                  then setCoordonneeResidu
                  else setCoordonnee(indexGr);
              if Agraphe.UseDefaut then with Agraphe do begin
                  monde[mondeX].defini := true;
                  for i := 1 to maxOrdonnee do with Coordonnee[i] do
                      if codeX<>grandeurInconnue then
                         monde[iMondeC].defini := true;
               end;
            end;
end;

procedure MajIntersection;

procedure Supprime(i : integer);
var
    j : integer;
    equ : Tequivalence;
begin
    j := 0;
    while j<Agraphe.equivalences[pageCourante].count do begin
        equ := Agraphe.equivalences[pageCourante].items[j];
        if (equ.ligneRappel=lrReticule) and (equ.indexModele=i)
            then Agraphe.equivalences[pageCourante].Delete(j)
            else inc(j);
    end;
end;

var i,j : integer;
    equ : Tequivalence;
    trouve : boolean;
begin
  if not intersectionItem.visible or
     not intersectionItem.checked or
     (NbreModele<2)
     then begin // suppression
         for i := 2 to maxIntervalles do Supprime(i);
         exit;
     end;
  for i := 2 to nbreModele do begin
     if isIncorrect(pages[pageCourante].X_inter[i]) or
        isIncorrect(pages[pageCourante].Y_inter[i])
     then supprime(i)
     else begin
        trouve := false;
        for j := 0 to Pred(Agraphe.equivalences[pageCourante].count) do begin
             equ := Agraphe.equivalences[pageCourante].items[j];
             if (equ.ligneRappel=lrReticule) and (equ.indexModele=i) then begin
                trouve := true;
                equ.ve := pages[pageCourante].X_inter[i];
                equ.phe := pages[pageCourante].Y_inter[i];
                break;
             end;
        end;
        if not trouve then begin
             equ := Tequivalence.create(0,0,0,0,
                 pages[pageCourante].X_inter[i],
                 pages[pageCourante].Y_inter[i],
                 0,agraphe);
             equ.mondepH := mondeY;
             equ.varY := Agraphe.monde[mondeY].axe;
             equ.indexModele := i;
             equ.ligneRappel := lrReticule;
             ligneRappelCourante := lrReticule;
             Agraphe.Equivalences[pageCourante].Add(equ);
        end;
     end;
  end;
  for i := succ(nbreModele) to maxIntervalles do supprime(i);
end; // MajIntersection

var i,j : integer;
    indexGr : integer;
label finProc;
begin // PaintBoxPaint
        if (pageCourante=0)
           or majFichierEnCours
           or lecturePage
           or sizing
              then exit;
        indexGr := (sender as Tcontrol).tag;
        if indexGr<1 then indexGr := 1;
        tailleEcran := PanelCentral.Height;
        largeurEcran := (sender as TPaintBox).Width;
        Agraphe := graphes[indexGr];
        Agraphe.grapheOK := (NbreVariab>1) and (NbrePages>0);
        (sender as TpaintBox).canvas.font.Name := FontName;
        Agraphe.canvas := Agraphe.paintBox.canvas;
        if (indexGr<>1) and residusItem.checked and
           not (Agraphe.grapheOK and
                pages[pageCourante].modeleCalcule and
                (ModeleDefini in etatModele) and
                (ModeleConstruit in etatModele)) then begin
                   Agraphe.canvas.TextOut(Agraphe.paintBox.clientRect.right div 2,
                                          Agraphe.paintBox.clientRect.bottom div 2,
                                          'Non disponible ; cliquer sur ajuster');
                   goto finProc;
        end;
        if not Agraphe.grapheOK then begin
           Agraphe.reset;
           Agraphe.resetEquivalence;
           Agraphe.optionGraphe := optionGrapheDefault;
           goto finProc;
        end;
        if paintBox1=sender then begin
           AbscisseCB.Top := paintBox1.top+graphes[1].monde[mondeX].yTitre;
           AbscisseCB.left := paintBox1.left+graphes[1].monde[mondeX].xTitre-abscisseCB.width;
        end;
        incertBtn.down := avecEllipse;
        if avecEllipse
           then incertBtn.ImageIndex := incertYesbitmap
           else begin
              incertBtn.ImageIndex := incertNobitmap;
              for j := 0 to Agraphe.courbes.count-1 do
                  with Agraphe.courbes[j] do
                    if (motif=mIncert) then motif := TMotif(j);
           end;
        Agraphe.limiteFenetre := Agraphe.paintBox.clientRect;
        ZoomManuelBtn.down := Agraphe.UseDefaut;
        if ZoomManuelBtn.down
           then begin
              ZoomManuelBtn.imageIndex := 38;
              ZoomManuelBtn.hint := hZoomManuelDebloq;
              ZoomAutoBtn.imageIndex := 38;
              ZoomAutoBtn.hint := hZoomAutoBloq;
           end
           else begin
              ZoomManuelBtn.imageIndex := 17;
              ZoomManuelBtn.hint := hZoomManuelBloq;
              ZoomAutoBtn.imageIndex := 18;
              ZoomAutoBtn.hint := 'Echelle automatique';
           end;
        Agraphe.curReticuleDataActif := curseur=curReticuleData;
        Agraphe.curReticuleModeleActif := curseur=curReticuleModele;
        PanelAjuste.enabled := false;
        TestModif(indexGr);
        if not Agraphe.useDefaut and (Pages[pageCourante].nmes>0) then begin
           Agraphe.miniTemps := Pages[pageCourante].valeurVar[0,0];
           Agraphe.maxiTemps := Pages[pageCourante].valeurVar[0,pred(Pages[pageCourante].nmes)];
        end;
        IdentAction.enabled :=
           (grapheCourant.superposePage
            and (NbrePages>1) )  or
           (grapheCourant.NbreOrdonnee>1);
        if ((gmEchelle in Agraphe.modif) or
            (gmValeurModele in Agraphe.modif) or
            ((gmPage in Agraphe.modif) and
              pages[pageCourante].modeleCalcule)) then
                AffecteModele(indexGr);
        if ([ModeleConstruit,ModeleLineaire] <= etatModele) and
           withBandeConfiance then BandeConfiance;
        Agraphe.chercheMonde;
        if not Agraphe.grapheOK then goto finProc;
        if (curseur=curModeleGr) and (NbreModele>2) then curseur := curSelect;
        if curseur=curModeleGr then for j := 1 to NbreModele do
           with ModeleGraphique[j] do
                for i := 1 to indexPointMax[genre] do
                    Agraphe.AjoutePoint(mondeY,xs[i],ys[i]);
        if ((gmEchelle in Agraphe.modif) or (pages[pageCourante].nmes<128)) and
           (omExtrapole in Agraphe.OptionModele) then begin
              ZoomModele(indexGr);
              ZoomSuperpose(indexGr);
        end;
        MajIdentPage(indexGr);
        (*
        Agraphe.canvas.Pen.mode := pmCopy;
        Agraphe.canvas.brush.Color := clWindow;
        Agraphe.canvas.brush.style := bsSolid;
        Agraphe.canvas.FillRect(Agraphe.paintBox.clientRect);
        *)
        Agraphe.avecBorne := withModele;
        MajIntersection;
        Agraphe.draw;
        if active then begin
           setPenBrush(curseur);
           if curseur=curModeleGr
              then for j := 1 to NbreModele do modeleGraphique[j].DessineTout
              else traceCurseurCourant(indexGr);
        end;
        if (curseur=curEquivalence) and
           ((Agraphe.indexCourbeEquivalence<0) or
            (Agraphe.indexCourbeEquivalence>=Agraphe.courbes.count))
              then Agraphe.setCourbeDerivee;
        Agraphe.modif := [];
        if DeuxGraphesVert.checked or
           DeuxGraphesHoriz.checked
           then Agraphe.EncadreTitre(Agraphe=grapheCourant);
        if orthoAverifier and Agraphe.VerifierOrtho then begin
             afficheErreur(erOrtho,0);
             Agraphe.verifierOrtho := false;
             orthoAverifier := false;
        end;
        if Agraphe.VerifierInv then begin
             afficheErreur(erInv,0);
             Agraphe.verifierInv := false;
        end;
        if VerifierLog then begin
             afficheErreur(erAxeLog,0);
             verifierLog := false;
        end;
        finProc :
        VecteursBtn.visible := (agraphe.courbes.Count>0) and agraphe.grapheOK;
        if vecteursBtn.Visible then with agraphe.courbes[0] do VecteursBtn.visible :=
            (varX<>nil) and (varY<>nil) and
            (varX<>grandeurs[0]) and (varY<>grandeurs[0]);
        borneSelect.style := bsAucune;
        PanelAjuste.enabled := true;
        if curseur<>curMove then begin
           IndexPointCourant := -1;
           CourbePointCourant := 0;
        end;
        if (PropCourbeForm<>nil) and
           PropCourbeForm.visible and
           Agraphe.grapheOK then begin
            if (iPropCourbe>=Agraphe.courbes.Count) or
               (iPropCourbe<=0) then iPropCourbe := 0;
            PropCourbeForm.Acourbe := Agraphe.courbes[iPropCourbe];
            PropCourbeForm.MaJ;
        end;
end; // PaintBoxPaint

procedure TFgrapheVariab.SetPanelModele(avecModele : boolean);
begin
   if not grapheCourant.grapheOK then avecModele := false;
   withModele := avecModele;
   setPanelParam;
end;

procedure TFgrapheVariab.SetPanelParam;
var i : integer;
begin
if withModele
      then begin
           TraceAutoCB.checked := not(OmManuel in Graphes[1].OptionModele);
           panelModele.visible := true;
           splitterModele.visible := true;
           PanelModele.invalidate;
           VerifGraphe := false;
           ModeleItem.checked := true;
           if not(ModeleDefini in etatModele)
               then LanceCompile
               else SetBoutonsModele;
           MajParametres;
           Resize;
        //   MemoModele.setFocus;
     end
     else begin
       ModeleItem.checked := false;
       splitterModele.visible := false;
       panelModele.visible := false;
       if ModeleDefini in etatModele then
       for i := 1 to Graphes[1].NbreOrdonnee do
           with Graphes[1].Coordonnee[i] do begin
                include(trace,trLigne);
                ligne := liModele;
                include(trace,trPoint);
           end;
     end
end;

procedure TFgrapheVariab.AjusteBtnClick(Sender: TObject);
begin
// vérification : entrée dans éditeur paramètres à valider }
     if (activeControl<>nil) and
        (activeControl.width=editValeur1.width) and
        (activeControl.height=editValeur1.height) and
        (activeControl.tag>0)
            then ValideParam(activeControl.tag,false);
     LanceModele(true)
end;

procedure TFgrapheVariab.MemoModeleChange(Sender: TObject);
var i : codePage;
begin if MajModelePermis then begin
     MajModelePermis := false;
     etatModele := [];
     if NbrePages=0 then begin
        MemoModele.clear;
        MajModelePermis := true;
        exit;
     end;
     MemoResultat.clear;
     if length(MemoModele.text)>5 then begin
        MemoResultat.Lines.Add(stTest4);
        MemoResultat.Lines.Add(stTest5);
        MemoResultat.Lines.Add(stTest6);
        MajBtn.enabled := true;
     end
     else MajBtn.enabled := false;
     for i := 1 to NbrePages do begin
         pages[i].ModeleCalcule := false;
         pages[i].ModeleErrone := false;
     end;
     if curseur=curModeleGr then setPenBrush(curSelect);
     MajModelePermis := true;
     setBoutonsModele;
end end;

Procedure TFgrapheVariab.MajParametres;
var i : integer;
begin with pages[pageCourante] do begin
       VerifIntervalles;
       verifParametres;
       for i := 1 to NbreParam[paramNormal] do begin
           PanelParam[i].visible := true;
           PanelParam[i].caption := Parametres[paramNormal,i].nom;
           EditValeur[i].text := formatGeneral(valeurParam[paramNormal,i],3);
       end;
       for i := succ(NbreParam[paramNormal]) to MaxParametres do
           PanelParam[i].visible := false;
       setBoutonsModele;
end end; // MajParametres 

procedure TFgrapheVariab.MajResultat;

Procedure Ajoute(const Aligne : string);
begin
    MemoResultat.Lines.add(Aligne);
    Pages[pageCourante].TexteResultatModele.add(Aligne);
end;

var m : shortInt;
    i : integer;
    PourCent : string;
    DebutCalcul : integer;
    setParam : TsetGrandeur;
begin with pages[pageCourante] do begin
    MemoResultat.Clear;
    TexteResultatModele.clear;
    setParam := [];
    if ModeleCalcule then begin
    for m := 1 to NbreModele do with fonctionTheorique[m] do begin
        //couleurM := fonctionTheorique[m].couleur;
        if modelePourCent then begin
           PourCent := chainePrec(PrecisionModele[m]);
           Ajoute(stEcartRelatif+' ('+enTete+') '+pourCent);
        end;
        if Chi2Actif
           then Ajoute('Chi2/(N-p)='+formatGeneral(chi2Relatif,precisionMin))
           else begin
               Ajoute(stSigmaY+addrY.formatNomEtUnite(sigmaY));
               if Lineaire then with stat2 do begin
                  if withCoeffCorrel then begin
                     init(valeurVar[indexX],valeurVar[indexY],debut[m],fin[m]);
                     Ajoute(stCoeffCorrel+'='+FormatGeneral(Correlation,precisionCorrel));
                  end;
                  if withPvaleur then begin
                     init(valeurVar[indexX],valeurVar[indexY],debut[m],fin[m]);
                     Ajoute('F='+FormatGeneral(Fisher,5));
                     Ajoute(stPvaleur+'F)'+
                         chainePrec(PSnedecor(1,NbrePointsModele,Fisher)));
// function PSnedecor(Nu1, Nu2 : Integer; X : Float) : Float; // Prob(F >= X)
                     if ParamAjustes then for i := 1 to NbreParam[paramNormal] do begin
                        try
                         Ajoute(stPvaleur+parametres[paramNormal,i].nom+')'+
                               chainePrec(PStudent(NbrePointsModele-1,
                                      abs(ValeurParam[paramNormal,i]/incertParam[i]))));
// FStudent(Nu : Integer; X : Float) : Float; // Prob(|t| >= X)
// P-valeur = Prob(t(n-p-1)>valeur/ecarttype)
                        except
                        end;
                     end; // paramAjustes
                   end; // Pvaleur
               end; // lineaire
           end; // non Chi2
        if (PrecisionModele[m]>0.5) and InitiationAfaire then begin
           InitiationAfaire := false;
           afficheErreur(erEcart,HELP_EcartModelisationExperience);
        end;
        if ErreurModele then Ajoute(erPointModele+IntToStr(PosErreurModele));
    end;
    IntersectionItem.visible := false;
    for m := 2 to NbreModele do with fonctionTheorique[m] do
        if isCorrect(Y_inter[m]) and isCorrect(X_inter[m]) then begin
             Ajoute(stIntersection+
                    IntToStr(pred(m))+'-'+IntToStr(m)+' : ');
             if sigmaX_inter[m]>0
                then Ajoute(grandeurs[indexX].formatNomPrecisionUnite(X_inter[m],sigmaX_inter[m]))
                else Ajoute(grandeurs[indexX].formatNomEtUnite(X_inter[m]));
             if sigmaY_inter[m]>0
                then Ajoute(grandeurs[indexY].formatNomPrecisionUnite(Y_inter[m],sigmaY_inter[m]))
                else Ajoute(grandeurs[indexY].formatNomEtUnite(Y_inter[m]));
             Ajoute('');
             IntersectionItem.visible := true;
        end;
    IntersectionTer.visible := IntersectionItem.visible;
    if ParamAjustes
       then begin
          for i := 1 to NbreParam[paramNormal] do begin
             EditValeur[i].text := FormatGeneral(ValeurParam[paramNormal,i],3);
             include(setParam,ParamToIndex(paramNormal,i));
          end;
          if AffIncertParam in [i95,iBoth] then begin
          Ajoute(stIntervalle95);
          for i := 1 to NbreParam[paramNormal] do
             Ajoute(ParamEtPrec(i,false));
          end;
          if AffIncertParam in [iType,iBoth] then begin
          Ajoute(stIncertitudeType);
          for i := 1 to NbreParam[paramNormal] do
             Ajoute(ParamEtPrec(i,true));
          end;
       end
       else begin
           Ajoute(stTest1);
           Ajoute(stTest2);
           Ajoute(stTest3);
           MajBtn.Enabled := true;
       end;
    debutCalcul := 0;
    for i := 0 to pred(NbreGrandeurs) do with grandeurs[i] do begin
        if (fonct.genreC<>g_experimentale) and
           ((setParam * fonct.depend) <> [] ) then begin
           debutCalcul := i;
           break;
        end;
    end;
    if debutCalcul>0 then RecalculP;
    end; // ModeleCalcule
end end; // MajResultat

procedure TFgrapheVariab.LanceModele(ajuste : boolean);
var k : codePage;
    m : integer;
label finProc;
begin
     if not(ajuste) and
       ( (OmManuel in Graphes[1].OptionModele)
          or not(withModele))
       then exit;
     TrigoLabel.Visible := fonctionTheorique[1].isSinusoide;
     if TrigoLabel.Visible
        then panel1.Height := 42
        else panel1.Height := 28;
     Screen.cursor := crHourGlass;
     PanelAjuste.enabled := true;
     if not (ModeleDefini in etatModele) then begin
        lanceCompile;
        if not (ModeleDefini in etatModele) then begin
     //      if MemoModele.showing then MemoModele.setFocus;
           goto finProc;
        end;
     end;
     if not (ModeleConstruit in etatModele) then begin
        ConstruitModele;
        if not (ModeleConstruit in etatModele) then begin // erreur ?
       //    if MemoModele.showing then MemoModele.setFocus;
           goto finProc;
        end;
     end;
     if not (UniteParamDefinie in etatModele) then setUniteParametre;
     pages[pageCourante].VerifIntervalles;
     VerifParametres;
     ajoutGrandeurNonModele := False;
     if modeleDependant or not ajuste
        then EffectueModele(ajuste,true)
        else begin
          for m := 1 to NbreModele do
              EffectueModeleMono(m);
          calcIntersection;
        end;
     if ajuste and
        (OmManuel in Graphes[1].OptionModele)
        and not pages[pageCourante].modeleCalcule then EffectueModele(false,true);
     MajBtn.enabled := false;
     if (OmEchelle in graphes[1].OptionModele) then
        graphes[1].monde[mondeX].Defini := false;
     finProc :
     PanelAjuste.enabled := true;
//     if active then SetFocus;
     if ajuste then ModifFichier := true;
     Screen.cursor := crDefault;
     setBoutonsModele;
     if (withModele) then setParamEdit;
     for k := 1 to NbrePages do ModeleCalc[k] := false;
     Application.MainForm.Perform(WM_Reg_Maj,MajModele,0);
end;// LanceModele 

procedure TFgrapheVariab.LanceCompile;
var PosErreur,longErreur,i : integer;
begin
     MajModelePermis := false;
     etatModele := [];
     NbreModele := 0;
     NbreFonctionSuper := 0;
     NbreParam[paramNormal] := 0;
     PosErreur := 0;
     LongErreur := 0;
     while (MemoModele.lines.count>0) and
           (MemoModele.lines[0]='') do MemoModele.lines.delete(0);
     while (MemoModele.lines.count>2) and
           (MemoModele.lines[MemoModele.lines.count-1]='') and
           (MemoModele.lines[MemoModele.lines.count-2]='')
          do MemoModele.lines.delete(MemoModele.lines.count-1);
     compileModele(posErreur,longErreur);
     if ModeleDefini in etatModele
        then begin
             ConstruitModele;
             InitEquaDiffBtn.visible := (fonctionTheorique[1].genreC=g_diff2) or
              ( ajusteOrigine and (fonctionTheorique[1].genreC=g_diff1) );
             if InitEquaDiffBtn.visible then InitEquaDiffBtn.hint :=
                 hInitialise+fonctionTheorique[1].addrYp.nom+'0 ';
             MajBtn.enabled := false;
             for i := 1 to NbreParam[ParamNormal] do
                 pages[pageCourante].nomParam[i] := parametres[ParamNormal,i].nom;
             for i := succ(NbreParam[ParamNormal]) to maxParametres do
                 pages[pageCourante].nomParam[i] := '';
        end
        else if not (PasDeModele in etatModele) then with memoModele do begin
             selStart := pred(posErreur);
             selLength := longErreur;
             InitEquaDiffBtn.visible := false;
             MajBtn.enabled := true;
             if showing then setFocus;
        end;
    setBoutonsModele;
    MajModelePermis := true;
    AjustePanelModele;
end; // LanceCompile

procedure TFgrapheVariab.ZoomArriereItemClick(Sender: TObject);
begin
    if not grapheCourant.grapheOK then exit;
    with grapheCourant.paintBox do begin
        grapheCourant.affecteZoomArriere;
        include(grapheCourant.modif,gmEchelle);
        invalidate;
    end;
end; // zoomArriere 

procedure TFgrapheVariab.FormDestroy(Sender: TObject);
var m : codeIntervalle;
    i : integer;                                
begin
     for i := 1 to 3 do Graphes[i].Free;
     for m := 1 to maxIntervalles do begin
         ValeurDeriveeX[m] := nil;
         ValeurYDiff[m] := nil;
         ValeurYpDiff[m] := nil;
         for i := 1 to maxParametres do
             libere(derf[m,i]);
     end;
     ValeurXDiff := nil;
     FGrapheVariab := nil;
     dXresidu := nil; dYresidu := nil;
     inherited;
end;

Procedure TFGrapheVariab.EcritConfigXML(Anode : TDOMNode);
var p,i : integer;
    OptionsXML,CoordXML,DefaultXML,Node1 : TDOMNode;
begin with Graphes[1] do begin
   for i := 1 to NbreOrdonnee do with coordonnee[i] do begin
       CoordXML := AddChildXML(ANode,'COORD');
       WriteIndexXML(CoordXML,i);
       writeIntegerXML(CoordXML,'LIGNE',ord(ligne));
       writeIntegerXML(CoordXML,'COULEUR',couleur);
       writeIntegerXML(CoordXML,'MOTIF',ord(Motif));
       writeStringXML(CoordXML,'X',coordonnee[i].nomX);
       writeStringXML(CoordXML,'Y',nomY);
    //   writeIntegerXML(CoordXML,'TRACE',word(TraceToXML(Trace)));
       writeIntegerXML(CoordXML,'MONDE',ord(iMondeC));
       writeIntegerXML(CoordXML,'GRADUATION',ord(monde[iMondeC].Graduation));
       writeIntegerXML(CoordXML,'ZERO',ord(monde[iMondeC].ZeroInclus));
   end;
   if useDefaut then begin
      DefaultXML := AddChildXML(ANode,'DEFAULT');
      for i := mondeX to mondeSans do begin
          Node1 := writeFloatXML(DefaultXML,'MAXI',monde[i].maxDefaut);
          WriteIndexXML(Node1,i);
      end;
      for i := mondeX to mondeSans do begin
          Node1 := writeFloatXML(DefaultXML,'MINI',monde[i].minDefaut);
          WriteIndexXML(Node1,i);
      end;
      writeFloatXML( DefaultXML,'TEMPSMIN',miniTemps);
      writeFloatXML(DefaultXML,'TEMPSMAX',maxiTemps);
   end;
   OptionsXML := addChildXML(ANode,'OPTIONS');
   writeIntegerXML(optionsXML,'OptGraphe',byte(optionGraphe));
   writeIntegerXML(optionsXML,'OptModele',byte(optionModele));
   writeBoolXML(optionsXML,'SuperPage',SuperposePage);
   writeBoolXML(optionsXML,'Ellipse',avecEllipse);
   writeBoolXML(optionsXML,'ProjeteVecteur',projeteVecteur);
   writeBoolXML(optionsXML,'FilDeFer',FilDeFer);
   writeBoolXML(optionsXML,'ProlongeVect',ProlongeVecteur);
   writeBoolXML(optionsXML,'CouleurVitesseImposee',couleurVitesseImposee);
   writeIntegerXML(optionsXML,'ReperePage',ord(reperePage));
//   writeIntegerXML(OrdreLissage);
//   writeIntegerXML(DimPointVGA);
//   writeBoolXML(UseDefaut);
//   writeIntegerXML(pasPoint);
//   writeBoolML(ajusteOrigine);
//   writeBool(UseSelect);
//   writeIntegerXML(trunc(Echellevecteur*100));
//   writeIntegerXML(NbreVecteurVitesseMax);
//   writeInteger(NbreTexteMax);
//   writeInteger(PenWidthVGA);
//   writeIntegerXML(zeroPolaire);
   for i := 0 to pred(Dessins.count) do ;
       //  dessins[i].StoreXML(ANode);
   for p := 1 to nbrePages do
       for i := 0 to pred(Equivalences[p].count) do ;
       //  equivalences[p][i].StoreXML(ANode,p);
end end;  // ecritConfigXML

Procedure TFgrapheVariab.litConfig;
var imax : integer;

procedure VerifCoord(indexGr : integer);
var i : integer;
begin with graphes[indexGr] do begin
    for i := mondeX to mondeSans do monde[i].axe := nil;
    for i := 1 to maxOrdonnee do with coordonnee[i] do begin
        codeY := indexNom(nomY);
        if codeY<>grandeurInconnue
           then monde[iMondeC].axe := grandeurs[codeY];
        codeX := indexNom(nomX);
        if codeX<>grandeurInconnue then
           monde[mondeX].axe := grandeurs[codeX];
        monde[iMondeC].defini := false;
        exclude(trace,trSonagramme);
    end;
    if not grapheCourant.useDefautX then
       monde[MondeX].defini := false; // ??
end end;

var i,j : integer;
    choix : integer;
    Agraphe : TgrapheReg;
    Adessin : Tdessin;
    Aequivalence : Tequivalence;
    zNom : string;
    zWord : word;
    zByte : byte;
    zInt : integer;
    zFloat : double;
    lectureOK : boolean;
    MajDefaut : boolean;
begin // litConfig
   UnGrapheItem.checked := true;
   majFichierEnCours := true;
   MajDefaut := false;
   Curseur := curSelect;
   for i := mondeX to mondeSans do
       for j := 1 to 3 do
           graphes[j].monde[i].axe := nil;
   for i := 1 to MaxPages do ModeleCalc[i] := false;
   nbreGraphe := UnGr;
   penWidthVGA := 1;
   while (Length(LigneWin)>0) and (ligneWin[1]=symbReg2) do begin
      configCharge := true;
      choix := 0;
      imax := NbreLigneWin(ligneWin);
      Agraphe := graphes[1];

      if pos('Bis',ligneWin)<>0 then begin
          Agraphe := graphes[2];
          nbreGraphe := DeuxGrVert;
      end;
      if pos('Ter',ligneWin)<>0 then begin
          Agraphe := graphes[3];
          nbreGraphe := DeuxGrHoriz;
      end;
      if pos('X',ligneWin)<>0 then choix := 1;
      if pos('Y',ligneWin)<>0 then choix := 2;
      if pos('MONDE',ligneWin)<>0 then choix := 3;
      if pos('GRADUATION',ligneWin)<>0 then choix := 4;
      if pos('OPTIONS',ligneWin)<>0 then choix := 5;
      if pos('ZERO',ligneWin)<>0 then choix := 6;
      if pos('MAXI',ligneWin)<>0 then choix := 7;
      if pos('MINI',ligneWin)<>0 then choix := 8;
      if pos('DESSIN',ligneWin)<>0 then choix := 9;
      if pos('MODELECALC',ligneWin)<>0 then choix := 10;
      if pos('TEMPSMINMAX',ligneWin)<>0 then choix := 11;
      if pos('CURSEUR',ligneWin)<>0 then choix := 12;
      if pos('TRACE',ligneWin)<>0 then choix := 13;
      if pos('COULEUR',ligneWin)<>0 then choix := 16;
      if pos('LIGNE',ligneWin)<>0 then choix := 17;
      if pos('MOTIF',ligneWin)<>0 then choix := 18;
      if pos('FENETRE',ligneWin)<>0 then choix := 21;
      if pos('TANGENTE',ligneWin)<>0 then choix := 22;
      if pos('INVERSE',ligneWin)<>0 then choix := 23;
      if pos('MAGNUM',ligneWin)<>0 then choix := 24;
      if pos('RetModele',ligneWin)<>0 then choix := 25;
      lectureOK := true;
      case choix of
           0 : for i := 1 to imax do readln(fichier);
           1 : with Agraphe do for i := 1 to imax do
                coordonnee[i].nomX := litLigneWin;
           2 : with Agraphe do for i := 1 to imax do
                coordonnee[i].nomY := litLigneWin;
           3 : with Agraphe do for i := 1 to imax do begin
                litLigneWin;
                if ligneWin[1]=symbReg2
                   then begin
                       lectureOK := false;
                       break;
                   end
                   else begin
                       zByte := StrToInt(ligneWin);
                       coordonnee[i].iMondeC := indiceMonde(zByte);
                   end
           end;
           4 : with Agraphe do for i := 1 to imax do begin
                litLigneWin;
                if ligneWin[1]=symbReg2
                   then begin
                       lectureOK := false;
                       break;
                   end
                   else begin
                       zByte := StrToInt(ligneWin);
                       monde[pred(i)].graduation := Tgraduation(zByte);
                   end;    
           end;
           5 : with Agraphe do begin
             litLigneWin; {1}
             optionGraphe := TsetOptionGraphe(byte(strToInt(ligneWin)));
             litLigneWin; {2}
             optionModele := TsetOptionModele(byte(strToInt(ligneWin)));
             litLigneWin; {3}
             try
             TraceDefaut := TtraceDefaut(strToInt(ligneWin));
             except
             TraceDefaut := tdPoint;
             end;
             litLigneWin; {4}
             OrdreLissage := strToInt(ligneWin);
             litLigneWin; {5}
             DimPointVGA := strToInt(ligneWin);
             if imax>=6 then useDefaut := litBooleanWin; {6}
             if imax>=7 then superposePage := litBooleanWin; {7}
             if imax>=8 then avecEllipse := litBooleanWin; {8}
             if imax>=9 then ProjeteVecteur := litBooleanWin;{9}
             if imax>=10 then begin
                litLigneWin;{10}
                zInt := strToInt(ligneWin);
             end;
             if imax>=11 then FilDeFer := litBooleanWin; {11}
             if imax>=12 then begin
                IdentAction.checked := litBooleanWin;{12}
                for i := 1 to 3 do graphes[i].identificationPages := IdentAction.checked;
             end;
             if imax>=13 then begin
                readln(fichier,echelleVecteur);{13}
                echelleVecteur := echelleVecteur/100;
             end;
             if imax>=14 then ProlongeVecteur := litBooleanWin; {14 }
             if imax>=15 then begin
                readln(fichier,zInt); {15 autrefois entier }
             end;
             if imax>=16 then readln(fichier,PasPoint); {16 }
             if imax>=17 then couleurVitesseImposee := litBooleanWin; {17 }
             if imax>=18 then ajusteOrigine := litBooleanWin; {18 }
             if imax>=19 then useSelect := litBooleanWin; {19 }
             if imax>=20 then readln(fichier,NbreVecteurVitesseMax); {20 }
             if imax>=21 then readln(fichier,NbreTexteMax); {21 }
             if imax>=22 then begin
                readln(fichier,zFloat);{22}
                PenWidthVGA := round(zFloat);
             end;
             if imax>=23 then readln(fichier,zeroPolaire);{23}
             for i := 24 to imax do readln(fichier);
           end;
           6 : with Agraphe do for i := 1 to imax do
                monde[pred(i)].zeroInclus := litBooleanWin;
           7 : with Agraphe do for i := 1 to imax do with monde[pred(i)] do begin
               readln(fichier,maxDefaut);
               MajDefaut := true;
           end;
           8 : with Agraphe do for i := 1 to imax do with monde[pred(i)] do begin
               readln(fichier,minDefaut);
               MajDefaut := true;
           end;
           9 : begin
               Adessin := Tdessin.create(agraphe);
               Adessin.load;
               Agraphe.dessins.add(Adessin);
           end;
           10 : for i := 1 to imax do modeleCalc[i] := litBooleanWin;
           11 : with Agraphe do begin
                readlnNombreWin(miniTemps);
                readlnNombreWin(maxiTemps);
           end;
           12 : with Agraphe do begin
               // curseur := curReticuleData;
               for i := curseurData1 to curseurData2 do with curseurOsc[i] do begin // 8*2=16
                  readln(fichier,xc);
                  readln(fichier,yc);
                  readln(fichier,ic);
                  readlnNombreWin(xr);
                  readlnNombreWin(yr);
                  readln(fichier,indexCourbe);
                  readln(fichier,mondeC);
                  zNom := litLigneWin;
                  zByte := indexNom(zNom);
                  if zByte=grandeurInconnue
                      then begin
                         grandeurCurseur := nil;
                         mondeC := mondeX;
                      end
                      else grandeurCurseur := grandeurs[zByte];
               end;
               readln(fichier,zByte); // 17
               agraphe.optionCurseur := TsetOptionCurseur(zByte);
           end;
           13 : with Agraphe do for i := 1 to imax do begin
                 readln(fichier,zWord);
                 coordonnee[i].Trace := TsetTrace(zWord);
                 exclude(coordonnee[i].trace,trTexte);
                 exclude(coordonnee[i].trace,trSonagramme);
           end;
           16 : with Agraphe do for i := 1 to imax do
               coordonnee[i].couleur := litColor(couleurInit[i]);
           17 : with Agraphe do for i := 1 to imax do begin
               readln(fichier,zByte);
               coordonnee[i].ligne := Tligne(zByte);
           end;
           18 : with Agraphe do for i := 1 to imax do begin
               readln(fichier,zByte);
               coordonnee[i].motif := Tmotif(zByte);
           end;
           21 : begin
              readln(fichier,zByte);
              windowState := TwindowState(zByte);
              readln(fichier,zint);
              top := zint;
              readln(fichier,zint);
              left := zint;
              readln(fichier,zint);
              width := zint;
              readln(fichier,zint);
              height := zint;
              position := poDesigned;

           end;
           22 : begin
               zWord := pos('TANGENTE',ligneWin)+length('TANGENTE');
               try
               zNom := copy(LigneWin,zWord,length(ligneWin)-zword+1);
               j := StrToInt(zNom);
               except
               j := 0;
               end;
               Aequivalence := Tequivalence.createVide(agraphe);
               Aequivalence.load;
               if (j>0) and (j<MaxPages) then
                  Agraphe.equivalences[j].Add(Aequivalence);
           end;
           23 : for i := 1 to imax do litBooleanWin;
           24 : begin
              isModeleMagnum := true;
              modeleGraphique[1].load;
              modeleGraphique[1].graphe := graphes[1];
           end;
           25 : with Agraphe.CurseurOsc[curseurData2] do begin
                  curseur := curReticuleModele;
                  readln(fichier,xc);
                  readln(fichier,yc);
                  readln(fichier,ic);
                  readlnNombreWin(xr);
                  readlnNombreWin(yr);
                  readln(fichier,indexCourbe);
                  readln(fichier,mondeC);
                  zNom := litLigneWin;
                  zByte := indexNom(zNom);
                  if zByte=grandeurInconnue
                      then begin
                         grandeurCurseur := nil;
                         mondeC := mondeX;
                      end
                      else grandeurCurseur := grandeurs[zByte];
               readln(fichier,zByte); {9}
               agraphe.optionCurseur := TsetOptionCurseur(zByte);
           end;
       end; // case
       if lectureOK then litLigneWin;
    end;
    for j := 1 to 3 do verifCoord(j);
    if majDefaut then for i := mondeX to high(indiceMonde) do
       with graphes[1].monde[i] do
            setMinMaxDefaut(minDefaut,maxDefaut); // prise en compte des coord log
end; // litConfig

Procedure TFgrapheVariab.litConfigXML(ANode : TDOMNode);
var CoordCourante : integer;

procedure VerifCoord;
var i : integer;
begin with graphes[1] do begin
    for i := mondeX to mondeSans do monde[i].axe := nil;
    for i := 1 to maxOrdonnee do with coordonnee[i] do begin
        codeY := indexNom(nomY);
        if codeY<>grandeurInconnue
           then monde[iMondeC].axe := grandeurs[codeY];
        codeX := indexNom(nomX);
        if codeX<>grandeurInconnue then
           monde[mondeX].axe := grandeurs[codeX];
        monde[iMondeC].defini := false;
        exclude(trace,trSonagramme);
    end;
    if not grapheCourant.useDefautX then
       monde[MondeX].defini := false; // ??
end end;

procedure LoadXMLInReg(ANode: TDOMNode);

procedure Suite;
var Node:TDOMNode;
begin
    Node := ANode.FirstChild;
    while Node <> Nil do begin
      LoadXMLInReg(Node);
      Node := Node.NextSibling;
    end;
end;

var Agraphe : TgrapheReg;
    Adessin : Tdessin;
    zWord : word;
    zByte : byte;
    aTrace : TsetTraceXML;
begin // loadXMLInReg
      Agraphe := graphes[1];
      if ANode.NodeName='COORD' then begin
         coordCourante := getIndexXML(ANode);
         suite;
         exit;
      end;
      if ANode.NodeName='X' then begin
         Agraphe.coordonnee[coordCourante].nomX := ANode.NodeValue;
         exit;
      end;
      if ANode.NodeName='Y' then  begin
         Agraphe.coordonnee[coordCourante].nomY := ANode.NodeValue;
         exit;
      end;
      if ANode.NodeName='MONDE' then begin
        zByte := GetIntegerXML(ANode);
        AGraphe.coordonnee[coordCourante].iMondeC := indiceMonde(zByte);
        exit;
      end;
      if ANode.NodeName='GRADUATION' then begin
         zByte := GetIntegerXML(ANode);
         AGraphe.monde[coordCourante].graduation := Tgraduation(zByte);
         exit;
      end;
      if ANode.NodeName='OPTIONS' then begin
         suite;
         exit;
      end;
      if ANode.NodeName='ZERO' then begin
         AGraphe.monde[coordCourante].zeroInclus := GetBoolXML(ANode);
         exit;
      end;
      if ANode.NodeName='MAXI' then begin
         AGraphe.maxiTemps := GetFloatXML(ANode);
         exit;
      end;;
      if ANode.NodeName='OptGraphe' then begin
         zByte := GetIntegerXML(ANode);
         AGraphe.optionGraphe := TsetOptionGraphe(zByte);
         exit;
      end;
      if ANode.NodeName='OptModele' then begin
         zByte := GetIntegerXML(ANode);
         AGraphe.optionModele := TsetOptionModele(zByte);
         exit;
      end;
      if ANode.NodeName='MINI' then begin
         AGraphe.miniTemps := GetFloatXML(ANode);
         exit;
      end;
      if ANode.NodeName='DESSIN' then begin
          Adessin := Tdessin.create(agraphe);
         // Adessin.loadXML(ANode);
          Agraphe.dessins.add(Adessin);
          exit;
      end;
//      if ANode.NodeName='MODELECALC' then begin
// modeleCourant := GetIndexXML(ANode);
// modeleCalc[modeleCourant] := getBool;
//  end;
//      if ANode.NodeName='TEMPSMINMAX' then ;
//      if ANode.NodeName='CURSEUR' then ;
      if ANode.NodeName='TRACE' then begin
         zByte := GetIntegerXML(ANode);
         aTrace := TsetTraceXML(zByte);
         Agraphe.coordonnee[coordCourante].Trace := traceFromXML(aTrace);
         exclude(Agraphe.coordonnee[coordCourante].trace,trTexte);
         exclude(Agraphe.coordonnee[coordCourante].trace,trSonagramme);
         exit;
      end;
      if ANode.NodeName='COULEUR' then begin
         zWord := GetIntegerXML(ANode);
         Agraphe.coordonnee[coordCourante].couleur := TColor(zWord);
         exit;
      end;
      if ANode.NodeName='CouleurVitesseImposee' then begin
         CouleurVitesseImposee := GetBoolXML(ANode);
         exit;
      end;
      if ANode.NodeName='ReperePage' then begin
         reperePage := TreperePage(GetIntegerXML(ANode));
         exit;
      end;
      if ANode.NodeName='SuperPage' then begin
         AGraphe.superposePage := GetBoolXML(ANode);
         exit;
      end;
      if ANode.NodeName='ProjeteVect' then begin
         ProjeteVecteur := GetBoolXML(ANode);
         exit;
      end;
      if ANode.NodeName='FilDeFer' then begin
         AGraphe.FilDeFer := GetBoolXML(ANode);
         exit;
      end;
      if ANode.NodeName='Ellipse' then begin
         AvecEllipse := GetBoolXMl(ANode);
         exit;
      end;
      if ANode.NodeName='ProlongeVect' then begin
         ProlongeVecteur := GetBoolXML(ANode);
         exit;
      end;
      if ANode.NodeName='LIGNE' then begin
         zByte := GetIntegerXML(ANode);
         AGraphe.coordonnee[coordCourante].ligne := Tligne(zByte);
         exit;
      end;
      if ANode.NodeName='MOTIF' then begin
         zByte := GetIntegerXML(ANode);
         Agraphe.coordonnee[coordCourante].motif := Tmotif(zByte);
         exit;
      end;
      if ANode.NodeName='TANGENTE' then begin

      end;
      with Agraphe do begin
             OrdreLissage := GetIntegerXML(ANode);
             DimPointVGA := GetIntegerXMl(ANode);
             echelleVecteur := GetIntegerXML(ANode);
             echelleVecteur := echelleVecteur/100;
             PasPoint := getIntegerXMl(ANode);
             ajusteOrigine := getBoolXML(ANode);
             useSelect := getBoolXML(ANode);
             NbreVecteurVitesseMax := getIntegerXML(ANode);
             NbreTexteMax := getIntegerXML(ANode);
      end;
end; // LoadXMLInReg

var i,j: Integer;
    Node: TDOMNode;
begin
   UnGrapheItem.checked := true;
   majFichierEnCours := true;
   Curseur := curSelect;
   for i := mondeX to mondeSans do
       for j := 1 to 3 do
           graphes[j].monde[i].axe := nil;
   for i := 1 to MaxPages do ModeleCalc[i] := false;
   nbreGraphe := UnGr;
   penWidthVGA := 1;
   configCharge := true;
   Node := ANode.FirstChild;
   while Node <> Nil do begin
      LoadXMLInReg(Node);
      Node := ANode.NextSibling;
   end;
   verifCoord;
end; // litConfigXML

procedure TFgrapheVariab.LineaireItemClick(Sender: TObject);
begin
  setModeleGr(mgLineaire)
end;

Procedure TFgrapheVariab.ValideParam(paramCourant : integer;maj : boolean);
var valeurLoc : double;
begin
     if ValeursParamChange then begin
        try
          valeurLoc := GetFloat(editValeur[paramCourant].text);
          Pages[pageCourante].valeurParam[paramNormal,paramCourant] := valeurLoc;
          if maj then PostMessage(handle,WM_Reg_Calcul,CalculModele,0);
          ValeursParamChange := false;
        except
          EditValeur[paramCourant].SetFocus
        end;
     end
end;

procedure TFgrapheVariab.ValeursParamKeyPress(Sender: TObject; var Key: Char);
begin
     VerifKeyGetFloat(key);
     if key<>#0 then begin
           ValeursParamChange := true;
           TraceAutoCB.checked := false;
     end;
end;

procedure TFgrapheVariab.FormActivate(Sender: TObject);
begin
     paintBox1.invalidate;
end;

procedure TFgrapheVariab.ZoomManuelItemClick(Sender: TObject);
var reponse : integer;
begin
     if not grapheCourant.grapheOK then exit;
     if ZoomManuelDlg=nil then
        Application.CreateForm(TzoomManuelDlg, ZoomManuelDlg);
     zoomManuelDlg.Echelle := grapheCourant;
     reponse := ZoomManuelDlg.showModal;
     if reponse in [mrOK,mrNo] then begin
        grapheCourant.modif := [gmEchelle];
        grapheCourant.monde[mondeX].defini := reponse=mrOK;
        modifGraphe(indexGrCourant);
     end;
end;

Procedure TFgrapheVariab.SetUniteParametre;
begin
    if chercheUniteParam then chercheUniteParametre;
    include(etatModele,UniteParamDefinie);
end;

procedure TFgrapheVariab.Compile;
var memoActif : boolean;
    i,posFin : integer;
begin
     memoActif := memoModele=activeControl;
     LanceCompile;
     if ModeleDefini in etatModele
        then begin if OptionsDlg.AjustageAutoLinCB.checked and
                    (ModeleLineaire in etatModele)
          then lanceModele(true)
          else begin
             Application.MainForm.Perform(WM_Reg_Maj,MajModele,0);
             if memoActif then begin
                posFin := 0;
                for i := 0 to pred(MemoModele.Lines.count) do
                   posFin := posFin+Length(MemoModele.Lines[i])+2;
                memoModele.selStart := posFin;
                memoModele.selLength := 0;
                MemoModele.setFocus;
             end
             else if (withModele) then begin
                    setParamEdit;
                    AjustePanelModele;
             end;
          end;
      end
      else if PasDeModele in etatModele then
           Application.MainForm.Perform(WM_Reg_Maj,MajModele,0);
end;

procedure TFgrapheVariab.WMRegModele(var Msg : TWMRegMessage);
begin
     if (withModele) and
        VisualisationAjustement then begin
        MajParametres;
        MajResultat;
        include(graphes[1].modif,gmModele);
        include(Graphes[1].modif,gmValeurModele);
        PaintBox1.invalidate;
        if paintBox3.visible and residusItem.checked then
           PaintBox3.invalidate;
        if paintBox2.visible and residusItem.checked then
           PaintBox2.invalidate;
    end;
end;

procedure TFgrapheVariab.WMRegCalcul(var Msg : TWMRegMessage);
begin
     case msg.typeMaj of
          calculCompile : compile;
          calculModele : lanceModele(false);
          calculAjuste : lanceModele(true);
     end;
end;

procedure TFgrapheVariab.CopierModeleItemClick(Sender: TObject);
var page : codePage;
begin
     formDDE.RazRTF;
     formDDE.AjouteMemo(memoModele);
     for page := 1 to NbrePages do with pages[page] do begin
         formDDE.Editor.lines.Add(commentaireP);
         formDDE.ajouteStringList(TexteResultatModele);
     end;
     formDDE.envoieRTF;
end;

Procedure TFgrapheVariab.MajIdentPage(indexGr : integer);
begin
    if ChoixIdentPagesDlg=nil then
         Application.CreateForm(TChoixIdentPagesDlg, ChoixIdentPagesDlg);
    ChoixIdentPagesDlg.InitParam;
end; // MajIdentPage

procedure TFgrapheVariab.InitEquaDiffBtnClick(Sender: TObject);
const Maxi = 2;
var pente : double;
    k,j : integer;
    i,iDebut,iFin : integer;
begin with pages[pageCourante] do begin
     if FonctionTheorique[1].genreC in [g_diff1,g_diff2] then begin
          for k := 1 to NbreModele do with FonctionTheorique[k] do begin
               if calcul^.varx<>nil
                  then j := calcul^.varx.indexG
                  else j := 0;
               if (j>parametre0) then begin // origine est bien un paramètre
                     j := IndexToParam(paramNormal,j);
                     if j<>grandeurInconnue then
                        ValeurParam[paramNormal,j] := valeurVar[indexY,debut[k]]; // inverse ?
               end;
          end; // origine
          if FonctionTheorique[1].genreC=g_diff2 then
             for k := 1 to NbreModele do with FonctionTheorique[k] do begin
                 if calcul^.vary<>nil
                    then j := calcul^.vary.indexG
                    else j := 0;
                 if (j>parametre0) and (j<=parametre0+maxParametres) then begin
                 // pente est bien un paramètre
                       j := IndexToParam(paramNormal,j);
                       Pente := 0;
                       i := 0;
                       iDebut := debut[k];
                       iFin := debut[k]+Maxi;
                       while (i<Maxi) and (iDebut>0) do begin dec(iDebut);inc(i) end;
                         { on essaye de centrer }
                       for i := iDebut to iFin do
                           Pente := Pente + valeurVar[indexYp,i];
                       Pente := Pente/succ(iFin-iDebut);
                       ValeurParam[paramNormal,j] := pente;
                       EditValeur[j].text := formatGeneral(valeurParam[paramNormal,j],3);
                  end;
          end; // pente initiale
     end;// diff
end end;

procedure TFgrapheVariab.IntersectionItemClick(Sender: TObject);
var i,j : integer;
begin
  if (sender as TmenuItem).Checked then // suppression
  for i := 2 to MaxIntervalles do begin
      j := 0;
      while j<graphes[1].equivalences[pageCourante].count do begin
      with graphes[1].equivalences[pageCourante].items[j] do
           if (ligneRappel=lrReticule) and (indexModele=i)
              then graphes[1].equivalences[pageCourante].Delete(j)
              else inc(j);
      end;
  end;
  IntersectionItem.Checked := not (sender as TmenuItem).Checked;
  IntersectionTer.Checked := IntersectionItem.Checked;
  graphes[1].paintBox.invalidate;
end;

procedure TFgrapheVariab.MemoResultatKeyPress(Sender: TObject; var Key: Char);
begin
    key := #0
end;

procedure TFgrapheVariab.MenuAxesPopup(Sender: TObject);
var P : Tpoint;
begin
     TableauXYItem.visible := graphes[1].equivalences[pageCourante].count>0;
     if  TableauXYItem.visible and
        (graphes[1].equivalences[pageCourante][0].ligneRappel in [lrXdeY,lrReticule])
           then begin
                TableauXYItem.caption := stGridValeurs;
                ViderXYItem.caption := stRazValeurs;
           end
           else begin
                TableauXYItem.caption := stGridTangente;
               ViderXYItem.caption := stRazTangente;
           end;
     GetCursorPos(P);
     P := grapheCourant.PaintBox.ScreenToClient(P);
     iPropCourbe := grapheCourant.CourbeProche(P.x,P.y);
     if iPropCourbe<0 then iPropCourbe := 0;
     if ((grapheCourant.courbes[iPropCourbe].indexModele=0) or
          grapheCourant.courbes[iPropCourbe].courbeExp) and
         (grapheCourant.courbes[iPropCourbe].page=pageCourante) then begin
        ProprieteCourbe.visible := true;
        with grapheCourant.courbes[iPropCourbe] do
             ProprieteCourbe.caption := stCaracDe+' '+varY.nom+'=f('+varX.nom+')'; // ' Y=f(X)'
     end;
     ViderXYItem.visible := TableauXYItem.visible;
     AffIncertBis.checked := avecEllipse;
     identAction.enabled := (grapheCourant.superposePage and (NbrePages>1)) or
                            (grapheCourant.NbreOrdonnee>1);
     identAction.visible := identAction.enabled;
     SavePosItem.visible := curseur in [curReticule,curEquivalence,curReticuleData,curReticuleModele];
end;

procedure TFgrapheVariab.TableauXYItemClick(Sender: TObject);
begin
    graphes[1].RemplitTableauEquivalence;
    if curseurModeleDlg=nil then
       curseurModeleDlg := TcurseurModeleDlg.create(self);
    if ligneRappelCourante=lrXdeY
      then CurseurModeleDlg.show
      else CurseurModeleDlg.showModal;
    PaintBox1.invalidate;
end;

procedure TFgrapheVariab.ModelGrClick(Sender: TObject);
var p : codePage;
    m : indiceMonde;
begin
    if NbreModele=maxIntervalles then NbreModele := maxIntervalles-1;
    with grapheCourant,fonctionTheorique[succ(NbreModele)] do begin
       if monde[mondeX].axe<>nil then
           indexX := indexNom(monde[mondeX].axe.nom);
       for m := mondeY to mondeSans do
           if monde[m].axe<>nil then begin
              indexY := indexNom(monde[m].axe.nom);
              break;
           end;
    end;
    ChoixModeleDlg.appelMagnum := true;
    ChoixModeleDlg.coordonnee := grapheCourant.coordonnee;
    ChoixModeleDlg.PageControl.ActivePage := ChoixModeleDlg.ModMagnum;
    if ChoixModeleDlg.showModal=mrOk then begin
       for p := 1 to NbrePages do
                pages[p].resetDebutFin(NbreModele);
       if ChoixModeleDlg.modeleChoisi=mgManuel
           then begin
              IsModeleMagnum := false;
              if ChoixModeleDlg.effaceModele then MemoModele.clear;
              with FonctionTheorique[NbreModele] do
                if enTete<>'' then begin
                      MemoModele.lines.Add(enTete+'='+expression);
                      etatModele := [];
                      lanceModele(genreC=g_fonction);
                      TPaintBox(sender).invalidate;
                      exit;
                end
           end
           else ModeleMagnum(ChoixModeleDlg.CoordRG.itemIndex+1,true);
    end;
end;

procedure TFgrapheVariab.ModeleMagnum(indiceCoord : integer;direct : boolean);
var j : integer;
begin with ModeleGraphique[NbreModele] do begin
     init(ChoixModeleDlg.ModeleChoisi,graphes[1],NbreModele,indiceCoord);
     initialiseParametre(
        Pages[pageCourante].debut[NbreModele],
        Pages[pageCourante].fin[NbreModele],
        direct);
     if not ModeleOK then begin
        afficheErreur(erModeleGr,0);
        NbreModele := 0;
        exit;
     end;
     MajModelePermis := false;
     if ChoixModeleDlg.effaceModele then MemoModele.clear;
     for j := 0 to pred(expression.count) do
         MemoModele.lines.Add(expression[j]);
     MajModelePermis := true;
     MemoModeleChange(nil);
     etatModele := [];
     IsModeleMagnum := true;
     Resize;
     LanceCompile;
     for j := 1 to 3 do if graphes[j].paintBox.Visible then
         include(Graphes[j].modif,gmModele);
     if not avecModeleManuel
        then setPenBrush(curModeleGr)
        else setPenBrush(curSelect);
     if direct then pages[pageCourante].resetDebutFin(NbreModele);
     MajModeleGr(NbreModele);
     LanceModele(OptionsDlg.AjustageAutoGrCB.checked);
end end; // modeleMagnum

procedure TFgrapheVariab.MajModeleGr(NumeroGr : integer);
var sauveParam : tableauParam;
begin with pages[pageCourante] do begin
     sauveParam := valeurParam[paramNormal];
     ModeleGraphique[NumeroGr].GetParametres(valeurParam[paramNormal]);
     if ModeleGraphique[NumeroGr].ModeleOK
        then begin
            EffectueModele(false,false);
            MajParametres;
            MajResultat;
        end
        else valeurParam[paramNormal] := sauveParam;
     include(etatModele,ajustementGraphique);
     include(Graphes[1].modif,gmEchelle);
     include(Graphes[1].Modif,gmValeurModele); {?}
     PaintBox1.invalidate;
end end;


procedure TFgrapheVariab.ViderXYItemClick(Sender: TObject);
var i : integer;
begin
     for i := 1 to 3 do with Graphes[i] do
     if paintBox.Visible then begin
         resetEquivalence;
         paintBox.invalidate;
     end;
end;

procedure TFgrapheVariab.SetPointCourant(i,c : integer);

Procedure AffichePointCourant;
var xi,yi,dimP : Integer;
begin if indexPointCourant>=0 then
with PaintBox1.canvas,graphes[1].courbes[courbePointCourant] do begin
        Pen.color := color;
        Pen.mode := pmNotXor;
        Pen.style := psSolid;
        Brush.color := pen.color;
        Brush.style := bsSolid;
        graphes[1].WindowRT(valX[indexPointCourant],valY[indexPointCourant],iMondeC,Xi,Yi);
        DimP := graphes[1].dimPoint+2;
        Ellipse(xi-dimP,yi-dimP,xi+dimP+1,yi+dimP+1);
        Brush.style := bsClear;
        canvas.pen.mode := pmCopy;
end end;

begin
     AffichePointCourant; // efface
     if curseur in [curSelect,curReticule,curReticuleData,curEquivalence,curMove]
        then indexPointCourant := i
        else indexPointCourant := -1;
     courbePointCourant := c;
     if indexPointCourant>=0 then
        Application.MainForm.perform(WM_Reg_Maj,MajNumeroMesure,i);
     AffichePointCourant; // retrace
end;

procedure TFgrapheVariab.TraceCurseurCourant(indexGr : integer);
var i : integer;
begin with graphes[indexGr] do
     case curseur of
          curReticule : begin
            for i := 1 to cCourant do traceReticule(i);
            if cCourant>1 then traceEcart;
          end;
          curReticuleData : if curseurGrid.visible then setStatusReticuleData(curseurGrid);
          curReticuleModele : ;
          curEquivalence : if equivalenceCourante<>nil then
               equivalenceCourante.drawFugitif;
          curOrigine : with canvas do begin
               pen.Width := 3;
               pen.Style := psDash;
               pen.Color := clRed;
               moveTo(XorigineInt,0);
               lineTo(XorigineInt,height);
          end;
    end;
end;

procedure TFgrapheVariab.TrigoBtnClick(Sender: TObject);
var modif : boolean;
begin
     if angleEnDegre
        then Modif := OKreg(OkAngleRadian,0)
        else Modif := OKreg(OkAngleDegre,0);
     if modif then Fvaleurs.ChangeAngle(not AngleEnDegre);
end;

procedure TFgrapheVariab.ToolButton1Click(Sender: TObject);
var indexV,j,k,numO : integer;
    couleursUtilisees : set of indiceOrdonnee;
begin with grapheCourant do begin
     indexV := (sender as TtoolButton).tag;
     numO := 0;
     couleursUtilisees := [];
     for j := 1 to NbreOrdonnee do // test existe déjà  ?
         if indexV=coordonnee[j].codeY then numO := j;
     if numO>0 then begin // supprime
        coordonnee[numO].nomX := '';
        coordonnee[numO].nomY := '';
     end
     else begin // ajoute
         if NbreOrdonnee<MaxOrdonnee
           then begin
              for j := 1 to NbreOrdonnee do
                  for k := 1 to MaxOrdonnee do
                      if coordonnee[j].couleur=couleurInit[k] then
                         include(couleursUtilisees,k);
              inc(NbreOrdonnee);
              numO := NbreOrdonnee;
           end
           else begin
              numO := 1;
              for j := 2 to MaxOrdonnee do
                  for k := 1 to MaxOrdonnee do
                      if coordonnee[j].couleur=couleurInit[k] then
                         include(couleursUtilisees,k);
           end;
         coordonnee[numO].codeY := indexV;
         coordonnee[numO].nomY := grandeurs[indexV].nom;
         coordonnee[numO].codeX := coordonnee[1].codeX;
         coordonnee[numO].nomX := coordonnee[1].nomX;
         if grandeurBoutonDroit
              then coordonnee[numO].iMondeC := mondeDroit
              else coordonnee[numO].iMondeC := mondeY;
         if numO>1 then begin
             coordonnee[numO].trace := coordonnee[1].trace;
             coordonnee[numO].ligne := coordonnee[1].ligne;
         end;
         for j := 1 to MaxOrdonnee do
             if not(j in couleursUtilisees) then begin
                 coordonnee[numO].couleur := couleurInit[j];
                 coordonnee[numO].motif := motifInit[j];
                 break;
             end;
     end;
  include(modif,gmXY);
  ModifGraphe(indexGrCourant);
  GrandeurBoutonDroit := false;
end end;

procedure TFgrapheVariab.ToolButton1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    GrandeurBoutonDroit := (button=mbRight);
end;

procedure TFgrapheVariab.TraceAutoBtnClick(Sender: TObject);
begin
     if TraceAutoCB.checked
        then exclude(Graphes[1].OptionModele,OmManuel)
        else include(Graphes[1].OptionModele,OmManuel)
end;

procedure TFgrapheVariab.SetBoutonsModele;
begin
     SaveParamItem.enabled := pages[pageCourante].ModeleCalcule;
     TitreModeleItem.enabled := pages[pageCourante].ModeleCalcule;
end;

procedure TFgrapheVariab.MemoModeleClick(Sender: TObject);
begin
     if curseur=curModeleGr then setPenBrush(curSelect)
end;

procedure TFgrapheVariab.MajBtnClick(Sender: TObject);
begin
     PostMessage(handle,WM_Reg_Calcul,CalculCompile,0)
end;

procedure TFgrapheVariab.ImprimeGrItemClick(Sender: TObject);
var i,bas : integer;
    hg : double;
begin
     if not graphes[1].grapheOK then exit;
     try
     if PaintBox2.visible
        then begin if OKreg(OkImprGr,0) then begin
              debutImpressionGr(poPortrait,bas);
              for i := 1 to 3 do
                  if graphes[i].paintBox.visible then
                     graphes[i].versImprimante(HautGrapheGr,bas);
              finImpressionGr;
        end end
        else begin
              debutImpressionGr(printer.orientation,bas);
              if printer.orientation=poLandscape
                 then HG := HautGraphePaysage
                 else HG := HautGrapheGr;
              graphes[1].versImprimante(HG,bas);
              finImpressionGr;
        end;
        except
             on E: Exception do begin
                 showMessage(E.message);
                 try
                 printer.abort;
                 except
                 end;
             end;
        end;
end;

Procedure TFgrapheVariab.WmRegOrigine(var Msg : TWMRegMessage);
var j,indexX : integer;
    nom : string;
    indexDelta,indexNewTime : integer;
    gdelta,gNewTime : tgrandeur;
    GrandeursModifiees : boolean;
    posErreur,longErreur : integer;
begin
     indexX := grapheCourant.coordonnee[1].codeX;
     if OrigineDlg=nil then
        Application.CreateForm(TOrigineDlg, OrigineDlg);
     OrigineDlg.index := indexX;
     GrandeursModifiees := false;
     case OrigineDlg.showModal of
        mrYes : with pages[pageCourante] do
            for j := 0 to pred(nmes) do
                valeurVar[indexX,j] := valeurVar[indexX,j]-xOrigine;
        mrNo : begin
            nom := deltaMaj+grandeurs[indexX].nom;
            indexDelta := indexNom(nom);
            if indexDelta=grandeurInconnue then begin
               gDelta := Tgrandeur.create;
               gdelta.Init(nom,grandeurs[indexX].NomUnite,'',constante);
               indexDelta := ajouteGrandeurE(gDelta);
               GrandeursModifiees := true;
               ModifFichier := true;
            end;
            pages[pageCourante].valeurConst[indexDelta] := xOrigine;
            nom := grandeurs[indexX].nom+'0';
            indexNewTime := indexNom(nom);
            if indexNewTime=grandeurInconnue then begin
               gNewTime := Tgrandeur.create;
               gNewTime.Init(nom,grandeurs[indexX].NomUnite,OrigineDlg.ligneCompile,variable);
               gNewTime.fonct.depend := [indexX,indexDelta];
               gNewTime.compileG(posErreur,longErreur,0);
               indexNewTime := ajouteGrandeurE(gNewTime);
               grandeurs[indexNewTime].fonct.genreC := g_fonction;
               ModifFichier := true;
               Fvaleurs.Memo.Lines.Add(OrigineDlg.ligneCompile);
               GrandeursModifiees := true;
            end;
            with pages[pageCourante] do
                 for j := 0 to pred(nmes) do
                     valeurVar[indexNewTime,j] := valeurVar[indexX,j]-xOrigine;
        end;
        mrCancel : exit;
      end;
      if grandeursModifiees then
         Application.MainForm.perform(WM_Reg_Maj,MajGrandeur,0);
      Application.MainForm.Perform(WM_Reg_Maj,MajValeurGr,0);
end; // WMRegOrigine

procedure TFgrapheVariab.MemoModeleKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (key=crCR) and (FregressiMain.KeypadReturn)
       then begin
           key := #0;
           MajBtnClick(Sender);
       end;
     etatModele := []; // ??
end;

procedure TFgrapheVariab.BornesClick(Sender: TObject);
begin
     BorneActive := (sender as Tcomponent).tag;
     AffecteStatusPanel(HeaderXY,0,(sender as TmenuItem).hint);
     setPenBrush(curBornes);
     activeControl := panelPrinc;
end;

procedure TFgrapheVariab.BornesMenuPopup(Sender: TObject);
var i : integer;
begin
     if not (ModeleDefini in etatModele) then LanceCompile;
     pages[pageCourante].VerifIntervalles;
     for i := 1 to NbreModele do
         with BornesMenu.items[pred(i)] do begin
            caption := stBorne+fonctionTheorique[i].enTete+'='+copy(fonctionTheorique[i].expression,1,10);
            hint := stBorneSelect;
            visible := true;
         end;
     if NbreModele<MaxIntervalles then
         with BornesMenu.items[NbreModele] do begin
            hint := hBornes;
            caption := trBornesModele;
            visible := true;
         end;
     for i := NbreModele+2 to MaxIntervalles do
         BornesMenu.items[pred(i)].visible := false;
end;

procedure TFgrapheVariab.RazBornesClick(Sender: TObject);
var i : integer;
begin with pages[pageCourante] do begin
         ModeleCalcule := false;
         ModeleErrone := false;         
         for i := 1 to maxIntervalles do resetDebutFin(i);
         include(graphes[1].modif,gmModele);
         graphes[1].borneVisible := true;
         for i := 0 to pred(MaxVecteurDefaut) do FPointActif[i] := true;
         PaintBox1.invalidate;
end end;

procedure TFgrapheVariab.ResidusItemClick(Sender: TObject);
begin
     ResidusItem.checked := not ResidusItem.checked;
     ResidusItemBis.checked := ResidusItem.checked;
     if ResidusItem.checked then begin
        Application.MainForm.Perform(WM_Reg_Maj,MajModele,0);
        PaintBox3.height := panelPrinc.height div 3;
     end
     else if UnGrapheItem.checked then begin
          paintBox3.Visible := false;
          panelBis.visible := false;
          paintBox2.Visible := false;
     end;
     indexGrCourant := 1;
     grapheCourant := Graphes[1];
     invalidate;
end;

procedure TFgrapheVariab.ResidusStudentCBClick(Sender: TObject);
begin
  setCoordonneeResidu;
end;

procedure TFgrapheVariab.ImprModeleBtnClick(Sender: TObject);
var bas : integer;

procedure ImprimeModele;
var i : integer;
    page : codePage;

Procedure ImprimePage(p : codePage);
var i : integer;
begin with pages[p] do begin
      ImprimerLigne(TitrePage,bas);
      for i := 0 to pred(TexteResultatModele.Count) do
          ImprimerLigne(TexteResultatModele[i],bas);
end end;

begin
         DebutImpressionTexte(bas);
         ImprimerLigne(stModelisation,bas);
         for i := 0 to pred(TexteModele.count) do
             if TexteModele[i]<>'' then ImprimerLigne(TexteModele[i],bas);
         if graphes[1].superposePage
            then begin
               for page := 1 to NbrePages do if pages[page].active
                   then ImprimePage(page)
            end
            else imprimePage(pageCourante);
end;

begin
     if OKreg(OkImprModele,0) then begin
           try
           debutImpressionGr(poPortrait,bas);
           bas := 0;
           graphes[1].versImprimante(HautGrapheGr,bas);
           ImprimeModele;
           finImpressionGr;
           except
           end;
     end;
end;

Procedure TFgrapheVariab.ResetHeaderXY;
var i : integer;
begin
     for i := 0 to pred(HeaderXY.panels.count) do VideStatusPanel(HeaderXY,i)
end;

procedure TFgrapheVariab.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var P : Tpoint;
    paintBoxC : TpaintBox;
    i : integer;
    xi,yi : Integer;

Function Supprime(posSouris : Tpoint) : boolean;
var i : integer;
    aEquivalence : Tequivalence;
    xi,yi : integer;
    trouve,trouveI : boolean;
begin with grapheCourant do begin
     trouve := false;
     i := 0;
     while i<equivalences[pageCourante].count do begin
            aEquivalence := equivalences[pageCourante].items[i];
            with aEquivalence do begin
               WindowRT(x1,y1,mondeY,xi,yi);
               trouveI := (abs(posSouris.x-xi)+abs(posSouris.y-yi))<12;
               if not trouveI then begin
                  WindowRT(x2,y2,mondeY,xi,yi);
                  trouveI := (abs(posSouris.x-xi)+abs(posSouris.y-yi))<12;
               end;
               if not trouveI then begin
                  WindowRT(ve,pHe,mondeY,xi,yi);
                  trouveI := (abs(posSouris.x-xi)+abs(posSouris.y-yi))<12;
               end;
                // supprime
               if trouveI then begin
                  equivalences[pageCourante].Remove(aEquivalence);
                  trouve := true;
               end
               else inc(i);
            end;// with newEq
      end; // while
      if trouve then PaintBox.invalidate;
      result := trouve;
end end;

Function MajI  : boolean;
begin with pages[pageCourante] do begin
      result := true;
      case key of
           vk_left,vk_down : dec(i);
           vk_return : ;
           vk_right,vk_up : inc(i);
           vk_prior : dec(i,10);
           vk_home : i := 0;
           vk_end : i := pred(nmes);
           vk_next : inc(i,10);
           else result := false;
       end;
       if i<0 then i := 0;
       if i>=nmes then i := pred(nmes);
       if result then key := 0;
end end;

Function MajImodele  : boolean;
var xc,yc : integer;
begin with grapheCourant do begin
      result := true;
      xc := curseurOsc[1].xc;
      yc := curseurOsc[1].yc;
      case key of
           vk_left : if ssShift in Shift
              then setReticuleModele(Xc-8,Yc,true,false)
              else setReticuleModele(Xc-2,Yc,true,false);
           vk_down : if ssShift in Shift
              then setReticuleModele(Xc,Yc+8,false,true)
              else setReticuleModele(Xc,Yc+2,false,true);
           vk_return : ;
           vk_right : if ssShift in Shift
              then setReticuleModele(Xc+8,Yc,true,false)
              else setReticuleModele(Xc+2,Yc,true,false);
           vk_up : if ssShift in Shift
              then setReticuleModele(Xc,Yc-8,false,true)
              else setReticuleModele(Xc,Yc-2,false,true);
           vk_prior : setReticuleModele(Xc-8,Yc,true,false);
           vk_home : ;
           vk_end : ;
           vk_next : setReticuleModele(Xc+8,Yc,true,false);
           else result := false;
       end;
       if result then key := 0;
end end;

begin // FormKeyDown
     if ssAlt in Shift then begin
        case key of
             ord('C') : CoordonneesItemClick(nil);
             ord('I') : ImprimeGrItemClick(nil);
        end;
        exit;
     end;
     paintBoxC := grapheCourant.PaintBox;
     P := mouse.CursorPos;
     P := PaintBoxC.ScreenToClient(P);
     if (P.X<0) or (P.X>paintBoxC.width) or
        (P.Y<0) or (P.Y>paintBoxC.height) then exit;
     with grapheCourant,pages[pageCourante] do case curseur of
       curXmaxi,curYMini,curXMini,curYmaxi : exit;
       curReticuleData : begin
            if key=vk_escape then begin
               setPenBrush(CurSelect);
               exit;
            end;
            GetCurseurProche(P.x,P.y,true);
            i := curseurOsc[cCourant].Ic;
            if MajI then begin
                 setPointCourant(i,0);
                 setSegmentReal(i);
                 if curseurGrid.visible then setStatusReticuleData(curseurGrid);
                 exit;
            end;
        end;
        curTexte,curLigne : if key=vk_escape then begin
           grapheCourant.dessinCourant.free;
           grapheCourant.dessinCourant := nil;
           exit;
        end;
        curReticuleModele : begin
           if key=vk_escape then begin
              setPenBrush(CurSelect);
              exit;
           end;
           if MajImodele then exit;
        end;
        curEfface,curReticule : if key=vk_escape then begin
           setPenBrush(CurSelect);
           exit;
        end;
        curEquivalence : if equivalenceCourante<>nil then with equivalenceCourante do begin
           windowRT(Ve,pHe,mondeY,xi,yi);
           P := Point(xi,yi);
           i := PointProche(P.x,P.y,indexCourbeEquivalence,true,ligneRappel=lrTangente);
           if (key=vk_return) and
              (i>=0) then begin
              AjouteEquivalence(i,true);
              setPointCourant(i,indexCourbeEquivalence);
              ResetHeaderXY;
              if curseurGrid.visible then begin
                 for i := 0 to pred(statusSegment[1].count) do
                    CurseurGrid.cells[1,i] := statusSegment[1].strings[i];
                 for i := 0 to pred(statusSegment[0].count) do
                    CurseurGrid.cells[0,i] := statusSegment[0].strings[i];
                 CurseurGrid.rowCount := statusSegment[0].count;
                 CurseurGrid.height := CurseurGrid.RowCount*CurseurGrid.DefaultRowHeight
              end;
              exit;
           end;
           if majI then begin
              equivalenceCourante.drawFugitif; // efface
              SetValeurEquivalence(i);
              ResetHeaderXY;
              if curseurGrid.visible then begin
                 CurseurGrid.cells[1,0] := 'n°'+IntToStr(i);
                 CurseurGrid.cells[1,1] := monde[mondeX].Axe.formatNomEtUnite(ve);
                 CurseurGrid.cells[1,2] := monde[mondeY].Axe.FormatNomEtUnite(phe);
                 CurseurGrid.cells[1,3] := unitePente.formatValeurPente(pente);
                 CurseurGrid.rowCount := 4;
                 CurseurGrid.height := 4*CurseurGrid.DefaultRowHeight;
              end;
              equivalenceCourante.drawFugitif; // trace
              exit;
           end;
        end;
     end; // case curseur
     case key of
          vk_left,vk_numpad4: if ssShift in Shift then dec(P.X,4)else dec(P.X);
          vk_right,vk_numpad6: if ssShift in Shift then inc(P.X,4)else inc(P.X);
          vk_down,vk_numpad2 : if ssShift in Shift then inc(P.Y,4)else inc(P.Y);
          vk_up,vk_numpad8 : if ssShift in Shift then dec(P.Y,4) else dec(P.Y);
          vk_prior : dec(P.X,16);
          vk_next : inc(P.X,16);
          vk_escape : ;
          vk_delete,8,110 : begin
             if curseur=curSelect then begin
                if supprime(P) then exit;
                grapheCourant.SetDessinCourant(P.x,P.y);
                if grapheCourant.posDessinCourant<>sdNone
                   then with grapheCourant.dessins do begin
                      Remove(grapheCourant.DessinCourant);
                      PaintBoxC.invalidate;
                      key := 0;
                      exit;
                   end;
             end;
             if (curseur in [curSelect,curReticule,curReticuleData,curEquivalence]) and
                (indexPointCourant>=0) then begin
                   Pages[pageCourante].SupprimeLignes(indexPointCourant,indexPointCourant);
                   Application.MainForm.Perform(WM_Reg_Maj,MajSupprPoints,0);
                   key := 0;
                   exit;
             end;
             exit;
          end
          else exit;
     end; // case key
     P := PaintBoxC.ClientToScreen(P);
     mouse.CursorPos := P;
end; // FormKeyDown

procedure TFgrapheVariab.DessinDeleteItemClick(Sender: TObject);
var P : Tpoint;
begin
      if grapheCourant.DessinCourant=nil then begin
         P := menuDessin.PopupPoint;
         P := grapheCourant.paintBox.ScreenToClient(P);
         grapheCourant.setDessinCourant(P.x,P.y);
      end;
      if grapheCourant.DessinCourant=nil then exit;
      if (grapheCourant.dessinCourant.identification=identDroite) and
         (grapheCourant.dessinCourant.proprietaire<>nil) then begin
         grapheCourant.DessinCourant.proprietaire.sousDessin := nil;
         grapheCourant.DessinCourant.proprietaire.TexteLigne := tlNone;
      end;
      graphes[indexGrCourant].dessins.Remove(grapheCourant.DessinCourant);
      ModifGraphe(indexGrCourant);
      grapheCourant.DessinCourant := nil;
end;

procedure TFgrapheVariab.DessinOptionsItemClick(Sender: TObject);
var P : Tpoint;
begin
    if grapheCourant.DessinCourant=nil then begin
         P := menuDessin.PopupPoint;
         P := grapheCourant.paintBox.ScreenToClient(P);
         grapheCourant.setDessinCourant(P.x,P.y);
    end;
    if grapheCourant.DessinCourant=nil then exit;
    grapheCourant.dessinCourant.litOption(grapheCourant);
    grapheCourant.paintBox.invalidate;
    grapheCourant.DessinCourant := nil;
end;

procedure TFgrapheVariab.FormShortCut(var Msg: TWMKey;var Handled: Boolean);

function SupprEquivalence : boolean;
var posSouris : Tpoint;
    i,xi,yi : integer;
    EquivalenceLoc : Tequivalence;
begin
    result := false;
    if not (curseur in [curReticule,curReticuleData,curSelect]) then exit;
    GetCursorPos(PosSouris);
    PosSouris := grapheCourant.PaintBox.ScreenToClient(PosSouris);
    if (PosSouris.X<0) or (PosSouris.X>grapheCourant.paintBox.width) or
       (PosSouris.Y<0) or (PosSouris.Y>grapheCourant.paintBox.height) then exit;
    for i := 0 to pred(grapheCourant.equivalences[pageCourante].count) do begin
        EquivalenceLoc := grapheCourant.equivalences[pageCourante].items[i];
        with EquivalenceLoc do begin
             grapheCourant.WindowRT(ve,pHe,mondeY,xi,yi);
             if (ligneRappel=lrReticule) and
                ((abs(posSouris.x-xi)+abs(posSouris.y-yi))<12) then begin // supprime
                     grapheCourant.equivalences[pageCourante].Remove(EquivalenceLoc);
                     grapheCourant.PaintBox.invalidate;
                     result := true;
                     handled := true;
                     break;
              end; // supprime
        end;// with Equivalence
   end;//for i
end;

begin
     case msg.charCode of
          vk_delete : if (curseur=curSelect) and (grapheCourant.dessinCourant<>nil)
            then with grapheCourant.dessins do begin
                Remove(grapheCourant.DessinCourant);
                grapheCourant.PaintBox.invalidate;
                handled := true;
            end
            else if not SupprEquivalence then
            if (curseur in [curSelect,curReticule,curReticuleData,curEquivalence]) and
                    (indexPointCourant>=0) then begin
                Pages[pageCourante].SupprimeLignes(indexPointCourant,indexPointCourant);
                Application.MainForm.Perform(WM_Reg_Maj,MajSupprPoints,0);
                handled := true;
          end; // vk_delete
          vk_F2 : if (withModele) and
                MajBtn.enabled then begin
                   MajBtnClick(nil);
                   handled := true;
          end;
          vk_F3 : if (withModele) then begin
                AjusteBtnClick(nil);
                handled := true;
          end;
          vk_F4 : if bruitPresent then begin
                handled := true;
          end;
          vk_F9 : begin
              setPanelModele(not withModele);
              handled := true;
          end;
          vk_F10 : if curseur in [curReticule,curEquivalence,
                                  curReticuleData,curReticuleModele] then begin
              SavePosItemClick(nil);
              handled := true;
          end;
          vk_cancel :  if curseur<>curSelect then begin
                setPenBrush(curSelect);
                grapheCourant.PaintBox.invalidate;
                handled := true;
          end;
     end;
end;

procedure TFgrapheVariab.identPages;
var Deltax : integer;

Procedure TraceMarque(p : codePage);
var Dessin : Tdessin;
    prompt : string;
    i : integer;
begin
      Dessin := nil;
      with grapheCourant.dessins do
           for i := 0 to pred(count) do
               if items[i].numPage=p then begin
                    Dessin := items[i];
                    if not pages[p].active then remove(dessin);
                    break;
               end;
      if not pages[p].active then exit;
      if Dessin=nil then begin
         Dessin := Tdessin.create(graphes[1]);
         with Dessin do begin
            isTexte := true;
            hauteur := 3;
            NumPage := p;
            vertical := false;
            pen.color := couleurPages[P mod NbreCouleur];
            motifCourbe := motifPages[P mod NbreCouleur];
            x1i := (deltax div 2)+deltax*(p-1);
            y1i := 32;
            x2i := x1i;
            y2i := 32;
            Agraphe.MondeXY(x1i, y1i, MondeY, x1, y1);
            Agraphe.MondeXY(x2i, y2i, MondeY, x2, y2);
            Agraphe := graphes[1];
            graphes[1].dessins.add(Dessin);
         end;
      end;
      with Dessin do begin
         try
         dessin.Texte.clear;
         if choixIdentPagesDlg.commentaireCB.checked then texte.Add('%S');
         prompt := '';
         for i := 0 to pred(NbreConst) do
             if choixIdentPagesDlg.ListeConstBox.checked[i] then
                if prompt=''
                   then prompt:='%C'+intToStr(1+i)
                   else prompt:=prompt+' ; %C'+intToStr(1+i);
         if prompt<>'' then texte.add(prompt);
         if ModeleDefini in etatModele then begin
            prompt := '';
            for i := 1 to NbreParam[ParamNormal] do
                if choixIdentPagesDlg.ListeConstBox.checked[NbreConst+pred(i)] then
                    if prompt=''
                       then prompt:='%P'+intToStr(i)
                       else prompt:=prompt+' ; %P'+intToStr(i);
            if prompt<>'' then texte.add(prompt);
         end;
         except // protection ListConstBox
             choixIdentPagesDlg.initParam;
         end;
      end;
end; // TraceMarque

Procedure SupprIdentPage;
var i : integer;
begin
    i := 0;
    with grapheCourant do
    while (i<dessins.count) do begin
       if (dessins[i].numPage>0)
          then Dessins.remove(dessins[i])
          else inc(i);
    end;
end; // SupprIdentPage

var i : integer;
    withPanneau : boolean;
    page : codePage;
begin
    if not grapheCourant.grapheOK then exit;
    withPanneau := IdentAction.enabled and IdentAction.checked;
    if withPanneau then begin
          if (ChoixIdentPagesDlg=nil) then
             Application.CreateForm(TChoixIdentPagesDlg, ChoixIdentPagesDlg);
          choixIdentPagesDlg.initParam;
          choixIdentPagesDlg.showModal;
          withPanneau := choixIdentPagesDlg.newIdentPageCB.checked;
          if not withPanneau then begin
               deltax := paintBox1.Width div NbrePages;
               for page := 1 to NbrePages do
                   traceMarque(page);
          end;
    end
    else SupprIdentPage;
    for i := 1 to 3 do begin
       graphes[i].identificationPages := withPanneau;
       modifGraphe(i);
    end;
end; // IdentPages

procedure TFgrapheVariab.identifierPages(Sender : TObject);
begin
     if grapheCourant.superposePage and (nbrePages>1)
        then IdentPages
        else MajIdentCoord(indexGrCourant);
end;

Procedure TFgrapheVariab.MajIdentCoord(indexGr : integer);

Procedure IdentifierCoord;
var Xcoord,Y1c,Y2c : double;
    Jc : integer;

Procedure MarqueCoord(coord : indiceOrdonnee);
var Dessin : Tdessin;
    i,codeLoc : integer;
    atexte : string;
begin with graphes[indexGr] do begin
      if unAxeX
          then codeLoc := Coordonnee[coord].codeY
          else codeLoc := Coordonnee[coord].codeX;
      with dessins do
           for i := 0 to pred(count) do
               if (items[i].identification=identCoord) and
                  (items[i].numPage=codeLoc) then
                    exit;
      Dessin := Tdessin.create(graphes[indexGr]);
      with Dessin do begin
           isTexte := true;
           avecLigneRappel := false;
           iMonde := Coordonnee[coord].iMondeC;
           vertical := false;
           pen.color := Coordonnee[coord].couleur;
           motifCourbe := Coordonnee[coord].motif;
           identification := identCoord;
           x1 := Xcoord;
           y1 := Y1c;
           x2 := Xcoord;
           y2 := Y2c;
           Agraphe := graphes[indexGr];
           numPoint := Jc;
           numCoord := codeLoc;
           numPage := pageCourante;
           atexte := grandeurs[numCoord].nom;
           if not UnAxeX then
              atexte := atexte+'('+grandeurs[coordonnee[coord].codeX].nom+')';
           with grandeurs[numCoord] do if fonct.expression<>'' then
                 atexte := atexte+' : '+fonct.expression;
           texte.add(atexte);
      end;
      dessins.add(Dessin);
end end; // MarqueCoord

var coord : indiceOrdonnee;
    deltaY,minY,maxY : double;
    pasJ : integer;
begin with graphes[indexGr] do begin
    pasJ := 2*pages[pageCourante].nmes div NbreOrdonnee div 3;
    for coord := 1 to NbreOrdonnee do with Coordonnee[coord] do begin
          if (grandeurs[codeX].fonct.genreC=g_Texte) or
             (grandeurs[codeY].fonct.genreC=g_Texte) then continue;
          Jc := pasJ*coord;
          deltaY := (monde[iMondeC].maxi-monde[iMondeC].mini)/10;
          MaxY := monde[iMondeC].maxi-deltaY;
          MinY := monde[iMondeC].mini+deltaY;
          Xcoord := pages[pageCourante].valeurVar[codeX,Jc];
          if grandeurs[codeY].genreG=variable
              then Y1c := pages[pageCourante].valeurVar[codeY,Jc]
              else Y1c := pages[pageCourante].valeurConst[codeY];
          if Y1c>maxY then Y1c := maxY;
          if Y1c<minY then Y1c := minY;
          if Y1c>(maxY+minY)/2
             then Y2c := Y1c-deltaY
             else Y2c := Y1c+deltaY;
          MarqueCoord(coord);
    end;
end end; // IdentifierCoord

Procedure SupprTout;
var i : integer;
begin with graphes[indexGr] do begin
    i := 0;
    while (i<dessins.count) do
       if dessins[i].identification=identCoord
          then Dessins.remove(dessins[i])
          else inc(i);
end end;

begin
    if IdentAction.checked
        then IdentifierCoord
        else SupprTout;
    graphes[indexgrCourant].identificationPages := false;
    modifGraphe(indexGrCourant);
end; // MajIdentCoord

procedure TFgrapheVariab.FormKeyPress(Sender: TObject; var Key: Char);

procedure CreerXYdata(typeXY : TligneRappel);
var NewEquivalence : Tequivalence;
begin with grapheCourant,curseurOsc[curseurData1] do begin
     NewEquivalence := Tequivalence.Create(
                 xr,yr,curseurOsc[curseurData2].xr,curseurOsc[curseurData2].yr,
                 0,0,0,grapheCourant);
     NewEquivalence.mondepH := mondeC;
     NewEquivalence.ligneRappel := typeXY;
     equivalences[pageCourante].Add(NewEquivalence);
end end;

procedure CreerXY(typeXY : TligneRappel);
var NewEquivalence : Tequivalence;
begin with grapheCourant,curseurOsc[1] do begin
     NewEquivalence := Tequivalence.Create(
                 xr,yr,curseurOsc[2].xr,curseurOsc[2].yr,
                 0,0,0,grapheCourant);
     NewEquivalence.mondepH := mondeC;
     NewEquivalence.ligneRappel := typeXY;
     equivalences[pageCourante].Add(NewEquivalence);
end end;

Function Supprime(posSouris : Tpoint;aMonde : indiceMonde) : boolean;
var i : integer;
    aEquivalence : Tequivalence;
    xi,yi : integer;
    trouve,trouveI : boolean;
begin with grapheCourant do begin
     trouve := false;
     i := 0;
     while i<equivalences[pageCourante].count do begin
            aEquivalence := equivalences[pageCourante].items[i];
            with aEquivalence do if (ligneRappel in [lrReticule,lrX,lrY,lrPente]) then begin
               WindowRT(x1,y1,aMonde,xi,yi);
               trouveI := (abs(posSouris.x-xi)+abs(posSouris.y-yi))<12;
               if not trouveI then begin
                  WindowRT(x2,y2,aMonde,xi,yi);
                  trouveI := (abs(posSouris.x-xi)+abs(posSouris.y-yi))<12;
               end;
               if not trouveI then begin
                  WindowRT(ve,pHe,aMonde,xi,yi);
                  trouveI := (abs(posSouris.x-xi)+abs(posSouris.y-yi))<12;
               end;
                // supprime
               if trouveI then begin
                  equivalences[pageCourante].Remove(aEquivalence);
                  trouve := true;
               end
               else inc(i);
            end// with aEquivalence
            else inc(i);
         end; // while
         if trouve then PaintBox.invalidate;
         result := trouve;
end end;

var posSouris : Tpoint;
    j : integer;
    xrS,yrS : double;
    NewEquivalence : Tequivalence;
    icourbe,mondeLoc : integer;
begin // FormKeyPress
     // if sender=editBidon then key:=#0;
if (key=' ') then case curseur of
    curReticule : with grapheCourant do begin
        GetCursorPos(PosSouris);
        PosSouris := PaintBox.ScreenToClient(PosSouris);
        if (PosSouris.X<0) or (PosSouris.X>paintBox.width) or
           (PosSouris.Y<0) or (PosSouris.Y>paintBox.height) then exit;
        with posSouris do iCourbe := courbeProche(x,y);
        if iCourbe<0
            then mondeLoc := mondeY
            else mondeLoc := courbes[iCourbe].iMondeC;
        if supprime(posSouris,mondeLoc) then exit;
        if cCourant=3 then begin
              CreerXY(lrX);
              CreerXY(lrY);
              CreerXY(lrPente);
         end
         else begin
            mondeRT(posSouris.x,posSouris.y,mondeLoc,xrS,yrS);
            NewEquivalence := Tequivalence.Create(0,0,0,0,xrS,yrS,
                   0,grapheCourant);
            NewEquivalence.mondepH := mondeLoc;
            NewEquivalence.ligneRappel := lrReticule;
            equivalences[pageCourante].Add(NewEquivalence);
         end;
         PaintBox.invalidate;
   end;// curReticule
   curReticuleData : with grapheCourant do begin
        if supprime(posSouris,curseurOsc[curseurData1].mondeC) then exit;
        for j := curseurData1 to curseurData2 do with curseurOsc[j] do
        if mondeC<>mondeX then begin
            NewEquivalence := Tequivalence.Create(0,0,0,0,
                         xr,yr,0,grapheCourant);
            NewEquivalence.mondepH := mondeC;
            NewEquivalence.ligneRappel := lrReticule;
            equivalences[pageCourante].Add(NewEquivalence);
         end;
         if ([coPente, coDeltaX, coDeltaY] * optionCurseur <> []) and
            (curseurOsc[curseurData1].mondeC=curseurOsc[curseurData2].mondeC) then begin
                if coDeltax in optionCurseur then CreerXYdata(lrX);
                if coDeltay in optionCurseur then CreerXYdata(lrY);
                if coPente in optionCurseur then CreerXYdata(lrPente);
         end;
         PaintBox.invalidate;
   end;// curReticuleData
   curReticuleModele : with grapheCourant,curseurOsc[1] do begin
        posSouris.X := xc;posSouris.y := yc;
        if supprime(posSouris,mondeC) then exit;
        NewEquivalence := Tequivalence.Create(0,0,0,0,xr,yr,
                   0,grapheCourant);
        NewEquivalence.mondepH := mondeC;
        NewEquivalence.ligneRappel := lrReticule;
        equivalences[pageCourante].Add(NewEquivalence);
        PaintBox.invalidate;
   end;// curReticuleModele
   curSelect : if (indexPointCourant>=0) then begin
         Pages[pageCourante].PointActif[indexPointCourant] :=
              not Pages[pageCourante].PointActif[indexPointCourant];
         Application.MainForm.Perform(WM_Reg_Maj,MajSupprPoints,0);
   end;
   end;// case curseur
end; // FormKeyPress

procedure TFgrapheVariab.TimerModeleTimer(Sender: TObject);
var newCursorPos : Tpoint;
begin
    newCursorPos := mouse.CursorPos;
    if ((abs(OldCursorPos.x-newCursorPos.x)+
         abs(OldCursorPos.y-newCursorPos.y))>2) and
        (indexGrCourant=1) and
        mouseMoving then MajModeleGr(1);
    TimerModele.enabled := (curseur=curModeleGr);
    OldCursorPos := newCursorPos;
end;

procedure TFgrapheVariab.TimerSizingTimer(Sender: TObject);
var i : integer;
begin
  TimerSizing.Enabled := false;
  Sizing := false;
  for i := 1 to 3 do modifGraphe(i);
end;


procedure TFgrapheVariab.FildeferItemClick(Sender: TObject);
begin
  inherited;
  grapheCourant.filDeFer := FilDeFerItem.checked;
  grapheCourant.paintBox.invalidate;
end;

procedure TFgrapheVariab.FinReticuleItemClick(Sender: TObject);
begin
  inherited;
  setPenBrush(curSelect);
end;

procedure TFgrapheVariab.PanelModeleResize(Sender: TObject);
begin
  if (pageCourante>0)
     and not majFichierEnCours
     and not lecturePage
     and not sizing
              then setPanelParam
end;

Procedure TFgrapheVariab.ModifGraphe(indexGr : integer);
begin
    if not majFichierEnCours
       and not lecturePage
       and graphes[indexGr].paintBox.visible
       and not Sizing
       then graphes[indexGr].paintBox.invalidate;
end;

procedure TFgrapheVariab.Options1Click(Sender: TObject);
var code : integer;
begin
     OptionsVitesseDlg := TOptionsVitesseDlg.create(self);
     code := grapheCourant.coordonnee[1].codeX;
     OptionsVitesseDlg.grandeurX := grandeurs[code];
     code := grapheCourant.coordonnee[1].codeY;
     OptionsVitesseDlg.grandeurY := grandeurs[code];
     if OptionsVitesseDlg.showModal=mrOK then PaintBox1.invalidate;
     OptionsVitesseDlg.free;
end;

procedure TFgrapheVariab.RaZModeleClick(Sender: TObject);
var p : codePage;
    i : integer;
begin
     if OKreg(okRazParam,0) then
     for p := 1 to NbrePages do with pages[p] do begin
         ModeleCalcule := false;
         ModeleErrone := false;
         TexteResultatModele.clear;
         for i := 1 to MaxParametres do
             ValeurParam[paramNormal,i] := parametres[paramNormal,i].ordreDeGrandeur;
     end;
     if OKreg(okRazModele,0) then begin
           MemoModele.clear;
           MemoResultat.clear;
           IsModeleMagnum := false;
           EtatModele := [PasDeModele];
           for i := 1 to 3 do
               exclude(Graphes[i].modif,gmModele);
     end;
end;

Procedure TFgrapheVariab.WmRegInitMagnum(var Msg : TWMRegMessage);
var i : integer;
begin
     if ModeleGraphique[1].nX='' then
        ModeleGraphique[1].nX := graphes[1].monde[mondeX].axe.nom;
     if ModeleGraphique[1].nY='' then
        ModeleGraphique[1].nY := graphes[1].monde[mondeY].axe.nom;
     for i := 1 to NbreModele do begin
        ModeleGraphique[i].initialiseParametre(
           Pages[pageCourante].debut[i],
           Pages[pageCourante].fin[i],true);
        if not ModeleGraphique[i].ModeleOK then begin
           afficheErreur(erModeleGr,0);
           NbreModele := i-1;
           exit;
        end;
        MajModeleGr(i);
     end;
     LanceModele(OptionsDlg.AjustageAutoGrCB.checked);
end;

procedure TFgrapheVariab.VerifParametres;
var i : integer;
begin with pages[pageCourante] do
    for i := 1 to NbreParam[paramNormal] do begin
        if isIncorrect(valeurParam[paramNormal,i]) and
           (pageCourante>1) then
               valeurParam[paramNormal,i] := pages[pred(pageCourante)].valeurParam[paramNormal,i];
        if isIncorrect(valeurParam[paramNormal,i])
           then valeurParam[paramNormal,i] := pages[1].valeurParam[paramNormal,i];
        if isIncorrect(valeurParam[paramNormal,i])
           then valeurParam[paramNormal,i] := parametres[paramNormal,i].ordreDeGrandeur;
   end;
end;

procedure TFgrapheVariab.Sauvermodle1Click(Sender: TObject);
begin
   SaveModeleDlg := TSaveModeleDlg.create(self);
   SaveModeleDlg.showModal;
   SaveModeleDlg.free;
end;

procedure TFgrapheVariab.SaveParamItemClick(Sender: TObject);
begin
   SaveParamDlg := TSaveParamDlg.create(self);
   SaveParamDlg.showModal;
   SaveParamDlg.free;
end;

procedure TFgrapheVariab.EditValeurExit(Sender: TObject);
begin
   ValideParam((sender as Tcomponent).tag,true)
end;

procedure TFgrapheVariab.EditValeurKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var paramCourant : integer;
begin
     paramCourant := (sender as Tcomponent).tag;
     if toucheValidation(key)
        then begin
           valideParam(paramCourant,true);
           case key of
                vk_up,vk_prior : begin
                   dec(paramCourant);
                   if paramCourant>0 then
                       EditValeur[paramCourant].setFocus;
                end;
                vk_down,vk_next,vk_tab,vk_return : begin
                   inc(paramCourant);
                   if paramCourant<=NbreParam[paramNormal] then
                       EditValeur[paramCourant].setFocus;
                end;
           end;
        end
        else if (key=vk_F9) then setPanelModele(not withModele);
end;

procedure TFgrapheVariab.EditValeurKeyPress(Sender: TObject;
  var Key: Char);
begin
     VerifKeyGetFloat(key);
     if key<>#0 then ValeursParamChange := true;
end;

procedure TFgrapheVariab.EnregistrerGraphe1Click(Sender: TObject);

function nomFichier(carac : string) : string;
var posP : integer;
begin
    result := saveDialog.fileName;
    posP := pos('.',result);
    insert(carac,result,posP);
end;

begin
    saveDialog.options := saveDialog.options-[ofOverwritePrompt];
    if saveDialog.Execute then begin
       Graphes[1].VersFichier(saveDialog.fileName);
       if unGrapheItem.checked and
          residusItem.checked then
             Graphes[2].VersFichier(nomFichier('R'));
       if deuxGraphesVert.checked then
           Graphes[2].VersFichier(nomFichier('2'));
       if deuxGraphesHoriz.checked then
          Graphes[3].VersFichier(nomFichier('3'));
    end;
end;

procedure TFgrapheVariab.EchelonItemClick(Sender: TObject);
begin
    setModeleGr(mgEchelon1)
end;

procedure TFgrapheVariab.MoinsRapideBtnClick(Sender: TObject);
begin
     ModifParam((sender as Tcomponent).tag,1/CoeffParamRapide)
end;

procedure TFgrapheVariab.MoinsBtnClick(Sender: TObject);
begin
     ModifParam((sender as Tcomponent).tag,1/CoeffParam)
end;

procedure TFgrapheVariab.PlusBtnClick(Sender: TObject);
begin
    ModifParam((sender as Tcomponent).tag,CoeffParam)
end;

procedure TFgrapheVariab.PlusRapideBtnClick(Sender: TObject);
begin
    ModifParam((sender as Tcomponent).tag,CoeffParamRapide)
end;

procedure TFgrapheVariab.PopupMenuModelePopup(Sender: TObject);
begin
  inherited;
  residusItem.Visible := UnGrapheItem.checked and fonctionTheorique[1].residuStat.statOK;
  residusItem.checked := residusItem.checked and residusItem.Visible;
  ResidusItemBis.visible := residusItem.Visible;
  ResidusItemBis.checked := residusItem.checked;
  Incertchi2Item.checked := avecChi2;
  AffIncert.checked := avecEllipse;
end;

procedure TFgrapheVariab.SigneBtnClick(Sender: TObject);
begin
    ModifParam((sender as Tcomponent).tag,-1)
end;

procedure TFGrapheVariab.ModifParam(paramCourant : integer;coeff : double);
begin
    // ParamEditCourant := paramCourant;
     if valeursParamChange then valideParam(paramCourant,true);
     with pages[pageCourante] do
         ValeurParam[paramNormal,paramCourant] :=
              ValeurParam[paramNormal,paramCourant]*coeff;
     if TraceAutoCB.checked
        then PostMessage(handle,WM_Reg_Calcul,CalculModele,0)
        else MajParametres
end;

procedure TFgrapheVariab.ModeleBtnClick(Sender: TObject);
begin
   setPanelModele(not withModele)
end;

procedure TFgrapheVariab.ModeleItemClick(Sender: TObject);
begin
    setPanelModele(not ModeleItem.checked);
end;

procedure TFgrapheVariab.ParaboleItemClick(Sender: TObject);
begin
    setModeleGr(mgParabolique)
end;

procedure TFgrapheVariab.ParamBtnMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  RepeatTimer.Interval := InitRepeatPause;
  RepeatTimer.Enabled  := True;
  TimerButton := sender as TspeedButton;
end;

procedure TFgrapheVariab.ParamBtnMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  RepeatTimer.Enabled  := False
end;

procedure TFgrapheVariab.RepeatTimerTimer(Sender: TObject);
begin
  RepeatTimer.Interval := RepeatPause;
  if csLButtonDown in TimerButton.ControlState
     then begin
        TimerButton.onClick(timerButton);
        Application.processMessages;
     end
     else RepeatTimer.Enabled := False
end;

procedure TFgrapheVariab.OrigineBtnClick(Sender: TObject);
var i : integer;
    Origin : Tpoint;
begin
     exclude(etatModele,ajustementGraphique);
     for i := 1 to 3 do
         if graphes[i].paintBox.Visible then
            traceCurseurCourant(i); // efface l'actuel
     setPenBrush(curOrigine);
     for i := 1 to 3 do with graphes[i].paintBox do if visible then begin
            curseur := curOrigine;
            GetCursorPos(Origin);
            Origin := ScreenToClient(Origin);
            XOrigineInt := Origin.X;
            TraceCurseurCourant(i);
     end;
end; // OrigineBtnClick

procedure TFgrapheVariab.AjustePanelModele;
const HauteurMini = 76;
var hauteurAjuste,hauteurPanel,hauteurMemo,
    hauteurResultat,i : integer;
begin
     hauteurPanel := ClientHeight-ModeleToolbar.height;
// valeurs par défaut
     if MemoModele.lines.count<3
        then i := 3
        else i := MemoModele.lines.count;
     hauteurMemo := (abs(memoModele.Font.height)+4)*(i+2)+20; // 20 = titre GroupBox
     if NbreParam[paramNormal]<3
        then i := 3
        else i := NbreParam[paramNormal];
     HauteurAjuste := PanelParam1.height*i+Panel1.height+Font.Size;
     hauteurResultat := hauteurPanel-hauteurMemo-HauteurAjuste;
// on teste si résultat pas trop petit
     if (hauteurResultat<HauteurMini) then
        if ((HauteurPanel-HauteurMemo-HauteurMini)>HauteurMini)
           then begin
              HauteurAjuste := HauteurPanel-HauteurMemo-HauteurMini;
              // on ajuste panelAjuste résultat à mini
           end
           else begin // on divise en trois
              HauteurMemo := HauteurPanel div 3;
              HauteurAjuste := hauteurMemo;
           end;
// on affecte
     MemoModeleGB.Height := hauteurMemo;
     PanelAjuste.height := hauteurAjuste;
end;

procedure TFgrapheVariab.VecteursItemClick(Sender: TObject);
var recalcul : boolean;

procedure SetVecteurs(j : integer);
var nomVitesseX,nomVitesseY : string;
    nomAccelerationX,nomAccelerationY : string;
    complement : string;

Function AddVitesse(y : string) : string;
begin
    if y[length(y)]='''' then delete(y,length(y),1);
    result := 'v'+y;
    Fvaleurs.memo.lines.add('v'+y+'=Diff('+y+','+grandeurs[0].nom+')');
    recalcul := true;
end;

Function AddAcceleration(y,v : string) : string;
begin
    if y[length(y)]='''' then delete(y,length(y),1);
    result := 'a'+y;
    Fvaleurs.memo.lines.add('a'+y+'=Diff('+v+','+grandeurs[0].nom+')');
    recalcul := true;
end;

Procedure AddNorme(x,y : string);
begin
    Fvaleurs.memo.lines.add(x[1]+complement+'=Sqrt('+x+'^2+'+y+'^2)');
    recalcul := true;
end;

var indexvX,indexvY : integer;
    indexX,indexY : integer;
    indexaX,indexaY : integer;
    creationvx,creationvy,creationax,creationay : boolean;
begin with grapheCourant do begin
      complement := '';
      indexX := indexNom(coordonnee[j].nomX);
      if (indexX=0) and (NbreOrdonnee=2) and (j=1) then begin
          coordonnee[1].nomX := coordonnee[1].nomY;
          indexX := indexNom(coordonnee[1].nomX);
          coordonnee[1].nomY := coordonnee[2].nomY;
          NbreOrdonnee := 1;
          coordonnee[2].nomX := '';
          coordonnee[2].nomY := '';
          include(OptionGraphe,OgOrthonorme);
      end;
      if (indexX=grandeurInconnue) or (indexX=0) then begin
         exclude(coordonnee[j].trace,trVitesse);
         exclude(coordonnee[j].trace,trAcceleration);
         exit;
      end;
      indexY := indexNom(coordonnee[j].nomY);
      if indexY=grandeurInconnue then begin
         exclude(coordonnee[j].trace,trVitesse);
         exclude(coordonnee[j].trace,trAcceleration);
         exit;
      end;
      indexvX := IndexVitesse(grandeurs[indexX].nom);
      creationvX := indexvX=GrandeurInconnue;
      if creationvX
         then nomVitesseX := AddVitesse(coordonnee[j].nomX)
         else nomVitesseX := grandeurs[indexvX].nom;
      if length(nomVitesseX)>2
         then complement := copy(nomVitesseX,3,length(nomVitesseX)-2)
         else complement := '';
      indexvY := IndexVitesse(grandeurs[indexY].nom);
      creationvY := indexvY=GrandeurInconnue;
      if creationvY
         then nomVitesseY := AddVitesse(coordonnee[j].nomY)
         else nomVitesseY := grandeurs[indexvY].nom;
      if creationvX and creationvY then AddNorme(nomVitesseX,nomVitesseY);
      if trAcceleration in grapheCourant.coordonnee[1].trace then begin
      indexaX := IndexAcceleration(grandeurs[indexX].nom);
      creationaX := indexaX=GrandeurInconnue;
      if creationaX
         then nomAccelerationX := AddAcceleration(coordonnee[j].nomX,nomVitesseX)
         else nomAccelerationX := grandeurs[indexaX].nom;
      if length(nomVitesseX)>2
         then complement := copy(nomVitesseX,3,length(nomVitesseX)-2)
         else complement := '';
      indexaY := IndexAcceleration(grandeurs[indexY].nom);
      creationaY := indexaY=GrandeurInconnue;
      if creationaY
         then nomAccelerationY := AddAcceleration(coordonnee[j].nomY,nomVitesseY)
         else nomAccelerationY := grandeurs[indexaY].nom;
      if creationaX and creationaY then AddNorme(nomAccelerationX,nomAccelerationY);
      end;
end end; // setVecteurs

var j : integer;
begin // VecteursItemClick
     (sender as TmenuItem).checked := true;
     if AccelerItem.checked or NoVecteursItem.checked
        then exclude(grapheCourant.coordonnee[1].trace,trVitesse);
     if VitesseItem.checked or NoVecteursItem.checked
        then exclude(grapheCourant.coordonnee[1].trace,trAcceleration);
     if AccelerItem.checked or VitesseAccItem.checked
        then include(grapheCourant.coordonnee[1].trace,trAcceleration);
     if VitesseItem.checked or VitesseAccItem.checked
        then include(grapheCourant.coordonnee[1].trace,trVitesse);
     for j := 2 to grapheCourant.NbreOrdonnee do
         grapheCourant.coordonnee[j].trace := grapheCourant.coordonnee[1].trace;         
     if (trVitesse in grapheCourant.coordonnee[1].trace) or
        (trAcceleration in grapheCourant.coordonnee[1].trace)
        then begin
           recalcul := false;
           for j := 1 to grapheCourant.NbreOrdonnee do setVecteurs(j);
           if recalcul then begin
               Fvaleurs.MajBtnClick(nil);
               Application.processMessages;
           end;
        end;
     include(grapheCourant.modif,gmXY);
     ModifGraphe(indexGrCourant);
end;// VecteursItemClick

procedure TFgrapheVariab.VecteursMenuPopup(Sender: TObject);
begin
  inherited;
  with grapheCourant do begin
       filDeFerItem.visible := (courbes.Count>1) and
       (coordonnee[1].nomX<>coordonnee[2].nomX) and
       (coordonnee[2].codeX<>grandeurInconnue);
       filDeFerItem.checked := filDeFer;
  end;
end;

procedure TFgrapheVariab.AbscisseCBClick(Sender: TObject);
var indexV,j : integer;
begin with graphes[1] do begin
     indexV := indexNomVariab(abscisseCB.text);
     if indexV=coordonnee[1].codeX then exit; // pas de changement
     for j := 1 to NbreOrdonnee do begin
         coordonnee[j].codeX := indexV;
         coordonnee[j].nomX := grandeurs[indexV].nom;
     end;
     include(modif,gmXY);
     ModifGraphe(1);
end end;

procedure TFgrapheVariab.OptionsModeleBtnClick(Sender: TObject);
begin
   OptionModeleDlg := TOptionModeleDlg.create(self);
   OptionModeleDlg.DlgGraphique := grapheCourant;
   OptionModeleDlg.ModelePagesIndependantesCB.checked := ModelePagesIndependantesMenu.checked;
   if optionModeleDlg.ShowModal=mrOK then begin
       include(grapheCourant.modif,gmOptions);
       ModelePagesIndependantesMenu.checked := OptionModeleDlg.ModelePagesIndependantesCB.checked;
       ModelePagesIndependantes := OptionModeleDlg.ModelePagesIndependantesCB.checked;
       ModifGraphe(indexGrCourant);
       PostMessage(handle,WM_Reg_Calcul,CalculCompile,0)
   end;
   OptionModeleDlg.free;
end;

procedure TFgrapheVariab.IncertBtnClick(Sender: TObject);
begin
    avecEllipse := not avecEllipse;
    if avecEllipse then avecChi2 := true; // forçage
    PaintBox1.invalidate;
end;

procedure TFgrapheVariab.IncertChi2ItemClick(Sender: TObject);
begin
    avecChi2 := Incertchi2Item.Checked;
    PostMessage(handle,WM_Reg_Calcul,CalculCompile,0)
end;

procedure TFgrapheVariab.AffIncertBisClick(Sender: TObject);
var i : integer;
begin
    avecEllipse := not avecEllipse;
    if avecEllipse then avecChi2 := true; // forçage
    for i := 1 to 3 do modifGraphe(i);
end;

procedure TFgrapheVariab.AffIncertClick(Sender: TObject);
begin
    avecEllipse := AffIncert.Checked;
    PaintBox1.invalidate;
end;

procedure TFgrapheVariab.AffineItemClick(Sender: TObject);
begin
    setModeleGr(mgAffine)
end;

procedure TFgrapheVariab.ChoixCurseurClick(Sender: TObject);

Procedure ChangeGraphe(indexGr : integer);
var Origin : Tpoint;
begin with graphes[indexGr],paintBox do begin
      case curseur of
          curReticule : begin
              GetCursorPos(Origin);
              Origin := ScreenToClient(Origin);
              InitReticule(Origin.x,Origin.y);
              pages[pageCourante].affecteConstParam;
              ligneRappelCourante := lrReticule;
          end;
          curReticuleData,curSelect,curReticuleModele : paintBox.invalidate;
          curModele : initCurseurModele;
          curEquivalence : begin
              if (monde[mondeX].graduation<>gLin) or
                 (monde[mondeY].graduation<>gLin)
                  then begin
                     afficheErreur(erGradNonLineaire,0);
                     setPenBrush(curSelect);
                  end;
              if (monde[mondeY].axe=nil)
                  then begin
                     afficheErreur(erTangenteAGauche,0);
                     setPenBrush(curSelect);
                  end;
              TraceCurseurCourant(indexGr);
          end;
      end;
end end;

Function SetEquivalence : boolean;
var reponse : integer;
    codeDerivee : integer;
begin
         setEquivalence := false;
         if ChoixTangenteDlg=nil then
            ChoixTangenteDlg := TChoixTangenteDlg.create(self);
         reponse := ChoixTangenteDlg.showModal;
         if reponse<>mrOK then begin
            setPenBrush(curSelect);
            setEquivalence := true;
            exit;
         end;
         if ChoixTangenteDlg.SupprCB.checked then
            graphes[1].equivalences[pageCourante].clear;
         with Graphes[1] do begin
            setCourbeDerivee;
            codeDerivee :=
                 indexDerivee(monde[mondeY].axe,monde[mondeX].axe,false,false);
            if codeDerivee=grandeurInconnue then begin
                setPenBrush(curSelect);
                setEquivalence := true;
                exit;
            end;
         end;
         curTangenteItem.checked := true;

end;

var i : integer;
    newCurseur : Tcurseur;
begin // ChoixCurseurClick
     newCurseur := Tcurseur((sender as Tcomponent).tag);
     if (newCurseur=curEquivalence) and
         setEquivalence then exit;
     if NewCurseur=curReticuleData then begin
        ReticuleDataDlg := TReticuleDataDlg.create(self);
        ReticuleDataDlg.Agraphe := graphes[1];
        if ReticuleDataDlg.showModal<>mrOK then begin
           setPenBrush(curSelect);
           for i := 1 to 3 do modifGraphe(i);
           exit;
        end;
        if PaintBox2.visible then begin
           ReticuleDataDlg.Agraphe := graphes[2];
           ReticuleDataDlg.showModal;
        end;
        ReticuleDataDlg.free;
     end;
     if NewCurseur=curReticuleModele then begin
        graphes[1].curseurOsc[curseurData1].mondeC := mondeX;
        graphes[1].curseurOsc[curseurData2].mondeC := mondeX;
        ReticuleComplet := true;
        graphes[1].curseurOsc[curseurData1].grandeurCurseur := nil;
        graphes[1].curseurOsc[curseurData2].grandeurCurseur := nil;
     end;
     exclude(etatModele,ajustementGraphique);
     for i := 1 to 3 do
         if graphes[i].paintBox.Visible then
            traceCurseurCourant(i); // efface l'actuel
     (sender as TmenuItem).checked := true;
     Effaces := (newCurseur=curEfface);
                //and ((GetAsyncKeyState(VK_Control) And $8000)<>0);
     setPenBrush(NewCurseur);
     grapheCourant.dessinCourant := nil;
     if curseur in [curLigne,curTexte] then begin
           grapheCourant.DessinCourant := Tdessin.create(grapheCourant);
           grapheCourant.DessinCourant.isTexte := curseur=curTexte;
           if curseur=curLigne
              then grapheCourant.DessinCourant.pen.color := couleurLigne
              else grapheCourant.DessinCourant.pen.color := couleurTexte;
     end;
     if curseur=curEquivalence then with graphes[1] do
        UnitePente.UniteDerivee(monde[mondeX].axe,monde[mondeY].axe,gLin,gLin);
     for i := 1 to 3 do
         if graphes[i].paintBox.Visible then
            ChangeGraphe(i);
end;  // ChoixCurseurClick

procedure TFgrapheVariab.CurseurBtnClick(Sender: TObject);
begin
   (sender as TtoolButton).CheckMenuDropdown
end;

procedure TFgrapheVariab.CurseurMenuPopup(Sender: TObject);
begin
  inherited;
  ModeleSepare.Visible := pages[pageCourante].modeleCalcule;
  curModeleItem.Visible := pages[pageCourante].modeleCalcule;
  curReticuleModeleItem.Visible := pages[pageCourante].modeleCalcule;
  addModeleItem.Visible := pages[pageCourante].modeleCalcule;
  CurOrigineItem.enabled := not grapheCourant.superposePage;
end;

procedure TFgrapheVariab.LabelYMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var P : Tpoint;
begin
     P := (sender as Tlabel).ClientToScreen(point(X,Y));
     P := paintBox1.ScreenToClient(P);
     PaintBoxMouseMove(paintBox1,shift,P.x,P.Y);
end;

procedure TFgrapheVariab.MenuReticulePopup(Sender: TObject);
const finCaption = 'Terminez ';
begin
     TableauXYItemBis.visible := graphes[1].equivalences[pageCourante].count>0;
     if TableauXYItemBis.visible and
        (graphes[1].equivalences[pageCourante][0].ligneRappel in [lrXdeY,lrReticule])
        then begin
            TableauXYItemBis.caption := stGridValeurs;
            ViderXYItemBis.caption := stRazValeurs;
        end
        else begin
            TableauXYItemBis.caption := stGridTangente;
            ViderXYItemBis.caption := stRazTangente;
        end;
     case curseur of
          curReticule : FinReticuleItem.caption := Fincaption+'réticule libre';
          curReticuleModele : FinReticuleItem.caption := Fincaption+'réticule données';
          curReticuleData : FinReticuleItem.caption := Fincaption+'réticule modèle';
          curEquivalence : FinReticuleItem.caption := Fincaption+'équivalence';
          else FinReticuleItem.caption := '';
     end;
     FinReticuleItem.caption := FinReticuleItem.caption+' (ESC)';
     ViderXYItemBis.visible := TableauXYItem.visible;
end;

procedure TFgrapheVariab.metafile2Click(Sender: TObject);
begin
  inherited;
  grapheClip := TgrapheClipBoard((sender as TMenuItem).tag);
  CopierItemClick(sender);
end;

procedure TFgrapheVariab.DeuxGraphesVertClick(Sender: TObject);
begin
     if MajFichierEnCours then exit;
     (sender as TmenuItem).checked := true;
     if DeuxGraphesVert.checked
        then PanelBis.Width := PanelCentral.width div 2
        else PanelBis.Width := 0;
     if DeuxGraphesHoriz.checked
        then PaintBox3.height := panelPrinc.height div 2
        else PaintBox3.height := 0;
     if sender=UnGrapheItem then NGraphesBtn.ImageIndex := 31;
     if sender=DeuxGraphesHoriz then NGraphesBtn.ImageIndex := 33;
     if sender=DeuxGraphesVert then NGraphesBtn.ImageIndex := 34;
     PanelBis.visible := DeuxGraphesVert.checked;
     PaintBox2.visible := PanelBis.visible;
     PaintBox3.visible := DeuxGraphesHoriz.checked;
     indexGrCourant := 1;
     grapheCourant := Graphes[1];
     invalidate;
end;

procedure TFgrapheVariab.SetCoordonneeResidu;
var iX,iY : integer;
    ValeurX,ValeurY : vecteur;

procedure TraceSigma;
var Xlegende : double;

Procedure traceH(yr : double;couleur : Tcolor;titre : string);
var dessin : Tdessin;
begin with graphes[3] do begin
     Dessin := Tdessin.create(graphes[3]);
     with Dessin do begin
        isTexte := false;
        pen.color := couleur;
        pen.style := psSolid;
        x1 := monde[mondeX].mini;
        y1 := yr;
        x2 := monde[mondeX].maxi;
        y2 := yr;
     end;
     dessins.Add(Dessin);
     if titre='' then  exit;
     Dessin := Tdessin.create(graphes[3]);
     with Dessin do begin
        isTexte := true;
        isOpaque := true;
        couleurFond := colorToRGB(clWindow);
        texte.add(titre);
        pen.color := couleur;
        x1 := xLegende;
        y1 := yr;
        x2 := x1;
        y2 := y1;
     end;
     dessins.Add(Dessin);
end end;

begin with fonctionTheorique[1].residuStat,PaintBox3.Canvas do begin
      with graphes[3].monde[mondeX] do
         xLegende := (mini+maxi)/3;
      traceH(-t95,clNavy,'95%');
      traceH(+t95,clNavy,'95%');
end end; // TraceSigma

procedure TraceResiduStudent;
var Acourbe : Tcourbe;
    mini,maxi : double;
    i,j : integer;
begin with pages[pageCourante],graphes[3] do begin
        j := 0;
        for i := debut[1] to fin[1] do begin
            valeurX[j] := valeurVar[iX,i];
            valeurY[j] := fonctionTheorique[1].residuStat.residusStudent[i];
            inc(j);
        end;
        if (NbreModele>1) then for i := debut[2] to fin[2] do begin
            valeurY[j] := fonctionTheorique[2].residuStat.residusStudent[i];
            valeurX[j] := valeurVar[iX,i];
            inc(j);
        end;
        Acourbe := Tcourbe.create(valeurX,valeurY,0,pred(j),
                  grandeurs[iX],grandeurs[iY]);
        Acourbe.page := pageCourante;
        Acourbe.setStyle(clRed,psSolid,mCroix);
        Acourbe.trace := [trResidus];
        monde[mondeX].mini := Graphes[1].monde[mondeX].mini;
        monde[mondeX].maxi := Graphes[1].monde[mondeX].maxi;
        monde[mondeX].Axe := grandeurs[iX];
        GetMinMax(valeurY,j,mini,maxi);
        monde[mondeY].Axe := grandeurs[iY];
        with fonctionTheorique[1] do begin
           if maxi<(residuStat.t95*1.1) then maxi := residuStat.t95*1.1;
           if mini>(-residuStat.t95*1.1) then mini := -residuStat.t95*1.1;
        end;
        if maxi>abs(mini)
            then mini := -maxi
            else maxi := abs(mini);
        monde[mondeY].mini := mini;
        monde[mondeY].maxi := maxi;
        monde[mondeY].Axe := grandeurs[iY];
        courbes.Add(Acourbe);
        Acourbe.aDetruire := true;
        withResidusStudent := true;
        TraceSigma;
end end; // residus studentisés

procedure TraceResiduNormal;
var Acourbe : Tcourbe;
    mini,maxi : double;
    i,j : integer;
    Vminmax : vecteur;
    tampon : double;
begin with pages[pageCourante],graphes[3] do begin
        j := 0;
        if avecEllipse then begin
           setLength(dXresidu,NmesMax+1);
           setLength(dYresidu,NmesMax+1);
        end;
        for i := debut[1] to fin[1] do begin
            valeurX[j] := valeurVar[iX,i];
            valeurY[j] := fonctionTheorique[1].residuStat.residus[i];
            if avecEllipse then begin
               dXresidu[j] := incertVar[iX,i];
               dYresidu[j] := incertVar[iY,i];
            end;
            inc(j);
        end;
        if (NbreModele>1) then for i := debut[2] to fin[2] do begin
            valeurY[j] := fonctionTheorique[2].residuStat.residus[i];
            valeurX[j] := valeurVar[iX,i];
            inc(j);
        end;
        Acourbe := Tcourbe.create(valeurX,valeurY,0,pred(j),
                  grandeurs[iX],grandeurs[iY]);
        Acourbe.page := pageCourante;
        Acourbe.setStyle(clRed,psSolid,mCroix);
        Acourbe.trace := [trResidus];
        monde[mondeX].mini := Graphes[1].monde[mondeX].mini;
        monde[mondeX].maxi := Graphes[1].monde[mondeX].maxi;
        monde[mondeX].Axe := grandeurs[iX];
        GetMinMax(valeurY,j,mini,maxi);
        monde[mondeY].Axe := grandeurs[iY];
        if avecEllipse then begin
           Acourbe.IncertX := dXresidu;
           Acourbe.IncertY := dYresidu;
           setLength(Vminmax,NmesMax+1);
           for i := 0 to pred(fonctionTheorique[1].residuStat.Nbre) do begin
               Vminmax[i] := fonctionTheorique[1].residuStat.residus[i]+dyResidu[i];
           end;
           GetMinMax(Vminmax,j,tampon,maxi);
           for i := 0 to pred(fonctionTheorique[1].residuStat.nbre) do begin
               Vminmax[i] := fonctionTheorique[1].residuStat.residus[i]-dyResidu[i];
           end;
           GetMinMax(Vminmax,j,mini,tampon);
           VminMax := nil;
        end;
        if maxi>abs(mini)
            then mini := -maxi
            else maxi := abs(mini);
        monde[mondeY].mini := mini;
        monde[mondeY].maxi := maxi;
        monde[mondeY].Axe := grandeurs[iY];
        courbes.Add(Acourbe);
        Acourbe.aDetruire := true;
        withResidusStudent := false;
end end;

begin with pages[pageCourante],graphes[3] do begin
        panelBis.visible := false;
        paintBox2.Visible := false;
        paintBox3.Visible := false;
        residusStudentCB.Visible := false;
        if not fonctionTheorique[1].residuStat.statOK then exit;
        grapheOK := (NbreVariab>1) and (NbrePages>0) and
              pages[pageCourante].modeleCalcule and
                (ModeleDefini in etatModele) and
                (ModeleConstruit in etatModele);
        if not grapheOK  then exit;
        residusStudentCB.Visible := true;
        paintBox3.Visible := true;
        optionGraphe := [];
        iX := fonctionTheorique[1].indexX;
        iY := fonctionTheorique[1].indexY;
        setLength(valeurX,MaxVecteurDefaut+1);
        setLength(valeurY,MaxVecteurDefaut+1);
        superposePage := false;
        Reset;
        Dessins.clear;
        if residusStudentCB.checked
           then traceResiduStudent
           else traceResiduNormal;
        monde[mondeY].defini := true;
        monde[mondeX].defini := true;
        with PaintBox3.Canvas do begin
           pen.mode := pmCopy;
           pen.color := couleurAxeX;
           pen.style := psSolid;
        end;
        modif := [];
end end; // setCoordonneeResidu

procedure TFgrapheVariab.PaintBoxClick(Sender: TObject);
var j : integer;
begin
 if DeuxGraphesVert.checked or
     DeuxGraphesHoriz.checked
     then begin
         indexGrCourant := (sender as TpaintBox).tag;
         grapheCourant := graphes[indexGrCourant];
         for j := 1 to 3 do
             if graphes[j].paintBox.visible then
                graphes[j].EncadreTitre(indexGrCourant=j)
     end;
end;

procedure TFgrapheVariab.FormPaint(Sender: TObject);
begin
  PanelBis.visible := DeuxGraphesVert.checked;
  PaintBox2.visible := PanelBis.visible;
  ResidusStudentCB.Visible := residusItem.checked and withModele;
  PaintBox3.visible := DeuxGraphesHoriz.checked or
                     (residusItem.checked and (withModele));
end;

procedure TFgrapheVariab.addModeleItemClick(Sender: TObject);
var Dessin : Tdessin;
    i : integer;
    nouveauDessin : boolean;
begin with graphes[1] do begin
      if OptionsAffModeleDlg=nil then
         Application.createForm(TOptionsAffModeleDlg,OptionsAffModeleDlg);
      OptionsAffModeleDlg.isGlobal := false;
      OptionsAffModeleDlg.showModal;
      nouveauDessin := true;
      dessin := nil;// pour le compilateur
      for i := 0 to pred(Dessins.count) do
         if dessins[i].isTexte and
            (pos('%M',dessins[i].texte[0])>0) then begin
               dessin := dessins[i];
               nouveauDessin := false;
            end;
      if nouveauDessin then Dessin := Tdessin.create(graphes[1]);
      with Dessin do begin
           texte.Clear;
           isTexte := true;
           //centre := false;
           avecCadre := true;
           avecLigneRappel := false;
           hauteur := 4;
           vertical := false;
           pen.color := couleurModele[1];
           identification := identNone;
           paramModeleBrut := true;
           with monde[mondeX] do x1 := (mini+maxi)/2;
           with monde[mondeY] do y1 := maxi-(maxi-mini)/32;
           x2 := x1;
           y2 := y1;
           numPage := 0; // pageCourante
           for i := 1 to NbreModele do
               if OptionsAffModeleDlg.listeModeleBox.checked[i-1] then
                  texte.Add('%M'+intToStr(i));
           for i := 1 to NbreParam[paramNormal] do
               if OptionsAffModeleDlg.listeParamBox.checked[i-1] then
                  texte.Add('%P'+intToStr(i));
      end;
      if nouveauDessin then dessins.add(Dessin);
      PaintBox1.invalidate;
end end; // addModele

procedure TFgrapheVariab.AffecteModele(indexGr : integer);
var i : integer;
    Agraphe : TgrapheReg;
begin
if not(ModeleDefini in etatModele) and
   not(SimulationDefinie in etatModele) then exit;
Agraphe := graphes[indexGr];
for i := 0 to pred(Agraphe.courbes.count) do with Agraphe.courbes[i] do begin
    if (indexModele<0) and (-indexModele<=NbreFonctionSuper) then
        with fonctionSuperposee[-indexModele],Pages[page] do begin
             DebutC := 0;
             AffecteConstParam;
             AffecteVariableP(false);
             if genreC=g_fonction then GenereM(valX,valY,
                   valeurVar[indexX,0],valeurVar[indexX,pred(nmes)],
                   Agraphe.monde[mondeX].graduation=gLog,FinC);
             if genreC=g_equation then GenereEquation(valX,valY,
                          Agraphe.monde[mondeX].axe,
                          Agraphe.monde[mondeY].axe,page,FinC);
    end;
    if (indexModele>0) and (indexBande=0) and
        not courbeExp and
       (indexModele<=NbreModele) then
        with fonctionTheorique[indexModele],Pages[page] do begin
               DebutC := debut[indexModele];
               FinC := fin[indexModele];
               copyVecteur(valX,valeurVar[indexX]);
               if ModeleCalcule
                    then copyVecteur(valY,valeurTheorique[indexModele])
                    else copyVecteur(valY,valeurVar[indexY]);
               continue;
       end; // with
    if ModeleParametrique and (NbreModele=2) then with Pages[page] do begin
               DebutC := debut[1];
               FinC := fin[1];
               if ModeleCalcule
                    then if ModeleParamX1
                           then begin
                               copyVecteur(valX,valeurTheorique[1]);
                               copyVecteur(valY,valeurTheorique[2]);
                           end
                           else begin
                               copyVecteur(valY,valeurTheorique[1]);
                               copyVecteur(valX,valeurTheorique[2]);
                           end
                    else if ModeleParamX1
                          then begin
                             copyVecteur(valX,valeurVar[fonctionTheorique[1].indexY]);
                             copyVecteur(valY,valeurVar[fonctionTheorique[2].indexY]);
                           end
                           else begin
                             copyVecteur(valX,valeurVar[fonctionTheorique[2].indexY]);
                             copyVecteur(valY,valeurVar[fonctionTheorique[1].indexY]);
                           end;
                continue;
       end; // with pages
       if PortraitDePhase and (NbreModele=1) then with Pages[page] do begin
               DebutC := debut[1];
               FinC := fin[1];
               if ModeleCalcule
                    then if ModeleParamX1
                           then begin
                               copyVecteur(valX,valeurTheorique[1]);
                               copyVecteur(valY,valeurLisse[fonctionTheorique[1].indexYp]);
                           end
                           else begin
                               copyVecteur(valY,valeurTheorique[1]);
                               copyVecteur(valX,valeurLisse[fonctionTheorique[1].indexYp]);
                           end
                    else if ModeleParamX1
                          then begin
                             copyVecteur(valX,valeurVar[fonctionTheorique[1].indexY]);
                             copyVecteur(valY,valeurVar[fonctionTheorique[1].indexYp]);
                           end
                           else begin
                             copyVecteur(valX,valeurVar[fonctionTheorique[1].indexY]);
                             copyVecteur(valY,valeurVar[fonctionTheorique[1].indexYp]);
                           end;
              continue;
       end;// with
   end // i
end; // affecteModele

procedure TFgrapheVariab.ZoomModele(indexGr : integer);
var i : integer;
    valT : vecteur;
begin
with graphes[indexGr] do begin
    if (NbreModele=0) or not(ModeleDefini in etatModele) then exit;
    ChercheMinMax;
    for i := 0 to pred(courbes.count) do with courbes[i] do begin
        if pages[page].modeleErrone then continue;
        pages[page].AffecteConstParam;
        pages[page].AffecteVariableP(false);
        if (indexModele>0) and
           (indexBande=0) and
            not courbeExp and
           (indexModele<=NbreModele) and
           (motif<>mHisto) then
              with fonctionTheorique[indexModele] do
                 case genreC of
                 g_fonction : begin
                     debutC := 0;
                     if varX.indexG=indexX
                        then GenereM(valX,valY,miniMod,maxiMod,
                           monde[mondeX].graduation=gLog,FinC)
                        else begin
                            if (indexX=0) and useDefaut
                               then begin
                                    maxiMod := maxiTemps;
                                    miniMod := miniTemps;
                               end
                               else GetMinMax(ValX,succ(FinC),miniMod,maxiMod);
                            GenereM(valX,valY,miniMod,maxiMod,false,FinC);
                        end;
                  end;// fonction
                  g_equation : GenereEquation(valX,valY,
                          monde[mondeX].axe,
                          monde[mondeY].axe,page,FinC);
                  g_diff2 : begin
                        if isModeleSysteme
                           then begin if indexModele=1 then
                               ExtrapoleDiff2(page,Pages[page].debut[1],
                                         monde[mondeX].mini,monde[mondeX].maxi,
                                         ValeurXDiff,ValeurYDiff,valeurYpDiff,1,NbreModele);
                            end
                            else ExtrapoleDiff2(page,Pages[page].debut[indexModele],
                                         monde[mondeX].mini,monde[mondeX].maxi,
                                         ValeurXDiff,ValeurYDiff,valeurYpDiff,indexModele,indexModele);
                        copyVecteur(valX,valeurXDiff);
                        copyVecteur(valY,ValeurYDiff[indexModele]);
                        debutC := 0;
                        finC := pred(Pages[page].NmesMax);
                    end;// diff2
                    g_diff1 : begin
                        if isModeleSysteme
                           then begin if indexModele=1 then
                               ExtrapoleDiff1(page,Pages[page].debut[1],
                                         monde[mondeX].mini,monde[mondeX].maxi,
                                         ValeurXDiff,ValeurYDiff,1,NbreModele);
                           end
                           else ExtrapoleDiff1(page,Pages[page].debut[indexModele],
                                         monde[mondeX].mini,monde[mondeX].maxi,
                                         ValeurXDiff,ValeurYDiff,indexModele,indexModele);
                        copyVecteur(valX,valeurXDiff);
                        copyVecteur(valY,ValeurYDiff[indexModele]);
                        debutC := 0;
                        finC := pred(pages[page].NmesMax);
                    end; // diff1
                end;// case
                if modeleParametrique and (NbreModele=2) then begin { Fonction }
                      if useDefaut
                         then begin
                              maxiMod := maxiTemps;
                              miniMod := miniTemps;
                         end
                         else GetMinMax(Pages[page].ValeurVar[0],pages[page].nmes,miniMod,maxiMod);
                      GenereModeleParametrique(valY,valX,miniMod,maxiMod,finC);
                      if ModeleParamX1 then swap(valX,valY);
                      debutC := 0;
                      continue;
                end;
                if portraitDePhase and
                   (fonctionTheorique[1].genreC=g_fonction) and
                   (NbreModele=1) then begin
                     if useDefaut
                         then begin
                              maxiMod := maxiTemps;
                              miniMod := miniTemps;
                         end
                         else GetMinMax(Pages[page].ValeurVar[0],pages[page].nmes,miniMod,maxiMod);
                      FonctionTheorique[1].GenereM(valX,valY,miniMod,maxiMod,false,FinC);
                      copyVecteur(valT,valX);
                      DeriveeGauss(valT,valY,finC+1,valX,degreDerivee,NbrePointDerivee);
                      valT := nil;
                      if ModeleParamX1 then swap(valX,valY);
                      debutC := 0;
                      continue;
                end;
                if portraitDePhase and
                   (fonctionTheorique[1].genreC=g_diff2) and
                   (NbreModele=1) then begin
                     if useDefaut
                         then begin
                              maxiMod := maxiTemps;
                              miniMod := miniTemps;
                         end
                         else GetMinMax(Pages[page].ValeurVar[0],pages[page].nmes,miniMod,maxiMod);
                       ExtrapoleDiff2(page,Pages[page].debut[1],
                              miniMod,maxiMod,
                              ValeurXDiff,ValeurYDiff,valeurYpDiff,1,1);
                       if ModeleParamX1
                          then begin
                              copyVecteur(valX,valeurYDiff[1]);
                              copyVecteur(valY,valeurYpDiff[1]);
                          end
                          else begin
                              copyVecteur(valX,valeurYpDiff[1]);
                              copyVecteur(valY,valeurYDiff[1]);
                          end;
                       debutC := 0;
                       finC := pred(pages[page].NmesMax);
                       continue;
                end; // case
                if (indexModele=1) and
                   (indexBande=0) and
                   not(courbeExp) and
                   (valYder<>nil) then begin
                       setLength(valYder,finC); // pb mémoire
                       DeriveeGauss(valX,valY,finC,valYder,degreDerivee,NbrePointDerivee);
                end;
          end; // for i
end end; // zoomModele

procedure TFgrapheVariab.ZoomSuperpose(indexGr : integer);
var i : integer;
begin with graphes[indexGr] do begin
    if (NbreFonctionSuper=0) or not(SimulationDefinie in etatModele) then exit;
    chercheMinMax;
    for i := 0 to pred(courbes.count) do with courbes[i] do begin
        if (indexModele<0) and (-indexModele<=NbreFonctionSuper) then
            with fonctionSuperposee[-indexModele] do begin
                 pages[page].AffecteConstParam;
                 pages[page].AffecteVariableP(false);
                 debutC := 0;
                 if genreC=g_equation
                    then GenereEquation(valX,valY,
                          monde[mondeX].axe,
                          monde[mondeY].axe,page,FinC)
                    else if indexX=varX.indexG
                    then GenereM(valX,valY,
                                 miniMod,maxiMod,monde[mondeX].Graduation=gLog,FinC)
                    else begin
                         if (varX=grandeurs[0]) and useDefaut
                            then begin
                                 maxiMod := maxiTemps;
                                 miniMod := miniTemps;
                            end
                            else GetMinMax(ValX,succ(FinC),miniMod,maxiMod);
                         GenereM(valX,valY,miniMod,maxiMod,false,FinC);
                    end; // not mondeX
         end; // boucle i
    end;
end end; // zoomSuperpose

procedure TFgrapheVariab.SetTaillePolice;
begin
         majModelePermis := false;
         majModelePermis := true;
         AjustePanelModele;
end;

procedure TFgrapheVariab.ModelisationItemClick(Sender: TObject);
begin
       setPanelModele(not withModele)
end;

procedure TFgrapheVariab.FermerItemClick(Sender: TObject);
begin
     setPanelModele(false)
end;

procedure TFgrapheVariab.SavePosItemClick(Sender: TObject);
var i : integer;
begin with grapheCourant do begin
              if savePositionDlg=nil then
                 Application.CreateForm(TsavePositionDlg, savePositionDlg);
              varXPos := monde[mondeX].axe;
              varYpos := nil;
              if curseur=curReticuleData then begin
                 varYPos := curseurOsc[curseurData1].grandeurCurseur;
                 if varYpos=nil then varYPos := monde[mondeY].axe;
              end;
              if curseur=curReticule then
                 varYPos := curseurOsc[1].grandeurCurseur;
              if curseur=curReticuleModele then
                 varYPos := curseurOsc[1].grandeurCurseur;
              if curseur=curEquivalence then
                 varYPos := equivalenceCourante.varY;
              if varYpos=nil then varYPos := monde[mondeY].axe;
              if (curseur=curEquivalence) and
                 (equivalenceCourante<>nil) then begin
                     valeurCurseur[coX] := equivalenceCourante.ve;
                     valeurCurseur[coY] := equivalenceCourante.phe;
                     valeurCurseur[coPente] := equivalenceCourante.pente;
              end;
              savePositionDlg.curseur := curseur;
              for i := 1 to 5 do
                  savePositionDlg.curseurData[i] := curseurOsc[i];
              savePositionDlg.unitePente := unitePente;
              savePositionDlg.graphe := grapheCourant;
              if savePositionDlg.showModal=mrOK then begin
                 AjoutGrandeurNonModele := true;
                 Application.MainForm.perform(WM_Reg_Maj,MajGrandeur,0);
              end;
end end;

procedure TFgrapheVariab.SetParamEdit;
begin
end;

procedure TFgrapheVariab.ProprieteCourbeClick(Sender: TObject);
begin
  if PropCourbeForm=nil then
     PropCourbeForm := TPropCourbeForm.create(self);
  PropCourbeForm.Acourbe := grapheCourant.courbes[iPropCourbe];
  PropCourbeForm.Agraphe := grapheCourant;
  PropCourbeForm.maj;
  PropCourbeForm.show;
end;

procedure TFgrapheVariab.RadioItemClick(Sender: TObject);
begin
  setModeleGr(mgRadio)
end;

procedure TFgrapheVariab.PaintBox2DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  if source is TStringGrid then (source as TstringGrid).EndDrag(true); // ??
end;


procedure TFgrapheVariab.setModeleGR(index : TgenreModeleGr);
var m : integer;
begin
   with grapheCourant,fonctionTheorique[1] do begin
       if monde[mondeX].axe<>nil then
           indexX := indexNom(monde[mondeX].axe.nom);
       for m := mondeY to mondeSans do
           if monde[m].axe<>nil then begin
              indexY := indexNom(monde[m].axe.nom);
              break;
           end;
   end;
   NbreModele := 1;
   ChoixModeleDlg.EffaceModele := true;
   ChoixModeleDlg.modeleChoisi := index;
   ModeleMagnum(1,true);
end;

end.

