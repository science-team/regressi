{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

// filerw3.pas : fichier include de regmain.pas

function TFRegressiMain.LitFichierWin : boolean;

procedure litPage;
var ligneWinaLire : boolean;

Procedure litValeurRepere(imax : integer);
var i,posTab : integer;
    valeur : double;
    texte : string;
begin
    for i := 1 to imax do begin
        readln(fichier,texte);
        posTab := pos(crTab,texte);
        valeur := strToFloatWin(copy(texte,1,pred(posTab)));
        texte := copy(texte,succ(posTab),length(texte)-posTab);
        pages[pageCourante].ajouteRepere(valeur,texte);
    end;
end;

Procedure litValeur(imax : LongInt);
var i,j : LongInt;
begin with pages[pageCourante] do begin
    nmes := imax;
    j := 0;
    try
    while j<nmes do begin
        for i := 0 to pred(NbreVariabExp) do
            read(fichier,valeurVar[i,j]);
        readln(fichier);
        inc(j);
    end;
    except
        nmes := j;
        repeat
           litLigneWin; // trop tard on a d�j� mang� le $2
        until finFichierWin or
              (Length(LigneWin)=0) or
              charinset(ligneWin[1],[symbReg2,symbReg]);
        ligneWinAlire := (Length(LigneWin)>0) and
                         not charInSet(ligneWin[1],[symbReg2,symbReg]);
    end;
    for i := 0 to pred(NbreVariabExp) do
        if isNan(valeurVar[i,pred(nmes)]) then
             nmes := nmes-1;
end end;

Procedure litIncertVariab(imax : integer);
var i,j,jmax : integer;
begin with pages[pageCourante] do begin
    if imax>nmes
       then jmax := nmes
       else jmax := imax;
    try
    for j := 0 to pred(jmax) do begin
        for i := 0 to pred(NbreVariabExp) do begin
            try
            read(fichier,incertVar[i,j]);
            except
                 incertVar[i,j] := Nan;
            end;
        end;
        readln(fichier);
    end;
    for j := jmax to pred(imax) do readln(fichier);
    except
    end;
end end;

Procedure litEquivalence(imax : integer);
var i : integer;
    equivalence : Tequivalence;
begin
    for i := 1 to imax do begin
        equivalence := Tequivalence.Create(0,0,0,0,0,0,0,FgrapheVariab.graphes[1]);
        with equivalence do begin
           read(fichier,pente,ve,phe);
           read(fichier,x1,y1,x2,y2);
           read(fichier,x1i,y1i,x2i,y2i);
           readln(fichier);
        end;
        if equivalence.pente<>0
           then FgrapheVariab.graphes[1].Equivalences[pageCourante].Add(equivalence)
           else equivalence.Free
    end;
end;

var i,j,imax,choix,iVar : integer;
begin // litPage
   if ajoutePage then with pages[pageCourante] do begin
      commentaireP := ligneWin;
      litLigneWin;
      while (Length(LigneWin)>0) and (ligneWin[1]=symbReg2) do begin
        imax := NbreLigneWin(ligneWin);
        choix := 0;
        if pos('VALEUR',ligneWin)<>0 then begin
           if pos('VAR',ligneWin)<>0 then choix := 1 else
              if pos('CONST',ligneWin)<>0 then choix := 2 else
              if pos('PARAM',ligneWin)<>0 then choix := 3 else
              if pos('GLB',ligneWin)<>0 then choix := 5 else
              if pos('STAT',ligneWin)<>0 then choix := 6;
        end else
        if pos('DEBUT',ligneWin)<>0 then choix := 10 else
        if pos('FIN',ligneWin)<>0 then choix := 11 else
        if pos('OPTIONS',ligneWin)<>0 then choix := 13 else
        if pos('INCERT',ligneWin)<>0 then begin
           if pos('VAR',ligneWin)<>0 then choix := 14 else
           if pos('CONST',ligneWin)<>0 then choix := 15 else
           if pos('GLB',ligneWin)<>0 then choix := 23 else
           if pos('PARAM',ligneWin)<>0 then choix := 20;
        end else
        if pos('STDEV',ligneWin)<>0 then begin
           if pos('PARAM',ligneWin)<>0 then choix := 21;
        end else
        if pos('ORIGINE TEMPS',ligneWin)<>0 then choix := 17 else
        if pos('TEXTE',ligneWin)<>0 then begin
           if pos('VAR',ligneWin)<>0 then choix := 18 else
           if pos('CONST',ligneWin)<>0 then choix := 19;
        end else
        if pos('EQUIVALENCE',ligneWin)<>0 then choix := 22 else
        if pos('INDICATEUR',ligneWin)<>0 then choix := 26 else
        if pos('MODELEPAGE',ligneWin)<>0 then choix := 24 else
        if (pos('BORNES FFT',ligneWin)<>0) and (imax=2) then choix := 25 else
        if pos('BITMAP',ligneWin)<>0 then choix := 16 else
        if pos('REPERE',ligneWin)<>0 then choix := 27;
        ligneWinalire := true;
        case choix of
             0 : for i := 1 to imax do litLigneWin;
             1 : litValeur(imax);
             2 : for i := 0 to pred(imax) do begin
                 j := indexConstExp[i];
                 try
                 if j=grandeurInconnue
                    then readln(fichier)
                    else readlnNombreWin(ValeurConst[j]);
                 except
                     ValeurConst[j] := 0;
                 end;
             end;
             3 : for i := 1 to imax do readlnNombreWin(valeurParam[paramNormal,i]);
             6 : with stat do begin
                 Nbre := imax;
                 StatOK := true;
                 for i := 0 to pred(imax) do
                     readlnNombreWin(Donnees[i]);
             end;
             10 : for i := 1 to imax do readln(fichier,debut[i]);
             11 : for i := 1 to imax do readln(fichier,fin[i]);
             13 : begin
                active := litBooleanWin;
                experimentale := litBooleanWin;
             end;
             14 : litIncertVariab(imax);
             15 : for i := 0 to pred(imax) do begin
                 j := indexConstExp[i];
                 if j=grandeurInconnue
                    then readln(fichier)
                    else readlnNombreWin(incertConst[j]);
             end;
             16 : litLigneWin; // Nom Fichier AVI
             17 : readln(fichier);
             18 : for i := 0 to pred(NbreVariabTexte) do begin
                      iVar := indexVariabTexte[i];
                      for j := 0 to pred(nmes) do
                          texteVar[iVar,j] := litLigneWin;
             end;
             19 : for i := 0 to pred(imax) do begin
                      j := indexConstTexte[i];
                      litLigneWin;
                      if j<>grandeurInconnue then begin
                            texteConst[j] := ligneWin;
                            valeurConst[j] := 0;
                      end;
             end;
             20 : for i := 1 to imax do readlnNombreWin(incertParam[i]);
             21 : for i := 1 to imax do readlnNombreWin(incert95Param[i]);
             22 : litEquivalence(imax);
             23 : for i := 0 to pred(imax) do begin
                      j := indexConstGlb[i];
                      if j=grandeurInconnue
                        then readln(fichier)
                        else readlnNombreWin(grandeurs[j].incertCourante);
             end;
             24 : begin
                 for i := 1 to imax do begin
                     litLigneWin;
                     TexteModeleP.add(ligneWin);
                 end;
                 FGrapheVariab.ModelePagesIndependantesMenu.Checked := true;
                 ModelePagesIndependantes := true;                 
             end;
             25 : begin
                readln(fichier,debutFFT);
                readln(fichier,finFFT);
             end;
             26 : begin
                readln(fichier,iVar);
             end;
             27 : litValeurRepere(imax);
        end;
        if ligneWinaLire then litLigneWin;
     end; // while
     if ModeAcquisition=AcqSimulation then begin
          MiniSimulation := valeurVar[0,0];
          MaxiSimulation := valeurVar[0,pred(nmes)];
          VerifMinMaxReal(MiniSimulation,MaxiSimulation);
          if MiniSimulation=MaxiSimulation then
             MaxiSimulation := MiniSimulation+1;
          valeurVar[0,pred(nmes)] := MaxiSimulation;
          MaxiSimulation := MaxiSimulation+(maxiSimulation-miniSimulation)/pred(nmes);
          valeurVar[0,0] := MiniSimulation;
      end;
   end
   else litLigneWin;
end; // litPage

procedure litListe;

procedure litMemoGrandeurs(imax : integer);

Procedure VerifBoucle;
var LigneMaj : string;

Function VerifN(strN : string) : boolean;
var posN : integer;
begin
    posN := pos(strN,ligneMaj);
    result := posN>10;
    if result then begin
       delete(ligneWin,posN,length(strN));
       insert(' '+grandeurs[cNombre].nom,ligneWin,posN)
    end;
end;

begin
     LigneMaj := UpperCase(LigneWin);
     if copy(ligneMaj,1,4)='FOR ' then begin
         if verifN(' NOMBRE') then exit;
         if verifN(' NBRE') then exit;
         if verifN(' NPOINTS') then exit;
         if verifN(' N') then exit;
     end;
end;

var i : integer;
begin
     for i := 1 to imax do begin
         VerifBoucle;
         Fvaleurs.Memo.lines.add(ligneWin);
         litLigneWin;
     end;
     Fvaleurs.Memo.lines.Add('');
end;

procedure litNom(Agenre : TgenreGrandeur;imax : integer);
var i : integer;
begin
     for i := 1 to imax do begin
         AjouteExperimentale(ligneWin,Agenre);
         litLigneWin
     end;
end;

procedure litNomConstTexte(imax : integer);
var i : integer;
    index : integer;
begin
     for i := 1 to imax do begin
         index := AjouteExperimentale(ligneWin,constante);
         grandeurs[index].fonct.genreC := g_texte;
         litLigneWin
     end;
     construireIndex;
end;

procedure litNomParam(genre : TgenreGrandeur;imax : integer;affecte : boolean);
var i : integer;
begin
     if affecte then NbreParam[genre] := imax;
     for i := 1 to imax do begin
         parametres[genre,i].nom := ligneWin;
         litLigneWin
     end;
end;

procedure litGenreVariab(Nbrei : integer);
var i,iTexte : integer;
    genre : TgenreCalcul;
begin
     iTexte := -1;
     for i := 0 to pred(Nbrei) do begin
         genre := TgenreCalcul(StrToInt(ligneWin));
         if genre<>g_experimentale then begin
            grandeurs[i].fonct.genreC := genre;
            dec(NbreVariabExp);
         end;
         if genre=g_texte then begin
            inc(NbreVariabTexte);
            iTexte := i;
         end;
         litLigneWin
     end;
     if (iTexte>0) and  (iTexte<NbreVariabExp) then begin
        transfereVariabE(iTexte,NbreVariabExp);
     // mettre g_texte � la fin
     end;
     construireIndex;
end;

procedure litUnite(iDebut,Nbrei : integer);
var i : integer;
begin
     for i := iDebut to pred(iDebut+Nbrei) do begin
         grandeurs[i].NomUnite := ligneWin;
         litLigneWin
     end;
end;

procedure litAff(iDebut,Nbrei : integer);
var i : integer;
begin
     for i := iDebut to pred(iDebut+Nbrei) do begin
         grandeurs[i].affSignif := ligneWin<>'0';
         litLigneWin
     end;
end;

procedure litMeca(Nbre : integer);
var i : integer;
    k : integer;
    nomPos : string;
begin
     for i := 0 to pred(Nbre) do with grandeurs[i] do begin
         k := strToInt(ligneWin[1]);
         genreMecanique := TgenreMecanique(k);
         if genreMecanique>gm_position then begin
            nomVarPosition := copy(ligneWin,3,length(ligneWin));
            k := indexNom(nomPos);
            if k=grandeurInconnue then nomvarPosition := '';
         end;
         litLigneWin
     end;
end;

procedure litUniteParam(imax : integer);
var i : integer;
begin
     for i := 1 to imax do begin
         parametres[paramNormal,i].NomUnite := ligneWin;
         litLigneWin
     end;
end;

procedure litValeurParamGlb(imax : integer);
var i : integer;
begin
     for i := 1 to imax do begin
         Parametres[paramGlb,i].valeurCourante := StrToFloatWin(ligneWin);
         litLigneWin
     end;
end;

procedure litSignif(iDebut,Nbrei : integer);
var i : integer;
begin
     for i := iDebut to pred(iDebut+Nbrei) do begin
         grandeurs[i].fonct.expression := ligneWin;
         litLigneWin
     end;
end;

procedure litPrecision(iDebut,Nbrei : integer);
var i : integer;
begin
     for i := iDebut to pred(iDebut+Nbrei) do begin
         try
         grandeurs[i].precisionU := strToInt(ligneWin);
         except
         end;
         litLigneWin
     end;
end;

procedure litFormat(iDebut,Nbrei : integer);
var i : integer;
begin
     for i := iDebut to pred(iDebut+Nbrei) do begin
         grandeurs[i].formatU := TnombreFormat(strToInt(ligneWin));
         litLigneWin
     end;
end;

procedure litIncertitude(Nbrei : integer);
var i : integer;
    posErreur,LongErreur : integer;
begin
     for i := 0 to pred(Nbrei) do begin
         grandeurs[i].IncertCalc.expression := ligneWin;
         grandeurs[i].compileIncertitude(i,posErreur,LongErreur);
         litLigneWin;
     end;
end;

Procedure litAcquisition(Nligne : byte);
var i : TmodeAcquisition;
    j : byte;
begin
   ModeAcquisition := AcqCan;
   LigneWin := AnsiUpperCase(ligneWin);
   for i := low(TmodeAcquisition) to high(TmodeAcquisition) do
       if ligneWin=NomModeAcq[i] then begin
          ModeAcquisition := i;
          break;
       end;
   if Nligne>=2 then begin
      litLigneWin;
   end;
   for j := 3 to Nligne do litLigneWin;
   litLigneWin;
end;

procedure litModeleGlb(imax : integer);
var i : integer;
begin
    GrapheConstOpen;
    FgrapheParam.MajModelePermis := false;
    FgrapheParam.MemoModele.Clear;
    for i := 0 to pred(imax) do begin
         FgrapheParam.MemoModele.Lines.Add(ligneWin);
         litLigneWin;
    end;
    FgrapheParam.MajModelePermis := true;
end;

procedure litStrList(T : TstringList;imax : integer);
var i : integer;
begin
     for i := 1 to imax do begin
         T.add(ligneWin);
         litLigneWin;
     end;
end;

Procedure litChoix(choix : integer);
var i : integer;
    Nligne : integer;
begin
    Nligne := NbreLigneWin(ligneWin);
    litLigneWin;
    case choix of
         0 : begin
            for i := 1 to Nligne do litLigneWin;
            while (Length(LigneWin)>0) and (ligneWin[1]=symbReg2) do begin
               Nligne := NbreLigneWin(ligneWin);
               for i := 0 to Nligne do litLigneWin;
            end;
         end;
         1 : litPage;
         2 : FgrapheVariab.litConfig;
         3 : begin
            FourierOpen;
            FgrapheFFT.litConfig;
         end;
         4 : litNom(variable,Nligne);
         5 : litNom(constante,Nligne);
         6 : litUnite(NbreVariab,Nligne);
         7 : litUnite(0,Nligne);
         8 : litIncertitude(Nligne);
         9 : begin
            GrapheConstOpen;
            FgrapheParam.litConfig;
         end;
         10 : LitMemoGrandeurs(Nligne);
         12 : LitStrList(TexteModele,Nligne);
         13 : LitModeleGlb(Nligne);
         14 : LitAcquisition(Nligne);
         15 : begin
            StatistiqueOpen;
            FgrapheStat.litConfig;
         end;
         16 : begin
             Fvaleurs.logBox.checked := ligneWin<>'0';
             logSimulation := ligneWin<>'0';
             litLigneWin;
         end;
         17 : begin
             AngleEnDegre := ligneWin<>'0';
             litLigneWin;
         end;
         18 : litGenreVariab(Nligne);
         19 : litSignif(NbreVariab,Nligne);
         20 : litSignif(0,Nligne);
         22 : litFormat(NbreVariab,Nligne);
         23 : litFormat(0,Nligne);
         24 : litPrecision(NbreVariab,Nligne);
         25 : litPrecision(0,Nligne);
         26 : litNomParam(paramNormal,Nligne,true);
         28 : litUniteParam(Nligne);
         29 : begin
             uniteSIGlb := ligneWin<>'0';
             litLigneWin;
         end;
         30 : litNomParam(paramNormal,Nligne,false);
         31 : begin
             litLigneWin;
             litLigneWin;
         end;
         32 : litNomParam(paramGlb,Nligne,true);
         33 : litValeurParamGlb(Nligne);
         34 : litAff(NbreVariab,Nligne);
         35 : litAff(0,Nligne);
         36 : litMeca(Nligne);
         38 : begin
             Fvaleurs.PagesIndependantesCB.checked := ligneWin<>'0';
             litLigneWin;
         end;
         39 : litNomConstTexte(Nligne);
         40 : litSignif(NbreVariab+NbreConstExp,Nligne);
         42 : begin
             avecEllipse := ligneWin<>'0';
             litLigneWin;
         end;
         43 : begin
             nomGrandeurTri := ligneWin;
             litLigneWin;
         end;
         44 : begin
             FichierTrie := ligneWin<>'0';
             litLigneWin;
         end;
         45 : Fvaleurs.litConfig;
         46 : begin
             avecChi2 := ligneWin<>'0';
             litLigneWin;
         end;
         47 : begin
              stoechBecherGlb := strToInt(ligneWin);
              litLigneWin;
              stoechBuretteGlb := strToInt(ligneWin);
              litLigneWin;
         end;
         48 : begin
            StatistiqueOpen;
            FgrapheStat.litConfig;
         end;
         49 : litNom(constanteGlb,Nligne);
   end;
end;

begin
    if ligneWin='PAGESINDEP' then ligneWin := 'CBPAGESINDEP';
    if Pos('PAGE',ligneWin)=4 then litChoix(1)
    else if Pos('GRAPHE',ligneWin)=4 then begin
       if Pos('CONST',ligneWin)<>0
          then litChoix(9)
          else litChoix(2);
    end
    else if Pos('FOURIER',ligneWin)=4 then litChoix(3)
    else if Pos('STATISTIQUE',ligneWin)=4 then litChoix(48)
    else if Pos('NOM',ligneWin)<>0 then begin
        if Pos('CONST',ligneWin)<>0 then litChoix(5)
        else if Pos('VAR',ligneWin)<>0 then litChoix(4)
        else if Pos('PARAM',ligneWin)<>0 then litChoix(26)
        else if Pos('SAUVE',ligneWin)<>0 then litChoix(30)
        else if Pos('GLB',ligneWin)<>0 then litChoix(32)
        else if Pos('TC',ligneWin)<>0 then litChoix(39) // texte const
        else litChoix(0)
    end
    else if (Pos('MECA',ligneWin)<>0) and
       (Pos('VAR',ligneWin)<>0) then litChoix(36)
    else if (Pos('GENRE',ligneWin)<>0) and
       (Pos('VAR',ligneWin)<>0) then litChoix(18)
    else if Pos('UNITE',ligneWin)<>0 then begin
       if Pos('CONST',ligneWin)<>0 then litChoix(6)
       else if Pos('VAR',ligneWin)<>0 then litChoix(7)
       else if Pos('PARAM',ligneWin)<>0 then litChoix(28);
    end
    else if Pos('FORMAT',ligneWin)<>0 then begin
       if Pos('CONST',ligneWin)<>0 then litChoix(22)
       else if Pos('VAR',ligneWin)<>0 then litChoix(23)
       else litChoix(0)
    end
    else if Pos('AFF',ligneWin)<>0 then begin
       if Pos('CONST',ligneWin)<>0 then litChoix(34)
       else if Pos('VAR',ligneWin)<>0 then litChoix(35)
       else litChoix(0)
    end
    else if Pos('PRECISION',ligneWin)<>0 then begin
       if Pos('CONST',ligneWin)<>0 then litChoix(24)
       else if Pos('VAR',ligneWin)<>0 then litChoix(25)
       else litChoix(0)
    end
    else if Pos('SIGNIF',ligneWin)<>0 then begin
       if Pos('CONST',ligneWin)<>0 then litChoix(19)
       else if Pos('VAR',ligneWin)<>0 then litChoix(20)
       else if Pos('TC',ligneWin)<>0 then litChoix(40) { texte const }
       else litChoix(0)
    end
    else if Pos('INCERTITUDE',ligneWin)<>0 then litChoix(8)
    else if Pos('GRANDEURS',ligneWin)<>0 then begin
       if Pos('MEMO',ligneWin)<>0
          then litChoix(10)
          else litChoix(0)
    end
    else if Pos('MODELISATION',ligneWin)<>0 then litChoix(12)
    else if Pos('TRIGO',ligneWin)<>0 then litChoix(17)
    else if Pos('CHI2',ligneWin)<>0 then litChoix(46)
    else if Pos('ELLIPSE',ligneWin)<>0 then litChoix(42)
    else if Pos('uniteSI',ligneWin)<>0 then litChoix(29)
    else if Pos('TRIDATA',ligneWin)<>0 then litChoix(44)
    else if Pos('MODELEGLB',ligneWin)<>0 then litChoix(13)
    else if Pos('ACQUISITION',ligneWin)<>0 then litChoix(14)
    else if Pos('STATISTIQUE',ligneWin)=4 then litChoix(15)
    else if Pos('LOGBOX',ligneWin)=4 then litChoix(16)
    else if Pos('CBPAGESINDEP',ligneWin)=4 then litChoix(38)
    else if Pos('DATETIME',ligneWin)=4 then litChoix(31)
    else if pos('VALEUR PARAM GLB',ligneWin)<>0 then litChoix(33)
    else if pos('GrandeurTri',ligneWin)<>0 then litChoix(43)
    else if pos('FenetreTxt',ligneWin)<>0 then litChoix(45)
    else if pos('DOSAGE',ligneWin)<>0 then litChoix(47)
    else litChoix(0)
end; // litListe

procedure litFichierRW3;
begin
     litLigneWin;
     FichierTrie := dataTrieGlb;
     while (Length(LigneWin)>0) and (ligneWin[1]=symbReg) do
            litListe;
     if pageCourante>0 then begin
        LitFichierWin := true;
        pages[pageCourante].affecteConstParam;
        pages[pageCourante].affecteVariableP(false);
     end;
end;

var i,j : integer;
    index : integer;
    trouve : boolean;
    p : codePage;
begin // LitFichierWin
     LectureFichier := true;
     result := false;
     codeErreurF := erFileData;
     ModeAcquisition := AcqClavier;
     FileMode := fmOpenRead;
     FGrapheVariab.ModelePagesIndependantesMenu.Checked := false;
     ModelePagesIndependantes := false;
     AssignFile(fichier,NomFichierData);
     Reset(fichier);
     finFichierWin := false;
     try
     if not enTeteRW3 then begin
            afficheErreur(erFormat,0);
            CloseFile(fichier);
            LectureFichier := false;
            exit
     end;
     litFichierRW3;
     except
         codeErreurF := erReadFile;
         result := false;
     end;
     CloseFile(fichier);
     FileMode := fmOpenReadWrite;
     for i := pred(NbreVariabTexte) downto 0 do begin
         index := indexVariabTexte[i];
         trouve := false;
         for p := 1 to NbrePages do
             trouve := trouve or (pages[p].FtexteVar[index]<>nil);
         if not trouve then begin
            grandeurs[index].fonct.genreC := g_forward;
            for j := succ(i) to pred(NbreVariabTexte) do
                indexVariabTexte[j-1] := indexVariabTexte[j];
            dec(NbreVariabTexte);
         end;
     end;
     LectureFichier := false;
end; // LitFichierWin

Procedure TFregressiMain.AjouteFichier(NomFichier : string);
var FichierOK : boolean;
    DecodeVariab,DecodeConst : array[codeGrandeur] of codeGrandeur;
    NomFichierCourt : string;
    NbreVariabFichier,NbreVariabExpFichier : integer;

procedure litGenreVariab(Nbrei : integer);
var i : integer;
    genre : TgenreCalcul;
begin
     NbreVariabExpFichier := 0;
     for i := 0 to pred(Nbrei) do begin
         litLigneWin;
         genre := TgenreCalcul(StrToInt(ligneWin));
         if genre=g_experimentale then begin
            inc(NbreVariabExpFichier);
         end;
     end;
     litLigneWin
end;

procedure litExpression(imax : integer);
var i,posEgal,k : integer;
    nom : string;
begin
    for i := 0 to pred(imax) do begin
        litLigneWin;
        PosEgal := Pos('=',ligneWin);
        if PosEgal>0 then begin
           nom := copy(ligneWin,1,posEgal-1);
           PosEgal := Pos('[',ligneWin);
           if PosEgal>0 then
              nom := copy(nom,1,posEgal-1);
           PosEgal := Pos('''',ligneWin);
           if PosEgal>0 then
              nom := copy(nom,1,posEgal-1);
           k := indexNom(nom);
           if k=grandeurInconnue then
              Fvaleurs.Memo.lines.add(ligneWin);
        end
        else Fvaleurs.Memo.lines.add(ligneWin); // commentaire
    end;
    litLigneWin;
end;

procedure litPage;

Procedure litValeur(imax : integer);
var i,j : integer;
begin with pages[pageCourante] do begin
    if imax>MaxMaxVecteur
       then nmes := MaxMaxVecteur
       else nmes := imax;
    j := 0;
    try
    while j<nmes do begin
        for i := 0 to pred(NbreVariabExpFichier) do
            read(fichier,valeurVar[decodeVariab[i],j]);
        litLigneWin;
     { TODO : mettre � 1 les variables non d�finies dans le fichier }
        inc(j);
    end;
    j := nmes;
    while j<imax do begin
        litLigneWin;
        inc(j);
    end;
    except
        nmes := j;
    end;
end end;

Procedure litValeurRepere(imax : integer);
var i,posTab : integer;
    valeur : double;
    texte : string;
begin
    for i := 1 to imax do begin
        readln(fichier,texte);
        posTab := pos(crTab,texte);
        valeur := strToFloatWin(copy(texte,1,pred(posTab)));
        texte := copy(texte,succ(posTab),length(texte)-posTab);
        pages[pageCourante].ajouteRepere(valeur,texte);
    end;
end;

var i,j,imax,choix,iVar : integer;
begin
   if ajoutePage then with pages[pageCourante] do begin
      commentaireP := nomFichierCourt+ligneWin;
      litLigneWin;
      while (Length(LigneWin)>0) and (ligneWin[1]=symbReg2) do begin
        imax := NbreLigneWin(ligneWin);
        choix := 0;
        if pos('VALEUR',ligneWin)<>0 then begin
           if pos('VAR',ligneWin)<>0 then choix := 1 else
           if pos('CONST',ligneWin)<>0 then choix := 2 else
           if pos('PARAM',ligneWin)<>0 then choix := 5;
        end else
        if pos('TEXTE',ligneWin)<>0 then begin
           if pos('VAR',ligneWin)<>0 then choix := 3 else
           if pos('CONST',ligneWin)<>0 then choix := 4;
        end else
        if pos('REPERE',ligneWin)<>0 then choix := 7;
        case choix of
             0 : for i := 1 to imax do litLigneWin;
             1 : litValeur(imax);
             2 : for i := 0 to pred(imax) do begin
                 j := decodeConst[i];
                 if j=grandeurInconnue
                    then readln(fichier)
                    else readlnNombreWin(ValeurConst[j]);
             end;
             3 : for i := 0 to pred(NbreVariabTexte) do begin
                      iVar := indexVariabTexte[i];
                      for j := 0 to pred(imax) do
                          texteVar[iVar,j] := litLigneWin;
             end;
             4 : for i := 0 to pred(imax) do begin
                     j := indexConstTexte[i];
                     litLigneWin;
                     if j<>grandeurInconnue then
                        texteConst[j] := ligneWin;
             end;
             5 : for i := 1 to imax do readlnNombreWin(valeurParam[paramNormal,i]);
             7 : litValeurRepere(imax);
        end;
        litLigneWin;
     end;
   end
   else litLigneWin
end; // litPage 

Procedure litListe;

Procedure LitVariab(N : integer);
var i,j,k,kc,p : integer;
    nomCourant : string;
begin
  NbreVariabFichier := N;
  NbreVariabExpFichier := N; // par d�faut avant lecture du genre
  for i := 0 to pred(NbreVariabFichier) do begin
        nomCourant := litLigneWin;
        k := indexNom(nomCourant);
        if (i<NbreVariabExp) and
           (k=grandeurInconnue) then begin
           if FusionDlg=nil then
              Application.CreateForm(TFusionDlg, FusionDlg);
           FusionDlg.nom := nomCourant;
           case FusionDlg.showModal of
               idYes  : begin // nouvelle colonne exp�rimentale
                   k := AjouteExperimentale(NomCourant,variable);
                   for p := 1 to pageCourante do with pages[p] do
                       for j := 0 to nmes - 1 do
                           valeurVar[k,j] := 1;
               end;
               idNo : begin // premi�re colonne libre
                 k := indexNom(FusionDlg.nomCB.text);
                 if k=grandeurInconnue then begin
                    kc := 0;
                    for j := 0 to pred(i) do
                        if decodeVariab[j]=kc then
                         repeat inc(kc)
                         until (grandeurs[kc].fonct.genreC=g_experimentale) or
                           (grandeurs[kc].genreG=variable) or
                           (kc=NbreGrandeurs);
                    if (grandeurs[kc].fonct.genreC=g_experimentale) or
                       (grandeurs[kc].genreG=variable) then k := kc;
                 end;
             end;// idNo
             idCancel : begin
                 afficheErreur(erFileAdd,0);
                 FichierOK := false;
                 exit;
             end;
           end;// case
        end;
        if (i<NbreVariabExp) and
           ((k=grandeurInconnue) or
            (grandeurs[k].fonct.genreC<>g_experimentale) or
            (grandeurs[k].genreG<>variable)) then begin
              afficheErreur(erFileAdd,0);
              FichierOK := false;
              exit;
        end;
        if (i<NbreVariabExp) then DecodeVariab[i] := k;
  end;
  litLigneWin;
end;

Procedure LitConst(NbreConstFichier : integer);
var i,k : integer;
begin
    for i := 0 to pred(NbreConstFichier) do begin
        litLigneWin;
        k := indexNom(ligneWin);
        if k=grandeurInconnue then
           k := ajouteExperimentale(ligneWin, constante);
        decodeConst[i] := k;
        if (i<NbreConstExp) and
           ((k=grandeurInconnue) or
           ((grandeurs[k].fonct.genreC<>g_experimentale) or
            (grandeurs[k].genreG<>constante))) then begin
                 afficheErreur(erFileAdd,0);
                 FichierOK := false;
                 exit;
        end;
  end;
  litLigneWin;
end;

var
    Nligne,i : integer;
begin
    Nligne := NbreLigneWin(ligneWin);
    if pos('NOM CONST',ligneWin)<>0
        then LitConst(Nligne)
        else if pos('NOM VAR',ligneWin)<>0
        then LitVariab(Nligne)
        else if pos('GENRE VAR',ligneWin)<>0
        then LitGenreVariab(Nligne)
        else if pos('MEMO GRANDEURS',ligneWin)<>0
        then litExpression(Nligne)
        else if pos('PAGE',ligneWin)<>0
        then begin
            litLigneWin;
            litPage  
        end
        else begin
           for i := 0 to Nligne do litLigneWin;
           while (Length(LigneWin)>0) and (ligneWin[1]=symbReg2) do begin
               Nligne := NbreLigneWin(ligneWin);
               for i := 0 to Nligne do litLigneWin;
            end;
       end;
end; // litListe

begin
     FileMode := fmOpenRead;
     AssignFile(Fichier,nomFichier);
     Reset(Fichier);
     finFichierWin := false;
     litLigneWin;
     if pos('REGRESSI WINDOWS',ligneWin)=0
          then AfficheErreur(erFormat,0)
          else begin
              litLigneWin;
              nomFichierCourt := extractFileName(NomFichier)+' : ';
              FichierOK := true;
              while fichierOK and
                   (ligneWin<>'') and
                   (ligneWin[1]=symbReg) do litListe;
          end;
     closeFile(fichier);
     FileMode := fmOpenReadWrite;     
end; // AjouteFichier

procedure TFRegressiMain.LitCalcul(FileName : string);
var termine : boolean;

Function LitCalculWin : boolean;

Procedure litListe;
var DecodeConst : array[codeGrandeur] of codeGrandeur;

procedure litMemoGrandeurs(imax : integer);
var i : integer;
    posEgal : integer;
    nomAjout : string;
    Ajoute : boolean;
begin
     Fvaleurs.Memo.lines.Add('''Ajout');
     for i := 1 to imax do begin
         litLigneWin;
         posEgal := pos('=',ligneWin);
         Ajoute := posEgal=0;
         if not Ajoute then begin
             nomAjout := copy(ligneWin,1,pred(posEgal));
             Ajoute := indexNom(nomAjout)=grandeurInconnue;
         end;
         if ajoute then Fvaleurs.Memo.lines.add(ligneWin);
     end;
     Fvaleurs.Memo.lines.Add('');
     LitLigneWin;
end;

procedure litModele(imax : integer);
var i : integer;
begin
    FgrapheVariab.MemoModele.Clear;
    TexteModele.Clear;
    for i := 0 to pred(imax) do begin
         litLigneWin;
         TexteModele.add(ligneWin);
         FgrapheVariab.MemoModele.Lines.Add(ligneWin);
    end;
    litLigneWin;
end;

procedure litModeleGlb(imax : integer);
var i : integer;
begin
    GrapheConstOpen;
    FgrapheParam.MemoModele.Clear;
    for i := 0 to pred(imax) do begin
         litLigneWin;
         FgrapheParam.MemoModele.Lines.Add(ligneWin);
    end;
    litLigneWin;
end;

Procedure LitVariab(N : integer);
var i,k : integer;
begin
  if N<NbreVariabExp then begin
     afficheErreur(erFileCalc,0);
     result := false;
     exit;
  end;
  for i := 0 to pred(N) do begin
        litLigneWin;
        k := indexNom(ligneWin);
        if (i<NbreVariabExp) and
           ((k=grandeurInconnue) or
           ((grandeurs[k].fonct.genreC<>g_experimentale) or
            (grandeurs[k].genreG<>variable))) then begin
              afficheErreur(erFileCalc,0);
              result := false;
              exit;
        end;
  end;
  litLigneWin;
end;

Procedure LitConst(NbreConstFichier : integer);
var i,k : integer;
begin
    for i := 0 to pred(NbreConstFichier) do begin
        litLigneWin;
        k := indexNom(ligneWin);
        if k=grandeurInconnue then
           k := ajouteExperimentale(ligneWin, constante);
        decodeConst[i] := k;
        if (i<NbreConstExp) and
           ((k=grandeurInconnue) or
           ((grandeurs[k].fonct.genreC<>g_experimentale) or
            (grandeurs[k].genreG<>constante))) then begin
                 afficheErreur(erFileCalc,0);
                 result := false;
                 exit;
        end;
  end;
  litLigneWin;
end;

procedure litValeurConst;
var i,j,imax : integer;
begin with pages[pageCourante] do begin
    litLigneWin;
    litLigneWin;
    while (Length(LigneWin)>0) and (ligneWin[1]=symbReg2) do begin
        imax := NbreLigneWin(ligneWin);
        if (pos('VALEUR',ligneWin)<>0) and (pos('CONST',ligneWin)<>0)
           then begin
              for i := 0 to pred(imax) do begin
                 j := decodeConst[i];
                 try
                    readlnNombreWin(ValeurConst[j]);
                 except
                    ValeurConst[j] := 0;
                 end;
              end;
              pages[pageCourante].affecteConstParam;
              break;
           end
           else for i := 0 to imax do litLigneWin;
        end;
   end; // while
end;

var
    Nligne,i : integer;
begin
    Nligne := NbreLigneWin(ligneWin);
    if pos('NOM CONST',ligneWin)<>0
        then LitConst(Nligne)
        else if pos('NOM VAR',ligneWin)<>0
        then LitVariab(Nligne)
        else if pos('MEMO GRANDEURS',ligneWin)<>0
        then LitMemoGrandeurs(Nligne)
        else if pos('MODELISATION',ligneWin)<>0
        then LitModele(Nligne)
        else if pos('MODELEGLB',ligneWin)<>0
        then LitModeleGlb(Nligne)
        else if pos('PAGE',ligneWin)<>0
        then begin
           LitValeurConst;
           termine := true;
           exit;
        end
        else begin
           for i := 0 to Nligne do litLigneWin;
           while (Length(LigneWin)>0) and (ligneWin[1]=symbReg2) do begin
               Nligne := NbreLigneWin(ligneWin);
               for i := 0 to Nligne do litLigneWin;
           end;
       end;
end; // litListe

begin // LitCalculWin
     FileMode := fmOpenRead;
     AssignFile(Fichier,FileName);
     Reset(Fichier);
     finFichierWin := false;
     result := false;
     termine := false;
     try
     litLigneWin;
     if pos('REGRESSI WINDOWS',ligneWin)=0
          then AfficheErreur(erFormat,0)
          else begin
              litLigneWin;
              result := true;
              repeat
                 if (length(ligneWin)>0) and (ligneWin[1]=symbReg)
                    then litListe
                    else litLigneWin;
              until termine or (ligneWin='') or not result;
          end;
     closeFile(fichier);
     except
         CloseFile(fichier);
         result := false;
         AfficheErreur(erReadFile,0);
     end;
     FileMode := fmOpenReadWrite;
end; // LitCalculWin

begin
if pageCourante=0 then begin
   nomFichierData := FileName;
   FinOuvertureFichier(LitFichierWin);
end
else begin try
if FileExists(FileName)
  then begin
     Screen.cursor := crHourGlass;
     if litCalculWin then begin
          FValeurs.MajBtnClick(nil);
          FgrapheVariab.Perform(WM_Reg_Maj,MajFichier,0);
     end;
  end
  else afficheErreur(erFileNotExist+' : '+FileName,0);
  except
  end;
  Screen.cursor := crDefault;
end end; // LitCalcul

Procedure TFRegressiMain.SauveEtatCourant;
var p : codePage;
begin
     if (pageCourante=0) or LectureFichier then exit;
     try // pb si r�pertoire interdit en �criture
     EcritFichierXML(NomFichierSauvegarde);
     HeureSauvegarde := now;
     NbreGrandeursSauvees := NbreGrandeurs;
     for p := 1 to NbrePages do
         pages[p].NbrePointsSauves := pages[p].nmes
     except
     end;    
end;

Procedure TFRegressiMain.VerifSauvegarde;
begin
    if (now-HeureSauvegarde)>IntervalleSauvegarde then sauveEtatCourant;
end;


