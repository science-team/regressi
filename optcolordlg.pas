{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit optcolordlg;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, Spin, ExtCtrls, SysUtils, Dialogs, ComCtrls, ColorBox,
  regutil, compile, graphker, aidekey;

type
  TOptionCouleurDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TButton;
    HelpBtn: TButton;
    PageControl1: TPageControl;
    PageTS: TTabSheet;
    ImagePoint: TImageList;
    ImageLigne: TImageList;
    ModeleTS: TTabSheet;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    CourbeTS: TTabSheet;
    Label3: TLabel;
    ResetBtn: TButton;
    Label4: TLabel;
    CourbeSE: TSpinEdit;
    AxeColorCombo: TColorBox;
    CouleurComboCourbe: TColorBox;
    CouleurComboPage: TColorBox;
    ColorBox1: TColorBox;
    ColorBox2: TColorBox;
    ColorBox3: TColorBox;
    ColorBox4: TColorBox;
    ColorBox5: TColorBox;
    ColorBox6: TColorBox;
    ColorBox7: TColorBox;
    ColorBox8: TColorBox;
    LigneComboPage: TComboBox;
    PointComboPage: TComboBox;
    PointComboCourbe: TComboBox;
    TabSheet1: TTabSheet;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    TangenteColor: TColorBox;
    ReticuleColor: TColorBox;
    TangenteCB: TComboBox;
    ReticuleCB: TComboBox;
    PageSE: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    LabelWidth: TLabel;
    WidthEcranSE: TSpinEdit;
    ReperePageRG: TRadioGroup;
    GroupBox1: TGroupBox;
    FondColor: TColorBox;
    Label11: TLabel;
    FondReticuleColor: TColorBox;
    Label14: TLabel;
    penWidthAxeSE: TSpinEdit;
    Label15: TLabel;
    TailleTickSE: TSpinEdit;
    procedure FormActivate(Sender: TObject);
    procedure SetModif(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure PointComboPageClick(Sender: TObject);
    procedure LigneComboPageClick(Sender: TObject);
    procedure CouleurComboPageClick(Sender: TObject);
    procedure ResetBtnClick(Sender: TObject);
    procedure ColorModeleCBClick(Sender: TObject);
    procedure PointComboCourbeClick(Sender: TObject);
    procedure CouleurComboCourbeClick(Sender: TObject);
    procedure CourbeSEClick(Sender: TObject);
    procedure PageSEChange(Sender: TObject);
  private
  public
    DlgGraphique : TgrapheReg;
    Modification : boolean;
  end;

var
  OptionCouleurDlg: TOptionCouleurDlg;

implementation

uses graphvar;

  {$R *.lfm}

procedure TOptionCouleurDlg.FormActivate(Sender: TObject);
var i : integer;
begin
     inherited;
     PageSE.MaxValue := MaxPages;
     PageSE.Value := 1;
     PageSEChange(Sender);
     CourbeSE.Value := 1;
     CourbeSEClick(Sender);
     TangenteColor.selected := pColorTangente;
     TangenteCB.itemIndex := ord(PstyleTangente);
     ReticuleColor.selected := pColorReticule;
     FondReticuleColor.selected := FondReticule;
     FondColor.selected := FondReticule;
     ReticuleCB.itemIndex := ord(PstyleReticule);
     widthEcranSE.value := penWidthVGA;
     penWidthAxeSE.value := penWidthGrid;
     tailleTickSE.value := tailleTick;
     AxeColorCombo.selected := couleurGrille;
     Modification := false;
     reperePageRG.itemIndex := ord(reperePage);
     with ModeleTS do for i := 0 to pred(ControlCount) do
         if controls[i] is TcolorBox
            then with controls[i] as TcolorBox do
                 selected := CouleurModele[tag]
end;

procedure TOptionCouleurDlg.SetModif(Sender: TObject);
begin
     Modification := true
end;

procedure TOptionCouleurDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(HELP_OptionsGraphe)
end;

procedure TOptionCouleurDlg.OKBtnClick(Sender: TObject);
var c,g : integer;
begin
    CouleurGrille := AxeColorCombo.selected;
    pColorTangente := TangenteColor.selected;
    PstyleTangente := TpenStyle(TangenteCB.itemIndex);
    pColorReticule := ReticuleColor.selected;
    FondReticule := FondReticuleColor.selected;
    FondReticule := FondColor.selected;
    PstyleReticule := TpenStyle(ReticuleCB.itemIndex);
    penWidthVGA := widthEcranSE.value;
    penWidthGrid := penwidthaxeSE.value;
    tailleTick := tailleTickSE.value;
    reperePage := TreperePage(reperePageRG.itemIndex);
    if FgrapheVariab=nil then exit;
    for g := 1 to 3 do with FgrapheVariab.graphes[g] do begin
        for c := 1 to MaxOrdonnee do with coordonnee[c] do begin
           couleur := couleurInit[c];
           motif := motifInit[c];
        end;
        if paintBox.visible then FgrapheVariab.setCoordonnee(g);
    end;
end;

procedure TOptionCouleurDlg.PointComboPageClick(Sender: TObject);
begin
     MotifPages[PageSE.value] := Tmotif(PointComboPage.itemIndex)
end;

procedure TOptionCouleurDlg.LigneComboPageClick(Sender: TObject);
begin
     stylePages[PageSE.value] := TpenStyle(LigneComboPage.itemIndex)
end;

procedure TOptionCouleurDlg.CouleurComboPageClick(Sender: TObject);
begin
     CouleurPages[PageSE.value] := CouleurComboPage.selected;
end;

procedure TOptionCouleurDlg.ResetBtnClick(Sender: TObject);
var i,j : integer;
begin
     for i := 0 to pred(NbreCouleur) do begin
         j := (i mod 4) +1;
         CouleurPages[i] := couleurInit[j];
         MotifPages[i] := motifInit[j];
     end;
end;

procedure TOptionCouleurDlg.ColorModeleCBClick(Sender: TObject);
begin
     with sender as TcolorBox do
        CouleurModele[tag] := selected
end;

procedure TOptionCouleurDlg.PointComboCourbeClick(Sender: TObject);
begin
     MotifInit[CourbeSE.value] := Tmotif(PointComboCourbe.itemIndex);
     if DlgGraphique<>nil then
        DlgGraphique.coordonnee[CourbeSE.value].motif := Tmotif(PointComboCourbe.itemIndex);
     Modification := true;
end;

procedure TOptionCouleurDlg.CouleurComboCourbeClick(Sender: TObject);
begin
     CouleurInit[CourbeSE.value] := CouleurComboCourbe.selected;
     if DlgGraphique<>nil then
        DlgGraphique.coordonnee[CourbeSE.value].couleur := CouleurComboCourbe.selected;
     Modification := true;
end;

procedure TOptionCouleurDlg.CourbeSEClick(Sender: TObject);
var index : integer;
begin
     index := CourbeSE.value;
     CouleurComboCourbe.selected := couleurInit[index];
     PointComboCourbe.itemIndex := ord(motifInit[index]);
end;

procedure TOptionCouleurDlg.PageSEChange(Sender: TObject);
var index : integer;
begin
     index := PageSE.value;
     CouleurComboPage.selected := couleurPages[index];
     LigneComboPage.itemIndex := ord(stylePages[index]);
     PointComboPage.itemIndex := ord(motifPages[index]);
end;

end.
