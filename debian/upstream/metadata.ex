# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/regressi/issues
# Bug-Submit: https://github.com/<user>/regressi/issues/new
# Changelog: https://github.com/<user>/regressi/blob/master/CHANGES
# Documentation: https://github.com/<user>/regressi/wiki
# Repository-Browse: https://github.com/<user>/regressi
# Repository: https://github.com/<user>/regressi.git
