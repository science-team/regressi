program Regressi;

  {$MODE Delphi}

uses
  Interfaces,
  Forms,
  constreg in 'constreg.pas',
  maths in 'maths.pas',
  regutil in 'regutil.pas',
  uniteker in 'uniteker.pas',
  fft in 'fft.pas',
  statcalc in 'statcalc.pas',
  tdiClass in 'tdiclass.pas',
  tdiConst in 'tdiconst.pas',
  systdiff in 'systdiff.pas',
  regmain in 'regmain.pas' {FRegressiMain},
  optcolordlg in 'optcolordlg.pas' {OptionCouleurDlg},
  optfft in 'optfft.pas' {OptionsFFTDlg},
  supprdlg in 'supprdlg.pas' {SuppressionDlg},
  lecttext in 'lecttext.pas' {LectureTexteDlg},
  optline in 'optline.pas' {OptionLigneDlg},
  zoomman in 'zoomman.pas' {ZoomManuelDlg},
  newvar in 'newvar.pas' {NewVarDlg},
  graphker in 'graphker.pas',
  statisti in 'statisti.pas' {Fstatistique},
  filerrr in 'filerrr.pas',
  curmodel in 'curmodel.pas' {CurseurModeleDlg},
  options in 'options.pas' {OptionsDlg},
  modelegr in 'modelegr.pas',
  choixtang in 'choixtang.pas' {ChoixTangenteDlg},
  graphpar in 'graphpar.pas' {fgrapheParam},
  graphvar in 'graphvar.pas' {FgrapheVariab},
  cursdata in 'cursdata.pas' {ReticuleDataDlg},
  pagecopy in 'pagecopy.pas' {PageCopyDlg},
  selpage in 'selpage.pas' {SelectPageDlg},
  pagecalc in 'pagecalc.pas' {PageCalcDlg},
  varkeyb in 'varkeyb.pas' {NewClavierDlg},
  regprint in 'regprint.pas' {PrintDlg},
  identPages in 'identPages.pas' {ChoixIdentPagesDlg},
  statopt in 'statopt.pas' {StatOptDlg},
  modif in 'modif.pas' {ModifDlg},
  valeurs in 'valeurs.pas' {FValeurs},
  choixmodeleGlb in 'choixmodeleGlb.pas' {ChoixModeleGlbDlg},
  coordphys in 'coordphys.pas' {FcoordonneesPhys},
  affectenom in 'affectenom.pas' {AffecteNomDlg},
  saveparam in 'saveparam.pas' {SaveParamDlg},
  optionsvitesse in 'optionsvitesse.pas' {OptionsVitesseDlg},
  regdde in 'regdde.pas' {FormDDE},
  selparam in 'selparam.pas' {selParamDlg},
  origineu in 'origineu.pas' {OrigineDlg},
  choixmodele in 'choixmodele.pas' {ChoixModeleDlg},
  optModele in 'optModele.pas' {OptionModeleDlg},
  compile in 'compile.pas',
  addPageExp in 'addPageExp.pas' {AddPageExpDlg},
  saveHarm in 'saveHarm.pas' {SaveHarmoniqueDlg},
  constante in 'constante.pas' {ConstanteUnivDlg},
  fspec in 'fspec.pas',
  savePosition in 'savePosition.pas' {SavePositionDlg},
  SysInfoCtrls in 'SysInfoCtrls.pas',
  PropCourbe in 'PropCourbe.pas' {PropCourbeForm},
  about in 'about.pas' {AboutBox},
  graphfft in 'graphfft.pas' {FGrapheFFT},
  Latex in 'Latex.pas',
  latexreg in 'latexreg.pas' {LatexDlg},
  aideKey in 'aideKey.pas',
  OptionsAffModeleU in 'OptionsAffModeleU.pas' {OptionsAffModeleDlg},
  FusionU in 'FusionU.pas' {FusionDlg},
  selColonne in 'selColonne.pas' {SelectColonneDlg},
  pageechant in 'pageechant.pas' {PageEchantillonDlg},
  savemodele in 'savemodele.pas' {SaveModeleDlg},
  pagedistrib in 'pagedistrib.pas' {PageDistribDlg};

{$R *.res}

begin
  Application.Scaled:=True;
  Application.Title := 'Regressi';
  Application.initialize;
  Application.CreateForm(TFRegressiMain, FRegressiMain);
  Application.CreateForm(TOptionsDlg, OptionsDlg);
  Application.CreateForm(TFormDDE, FormDDE);
  Application.CreateForm(TChoixModeleDlg, ChoixModeleDlg);
  Application.CreateForm(TConstanteUnivDlg, ConstanteUnivDlg);
  Application.CreateForm(TFcoordonneesPhys, FcoordonneesPhys);
  Application.Run;
end.
