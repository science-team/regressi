{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit identPages;

  {$MODE Delphi}

interface

uses SysUtils, Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, ExtCtrls, CheckLst, regutil, compile;

type
  TChoixIdentPagesDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    CommentaireCB: TCheckBox;
    GroupBox1: TGroupBox;
    ListeConstBox: TCheckListBox;
    NewIdentPageCB: TCheckBox;
  private
  public
    procedure InitParam;
  end;

var
  ChoixIdentPagesDlg: TChoixIdentPagesDlg;

implementation

  {$R *.lfm}

procedure TChoixIdentPagesDlg.InitParam;
var i : integer;
    sauve : TstringList;
begin with ListeConstBox do begin
     sauve := TstringList.create;
     for i := 0 to pred(count) do
         if checked[i] then
            sauve.add(items[i]);
     Items.Clear;
     for i := 0 to pred(NbreConst) do
         Items.add(grandeurs[indexConst[i]].nom);
     for i := 1 to NbreParam[paramNormal] do
         Items.add(Parametres[paramNormal,i].nom);
     for i := 0 to pred(count) do
         checked[i] := sauve.IndexOf(items[i])>=0;
     sauve.Free;
end end;

end.
