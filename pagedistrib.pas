{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit pageDistrib;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, ExtCtrls,Dialogs, sysutils,
  maths, regutil, fft, compile, aideKey, CheckLst;

type
  TPageDistribDlg = class(TForm)
    LabelY: TLabel;
    LabelX: TLabel;
    Panel1: TPanel;
    HelpBtn: TBitBtn;
    CancelBtn: TBitBtn;
    OKBtn: TBitBtn;
    ParamListBox: TCheckListBox;
    Label1: TLabel;
    procedure OKBtnClick(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
  public
  end;

var
  PageDistribDlg: TPageDistribDlg;

implementation

  {$R *.lfm}

procedure TPageDistribDlg.OKBtnClick(Sender: TObject);
var index,indexC,index0,indexNew,j,k : integer;
    nbrePagesCrees : integer;
    pageCopiee,NbrePagesInit : codePage;
begin
     NbrePagesInit := nbrePages;
     for pageCopiee := 1 to NbrePagesInit do begin
     NbrePagesCrees := NbreVariab;
     for j := 0 to pred(ParamListBox.Items.count) do
         if ParamListBox.checked[j]
            then dec(NbrePagesCrees);
     if NbrePagesCrees=NbreVariab then begin
        ParamListBox.checked[0] := true;
        dec(NbrePagesCrees);
     end;
     index := 0;
     indexNew := ajouteEXperimentale('XXX',variable);
     for j := 1 to NbrePagesCrees do begin
         if not AjoutePage then exit;
         while ParamListBox.checked[index] do inc(index); // la grandeur à garder
         with pages[nbrePages] do begin
           nmes := pages[pageCopiee].nmes;
           for k := 0 to pred(ParamListBox.Items.count) do
               if ParamListBox.checked[k]
                  then begin
                       index0 := indexVariab[k];
                       copyVecteur(valeurVar[index0],pages[pageCopiee].valeurVar[index0]);
                  end
                  else if k=index then begin
                       copyVecteur(valeurVar[indexNew],pages[pageCopiee].valeurVar[index]);
                  end;
           commentaireP := pages[pageCopiee].commentaireP+' grandeur='+grandeurs[index].nom;
           for k := 0 to pred(NbreConst) do begin
              indexC := indexConst[k];
              valeurConst[indexC] := pages[pageCopiee].valeurConst[indexC];
           end;
           inc(index);
        end;
     end;
     end;
     for j := pred(ParamListBox.count) downto 0 do begin
         if not ParamListBox.checked[j] then
            LibereGrandeurE(indexVariab[j]);
     end;
     Application.MainForm.perform(WM_Reg_Maj,MajAjoutPage,0);
     ModifFichier := true;
end;

procedure TPageDistribDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(HELP_PageCopiee)
end;

procedure TPageDistribDlg.FormActivate(Sender: TObject);
var j,index : integer;
begin
       inherited;
       ParamListBox.clear;
       for j := 0 to pred(NbreVariab) do begin
           index := indexVariab[j];
           ParamListBox.Items.Add(grandeurs[index].nom);
       end;
       ParamListBox.checked[0] := true;
end;


end.
