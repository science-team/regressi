{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit graphker;

  {$MODE Delphi}

interface

uses SysUtils, Graphics, Classes, Printers,
  Forms, Controls, Types,
  Dialogs, clipBrd, stdctrls, extctrls,
  Math, grids, fft, constreg, maths, regutil, uniteker, compile,
//  Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc,
  curmodel, latex, identPages;

const
  ta_left = 1;
  ta_right = 8;
  ta_top = 2;
  ta_center = 4;
  ta_bottom = 16;
  ta_baseline = 32;
  decalageModeleGlb = 5;
  MaxOrdonnee   = 6;
  MaxVecteurPoly = 2048;
  NbreDataEquiv = 10;
  MaxMonde      = 10;
  indexSpline   = -MaxIntervalles - 1;
  indexSinc     = MaxIntervalles + 1;
  indexLisse    = MaxIntervalles + 2;
  MondeVitesse  = 1;
  MondeAcc      = 2;
  NbreHarmoniqueAffMax = 32;
  couleurCurseurs: array[1..5] of tColor = (clRed,clRed,clRed,clBlue,clBlue);
  curseurData1  = 4;
  curseurData2  = 5;
  maxCurseur    = 5;
  penWidthReticule = 2;
  intSansSignif = -128;

type
  indexCurseur = 1..maxCurseur;
  TreperePage     = (SPstyle, SPmotif, SPcouleur);
  Tmotif          = (mCroix, mCroixDiag, mCercle, mCarre, mLosange,
    mCerclePlein, mCarrePlein, mIncert, mEchantillon, mHisto,
    mSpectre, mPixel, mReticule, mBarreD, mBarreG, mLigneH);

// etoile, triangle, pentagone, asterisque, losangePlein, pentagonePlein, trianglePlein
  Tidentification = (identNone, identCoord, identDroite, identRaie);
  TetatModele     = (ModeleConstruit, ModeleLineaire, ModeleDefini,
    SimulationDefinie, AjustementGraphique, UniteParamDefinie, PasDeModele);
  setEtatModele   = set of TetatModele;
  indiceOrdonnee  = 1..MaxOrdonnee;
  Tcurseur        = (curSelect, curTexte, curLigne, curEfface,
    curReticule, curReticuleData, curEquivalence, curOrigine,
    curModele, curZoomAv, curModeleGr, curBornes, curMove, curReticuleModele,
    curXMini, curXMaxi,curYMini, curYMaxi);
  TcurseurFrequence = (curSelectF, curTexteF, curReticuleF,
    curZoomF, curMoveF, curFmaxi, curFmaxiSon, curLigneF);
  TcurseurTemps   = (curNormal, curDebut, curFin, curDeplace);

const
  XmaxWMF          = 16000;
  YmaxWMF          = 9000; //16 cm x 9 cm en 0.01mm
  XmaxBitmap       = 800;
  YmaxBitmap       = 450; // 16/9
  TaillePoliceBitmap = 28;
  MargeCurseur     = 5;
  LongNombreAxe    = 6;
  LongNombreAxeLog = 4;
  NbreCouleur      = 17;
  mondeX           = 0;
  mondeY           = 1;
  mondeDroit       = 2;
  mondeSans        = 3;
  NbreGradMin      = 5;
  NbreGradMax      = 2 * NbreGradMin;
  NbreGradMaxMan   = 2 * NbreGradMax;
  coCadre          = 8;
  DimBorne         = 8;
  BrushCouleurSelect = clRed;
  BrushCouleurZoom = clSilver;
  CouleurBande     = clGreen;
  CouleurAxeX      = clBlack;
  MotifLatex : array[Tmotif] of string = ('+','x','o','square','diamond',
    '*','square*','star','asterik','pentagon','triangle','','','','','');

var
  CouleurGrille:      Tcolor = clMedGray;
  CouleurTexte:       Tcolor = clBlue;
  CouleurLigne:       Tcolor = clBlue;
  ImprimanteMonochrome: boolean = false;
  OrdreLissage:       integer = 3;
  reperePage:         TReperePage = SPcouleur;
  UseFaussesCouleurs: boolean = True;
  DimPointVGA:        integer = 3; // dimension du point en pixel VGA
  PStyleTangente:     TpenStyle = psSolid;
  PStyleReticule:     TpenStyle = psSolid;
  PStyleRepere:       TpenStyle = psDot;
  PcolorTangente:     Tcolor = clBlack;
  PcolorReticule:     Tcolor = clBlack;
  PcolorRepere:       Tcolor = clBlue;
  FondReticule:       Tcolor = clInfoBk;
  CouleurVitesseImposee: boolean = True;
  FichierSon : string;
  FreqEchWave : integer;

  CouleurPages: array[0..MaxPages] of
    Tcolor = (clGray, clBlue, clRed, clOlive, clNavy, clPurple, clTeal, clBlack,
      clMaroon, clFuchsia, clBlue, clGray, clAqua, clTeal, clBlue, clRed,
      clGray, clBlue, clRed, clOlive, clNavy, clPurple, clTeal, clBlack,
      clMaroon, clFuchsia, clBlue, clGray, clAqua, clTeal, clBlue, clRed,
      clGreen);
  CouleurModele: array[-MaxIntervalles..MaxIntervalles] of
    Tcolor = (clTeal, clMaroon, clFuchsia, clNavy, clWhite, clBlue, clRed, clPurple, clGreen);
  CouleurMecanique: array[1..2] of Tcolor = (clBlue, clRed);
  StylePages: array[0..MaxPages] of
    TpenStyle = (psSolid, psDash, psDot, psDashDot, psDashDotDot,
      psSolid, psDash, psDot, psDashDot, psDashDotDot,
      psSolid, psDash, psDot, psDashDot, psDashDotDot,
      psSolid, psDash, psDot, psDashDot, psDashDotDot,
      psSolid, psDash, psDot, psDashDot, psDashDotDot,
      psSolid, psDash, psDot, psDashDot, psDashDotDot,
      psSolid, psDash, psDot);
  MotifPages: array[0..MaxPages] of
    Tmotif = (mCroix, mCroixDiag, mCercle, mCarre, mLosange,mCerclePlein, mCarrePlein,
      mCroix, mCroixDiag, mCercle, mCarre, mLosange, mCerclePlein, mCarrePlein,
      mCroix, mCroixDiag, mCercle, mCarre, mLosange, mCerclePlein, mCarrePlein,
      mCroix, mCroixDiag, mCercle, mCarre, mLosange, mCerclePlein, mCarrePlein,
      mCroix, mCroixDiag, mCercle, mCarre, mLosange);

  CouleurInit: array[indiceOrdonnee] of
    Tcolor = (clBlack, clBlue, clRed, clPurple, clGreen, clNavy);
  MotifInit: array[indiceOrdonnee] of
    Tmotif = (mCroix, mCroixDiag, mCercle, mCarre, mLosange, mCerclePlein);
  NomCurseur: array[curZoomAv..curBornes] of
  string = ('Zoom', 'Modèle graphique', 'Bornes');
  IconCurseur: array[Tcurseur] of integer = (27, 2, 1, 0, 3, 30, 4, 27, 29, 16, 5, 7, 0, 41,
  56,55,58,57); // 0 à définir

type
  TmodifGraphe       = (gmEchelle, gmOptions, gmXY, gmValeurs,
                        gmModele, gmPage, gmValeurModele);
  TmotifTexte        = (mtNone, mtCroix, mtFleche, mtVert, mtHoriz,
                        mtLigneRappel,mtDoubleFleche);
  TselectDessin      = (sdNone, sdCadre, sdPoint1, sdPoint2,
                        sdCentre, sdHoriz, sdVert);
  TselectEquivalence = (seNone, sePente1, sePoint1, sePoint2,
    sePointEq, sePente2, sePenteEq);
  TtexteLigne        = (tlNone, tlPente, tlEquation, tlEcartY, tlEcartX);
  TsetModifGraphe    = set of TmodifGraphe;
  TcurseurStat       = (crsSelect, crsTexte, crsEfface);
  ToptionGraphe      = (OgQuadrillage, OgPolaire, OgOrthonorme, OgMemeZero,
                        OgPseudo3D, OgAnalyseurLogique, OgMemeEchelle, OgZeroGradue);
  ToptionModele      = (OmEchelle, OmExtrapole, OmManuel);

  TsetOptionGraphe = set of ToptionGraphe;
  TsetOptionModele = set of ToptionModele;
  Ttrace           = (trLigne, trPoint, trPixel,
    trResidus, trCouples, trStat, trVitesse, trAcceleration, trTexte, trSonagramme);
  TtraceXML        = (trLigneX, trPointX, trResidusX, trStatX,
                      trVitesseX, trAccelerationX, trSonagrammeX);

  TsetTrace        = set of Ttrace;
  TsetTraceXML     = set of TtraceXML;
  Tligne           = (LiDroite, LiModele, LiSpline, LiSinc);
  indiceMonde      = mondeX..MaxMonde;
  TsetOptionCurseur = set of ToptionCurseur;

  TcurseurDonnees = record
// 1 à 3 réticule ; 4 et 5 curseur données
    xc, yc, ic:      integer;
    xr, yr:          double;
    grandeurCurseur: tGrandeur;
    indexCourbe:     integer;
    mondeC:          indiceMonde;
  end;

  TCoordonnee = record
    nomX, nomY, nomZ:   string;
    codeX, codeY, codeZ: integer;
    iMondeC:      indiceMonde;
    iCourbe:      integer;
    Trace:        TsetTrace;
    Ligne:        Tligne;
    couleur:      Tcolor;
    styleT:       TpenStyle;
    Motif:        Tmotif;
  end;

  TListeY = array[indiceOrdonnee] of Tcoordonnee;

  TgrapheReg = class;

  Tdessin = class
    x1, y1, x2, y2: double;
    x1i, y1i, x2i, y2i: integer;
    nomX,nomY : string;
    iMonde:       indiceMonde;
    LigneMax, LigneMin : integer;
    cadre:        Trect;
    hauteur:      integer;
    souligne, avecCadre: boolean;
    centre, vertical: boolean;
    Alignement:   integer;
    pen:          Tpen;
    Identification: Tidentification;
    paramModeleBrut : boolean;
    isTexte, avecLigneRappel: boolean;
    isOpaque, isTitre: boolean;
    couleurFond:  Tcolor;
    MotifTexte:   TmotifTexte;
    MotifCourbe: Tmotif;
    TexteLigne:   TtexteLigne;
    Texte:        TStringList;
    NumPage:      integer;
    Deplacable:   boolean;
// false pour les lignes ajoutés par le logiciel type statistique
    sousDessin:   Tdessin;
    proprietaire: Tdessin;
    repere : Trepere;
    TexteLoc: TStringList;
    NumeroPage: codePage;
    Agraphe: TgrapheReg;
// si identification=identCoord, numCoord indique la grandeur à identifier
// et numPoint indique le numéro du point sur lequel pointe la flèche
    NumCoord:      integer;
    NumPoint:     integer;
    constructor Create(gr : TgrapheReg);
    function NbreData: integer;
    procedure Draw;
    procedure DrawLatex;
    procedure AffectePosition(x, y: integer;
      PosDessin: TselectDessin; Shift: TShiftState);
    function LitOption(grapheC: TgrapheReg): boolean;
    procedure SetPourCent(var i: integer);
    procedure Load;
    procedure Store;
//    procedure LoadXML(ANode : IXMLNode);
//    procedure StoreXML(ANode : IXMLNode);
    destructor Destroy; override;
  end;

  TlisteDessin = class(TList)
  protected
    function Get(Index: integer): Tdessin;
    procedure Put(Index: integer; Item: Tdessin);
  public
    function Add(Item: Tdessin): integer;
    function Remove(Item: Tdessin): integer;
    property Items[Index: integer]: Tdessin Read Get Write Put; default;
    procedure Clear; override;
  end;

  Tequivalence = class(Tdessin)
    pente, ve, phe: double;
    vei, phei:   integer;
    modelisee:    boolean;
    varY:         Tgrandeur;
    mondepH:      indiceMonde;
    ligneRappel: TligneRappel;
    indexModele:  integer;
    commentaire:  string;
    constructor Create(v1, ph1, v2, ph2, v, ph, p: double;gr : TgrapheReg);
    constructor CreateVide(gr: TgrapheReg);
    procedure Draw;
    procedure DrawFugitif;
    Procedure DrawLatex(var latexStr : TstringList);
    procedure Assign(Aequivalence: Tequivalence);
    procedure SetValeurEquivalence(i: integer);
    function Modif(x, y: integer;selectEq: TselectEquivalence): boolean;
    procedure Load;
    procedure Store;
//    procedure LoadXML(ANode : IXMLNode);
//    procedure StoreXML(ANode : IXMLNode;p : integer);
  end;

  TlisteEquivalence = class(TList)
  protected
    function Get(Index: integer): Tequivalence;
    procedure Put(Index: integer; Item: Tequivalence);
  public
    function Add(Item: Tequivalence): integer;
    function Remove(Item: Tequivalence): integer;
    property Items[Index: integer]: Tequivalence Read Get Write Put; default;
    procedure Clear; override;
  end;

  Tmonde = class
    minDefaut, maxDefaut: double;
    minInt,maxInt: integer;
    mini, maxi:   double;
    horizontal:   boolean;
    miniOrtho, maxiOrtho: double;
    MinidB:  double;
    a, b: double; // entier = réel*a+b
    az,bz: double; // idem pour zoom
    ecartBit:     integer; // écart entre deux tracés de bit
    valeurBit:    integer; // valeur pour bit=1
    nombreBits:   integer;
    Graduation:   Tgraduation;
    defini, zeroInclus, autoTick: boolean;
    Axe:          tgrandeur;
    deltaAxe, exposant, pointDebut: double;
    // deltaAxe = écart entre graduations sur les axes
    // Exposant=puissance de 10 multiple de 3
    Nticks, TickDebut: integer;
    // Nticks nombre de graduations sans valeur affichée 
    graduationPiActive: boolean;
    NbreChiffres,NbreDecimal: integer;
    TitreAxe:     TstringList;
    xTitre, yTitre: integer;
    courbeBottom, courbeTop: integer;
    formatDuree:  string;
    Arrondi:      double;
    Orthonorme, Polaire: boolean;
    constructor Create;
    procedure setDelta(grandAxe: boolean);
    procedure reset(IsPolaire: boolean);
    procedure affecteMinMax(x: double);
    function Trouve(V: vecteur; Debut, Fin: integer): boolean;
    function affecteEntier(r: double): integer;
    procedure setDefaut;
    function FormatMonde(x: double): string;
    destructor Destroy; override;
    procedure Assign(Amonde: Tmonde);
    procedure SetMinMaxDefaut(min, max: double);
    procedure SetZoom(y0, zoom : integer);
  end;

  TmondeVecteur = class
    maxi:      double;
    ax, ay:    double; // entier = réel*a
    defini, vitesse, echelleTrace: boolean;
    Dimension: integer;
    echelle:   double;
    MondeOrd:  indiceMonde;
    constructor Create;
    procedure Reset;
    procedure Trouve(Vx, Vy: vecteur; Nbre: integer);
    procedure GetEntier(x, y: double; var xi, yi: integer);
    procedure GetPolaire(r, teta, rp, tetap, rs, tetas: double; var x, y: double);
  end;

  TCourbe = class
    valX, valY, incertX, incertY: vecteur;
    valXder, valYder, valXder2, valYder2: vecteur;
    texteC:      TStringList;
    couleurTexte : Tcolor;
    trace:       TsetTrace;
    DebutC,finC: integer;
    iMondeC:     indiceMonde;
    varX, varY:  tGrandeur;
    varXder, varYder: tGrandeur;
    varDer:      array[1..2] of tGrandeur;
    Couleur:     Tcolor;
    StyleT:      TpenStyle;
    Motif:       Tmotif;
    IndexModele, IndexBande: shortInt;
    dimPointC,penWidthC : integer;

    Adetruire, DetruireIncert, CourbeExp, PortraitDePhase,
    ModeleParametrique, ModeleParamX1: boolean;
    Page:        integer;
    valeurEff, valeurMoy, valeurMin, valeurMax: double;
    periode:     double;
    decalage:    integer;
    PointSelect: set of byte;
    constructor Create(AX, AY: vecteur; D, F: integer;
      VX, VY: tgrandeur);
    procedure setStyle(Acouleur: Tcolor; Astyle: TpenStyle; Amotif: Tmotif);
    procedure calcul;
    destructor Destroy; override;
  end;

  TtraceDefaut = (tdLigne, tdPoint, tdLissage);
  TstyleBorne  = (bsDebut, bsFin, bsDebutVert, bsFinVert, bsAucune);

  Tborne = record
    Xb, Yb: integer;
    IndexModele, IndexCourbe: integer;
    style:  TstyleBorne;
  end;

  TlisteCourbe = class(TList)
  protected
    function Get(Index: integer): Tcourbe;
    procedure Put(Index: integer; Item: Tcourbe);
  public
    function Add(Item: Tcourbe): integer;
    function Remove(Item: Tcourbe): integer;
    property Items[Index: integer]: Tcourbe Read Get Write Put; default;
    procedure Clear; override;
  end;

  TgrapheReg = class
    alignement : integer;
    canvas:        Tcanvas;
    paintBox:      TpaintBox;
    panel:         Tpanel;
    coordonnee:    TlisteY;
    dimPoint:      integer;
    sansAxeX,sansAxeY : boolean;
    avecAxeX, avecAxeY: boolean;
    grandAxeX, grandAxeY: boolean;
    superposePage, empilePage: boolean;
    mondeDebut:    indiceMonde;
    mondeDerivee:  indiceMonde;
    monde:         array[indiceMonde] of Tmonde;
    mondeVecteur:  array[1..2] of TmondeVecteur;
    courbes:       TlisteCourbe;
    Dessins:       TlisteDessin;
    Equivalences:  array[codePage] of TlisteEquivalence;
    EquivalenceCourante, EquivalenceTampon: Tequivalence;
    indexCourbeEquivalence: integer;
    LimiteCourbe, LimiteFenetre: Trect;
    BorneFenetre: Trect;
    LargCarac, HautCarac, Marge: integer;
    CurseurOsc:   array[1..5] of TcurseurDonnees;
    CurReticuleDataActif, curReticuleModeleActif, withDebutFin: boolean;
    CurseurDebut:  Tpoint;
    UnitePente:    Tunite;
    grapheOK, verifierOrtho, verifierInv: boolean;
    modif:         TsetModifGraphe;
    OptionGraphe:  TsetOptionGraphe;
    OptionModele:  TsetOptionModele;
    StatusSegment: array[0..1] of TStringList;
    AutoTick, useDefaut, useDefautX, GraduationZeroX, GraduationZeroY,
    FilDeFer:      boolean;
    NbreOrdonnee:  integer;
    penWidth:      integer;
    MiniTemps, MaxiTemps: double;
    PasPoint:      integer;
    avecBorne:     boolean;
    margeHautBas:  integer;
    axeYBas, axeYpositif: boolean;
    margeCaracY:   integer;
    bitmapReg:     Tbitmap;
    UnAxeX:        boolean;
    miniMod, maxiMod: double;
    indexHisto:    integer;
    DessinCourant : Tdessin;
    posDessinCourant : TselectDessin;
    indexReticuleModele : integer;
    grapheParam : boolean;
    cCourant: integer;
    labelYcurseur: Tlabel;
    optionCurseur:   TsetOptionCurseur;
    valeurCurseur:   array[ToptionCurseur] of double;
    borneVisible : boolean; // sinon points de modélisation
    withResidusStudent : boolean;
    identificationPages : boolean;
    zeroPolaire : double;
    constructor Create;
    function basCourbe: integer;
    function hautCourbe: integer;
    procedure Draw;
    procedure TraceCourbe(index: integer);
    procedure TraceFilDeFer;
    procedure Reset;
    procedure ResetEquivalence;
    procedure WindowXY(xr, yr: double;m : indiceMonde; var xi, yi: integer);
    procedure WindowRT(xr, yr: double; m: indiceMonde; var xi, yi: integer);
    procedure RTversXY(var xr, yr: double; m: indiceMonde);
    function WindowX(xr: double): integer;
    procedure MondeXY(xi, yi: integer; m: indiceMonde; var xr, yr: double);
    procedure MondeRT(xi, yi: integer; m: indiceMonde; var xr, yr: double);
    procedure ChercheMonde;
    procedure SetMonde(m: indiceMonde; minX, minY, maxX, maxY: double);
    procedure AjoutePoint(m: indiceMonde; XX, YY: double);
    procedure DrawAxis(initialisation: boolean);
    procedure TraceTitreAxe(g: Tgrandeur; m: indiceMonde;
      indexCourbe: integer);
    procedure VersJpeg(NomFichier : string);
    procedure VersBitmap(NomFichier : string);
    procedure VersFichier(NomFichier : string);
    procedure VersPressePapier(grapheCB : TgrapheClipBoard);
    procedure VersImprimante(HauteurGr: double; var Bas: integer);
    procedure ChercheDroite(x, y, pente: double; m: indiceMonde;
      var x1, y1, x2, y2: integer);
    destructor Destroy; override;
    procedure AffecteZoomAvant(rect: Trect; avecY: boolean);
    procedure AffecteZoomArriere;
    procedure AffecteCentreRect(x, y: integer; var r: Trect);
    procedure SetDessinCourant(x, y: integer);
    procedure GetEquivalence(x, y: integer;
      var Aequivalence: Tequivalence; var selectEq: TselectEquivalence);
    procedure TraceDroite(x, y, pente: double; xMin, yMin, xMax, Ymax: double);
    procedure AjouteEquivalence(i: integer; effaceDouble: boolean);
    procedure RemplitTableauEquivalence;
    procedure TraceCurseur(i: indexCurseur);
    procedure TraceMotif(xi, yi: integer; motif: Tmotif);
    procedure TracePente;
    Procedure SetRepere;
    function GetReticuleModeleProche(x, y: integer) : boolean;
    procedure GetCurseurProche(x, y: integer; force: boolean);
    function GetBorne(x, y: integer; var Borne: Tborne): boolean;
    procedure SetBorne(x, y: integer; var Borne: Tborne);
    procedure InitReticule(x, y: integer);
    procedure SetSegmentInt(x, y: integer);
    procedure SetSegmentReal(i: integer);
    procedure SetReticuleModele(x, y: integer;selonX,selonY : boolean);
    procedure SetStatusReticuleData(var Agrid: TstringGrid);
    function PointProche(var x, y: integer; indexCourbe: integer;
      limite, Xseul: boolean): integer;
    function PointProcheModele(var x, y: integer): integer;
    function PointProcheModeleX(var x, y: integer): integer;
    function PointProcheModeleY(var x, y: integer): integer;
    function MondeProche(x, y: integer): indiceMonde;
    function CourbeProche(var x, y: integer): integer;
    function CourbeModeleProche(var x, y: integer): integer;
    function SupprReticule(x, y: integer): boolean;
    procedure TraceReticule(index: indexCurseur);
    procedure TraceEcart;
    function PointProcheReal(x, y: double; Acourbe: Tcourbe;
      m: indiceMonde): integer;
    function isAxe(x, y: integer): integer;
    function isAxeX(x, y: integer; var adroite : boolean): boolean;
    function isAxeY(x, y: integer; var enhaut : boolean): boolean;
    function AjouteCourbe(AX, AY: vecteur; Am: indiceMonde;
      Nbre: integer; grandeurX, grandeurY: Tgrandeur;
      Apage: integer): Tcourbe;
    procedure EncadreTitre(active: boolean);
    procedure RectangleGr(Arect: Trect);
    procedure LineGr(Arect: Trect);
    procedure MajNomGr(index: codeGrandeur);
    function AjusteMonde: boolean;
    procedure SetUnitePente(iMonde: indiceMonde);
    procedure traceBorne(xi, yi: integer;
      style: TstyleBorne; couleur: Tcolor; indexM: integer);
    procedure traceBorneFFT;
    procedure setCourbeDerivee;
    procedure resetCourbeDerivee;
    procedure chercheMinMax;
    procedure changeEchelleX(xnew, xold: integer;ismaxi : boolean);
    procedure changeEchelleY(ynew, yold: integer;isMaxi : boolean);
    procedure CreateSolidBrush(couleur : Tcolor);
    procedure createHatchBrush(aCode : TBrushStyle;couleur : Tcolor);
    procedure CreatePen(aStyle : TpenStyle;aWidth  : integer;couleur : Tcolor);
    procedure SetTextAlign(aligne : integer);
    procedure MyTextOut(x,y : integer;S : string);
    procedure MyTextOutFond(x,y : integer;S : string;couleur : Tcolor);
    procedure Segment(x1, y1, x2, y2: integer);
    procedure VersLatex(NomFichier : string;suffixe : char);
    procedure getIndexSonagramme(t,f : double;var it,ifreq : integer;var freq : double);
    procedure TraceIdentPages;
  end;

  TtransfertGraphe = class
    Grad:       array[indiceMonde] of Tgraduation;
    Zero:       array[indiceMonde] of boolean;
    MinidB:     array[indiceMonde] of double;
    Trace:      array[indiceOrdonnee] of TsetTrace;
    Ligne:      array[indiceOrdonnee] of Tligne;
    nomX, nomY: array[indiceOrdonnee] of string;
    iMonde:     array[indiceOrdonnee] of indiceMonde;
    Couleur:    array[indiceOrdonnee] of Tcolor;
    Style:      array[indiceOrdonnee] of TpenStyle;
    Motif:      array[indiceOrdonnee] of Tmotif;
    optionGr:   TsetOptionGraphe;
    optionModele:  TsetOptionModele;
    SuperposePage:  boolean;
    ZeroPolaire : double;
    FilDeFer:   boolean;
    UseDefaut, UseDefautX: boolean;
    PasPoint:   integer;
    AutoTick:   boolean;
    procedure AssignEntree(const Agraphe: TgrapheReg);
    procedure AssignSortie(var Agraphe: TgrapheReg);
    Procedure Ecrit(numero : integer);
    Procedure Lit;
    constructor Create;
  end;

function GetFausseCouleur(Niveau: double): Tcolor; // Niveau entre 0 et 1
function GetCouleurPages(index: integer): Tcolor;
function TriEquivalence(Item1, Item2: Pointer): integer;
function GetCouleurPale(color1: Tcolor): Tcolor;
function CouleurLatex(color : Tcolor) : string;
function TraceToXML(trace : TsetTrace) : TsetTraceXML;
function TraceFromXML(trace : TsetTraceXML) : TsetTrace;

var
  MotifIdent: TmotifTexte = mtFleche;
  hauteurIdent:      integer = 3;
  avecCadreIdent: boolean = false;
  isOpaqueIdent: boolean = false;
  couleurFondIdent:  Tcolor = clWhite;
  tailleEcran,largeurEcran : integer;
  penWidthGrid : integer = 1;
  tailleTick : integer = 1;
  LongueurTangente : real = 0.33;
  NbreVecteurVitesseMax: integer = 32;
  NbreTexteMax:       integer = 32;
  EchelleVecteur:     double = 0.06;
  ProlongeVecteur:    boolean = False;
  ProjeteVecteur:     boolean = False;
  TraceDefaut:        TtraceDefaut = tdPoint;
  DecadeDB:           integer = 2;
  UseSelect:          boolean = False;
  DecalageFFT:        integer = 2;
  PrecisionFFT:       integer = 10;   // pour mille
  CoeffDecalage:      integer = 1;
  CoeffSIpente:       double = 1;
  OptionGrapheDefault: TsetOptionGraphe = [];
  ImprimanteEnCours:  boolean = False;
  ImprimanteMono:     boolean = False;
  ImageEnCours:       boolean = False;
  verifierLog:        boolean = False;
  penWidthVGA:        integer = 1;

  AffichagePrecision: boolean = false;
  ModeleNumerique:    boolean = false;
  AffCoeffElarg:      boolean = false;

implementation

uses lectText, optLine, valeurs;

function TraceToXML(trace : TsetTrace) : TsetTraceXML;
begin
    result := [];
    if trLigne in trace then include(result,trLigneX);
    if trPoint in trace then include(result,trPointX);
    if trResidus in trace then include(result,trResidusX);
    if trStat in trace then include(result,trStatX);
    if trVitesse in trace then include(result,trVitesseX);
    if trAcceleration in trace then include(result,trAccelerationX);
    if trSonagramme in trace then include(result,trSonagrammeX);
end;

function TraceFromXML(trace : TsetTraceXML) : TsetTrace;
begin
    result := [];
    if trLigneX in trace then include(result,trLigne);
    if trPointX in trace then include(result,trPoint);
    if trResidusX in trace then include(result,trResidus);
    if trStatX in trace then include(result,trStat);
    if trVitesseX in trace then include(result,trVitesse);
    if trAccelerationX in trace then include(result,trAcceleration);
    if trSonagrammeX in trace then include(result,trSonagramme);
end;


function GetCouleurPale(color1: Tcolor): Tcolor;
begin
  case color1 of
    clBlack: Result := clGray;
    clMaroon: Result := clPurple;
    clGreen: Result := clTeal;
    clOlive: Result := clYellow;
    clNavy: Result := clBlue;
    clPurple: Result := clRed;
    clTeal: Result := clLime;
    clGray: Result := clMedGray;
    clRed: Result  := clFuchsia;
    clLime: Result := clMoneyGreen;
    clYellow: Result := clCream;
    clBlue: Result := clAqua;
    clAqua: Result := clSkyBlue;
    clWhite: Result := clWhite;
    clMedGray: Result := clSilver;
    else begin
    end;
  end;
end;

function CouleurLatex(color: Tcolor): string;
begin
  case color of
    clBlack: Result := 'black';
    clMaroon: Result := 'brown';
    clGreen: Result := 'green';
    clOlive: Result := 'olive';
    clNavy: Result := 'blue';
    clPurple: Result := 'red';
    clTeal: Result := 'teal';
    clGray: Result := 'gray';
    clSilver: Result := 'lightgray';
    clRed: Result  := 'red';
    clLime: Result := 'lime';
    clYellow: Result := 'yellow';
    clBlue: Result := 'blue';
    clFuchsia: Result := 'purple';
    clAqua: Result := 'blue';
    clWhite: Result := 'white';
    clMoneyGreen: Result := 'green';
    clSkyBlue: Result := 'blue';
    clCream: Result := 'lightgray';
    clMedGray: Result := 'darkgray';
    else Result := 'black';
  end;
end;

// cyan, magenta, darkgray, orange, pink, violet.

function GetFausseCouleur(Niveau: double): Tcolor; { Niveau entre 0 et 1 }

// BLEU bleu 255 vert 0 rouge 0  = 0
// bleu 255 vert augmente rouge 0
// bleu 255 vert 255 rouge 0 =0.25
// bleu diminue vert 255 rouge 0
// VERT bleu 0 vert 255 rouge 0 =0.5
// bleu 0 vert 255 rouge augmente
// bleu 0 vert 255 rouge 255 =0.75
// bleu 0 vert diminue rouge 255
// ROUGE bleu 0 vert 0 rouge 255=1

// bleu 1/4 => 1 1/2 => 0
// rouge 1/2 => 0 3.4 => 1
// vert 0 => 0 1/4 => 1
// vert 3/4 => 1 1 => 0

  function C(valeurR: double): integer;
  begin
    Result := round(255 * valeurR);
    if Result < 0 then
      Result := 0
    else if Result > 255 then
      Result := 255;
  end;

var
  R, G, B: integer;
begin
  if niveau < 0 then
    niveau := 0;
  if niveau > 1 then
    niveau := 1;
  B := C(2 - 4 * niveau);
  if niveau < 0.5
     then G := C(4 * niveau)
     else G := C(4 - 4 * niveau);
  R := C(4 * niveau - 2);
  Result := RGBToColor(R, G, B);
end; //  GetFausseCouleur

procedure TgrapheReg.Segment(x1, y1, x2, y2: integer);
begin
   canvas.moveTo(x1, y1);
   canvas.lineTo(x2, y2);
end;

destructor Tmonde.Destroy;
begin
  TitreAxe.Free;
  inherited Destroy;
end;

procedure Tmonde.SetZoom(y0, zoom : integer);
begin
    az := zoom*a;
    bz := zoom*b+y0*(1-zoom);
end;

constructor Tmonde.Create;
begin
  inherited Create;
  MinidB := 1e-6; // -120 dB / 1 V
  Axe := nil;
  ZeroInclus := True;
  horizontal := False;
  MinDefaut := 0;
  MaxDefaut := 1;
  Orthonorme := False;
  Graduation := gLin;
  TitreAxe := TstringList.Create;
  xtitre := 0;
  a := 1;
  reset(False);
end; // Tmonde.Create

procedure Tmonde.AffecteMinMax(x: double);
begin
  if x > Maxi
    then Maxi := x
    else if x < Mini
       then Mini := x;
end;

procedure Tmonde.SetDefaut;
begin
  if mini = maxReal then begin
    mini := 1;
    maxi := 2;
  end;
  case Graduation of
      gLog:  if (maxi - mini) < 0.1 then Maxi := Mini + 1;  // 1 décade
      gdB:  if (maxi - mini) < 1 then Maxi  := Mini + 20; // 20 dB
      gLin, gInv:  if (maxi - mini) < 1E-30 then Maxi := Mini + 1; // mini = masse d'un proton
 end;
end;

function Tmonde.Trouve(V: vecteur; Debut, Fin: integer): boolean;
var
  VariabMin, VariabMax: double;
  Ajuste: boolean;
  margeR: double;
  maxnul,minnul : boolean;
begin
  Trouve := True;
  if debut > fin then exit;
  if fin>high(V) then fin := high(v);
  MaxiMini(V, variabMax, variabMin, Debut, Fin);
  if (graduation=gLog) and
     (variabMin < 1E-12) then begin
    Graduation  := gLin;
    verifierLog := True;
  end;
  Ajuste := False;
  case Graduation of
    gLog: begin
      if Log10(variabMin) < Mini then
        Mini := Log10(variabMin);
      if (Mini - floor(Mini)) < 0.1 then
        Mini := floor(Mini) - 0.05;
      if Log10(variabMax) > Maxi then
        Maxi := Log10(variabMax);
      if (ceil(Maxi) - Maxi) < 0.1 then
        Maxi := ceil(Maxi) + 0.05;
    end;
    gdB: begin
      if (variabMin < minidB) then
        variabMin := minidB;
      if 20 * Log10(variabMin) < Mini then
        Mini := Log10(variabMin) * 20;
      if (variabMax < minidB) then
        variabMax := minidB * 100;
      if 20 * Log10(variabMax) > Maxi then
        Maxi := Log10(variabMax) * 20;
      if Mini < (Maxi - 120) then
        Mini := Maxi - 120;
    end;
    gInv: begin
      if variabMin < Mini then
        Mini := variabMin;
      if variabMax > Maxi then
        Maxi := variabMax;
    end;
    gLin: begin
      if variabMin < Mini then begin
        Mini := variabMin;
        ajuste := True;
      end;
      if variabMax > Maxi then begin
        Maxi := variabMax;
        ajuste := True;
      end;
      if not ajuste then ajuste := avecEllipse;
    end;
  end;
  if ajuste then begin
    margeR := (maxi - mini) / 25;
    maxNul := maxi<=0;
    minNul := mini>=0;
    maxi := maxi + margeR;
    if maxNul and (maxi > 0) then maxi := 0;
    mini := mini - margeR;
    if minNul and (mini < 0) then mini := 0;
  end;
  trouve := True;
end; // Tmonde.Trouve

procedure Tmonde.SetMinMaxDefaut(min, max: double);
begin
  VerifMinMaxReal(Min, Max);
  MinDefaut := Min;
  MaxDefaut := Max;
  case Graduation of
    gLog: begin
      try
        Mini := Log10(MinDefaut);
        Maxi := Log10(MaxDefaut);
      except
        MaxDefaut := 100;
        Maxi := 2;
        MinDefaut := 1;
        Mini := 0;
      end;
      zeroInclus := False;
    end;
    gdB: begin
      try
        Mini := Log10(MinDefaut) * 20;
        Maxi := Log10(MaxDefaut) * 20;
        zeroInclus := False;
      except
        MaxDefaut := 100;
        Maxi := 40;
        MinDefaut := 1;
        Mini := 0;
      end;
    end;
    gInv: begin
      try
        Maxi := 1 / MinDefaut;
        Mini := 1 / MaxDefaut;
        zeroInclus := False;
      except
        MaxDefaut := 10;
        Mini := 0.1;
        MinDefaut := 0.1;
        Maxi := 10;
      end;
    end;
    gLin: begin
      Maxi := MaxDefaut;
      Mini := MinDefaut;
      zeroInclus := mini * maxi <= 0;
    end;
  end;
end;

procedure Tmonde.Reset(isPolaire : boolean);
begin
  graduationPiActive := False;
  defini := False;
  autoTick := True;
  orthonorme := False;
  arrondi := 1;
  exposant := 1;
  a := 1;
  b := 0;
  polaire := isPolaire;
  if isPolaire then begin
    ZeroInclus := True;
  end;
  if Graduation <> gLin then
    ZeroInclus := False;
  if (axe <> nil) and (axe.formatU in [fDateTime, fDate, fTime]) then
    zeroInclus := False;
  if zeroInclus then begin
    Maxi := 0;
    Mini := 0;
  end
  else begin
    Maxi := -maxReal;
    Mini := maxReal;
  end;
end; //  Tmonde.Reset

function Tmonde.affecteEntier(r: double): integer;
begin
  if isNan(r) then
    Result := intSansSignif
  else
    try
      Result := round(A * r + B);
      if (result>maxInt) or (result<minInt) then result := intSansSignif;
    except
      Result := intSansSignif
    end;
end;

constructor TmondeVecteur.Create;
begin
  inherited Create;
  defini  := False;
  vitesse := True;
  MondeOrd := mondeY;
end;

const
  Nbre125: array[1..3] of integer = (1, 2, 5);

procedure TmondeVecteur.Trouve(Vx, Vy: vecteur; Nbre: integer);

  procedure SetDelta;
  const
    indexMax = 4;
    coeff: array[0..indexMax] of double = (10, 5, 2, 1, 0.5);
  var
    largeur: double;
    Exponent, index: integer;
  begin
    Exponent := floor(Log10(Maxi));
    largeur := maxi * dix(-exponent);
    index := 0;
    while (largeur < coeff[index]) and (index < indexMax) do
      Inc(index);
    Echelle := coeff[index] * dix(exponent);
  end; // setDelta

var
  j: integer;
begin
  if (Vx = nil) or (Vy = nil) or (Nbre < 3) then
    exit;
  for j := 0 to pred(Nbre) do begin
    if isNan(vx[j]) or isNan(vy[j]) then continue;
    if abs(vx[j]) > maxi then
      maxi := abs(vx[j]);
    if abs(vy[j]) > maxi then
      maxi := abs(vy[j]);
  end;
  Defini := maxi > 0;
  if defini then setDelta;
end;

procedure TmondeVecteur.Reset;
begin
  defini := False;
  Ax := 0;
  Ay := 0;
  maxi := 0;
  echelleTrace := False;
end;

procedure TmondeVecteur.getEntier(x, y: double; var xi, yi: integer);
begin
  if isNan(x) then
    xi := intSansSignif
  else
    try
      xi := round(Ax * x);
    except
      xi := intSansSignif
    end;
  if isNan(y) then
    yi := intSansSignif
  else
    try
      yi := round(Ay * y);
    except
      yi := intSansSignif
    end;
end;

procedure TmondeVecteur.GetPolaire(r, teta, rp, tetap, rs, tetas: double; var x, y: double);
var
  ar, ateta: double;
begin
  if vitesse then begin
    ar := rp;
    ateta := r * tetap;
  end
  else begin
    ar := rs - r * sqr(tetap);
    ateta := 2 * rp * tetap + r * tetas;
  end;
  x := ar * cos(teta) - ateta * sin(teta);
  y := ar * sin(teta) + ateta * cos(teta);
end;

procedure TGrapheReg.MondeXY(xi, yi: integer; m: indiceMonde; var xr, yr: double);
begin
  xr := (xi - monde[mondeX].B) / monde[mondeX].A;
  yr := (yi - monde[m].B) / monde[m].A;
end;

function GetCouleurPages(index: integer): Tcolor;
begin
   if ImprimanteMono then
    Result := clBlack
  else begin
    index  := index mod MaxPages;
    Result := couleurPages[index];
    if Result = clWindow then begin
      couleurPages[index] := clActiveCaption;
      Result := clActiveCaption;
    end;
  end;
end;

procedure TGrapheReg.MondeRT(xi, yi: integer; m: indiceMonde; var xr, yr: double);
var
  rayon, angle: double;
begin
  mondeXY(xi, yi, m, xr, yr);
  case monde[mondeX].graduation of
    gLog: xr := power(10,xr);
    gdB: xr  := power(10,xr/20);
    gInv: xr := 1 / xr;
  end;
  case monde[m].graduation of
    gLog: yr := power(10,yr);
    gdB: yr  := power(10,yr/20);
    gInv: yr := 1 / yr;
  end;
  if OgPolaire in OptionGraphe then begin
    angle := atan2(yr, xr);
    rayon := sqrt(xr * xr + yr * yr);
    xr := angle;
    yr := rayon+zeroPolaire;
  end;
end;

procedure TGrapheReg.VersImprimante(hauteurGr: double; var bas: integer);
var
  margeI, hauteur: integer;
  sauveCanvas : Tcanvas;
begin
  sauveCanvas := canvas;
  try
//  setEchelle(Printer.Canvas);
  limiteFenetre := Printer.Canvas.ClipRect;
  coeffDecalage := marge div 20;
  margeI := printerParam.margeTexte;
  if printer.orientation = poLandscape then with limiteFenetre do begin
      if bas > 3 * margeI then begin
        Printer.NewPage;
        bas := margeI div 2;
      end;
      top  := top + bas + (margeI div 2);
      bas  := bottom;
      bottom := bottom - margeI;
      right := right - margeI;
      left := left + margeI;
    end
  else with limiteFenetre do begin
      right := right - margeI;
      left := left + margeI;
      hauteur := round((bottom - top) * hauteurGr);
      top := bas + margeI div 2;
      bottom := top + hauteur;
      if bottom > Printer.PageHeight then begin
        Printer.NewPage;
        top := margeI;
        bottom := top + hauteur;
      end;
      bas := bottom + margeI div 2;
    end;
  ImprimanteEnCours := True;
  canvas := Printer.Canvas;
  Draw;
  ImprimanteEnCours := False;
  coeffDecalage := 1;
  except
    AfficheErreur(erPbImprimante,0)
  end;
  imprimanteMono := false;
  canvas := sauveCanvas;
end;

procedure TGrapheReg.RTversXY(var xr, yr: double; m: indiceMonde);
var
  angle: double;
begin
  if OgPolaire in OptionGraphe then
    if isNan(yr) then begin
      xr := 0;
      yr := 0;
    end
    else begin
      angle := xr;
      AngleEnRadian(angle);
      xr := (yr-zeroPolaire) * cos(angle);
      yr := (yr-zeroPolaire) * sin(angle);
    end
  else begin
    case monde[mondeX].graduation of
      gLog: xr := Log10(xr);
      gInv: xr := 1 / xr;
    end;
    case monde[m].graduation of
      gLog: yr := Log10(yr);
      gdB: if yr < monde[m].minidB
         then yr := 20 * log10(monde[m].minidB)
         else yr := 20 * Log10(yr);
      gInv: yr := 1 / yr;
    end;
  end;
end;

procedure TGrapheReg.WindowRT(xr, yr: double; m: indiceMonde; var xi, yi: integer);
// raisonne suivant type de coordonnee
begin
  RTversXY(xr, yr, m);
  xi := monde[mondeX].affecteEntier(xr);
  yi := monde[m].affecteEntier(yr);
end; // windowRT

procedure TGrapheReg.WindowXY(xr, yr: double; m: indiceMonde; var xi, yi: integer);
begin
  yi := monde[m].affecteEntier(yr);
  xi := monde[mondeX].affecteEntier(xr);
end;

procedure TGrapheReg.ChercheMonde;

  procedure FindWorld(m: indiceMonde; XX, YY: vecteur; Debut, Fin: integer);

    procedure worldPolaire;
    var
      j: integer;
      angle, rayon: double;
    begin
      if zeroPolaire<0 then begin
         for j := Debut to Fin do
            if YY[j]<zeroPolaire then zeroPolaire := YY[j];
         zeroPolaire := 10*floor(zeroPolaire/10);
      end;
      for j := Debut to Fin do begin
        angle := XX[j];
        rayon := YY[j]-zeroPolaire;
        angleEnRadian(angle);
        monde[mondeX].affecteMinMax(rayon * cos(angle));
        monde[m].affecteMinMax(rayon * sin(angle));
      end;
    end; // worldPolaire

  begin // findworld
    if OgPolaire in OptionGraphe
       then worldPolaire
       else grapheOK := monde[mondeX].Trouve(XX, debut, fin) and
                        monde[m].Trouve(YY, debut, fin);
  end; // findworld

  procedure chercheProjectionY(m: indiceMonde);

    procedure ajusteOrthonorme;
    var
      rapport: double;
      acmX, acmY, aYnew: double;
    begin
      if not (OgPolaire in OptionGraphe) and (monde[m].axe <> nil) and
        (monde[m].axe.nomUnite <> '') and (monde[mondeX].axe <> nil) and
        (monde[mondeX].axe.nomUnite <> '') then
        verifierOrtho := verifierOrtho or
          (monde[m].axe.nomUnite <> monde[mondeX].axe.nomUnite) or
          (monde[m].graduation <> gLin) or
          (monde[mondeX].graduation <> gLin);
      acmX := monde[mondeX].a;
      acmY := monde[m].a;
      rapport := abs(acmX / acmY);
      verifierOrtho := verifierOrtho or
        (((rapport > 100) or (rapport < 0.01)));
      if rapport > 1.002 then
        with monde[mondeX] do begin
          b := b + (maxi + mini) / 2 * (a - abs(monde[m].a));
          // On conserve la position du milieu
          a := abs(monde[m].a);
          miniOrtho := (limiteCourbe.left - b) / a;
          maxiOrtho := (limiteCourbe.right - b) / a;
          orthonorme := True;
          setDelta(grandAxeX);
        end;
      // 0.2 % pour éviter mise à jour intempestive durant animation
      if rapport < 0.998 then
        with monde[m] do begin
          aYnew := -monde[mondeX].a;
          if axeYpositif then aYnew := -aYnew;
          b := b + (maxi + mini) / 2 * (a - aYnew);
          // On conserve le milieu
          a := aYnew;
          maxiOrtho := (CourbeTop - b) / a;
          miniOrtho  := (CourbeBottom - b) / a;
          orthonorme := True;
        end;
    end; // ajusteOrthonorme

  begin with monde[m] do begin
      if abs(Mini) < (Maxi - Mini) * 1e-2 then Mini := 0;
      case Graduation of
         gLog:  if (maxi - mini) < 0.1 then Maxi := Mini + 1;  // 1 décade
         gdB:  if (maxi - mini) < 1 then Maxi  := Mini + 20; // 20 dB
         gLin, gInv:  if (maxi - mini) < 1E-30 then Maxi := Mini + 1E-3;  // limite = masse d'un proton = 1e-27
      end;
      A := (CourbeTop - CourbeBottom) / (Maxi - Mini);
      b := CourbeBottom - Mini * a;
      maxiOrtho := maxi;
      miniOrtho := mini;
      maxInt := limiteFenetre.bottom;
      minInt := limiteFenetre.top;
      if (OgPolaire in OptionGraphe) or (OgOrthonorme in OptionGraphe) then
        ajusteOrthonorme;
      setDelta(grandAxeY);
  end  end; // chercheProjectionY

  procedure chercheProjectionX;
  begin with monde[mondeX] do begin
      if abs(Mini) < (Maxi - Mini) * 1e-2 then Mini := 0;
      if (Maxi - Mini)<1E-12 then Maxi := Mini + 1e-9;
      if (axe <> nil) and (axe.formatU in [fDateTime, fDate, fTime]) then
        zeroInclus := False;
      A := (limiteCourbe.right - limiteCourbe.left) / (Maxi - Mini);
      B := limiteCourbe.left - Mini * A;
      maxiOrtho := maxi;
      miniOrtho := mini;
      maxInt := limiteFenetre.right;
      minInt := limiteFenetre.left;
      setDelta(grandAxeX);
  end  end;

  procedure AjusteZero;

    procedure ajusteYNeg(m: indiceMonde; NouveauZero: integer);
    begin with monde[m] do begin
        b := NouveauZero;
        if (a * Mini + b) > CourbeBottom then
          a := (CourbeBottom - b) / Mini;
        if (a * Maxi + b) < CourbeTop then
          a := (CourbeTop - b) / Maxi;
        mini := (CourbeBottom - b) / a;
        maxi := (CourbeTop - b) / a;
      end;
    end;

    procedure ajusteYPos(m: indiceMonde; NouveauZero: integer);
    begin with monde[m] do begin
        b := NouveauZero;
        if (a * Mini + b) < CourbeBottom then
          a := (CourbeBottom - b) / Mini;
        if (a * Maxi + b) > CourbeTop then
          a := (CourbeTop - b) / Maxi;
        mini := (CourbeBottom - b) / a;
        maxi := (CourbeTop - b) / a;
      end;
    end;

  var
    m: indiceMonde;
    z, z1, n, zMilieu: integer;
  begin // ajusteZero
    n := 0;
    z := 0;
    for m := mondeY to high(indiceMonde) do
      with monde[m] do
        if defini and (graduation = gLin) then begin
          z1 := round(b);
          if axeYpositif then begin
            if z1 > CourbeTop then
              z1 := CourbeTop;
            if z1 < CourbeBottom then
              z1 := CourbeBottom;
          end
          else begin
            if z1 < CourbeTop then
              z1 := CourbeTop;
            if z1 > CourbeBottom then
              z1 := CourbeBottom;
          end;
          z := z + z1;
          Inc(n);
        end;
    if n < 2 then
      exit;
    z := z div n;
    // On prend le zéro moyen
    with monde[mondeY] do
      zMilieu := (courbeTop + CourbeBottom) div 2;
    for m := mondeY to high(indiceMonde) do
      with monde[m] do
        if defini and (graduation = gLin) and
          (abs(b - zMilieu) < abs(z - zMilieu)) then
          z := round(b);
    // On prend le zéro le plus proche du centre
    for m := mondeY to high(indiceMonde) do
      with monde[m] do
        if defini and (graduation = gLin) then begin
          if axeYpositif
             then ajusteYpos(m, z)
             else ajusteYNeg(m, z);
        end;
  end; // ajusteZero

  procedure ChercheMondeVecteur;
  var
    i, j, indexXvar, indexXder, indexYder, m: integer;
    xx, yy: vecteur;
  begin
    for m := 1 to 2 do
      mondeVecteur[m].reset;
    for i := 0 to pred(courbes.Count) do
      with courbes[i] do begin
        if (trVitesse in trace) then begin
          try
            indexXvar := varX.indexG;
            indexXder := indexVitesse(varX.nom);
            indexYder := indexVitesse(varY.nom);
            varder[mondeVitesse] := grandeurs[indexXder];
            if pages[page].valeurVar[indexXvar] = valX then begin
              valXder := pages[page].valeurVar[indexXder];
              valYder := pages[page].valeurVar[indexYder];
            end
            else begin
              valXder := pages[page].valeurLisse[indexXder];
              valYder := pages[page].valeurLisse[indexYder];
            end;
            if OgPolaire in OptionGraphe then begin
              setLength(xx, pages[page].nmes);
              for j := 0 to pred(pages[page].nmes) do
                  xx[j] := valXder[j] * valY[j]; { r d(teta)/dt }
              mondeVecteur[MondeVitesse].trouve(xx, valYder, pages[page].nmes);
              xx := nil;
            end
            else
              mondeVecteur[MondeVitesse].trouve(valXder, valYder, pages[page].nmes);
            mondeVecteur[MondeVitesse].mondeOrd := iMondeC;
          except
            exclude(trace, trVitesse);
          end;
        end; // vitesse
        if trAcceleration in trace then begin
          try
            indexXvar := varX.indexG;
            indexXder := indexAcceleration(varX.nom);
            indexYder := indexAcceleration(varY.nom);
            varDer[mondeAcc] := grandeurs[indexXder];
            if pages[page].valeurVar[indexXvar] = valX then begin
              valXder2 := pages[page].valeurVar[indexXder];
              valYder2 := pages[page].valeurVar[indexYder];
            end
            else begin
              valXder2 := pages[page].valeurLisse[indexXder];
              valYder2 := pages[page].valeurLisse[indexYder];
            end;
            if OgPolaire in OptionGraphe then begin
              setLength(xx, pages[page].nmes);
              setLength(yy, pages[page].nmes);
              for j := 0 to pred(pages[page].nmes) do begin
                xx[j] := 2 * valXder[j] * valYder[j] + valYder[j] * valXder2[j];
                { 2.dr/dt.d(teta)/dt+rd2tezta/dt2 }
                yy[j] := valYder2[j] - valY[j] * sqr(valXder[j]);
                { d2r/dt2-r.d(teta)/dt }
              end;
              mondeVecteur[mondeAcc].trouve(xx, yy, pages[page].nmes);
              xx := nil;
              yy := nil;
            end
            else
              mondeVecteur[mondeAcc].trouve(valXder2, valYder2, pages[page].nmes);
            mondeVecteur[mondeAcc].mondeOrd := iMondeC;
          except
            exclude(trace, trAcceleration);
          end;
        end; // acceleration
      end;{i}
  end; // ChercheMondeVecteur

  procedure setGraduationZero;

  function testZero(m : indiceMonde) : boolean;
  begin
      result := monde[m].mini*monde[m].maxi <= 0;
      if not result then
         if monde[m].maxi>0
            then begin
               result := monde[m].mini<monde[m].maxi/100;
               if result then monde[m].mini := 0;

            end
            else begin
              result := abs(monde[m].maxi)<abs(monde[m].mini)/100;
              if result then monde[m].maxi := 0;
            end;
  end;

  var
    m: indiceMonde;
    gradZero: boolean;
  begin
    gradZero := (OgZeroGradue in OptionGraphe) and not
      (OgAnalyseurLogique in OptionGraphe) and not (OgPolaire in OptionGraphe);
    graduationZeroY := gradZero and (monde[mondeY].graduation = gLin) and
      testZero(mondeY);
    if graduationZeroY and not (OgMemeZero in OptionGraphe) then
      for m := mondeDroit to high(IndiceMonde) do
        GraduationZeroY := GraduationZeroY and not (monde[m].defini);
    graduationZeroX := gradZero and (monde[mondeX].graduation = gLin) and
      testZero(mondeX);
  end;

var
  i, k, N: integer;
  m: indiceMonde;
  margeCourbe, hauteurCourbe: integer;
  NbreMonde: integer;
  MiniCommun, MaxiCommun: double;
  pixelsX: integer;
  limite, decalage: integer;
  ecartBottom, ecartTop: integer;
begin // ChercheMonde 
  grapheOK := True;
  for m := low(indiceMonde) to high(indiceMonde) do
      monde[m].autoTick := autoTick;
  if not monde[mondeX].Defini then begin
    if not useDefautX
       then monde[mondeX].reset(OgPolaire in OptionGraphe);
    if not useDefaut then
      for m := mondeY to high(IndiceMonde) do
        monde[m].reset(OgPolaire in OptionGraphe);
    for i := 0 to pred(courbes.Count) do
      with courbes[i] do
        if (indexModele = 0) or courbeExp or
          ((OmEchelle in OptionModele) and (indexModele > 0)) then begin
          if not useDefaut then
            if useDefautX
               then grapheOK := grapheOK and monde[iMondeC].Trouve(valY, debutC, finC)
               else findWorld(iMondeC, valX, valY, DebutC, FinC);
          monde[iMondeC].Defini := True;
          monde[mondeX].Defini := True;
        end; // OmEchelle in OptionModele 
    grapheOK := grapheOK and (Courbes.Count > 0);
    for m := low(IndiceMonde) to high(IndiceMonde) do
      with monde[m] do
        if graduation = gInv then begin
          swap(mini, maxi);
          try
            maxi := 1 / maxi;
            mini := 1 / mini;
          except
            verifierInv := True;
            mini := 1;
            maxi := 10;
          end;
        end;
  end; // not(monde[mondeX].Defini)
  ChercheMondeVecteur;
  for m := low(IndiceMonde) to high(IndiceMonde) do
      monde[m].setDefaut;
  if OgAnalyseurLogique in OptionGraphe then begin
    NbreMonde  := 0;
    MondeDebut := 0;
    MiniCommun := +MaxReal;
    MaxiCommun := -MaxReal;
    for m := mondeY to high(IndiceMonde) do
      with monde[m] do
        if defini then begin
          Inc(NbreMonde);
          if mini < miniCommun then
            miniCommun := mini;
          if maxi > maxiCommun then
            maxiCommun := maxi;
          if MondeDebut = 0 then
            MondeDebut := m;
        end;
    if OgMemeEchelle in optionGraphe then
      for m := mondeY to high(IndiceMonde) do
        with monde[m] do
          if defini then begin
            mini := miniCommun;
            maxi := maxiCommun;
          end;
    if NbreMonde = 0 then NbreMonde := 1;
  end
  else NbreMonde := 1;
  with limiteFenetre do begin
    grapheOK  := (bottom - top) > succ(nbreMonde) * 20;
    avecAxeY  := not sansAxeY and
       ((bottom - top) div 5 > NbreGradMin * hautCarac * NbreMonde div 4);
    avecAxeX  := not sansAxeX and
       ((right - left) > NbreGradMin * 4 * largCarac);
    grandAxeY := (bottom - top) div 5 > NbreGradMax * HautCarac * NbreMonde div 4;
    grandAxeX := (right - left) > NbreGradMax * 4 * largCarac;
  end;
  limiteCourbe := limiteFenetre;
  Inc(limiteCourbe.left, Marge);
  Dec(limiteCourbe.bottom, Marge);
  if not (OgAnalyseurLogique in OptionGraphe) then
    Inc(limiteCourbe.top, hautCarac + Marge);
  Dec(limiteCourbe.right, Marge);
  if identificationPages and superposePage then
     Dec(limiteCourbe.right, 20*largCarac);
  setGraduationZero;
  decalage := succ(monde[mondeY].NbreChiffres) * largCarac;
  if avecAxeY then begin
    if monde[mondeY].defini and not (OgPolaire in OptionGraphe) and
      not (graduationZeroX) then
      Inc(limiteCourbe.left, decalage)
    else if GraduationZeroX then begin
      with monde[mondeX] do
        limite :=
          round((limiteCourbe.right - limiteCourbe.left) * mini /
          (mini - maxi)); { mini <=0 }
      if limite < decalage then
        Inc(limiteCourbe.left, decalage);
    end;
    if monde[mondeDroit].defini and not
      (OgAnalyseurLogique in OptionGraphe) then
      Dec(limiteCourbe.right, succ(monde[mondeDroit].NbreChiffres) * largCarac);
  end;
  if avecAxeX and not (OgPolaire in OptionGraphe) and not
    (GraduationZeroY) then
    Dec(limiteCourbe.bottom, 2 * HautCarac + marge)
  else if graduationZeroY then begin
    with monde[mondeY] do
      limite := round(
        (limiteCourbe.bottom - limiteCourbe.top) * mini /
        (mini - maxi));
    if limite < 2 * hautCarac then
      Dec(limiteCourbe.bottom, 2 * HautCarac);
  end;
  if axeYpositif then begin
    ecartBottom := abs(limiteFenetre.bottom - limiteCourbe.bottom);
    ecartTop := abs(limiteCourbe.top - limiteFenetre.top);
    limiteCourbe.top := limiteFenetre.top + ecartBottom;
    limiteCourbe.bottom := limiteFenetre.bottom - ecartTop;
  end;
  for m := mondeY to high(IndiceMonde) do
    with monde[m] do
      if axeYpositif then begin
        CourbeTop := LimiteCourbe.bottom;
        CourbeBottom := LimiteCourbe.top;
      end
      else begin
        CourbeTop := LimiteCourbe.top;
        CourbeBottom := LimiteCourbe.bottom;
      end;
  for m := 1 to 2 do
    with mondeVecteur[m], limiteCourbe do
      Dimension := (bottom - top) + (right - left);
  if OgAnalyseurLogique in OptionGraphe then begin
    i := 0;
    if superposePage then begin
      margeCourbe := marge;
      Inc(limiteCourbe.top, hautCarac + Marge);
    end
    else margeCourbe := hautCarac + marge;
    hauteurCourbe := (LimiteCourbe.bottom - LimiteCourbe.top) div
      NbreMonde - margeCourbe;
    if axeYpositif then begin
      for m := high(IndiceMonde) downto mondeY do
        with monde[m] do
          if defini then begin
            CourbeBottom := LimiteCourbe.top + i * hauteurCourbe + succ(i) * margeCourbe;
            CourbeTop := CourbeBottom + hauteurCourbe;
            Inc(i);
          end;
    end
    else begin
      for m := mondeY to high(IndiceMonde) do
        with monde[m] do
          if defini then begin
            CourbeTop := LimiteCourbe.top + i * hauteurCourbe + succ(i) * margeCourbe;
            CourbeBottom := CourbeTop + hauteurCourbe;
            Inc(i);
          end;
    end;
  end;
  grapheOK := grapheOK and monde[mondeX].defini;
  if grapheOK then begin
    chercheProjectionX;
    for m := mondeY to high(indiceMonde) do
      if monde[m].defini then
        chercheProjectionY(m);
    if (OgOrthonorme in OptionGraphe) then
      setGraduationZero;
    for m := 1 to 2 do with mondeVecteur[m] do
        if defini then begin
          if -monde[mondeY].a > monde[mondeX].a then begin
            ax := dimension * EchelleVecteur / maxi;
            ay := ax * monde[mondeOrd].a / monde[mondeX].a;
          end
          else begin
            ay := -dimension * EchelleVecteur / maxi;
            if axeYpositif then ay := -ay;
            ax := ay * monde[mondeX].a / monde[mondeOrd].a;
          end;
          if imprimanteEnCours then begin // echelle multiple du cm
            try
              k := 0;
              repeat
                Inc(k);
                N := floor(echelle * Nbre125[k] * ax * 2.54 / pixelsX);
              until (N > 0) or (k = 3);
              if N > 0 then begin
                ax := N * pixelsX / (echelle * Nbre125[k] * 2.54);
                ay := ax * monde[mondeOrd].a / monde[mondeX].a;
              end;
            except
            end;
          end; { defini }
        end;
    if (OgMemeZero in OptionGraphe) and not
      (OgAnalyseurLogique in OptionGraphe) then
      ajusteZero;
  end;
end; // ChercheMonde

procedure TGrapheReg.SetMonde(m: indiceMonde; minX, minY, maxX, maxY: double);

  procedure chercheProjection;
  begin
    with monde[mondeX] do begin
      defini := True;
      Maxi := MaxX;
      Mini := MinX;
      A := (limiteCourbe.right - limiteCourbe.left) / (Maxi - Mini);
      B := limiteCourbe.left - Mini * A;
    end;
    with monde[m] do begin
      defini := True;
      Maxi := MaxY;
      Mini := MinY;
      A := (CourbeTop - CourbeBottom) / (Maxi - Mini);
      B := CourbeBottom - Mini * A;
    end;
  end;

begin // setMonde
  with limiteFenetre do begin
    avecAxeY  := (bottom - top) > NbreGradMin * hautCarac * 5 div 4;
    avecAxeX  := (right - left) > NbreGradMin * 4 * largCarac;
    grandAxeY := (bottom - top) > NbreGradMax * hautCarac * 5 div 4;
    grandAxeX := (right - left) > NbreGradMax * 4 * largCarac;
  end;
  limiteCourbe := limiteFenetre;
  Inc(limiteCourbe.left, Marge);
  Dec(limiteCourbe.bottom, Marge);
  Inc(limiteCourbe.top, hautCarac + Marge);
  Dec(limiteCourbe.right, Marge);
  if avecAxeX then
    Dec(limiteCourbe.bottom, 2 * hautCarac);
  if avecAxeY and monde[mondeY].defini then
    Inc(limiteCourbe.left, succ(monde[mondeY].NbreChiffres) * largCarac);
  for m := mondeY to high(IndiceMonde) do
    with monde[m] do begin
      CourbeTop := LimiteCourbe.top;
      CourbeBottom := LimiteCourbe.bottom;
    end;
  chercheProjection;
end; // SetMonde

procedure TGrapheReg.AjoutePoint(m: indiceMonde; XX, YY: double);
begin
  with monde[mondeX] do begin
    case graduation of
      gLog: xx := Log10(xx);
      gInv: xx := 1 / xx;
    end;
    if XX > Maxi then
      Maxi := XX;
    if XX < Mini then
      Mini := XX;
  end;
  with monde[m] do begin
    case graduation of
      gLog: yy := Log10(yy);
      gdB: yy  := 20 * Log10(yy);
      gInv: yy := 1 / yy;
    end;
    if YY > Maxi then
      Maxi := YY;
    if YY < Mini then
      Mini := YY;
  end;
end;

procedure TGrapheReg.TraceCourbe(index: integer);
var
  couleurLoc: Tcolor;

  procedure TracePoint(Pixel: boolean);
  var
    x0, y0, decaleLoc: integer;
    xi, yi: integer;
    motifLoc: Tmotif;

    procedure InitSpectre;
    var zeroY : double;
    begin with courbes[index] do begin
        if monde[iMondeC].graduation = gLin
           then zeroY := 0
           else zeroY := monde[iMondeC].mini;
        WindowXY(valX[debutC], zeroY, iMondeC, x0, y0);
        if OgAnalyseurLogique in OptionGraphe
           then decaleLoc := 0
           else decaleLoc := decalage * coeffDecalage;
        WindowRT(valX[finC], zeroY, iMondeC, xi, yi);
        Segment(x0 + decaleLoc, y0 - decaleLoc, xi + decaleLoc, yi - decaleLoc);
       // ligne de base
        pixel := False;
    end end;

  var
    maxiY, miniY: integer;
    PolyLigne: array[1..5] of Tpoint;
    x1, y1: integer;
    x2, y2: integer;
    i, largeurHisto: integer;

    procedure InitHisto;
    var
      i, larg: integer;
    begin with courbes[index] do begin
        if (monde[iMondeC].graduation = gLin) and
           (monde[MondeX].graduation = gLin) then begin
          WindowXY(0, monde[iMondeC].mini, iMondeC, x0, y0);
          WindowXY(valX[DebutC], valY[DebutC], iMondeC, x1, y1);
          WindowXY(valX[FinC], valY[FinC], iMondeC, x2, y2);
          largeurHisto := (abs(x2 - x1) div (FinC - debutC)) div 2;
          pixel := False;
          if (FinC - debutC) < 64 then begin
            for i := DebutC to pred(FinC) do begin
              WindowXY(valX[i], valY[i], iMondeC, x1, y1);
              WindowXY(valX[i + 1], valY[i + 1], iMondeC, x2, y2);
              larg := abs(x2 - x1) div 2;
              if larg < largeurHisto then
                largeurHisto := larg;
            end;
          end;
          if largeurHisto > 1 then
            largeurHisto := largeurHisto - 1;
        end
        else motif := mCroix;
    end end;

    procedure TraceUnPoint;
    var PoinTinactif : boolean;
    begin with courbes[index] do begin
        WindowRT(valX[i], valY[i], iMondeC, xi, yi);
        if (xi > 0) and (yi >= miniY) and (yi <= maxiY) then
          if Pixel then
            canvas.Pixels[xi, yi] := couleurLoc
          else begin
            PointInactif := not pages[page].PointActif[i];
            if PointInactif then begin
               CreatePen(psSolid, 1, clSilver);
               if motif in [mCerclePlein,mCarrePlein] then CreateSolidBrush(clSilver);
            end;
            case MotifLoc of
              mCroix: begin
                Segment(xi - dimPoint, yi, xi + dimPoint, yi);
                Segment(xi, yi - dimPoint, xi, yi + dimPoint);
              end;
              mCroixDiag: begin
                segment(xi - dimPoint, yi - dimPoint, xi + dimPoint, yi + dimPoint);
                segment(xi + dimPoint, yi - dimPoint, xi - dimPoint, yi + dimPoint);
              end;
              mCercle, mCerclePlein:
                canvas.ellipse(xi - dimPoint, yi - dimPoint, xi + dimPoint + 1, yi + dimPoint + 1);
              mCarre, mCarrePlein: canvas.rectangle(xi - dimPoint, yi -
                  dimPoint, xi + dimPoint, yi + dimPoint);
              mLosange: begin
                PolyLigne[1] := Point(xi - dimPoint, yi);
                PolyLigne[2] := Point(xi, yi - dimPoint);
                PolyLigne[3] := Point(xi + dimPoint, yi);
                PolyLigne[4] := Point(xi, yi + dimPoint);
                PolyLigne[5] := Point(xi - dimPoint, yi);
                canvas.polyLine(PolyLigne);
// polyLine ne remplit pas donc pas LosangePlein
              end;
              mSpectre: begin
                segment(xi, y0, xi + decaleLoc, y0 - decaleLoc); // ligne de rappel
                canvas.lineTo(xi + decaleLoc, yi - decaleLoc); // harmonique
              end;
              mEchantillon: begin
                segment(xi, y0, xi, yi);
                canvas.ellipse(xi - dimPoint, yi - dimPoint, xi + dimPoint + 1, yi + dimPoint + 1);
              end;
              mBarreD: Segment(xi - 2, y0, xi - 2, yi);
              mBarreG: Segment(xi + 2, y0, xi + 2, yi);
              mHisto: if largeurHisto = 0
                 then segment(xi, y0, xi, yi) // sinon windows ne trace rien !
                 else canvas.rectangle(xi - largeurHisto, y0, xi + largeurHisto, yi);
              mReticule: begin
                WindowRT(monde[mondeX].mini,monde[iMondeC].mini, iMondeC, x0, y0);
//                WindowRT(monde[mondeX].maxi,monde[iMondeC].maxi, iMondeC, x1, y1);
                if yi<>y0 then Segment(x0, yi, xi, yi);
                if xi<>x0 then Segment(xi, y0, xi, yi);
                canvas.ellipse(xi - dimPoint, yi - dimPoint, xi + dimPoint + 1, yi + dimPoint + 1);
              end;
              mLigneH: Segment(xi - dimPoint, yi, xi + dimPoint, yi);
              mIncert: if (varX = nil) or
                          (IncertX = nil) or
                          (varY = nil) or
                          (IncertY = nil)
                then case traceIncert of
                   iRectangle : canvas.rectangle(xi - 1, yi - 1, xi + 1, yi + 1);
                   iCroix : begin
                      Segment(xi-1, yi, xi+1, yi);
                      Segment(xi, yi-1, xi, yi+1);
                   end;
                   iEllipse :  canvas.ellipse(xi - 1, yi - 1, xi + 1, yi + 1);
                end
                else begin
                  WindowRT(valX[i] + CoeffEllipse * IncertX[i],
                    valY[i] - CoeffEllipse * IncertY[i], iMondeC, x2, y2);
                  WindowRT(valX[i] - CoeffEllipse * IncertX[i],
                    valY[i] + CoeffEllipse * IncertY[i], iMondeC, x1, y1);
                  if ((x1 = x2) and (y2 = y1)) or
                    (x1 <=intSansSignif) or
                    (x2 <=intSansSignif) or
                    (y1 <=intSansSignif) or
                    (y2 <=intSansSignif)
                  then case traceIncert of
                     iRectangle : canvas.rectangle(xi - 1, yi - 1, xi + 1, yi + 1);
                     iEllipse : canvas.rectangle(xi - 1, yi - 1, xi + 1, yi + 1);
                     iCroix : begin
                          Segment(xi-1, yi, xi+1, yi);
                          Segment(xi, yi-1, xi, yi+1);
                     end;
                  end
                  else case traceIncert of
                  iCroix : begin
                    Segment(x1, yi, x2, yi);
                    Segment(xi, y1, xi, y2);
                  end;
                  iEllipse : if (x1 = x2) or (y1 = y2)
                    then begin
                         Segment(x1, yi, x2, yi);
                         Segment(xi, y1, xi, y2);
                    end
                    else begin
                    Segment(xi, yi - 1, xi, yi + 1);
                    Segment(xi - 1, yi, xi + 1, yi);
                    canvas.ellipse(x1, y1, x2 + 1, y2 + 1);
                    // +1 nécessaire pour centrer mais pourquoi ??
                  end;
                  iRectangle : begin
                     Segment(xi, yi, xi, yi);
                     Segment(xi, yi, xi, yi);
                     canvas.rectangle(x1, y1, x2, y2);
                  end;
                  end;
                end // Incertitude
            end; // case motif
          if PointInactif then begin // Mise en évidente et retour état initial
            canvas.brush.style := bsClear;
            canvas.ellipse(xi - 2 * dimPoint, yi - 2 * dimPoint, xi + 2 * dimPoint + 1, yi + 2 * dimPoint + 1);
            CreatePen(psSolid, penWidth, couleurLoc);
            if motif in [mCerclePlein,mCarrePlein] then CreateSolidBrush(couleurLoc);
          end;
          end;
      end;
    end; // traceUnPoint

  var largeur: integer;
      codeHatch : TbrushStyle;
      avecIncert : boolean;
  begin with courbes[index] do begin
      if motif in [mHisto, mBarreD, mBarreG] then
        initHisto;
      if axeYpositif then begin
        maxiY := monde[iMondeC].courbeTop + margeHautBas;
        miniY := monde[iMondeC].courbeBottom - margeHautBas;
      end
      else begin
        maxiY := monde[iMondeC].courbeBottom + margeHautBas;
        miniY := monde[iMondeC].courbeTop - margeHautBas;
      end;
      avecIncert := avecEllipse and
          (varY.incertDefinie or varX.incertDefinie);
      if avecIncert
         then begin
            if not (motif in [mSpectre, mEchantillon, mHisto, mBarreD, mBarreG])
               then motif := mIncert;
        end
        else if (motif = mIncert) then motif := motifInit[(index mod maxOrdonnee)+1];
      CreatePen(psSolid, penWidth, couleurLoc);// par défaut
      motifLoc := motif;
      if superposePage and (motif <= mCarrePlein) and
         (page <> 0) and (SPmotif=reperePage)
              then motifLoc := motifPages[page mod MaxPages];
      case MotifLoc of
        mCerclePlein, mCarrePlein: CreateSolidBrush(couleurLoc);
        mHisto: if largeurHisto < 11 then begin
            CreateSolidBrush(couleurLoc);
            Canvas.pen.Width := 1;
          end
          else begin
            largeurHisto := largeurHisto - 2; // pour la taille du crayon
            if (indexModele = 0) or courbeExp then begin
              case indexHisto of
                0: codeHatch := bsFDIAGONAL; //  45-degree upward left-to-right hatch
                1: codeHatch := bsHORIZONTAL; //  Horizontal hatch
                2: codeHatch := bsVERTICAL; //  Vertical hatch
                //  3 : codeHatch := bsCROSS; //  Horizontal and vertical crosshatch
                //  4 : codeHatch := bsDIAGCROSS; // 45-degree crosshatch
                else codeHatch := bsBDIAGONAL; // 45-degree downward left-to-right hatch
              end;
              Inc(indexHisto);
              indexHisto := indexHisto mod 3;
              largeur := 3;
            end
            else begin // modélisation
              largeur := 1;
              codeHatch := bsHORIZONTAL; //  Horizontal hatch
            end;
               createHatchBrush(codeHatch, couleurLoc);
               canvas.pen.Width := largeur;
          end;
        mBarreD, mBarreG: CreatePen(psSolid, 3*penWidth, couleurLoc);
        mEchantillon: begin
          if monde[iMondeC].graduation = gLin
             then WindowXY(0, 0, iMondeC, x0, y0)
             else WindowXY(0, monde[iMondeC].mini, iMondeC, x0, y0);
          CreateSolidBrush(couleurLoc);
          pixel := False;
        end;
        mLigneH: ;
        mSpectre: initSpectre;
        mIncert: pixel := False;
      end;
           for i := DebutC to FinC do
               if (i mod pasPoint) = 0 then
                   traceUnPoint;
    end;
    canvas.brush.style := bsClear;
  end; // tracePoint

  function TraceBits: boolean;
  var
    masque: integer;
    ligne: integer;
    maxi: double;

    procedure traceLigneBits;
    var
      xi, yi: integer;
      niveau: array[boolean] of integer;
      i, k: integer;
      PolyLigne: array[0..MaxVecteurPoly] of Tpoint;
    begin with courbes[index] do begin
        xi := monde[mondeX].affecteEntier(valX[debutC]);
        niveau[True] := monde[iMondeC].courbeBottom + ligne * monde[iMondeC].ecartBit;
        niveau[False] := niveau[True] + monde[iMondeC].valeurBit;
        try
        yi := round(valY[debutC]);
        if yi>255 then yi := 255;
        if yi<0 then yi := 0;
        except
        yi := 0;
        end;
        yi := niveau[(yi and masque) = 0];
        PolyLigne[0] := Point(xi, yi);
        k  := 0;
        for i := succ(debutC) to finC do begin
          xi := monde[mondeX].affecteEntier(valX[i]);
          Inc(k);
          PolyLigne[k] := Point(xi, yi);
          try
          yi := round(valY[i]);
          if yi>1023 then yi := 1023;
          if yi<0 then yi := 0;
          except
          yi := 0;
          end;
          yi := niveau[(yi and masque) = 0];
          Inc(k);
          PolyLigne[k] := Point(xi, yi);
          if k>(MaxVecteurPoly-2) then begin
             canvas.PolyLine(slice(PolyLigne,k));
             k := 0;
          end;
        end;{boucle i}
        if k > 1 then
          canvas.PolyLine(slice(PolyLigne,k));
    end end; // traceLigneBits

  var
    i: integer;
  begin with courbes[index] do begin
      Result := False;
      maxi := 0;
      for i := succ(debutC) to finC do begin
        if valY[i] < 0 then
          exit;
        if valY[i] >= 256 then
          exit;
        if valY[i] > maxi then
          maxi := valY[i];
      end;
      masque := 1;
      for ligne := 0 to monde[iMondeC].NombreBits - 1 do begin
        traceLigneBits;
        masque := masque * 2;
      end;
      Result := True;
    end;
  end; // traceBits

  procedure TraceFaussesCouleurs;
  var
    i, i0:  integer;
    x1, x2: integer;
    k, kmax, pas: integer;
    couleurT: Tcolor;
  begin
    if axeYpositif
       then i0 := monde[mondeY].CourbeTop
       else i0 := monde[mondeY].CourbeBottom;
    kMax := abs(monde[mondeY].CourbeBottom - monde[mondeY].CourbeTop);
    pas := succ(kMax div 256);
    kmax := kmax div pas;
    x1 := LimiteFenetre.Left;
    x2 := x1 + largCarac;
    for k := 0 to kmax do begin
      i := i0 - k * pas;
      couleurT := GetFausseCouleur(k / kmax);
      createPen(psSolid, pas, couleurT);
      Segment(x1, i, x2, i);
    end;// for k
  end; // traceFaussesCouleurs

  procedure TraceTexte;
  var
    i, h: integer;
    xi, yi: integer;
    maxiY, miniY,maxiX, miniX: integer;
    pas,oldH: integer;
  begin with courbes[index] do begin
      with limiteFenetre do
           h := (right - left + bottom - top) div 2;
      h := round(h*TexteGrapheSize/100);
      oldH := canvas.font.height;
      canvas.font.height := h;
      canvas.font.color := couleurTexte;

      if axeYpositif then begin
          maxiY  := monde[iMondeC].courbeTop;
          miniY  := monde[iMondeC].courbeBottom;
      end
      else begin
          maxiY  := monde[iMondeC].courbeBottom;
          miniY  := monde[iMondeC].courbeTop;
      end;
      miniX := limiteCourbe.left;
      maxiX := limiteCourbe.right;

      canvas.font.color := couleurLoc;
      canvas.brush.style := bsClear; // transparent
 //     setTextAlign(TAcenter + TAbaseLine);
      pas := succ((finC - debutC) div NbreTexteMax);
      if pasPoint>pas then pas := pasPoint;
      for i := DebutC to FinC do
        if ((i mod pas) = 0) then begin
          WindowRT(valX[i], valY[i], iMondeC, xi, yi);
          if (xi > 0) and
             (yi > miniY) and (yi < maxiY) and
             (xi > miniX) and (xi < maxiX) then begin
// test haut bas : Windows est incapable de gérer une cloture dans un EMF
            if not pages[page].PointActif[i] then
               canvas.font.Color := clSilver;
            mYTextOut(xi, yi,texteC[i]);
            if not pages[page].PointActif[i] then
               canvas.font.Color := couleurLoc;
          end;
        end; {i}
        canvas.font.height := oldH;
  end end; // traceTexte

  procedure TraceSonagramme;
  var
    i, j, Int, NbreY: integer;
    x1, y1: integer;
    x2, y2: integer;
    xi,xii : integer;
    IntReal, deltaY: double;
    couleurT: Tcolor;
  begin with courbes[index] do begin
      canvas.pen.style := psClear;
      deltaY := valY[1] / 2;
      NbreY  := 0;
      canvas.Brush.Style := bsSolid;
      repeat
        Inc(NbreY)
      until (valY[NbreY] = 0) or (NbreY = NbrePointsSonagramme div 2);
// valY = valeur de fréquence
      xii := monde[mondeX].affecteEntier(valX[0]); 
      xi := monde[mondeX].affecteEntier(valX[nbreSonagrammeAff]);
      x1 := (xi+xii) div 2;
// départ de 1 pour axe Y visible
      for i := 1 to pred(NbreSonagramme) do begin
        xii := monde[mondeX].affecteEntier(valX[(i + 1) * nbreSonagrammeAff]); // point suivant
        x2 := (xi+xii) div 2; // à cheval
        y1 := monde[mondeY].affecteEntier(deltaY);
        for j := 1 to pred(NbreY) do begin
// départ de 1 pour axe X visible        
          y2 := monde[mondeY].affecteEntier(valY[j] + deltaY);
          IntReal := son[i, j]; // entre 0 et 1
          if DecadeDB > 0 then begin // logarithmique sur x décades
            try
              IntReal := 1 + log10(IntReal) / DecadeDB;
              if IntReal < 0 then
                intReal := 0;
            except
              IntReal := 0;
            end;
          end;
          if useFaussesCouleurs
             then couleurT := GetFausseCouleur(IntReal)
             else begin
                Int := 255 - round(255 * IntReal);
                CouleurT := RGBToColor(Int, Int, Int);
             end;
          canvas.Brush.Color := couleurT;
          createSolidBrush(couleurT);
          canvas.Rectangle(x1, y2, x2+1, y1+1); // Trace de x1 à x2 et de y2 à y1
          // +1 à cause de ps_null
          y1 := y2;
        end; {j}
        x1 := x2;
        xi := xii;
      end; {i}
      canvas.brush.style := bsClear;
  end end; // traceSonagramme


  procedure traceLigne;
  var
    xi, yi, pas, Nvisible :  integer;
    oldXi, oldYi: integer;
    idebut, ifin: integer;
    sincActif : boolean;

    function xNew(y: integer): integer;
    begin
      Result := round(xi + (y - yi) * (oldXi - xi) / (oldYi - yi));
    end;

  type
    Tprecedent = (pOK, pHaut, pBas, pNone);
  var
    i, j: integer;
    Precedent, Courant: Tprecedent;
    yy, maxiY, miniY: integer;
    PolyLigne: array[1..MaxVecteurPoly] of Tpoint;

procedure AffecteSinc;
var
  k, ordreSinc : integer;
  iX, iY: integer;
  valeurX, valeurY: vecteur;
begin with courbes[index] do begin
        ordreSinc := largeurEcran div Nvisible;
        if ordreSinc<3 then exit;
        iX := varX.indexG;
        iY := varY.indexG;
        setLength(valeurX,Nvisible+1);
        setLength(valeurY,Nvisible+1);
        for k := iDebut to iFin do begin
            valeurX[k-iDebut] := pages[page].valeurVar[iX][k];
            valeurY[k-iDebut] := pages[page].valeurVar[iY][k];
        end;
        InterpoleSinc(valeurX, valeurY, OrdreSinc, Nvisible, valx, valy);
        iDebut := 0;
        iFin := Nvisible-1;
        valeurX := nil; valeurY := nil;
        sincActif := true;
end end; // affecteSinc

procedure AffecteSpline;
var
  k: integer;
  iX, iY: integer;
  valeurX, valeurY: vecteur;
begin with courbes[index] do begin
      if Nvisible>(LargeurEcran div 2) then exit;
      iX := varX.indexG;
      iY := varY.indexG;
      setLength(valeurX,Nvisible+1);
      setLength(valeurY,Nvisible+1);
      for k := iDebut to iFin do begin
          valeurX[k-iDebut] := pages[page].valeurVar[iX][k];
          valeurY[k-iDebut] := pages[page].valeurVar[iY][k];
      end;
      CalculBspline(valeurX, valeurY, OrdreLissage, Nvisible, valx, valy);
      iDebut := 0;
      iFin := pred(Nvisible);
      valeurX := nil; valeurY := nil;
      sincActif := true;
end end; // affecteSpline

procedure RestaureSinc; // pour les points
var iX, iY: integer;
begin with courbes[index] do begin
        iX := varX.indexG;
        iY := varY.indexG;
        CopyVecteur(valX, pages[page].valeurVar[iX]);
        CopyVecteur(valY, pages[page].valeurVar[iY]);
end end; // restaureSinc

    procedure TracePolyLine;
    var
      i: integer;
      xcourant, ycourant: integer;

      procedure Nettoie(var idebut: integer);
      var
        k, ifin, ecart: integer;
        maxi, mini, entree, sortie: integer;
      begin // idebut et suivant sur la même verticale
        xCourant := PolyLigne[idebut].x;
        entree := PolyLigne[idebut].y;
        maxi := entree;
        mini := entree;
        k := idebut + 1;
        repeat
          yCourant := PolyLigne[k].y;
          if yCourant > maxi then
            maxi := ycourant;
          if yCourant < mini then
            mini := ycourant;
          Inc(k);
        until (k = j) or (PolyLigne[k].x <> xCourant); // verticale différente
     // enlever points inutiles
        iFin := pred(k);
        sortie := PolyLigne[ifin].y;
        k := idebut; // on garde début
        if entree > sortie then begin
          if mini < sortie then begin
            Inc(k);
            PolyLigne[k].y := mini;
          end;
          if maxi > entree then begin
            Inc(k);
            PolyLigne[k].y := maxi;
          end;
        end
        else begin
          if mini < entree then begin
            Inc(k);
            PolyLigne[k].y := mini;
          end;
          if maxi > sortie then begin
            Inc(k);
            PolyLigne[k].y := maxi;
          end;
        end;
        Inc(k);
        PolyLigne[k].y := sortie;
        ecart  := ifin - k;
        idebut := succ(k); // prochain debut
        Dec(j, ecart);
        for k := idebut to j do
          PolyLigne[k] := PolyLigne[k + ecart];
      end;

    begin
      if j<=1 then begin
         j := 0;
         precedent := pNone;
         exit;
      end;
      i := 1;
      repeat
        if (PolyLigne[i].x = PolyLigne[i + 1].x)
           then Nettoie(i)
           else Inc(i);
      until i > j - 2;
      canvas.PolyLine(slice(PolyLigne,j));
      j := 0;
      precedent := pNone;
    end;

    procedure XYdirect;
    var i : integer;
    begin with courbes[index] do begin
      j := 0;
      for i := debutC to finC do begin
        WindowRT(valX[i], valY[i], iMondeC, xi, yi);
        if (xi <0) or (yi<0)
           then TracePolyLine
           else begin
              Inc(j);
              PolyLigne[j] := Point(xi, yi);
              if j>=MaxVecteurPoly then TracePolyLine;
           end;
      end; // boucle i
      if j > 1 then TracePolyLine;
    end end;

  begin with courbes[index] do begin
      sincActif := false;
      Nvisible := finC-debutC+1;
      if axeYpositif then begin
        maxiY := monde[iMondeC].courbeTop + margeHautBas;
        miniY := monde[iMondeC].courbeBottom - margeHautBas;
      end
      else begin
        maxiY := monde[iMondeC].courbeBottom + margeHautBas;
        miniY := monde[iMondeC].courbeTop - margeHautBas;
      end;
      Precedent := pNone;
      if (grandeurs[indexTri] = varX) or
         (grandeurs[cFrequence] = varX) then
        pas := succ(Nvisible div LargeurEcran)
      else begin
        pas := 1; // mode XY
        if Nvisible>LargeurEcran then begin
           XYdirect; // cas rapide XY si beaucoup de points
           exit;
        end;
      end;
      idebut := debutC;
      ifin := finC;
      if (pas > 1) and not (OgPolaire in OptionGraphe) then begin
        xi := windowX(valX[idebut]);
        while (xi<0) and (idebut < finC) do begin
          Inc(idebut);
          xi := windowX(valX[idebut]);
        end;
        xi := windowX(valX[ifin]);
        while (xi<0) and (ifin > debutC) do begin
          Dec(ifin);
          xi := windowX(valX[ifin]);
        end;
        Nvisible := iFin-iDebut+1;
        pas := succ(Nvisible div LargeurEcran);
      end;
      if (indexModele=indexSinc) and (grandeurs[indexTri] = varX) then affecteSinc;
      if (indexModele=indexSpline) then affecteSpline;
      j := 0;
      i := iDebut;
      while i <= iFin do begin
        WindowRT(valX[i], valY[i], iMondeC, xi, yi);
        if (xi<0) or (yi<0)
        then TracePolyLine
        else begin
          if yi < miniY then
            Courant := pBas
          else if yi > maxiY then
            Courant := pHaut
          else
            Courant := pOK;
          case Courant of
            pOK: case Precedent of
                pNone, pOK: begin
                  Inc(j);
                  PolyLigne[j] := Point(xi, yi);
                end;
                pHaut, pBas: begin
                  j := 2;
                  if Precedent = pHaut then
                    yy := maxiY
                  else
                    yy := miniY;
                  PolyLigne[1] := Point(xNew(yy), yy);
                  PolyLigne[2] := Point(xi, yi);
                end;
              end;
            pHaut: case Precedent of
                pOK: begin
                  Inc(j);
                  PolyLigne[j] := Point(xNew(maxiY), maxiY);
                  if j > 1 then TracePolyLine;
                end;
                pHaut: ;
                pBas: begin
                  PolyLigne[1] := Point(xNew(miniY), miniY);
                  PolyLigne[2] := Point(xNew(maxiY), maxiY);
                  canvas.PolyLine(slice(PolyLigne,2));
                  j := 0;
                end;
                pNone: ;
              end;
            pBas: case Precedent of
                pOK: begin
                  Inc(j);
                  PolyLigne[j] := Point(xNew(miniY), miniY);
                  TracePolyLine;
                end;
                pBas: ;
                pHaut: begin
                  PolyLigne[1] := Point(xNew(maxiY), maxiY);
                  PolyLigne[2] := Point(xNew(miniY), miniY);
                  canvas.PolyLine(slice(PolyLigne,2));
                  j := 0;
                end;
                pNone: ;
              end;
          end;{case courant}
          Precedent := Courant;
          oldYi := Yi;
          oldXi := Xi;
        end;
        Inc(i, pas);
        if j>1024 then TracePolyLine;
      end; // boucle i
      if j > 1 then TracePolyLine;
      if sincActif then restaureSinc;
  end end; // traceLigne

  procedure traceLigneAvecDecalage;
  var
    xi, yi, pas:  integer;
    oldXi, oldYi: integer;

    function xNew(y: integer): integer;
    begin
      Result := round(xi + (y - yi) * (oldXi - xi) / (oldYi - yi));
    end;

  type
    Tprecedent = (pOK, pHaut, pBas, pNone);
  var
    i, j: integer;
    Precedent, Courant: Tprecedent;
    decaleLoc: integer;
    yy, maxiY, miniY: integer;
    PolyLigne: array[1..MaxVecteurPoly] of Tpoint;
    iDebut, iFin: integer;
  begin with courbes[index] do begin
      if axeYpositif then begin
        maxiY := monde[iMondeC].courbeTop + margeHautBas;
        miniY := monde[iMondeC].courbeBottom - margeHautBas;
      end
      else begin
        maxiY := monde[iMondeC].courbeBottom + margeHautBas;
        miniY := monde[iMondeC].courbeTop - margeHautBas;
      end;
      Precedent := pNone;
      decaleLoc := decalage * coeffDecalage;
      j := 0;
      pas := succ((finc - debutC) div LargeurEcran);
      idebut := debutC;
      ifin := finC;
      if not (OgPolaire in OptionGraphe) then begin
        xi := windowX(valX[idebut]);
        while (xi<0) and (idebut < finC) do begin
          Inc(idebut);
          xi := windowX(valX[idebut]);
        end;
        xi := windowX(valX[ifin]);
        while (xi<0) and (ifin > debutC) do begin
          Dec(ifin);
          xi := windowX(valX[ifin]);
        end;
        pas := succ((ifin - idebut) div LargeurEcran);
      end;
      i := iDebut;
      while i <= iFin do begin
        WindowRT(valX[i], valY[i], iMondeC, xi, yi);
        if (xi<0) or (yi<0) then begin
          if j > 1 then
            canvas.polyLine(slice(PolyLigne,j));
          j := 0;
          precedent := pNone;
        end
        else begin
          if yi < miniY then
            Courant := pBas
          else if yi > maxiY then
            Courant := pHaut
          else
            Courant := pOK;
          case Courant of
            pOK: case Precedent of
                pNone, pOK: begin
                  Inc(j);
                  PolyLigne[j] := Point(xi + decaleLoc, yi - decaleLoc);
                end;
                pHaut, pBas: begin
                  j := 2;
                  if Precedent = pHaut
                     then yy := maxiY
                     else yy := miniY;
                  PolyLigne[1] := Point(xNew(yy) + decaleLoc, yy - decaleLoc);
                  PolyLigne[2] := Point(xi + decaleLoc, yi - decaleLoc);
                end;
              end;
            pHaut: case Precedent of
                pOK: begin
                  Inc(j);
                  PolyLigne[j] := Point(xNew(maxiY) + decaleLoc, maxiY - decaleLoc);
                  canvas.PolyLine(slice(PolyLigne,j));
                  j := 0;
                end;
                pHaut: ;
                pBas: begin
                  PolyLigne[1] := Point(xNew(miniY) + decaleLoc, miniY - decaleLoc);
                  PolyLigne[2] := Point(xNew(maxiY) + decaleLoc, maxiY - decaleLoc);
                  canvas.PolyLine(slice(PolyLigne,2));
                  j := 0;
                end;
                pNone: ;
              end;
            pBas: case Precedent of
                pOK: begin
                  Inc(j);
                  PolyLigne[j] := Point(xNew(miniY) + decaleLoc, miniY - decaleLoc);
                  canvas.PolyLine(slice(PolyLigne,j));
                  j := 0;
                end;
                pBas: ;
                pHaut: begin
                  PolyLigne[1] := Point(xNew(maxiY) + decaleLoc, maxiY - decaleLoc);
                  PolyLigne[2] := Point(xNew(miniY) + decaleLoc, miniY - decaleLoc);
                  canvas.PolyLine(slice(PolyLigne,2));
                  j := 0;
                end;
                pNone: ;
              end;
          end;{case courant}
          Precedent := Courant;
          oldYi := Yi;
          oldXi := Xi;
        end;
        if j>=MaxVecteurPoly then begin
           canvas.polyLine(slice(PolyLigne,j));
           j := 0;
        end;
        Inc(i, pas);
      end;{boucle i}
      if j > 1 then
        canvas.polyLine(slice(PolyLigne,j));
    end;
  end; // traceLigneAvecDecalage

  procedure traceVecteur(xx, yy: vecteur; m: integer);
  var
    xi, yi: integer;
    i: integer;
    h: integer; // dimension en pixel de la flèche tq 1% de la largeur
    couleurV: Tcolor;

    procedure TraceProlongement(dX, dY: double);
    var
      coeff: double;
      deltaX, deltaY: integer;
    begin
      createPen(psDash, 1, couleurV);
      canvas.MoveTo(xi, yi);
      try
        if abs(dY) > abs(dX)
           then coeff := MondeVecteur[mondeAcc].maxi / dY
           else coeff := MondeVecteur[mondeAcc].maxi / dX;
        coeff := abs(coeff);
        if coeff > 1e+2 then coeff := 1e+2;
        coeff := coeff / EchelleVecteur;
        MondeVecteur[mondeAcc].getEntier(dX * coeff, dY * coeff, deltaX, deltaY);
        canvas.LineTo(xi + deltaX, yi + deltaY);
      except { dX=0 ou dY=0 }
      end;
      createPen(psSolid, penWidth, couleurV);
    end;

    procedure TraceFleche(dX, dY: double);
    var
      x, y, x1, y1, d: integer;
      deltaX, deltaY: integer;
      Points:  array[0..2] of Tpoint;
    begin
      MondeVecteur[m].getEntier(dX, dY, deltaX, deltaY);
      canvas.MoveTo(xi, yi);
      y1 := yi + deltaY;
      x1 := xi + deltaX;
 // x1,y1 = bout de la flèche
      canvas.LineTo(x1, y1);
      d := round(sqrt(sqr(deltaX) + sqr(deltaY)));
      if d<h then exit;
      deltaX := round(h * deltaX / d);
      deltaY := round(h * deltaY / d);
  // x,y = centre de la base de la flèche
      y := y1 - deltaY;
      x := x1 - deltaX;
      deltaX := deltaX div 4;
      deltaY := deltaY div 4;
      Points[0] := Point(x1, y1);
      Points[1] := Point(x - deltaY, y + deltaX);
      Points[2] := Point(x + deltaY, y - deltaX);
      canvas.Polygon(Points); // triangle 3 points
    end;

    Procedure TraceEchelle;
    var sauveax : double;
        S: string;
    begin
          with limiteFenetre do begin
            xi := m * (right - left) div 3;
            if axeYpositif
               then yi := bottom - (5 * HautCarac div 4)
               else yi := top + (5 * HautCarac div 2);
          end;
          if (ogOrthonorme in optionGraphe) or
             (MondeVecteur[m].ax<MondeVecteur[m].ay)
             then traceFleche(MondeVecteur[m].echelle, 0)
             else begin
                sauveAx := MondeVecteur[m].ax;
                MondeVecteur[m].ax := abs(MondeVecteur[m].ay);
                traceFleche(MondeVecteur[m].echelle, 0);
                MondeVecteur[m].ax := sauveAx;
             end;
          canvas.font.Color := couleurV;
          S := courbes[index].varDer[m].formatValeurEtUnite(MondeVecteur[m].echelle);
          if axeYpositif
             then Inc(yi, 3 * HautCarac div 2)
             else Dec(yi, 3 * HautCarac div 2);
          with limiteFenetre do
            xi := m * (right - left) div 3;
          canvas.Brush.Style := bsClear;
          canvas.TextOut(xi, yi,S);
          MondeVecteur[m].EchelleTrace := True;
      end;

  var
    pas: integer;
    iDebut, iFin: integer;
    dXr, dYr: double;

    function PointATracer(i: integer): boolean;
    begin with courbes[index] do begin
        if m=1
           then result := not isNan(valYder[i]) and not isNan(valXder[i])
           else result := not isNan(valYder2[i]) and not isNan(valXder2[i]);
        if not Result then exit;
        if UseSelect
              then if i < 256
                then Result := i in PointSelect
                else Result := False
              else Result := (i mod pas) = 0;
    end end;

  begin  with courbes[index] do begin // traceVecteur
    if (xx = nil) or (yy = nil) then exit;
    if OgPolaire in OptionGraphe then begin
       if (m=1) and ((valYder=nil) or (valXder=nil)) then exit;
       if (m=2) and ((valYder2=nil) or (valXder2=nil)) then exit;
    end;
      try
        couleurV := couleurLoc;
        if CouleurVitesseImposee then begin
          if (couleurMecanique[m]=clWindow) or
             (couleurMecanique[m]=clWhite) then
           if m=1
              then couleurMecanique[1] := clBlue
              else couleurMecanique[2] := clRed;
          couleurV := CouleurMecanique[m];
        end
        else if m > 1 then begin
        end;
        h := (limiteFenetre.Right - LimiteFenetre.Left) div 100;
        if h < 6 then h := 6;
        CreateSolidBrush(couleurV);
        createPen(psSolid, penWidth, couleurV);
        pas := succ((finC - debutC) div NbreVecteurVitesseMax);
        iDebut := debutC;
        iFin := finC;
        for i := iDebut to iFin do
          if PointAtracer(i) then begin
            WindowRT(valX[i], valY[i], iMondeC, xi, yi);
            if (xi>0) and (yi>0) then begin
              if OgPolaire in OptionGraphe then
                MondeVecteur[m].getPolaire(
                  valY[i], valX[i],
                  valYder[i], valXder[i],
                  valYder2[i], valXder2[i], dXr, dYr)
              else begin
                dXr := xx[i];
                dYr := yy[i];
              end;
              traceFleche(dXr, dYr);
              if (m = mondeAcc) and prolongeVecteur then
                  traceProlongement(dXr, dYr);
              if (m = mondeVitesse) and projeteVecteur then begin
                  createPen(psSolid, 1, couleurV);
                  traceFleche(0, dYr);
                  traceFleche(dXr, 0);
                  createPen(psSolid, penWidth, couleurV);
              end;
            end;
          end;{for i}
        if not MondeVecteur[m].EchelleTrace then traceEchelle;
      except
        if m = MondeVitesse then exclude(trace, trVitesse);
        if m = MondeAcc then exclude(trace, trAcceleration);
      end;
  end end; // traceVecteur

  procedure traceResidus;
  var
    i: integer;
    x0, y0, xi, yi, dxi, dyi: integer;
    TraceDeltaX, TraceDeltaY: boolean;
  begin with courbes[index] do begin
      WindowXY(0, 0, iMondeC, x0, y0);
      traceDeltaX := avecEllipse and (varX <> nil) and (IncertX <> nil);
      traceDeltaY := avecEllipse and (varY <> nil) and (IncertY <> nil);
      for i := debutC to finC do if pages[page].pointActif[i] then begin
        WindowRT(valX[i], valY[i], iMondeC, xi, yi);
        if TraceDeltaX
           then dxi := abs(round(CoeffEllipse * IncertX[i] * monde[MondeX].A))
           else dxi := 0;
        if TraceDeltaY
           then dyi := abs(round(CoeffEllipse * IncertY[i] * monde[iMondeC].A))
           else dyi := 0;
        if dxi + dyi = 0 then begin
          segment(xi, y0, xi, yi);
          canvas.rectangle(xi - dimPoint, yi - dimPoint, xi + dimPoint, yi + dimPoint);
        end
        else case TraceIncert of
        iCroix : begin
          segment(xi - dxi, yi, xi + dxi, yi);
          segment(xi, yi - dyi, xi, yi + dyi);
        end;
        iEllipse : begin
          canvas.Pixels[xi, yi] := couleurLoc;
          if dxi = 0
             then segment(xi, yi - dyi, xi, yi + dyi)
             else if dyi = 0
                then segment(xi - dxi, yi, xi + dxi, yi)
                else canvas.ellipse(xi - dxi, yi - dyi, xi + dxi + 1, yi + dyi + 1);
        end;
        iRectangle : begin
          canvas.Pixels[xi, yi] := couleurLoc;
          if dxi = 0
             then segment(xi, yi - dyi, xi, yi + dyi)
             else if dyi = 0
                then segment(xi - dxi, yi, xi + dxi, yi)
                else canvas.rectangle(xi - dxi, yi - dyi, xi + dxi, yi + dyi);
        end;
        end;
      end; // for i
    end;
  end; // traceResidus 

  procedure traceStat;
  var
    i: integer;
    xi, yi: integer;
    x0, y0, xpred: integer;
    largeur: integer;
    codeHatch: TBrushStyle;
  begin with courbes[index] do begin
      case indexHisto of
           0: codeHatch := bsBDIAGONAL;
           1: codeHatch := bsHORIZONTAL; //  Horizontal hatch
           2: codeHatch := bsVERTICAL; //  Vertical hatch
           else codeHatch := bsBDIAGONAL;
      end;
      inc(indexHisto);
      indexHisto := indexHisto mod 3;
      createHatchBrush(codeHatch, couleurLoc);
      WindowRT(valX[debutC], 0, iMondeC, x0, y0);
      WindowRT(valX[debutC], valY[debutC], iMondeC, xpred, yi);
      largeur := penWidth div 2 + 1;
      for i := succ(debutC) to finC do begin
        WindowRT(valX[i], valY[i], iMondeC, xi, yi);
        if (xi>0) and (valY[i] > 0) then begin
           canvas.rectangle(xpred, yi, xi - largeur, y0);
           xpred := xi + largeur;
        end;
      end;
    end;
  end; // traceStat

var
  xi, yi: integer;
  // TGrapheReg.TraceCourbe(index)
begin with courbes[index] do begin
    if monde[mondeX].Axe = nil then
      monde[mondeX].Axe := varX;
    if monde[iMondeC].Axe = nil then
      monde[iMondeC].Axe := varY;
    if FinC < DebutC then
      exit;
    if superposePage and (page <> 0) and (SPcouleur=reperePage)
       then couleurLoc := GetCouleurPages(page)
       else if ImprimanteMono
          then couleurLoc := clBlack
          else couleurLoc := couleur;
    if trace * [trCouples, trStat] <> [] then
      exclude(trace, trPoint);
    if trSonagramme in trace then begin
        traceSonagramme;
        if useFaussesCouleurs then
           traceFaussesCouleurs;
        exit;
    end;
    if (trPoint in trace) then begin
      createPen(psSolid, penWidth, couleurLoc);  // 1 ou penWidth ?
      tracePoint(((dimPointVGA = 1) and not avecEllipse) or (motif = mPixel));
    end;
    if superposePage and (page <> 0) and (SPstyle=reperePage)
       then createPen(stylePages[page mod MaxPages],penWidth, couleurLoc)
       else if penWidthC=0
           then createPen(styleT, penWidth, couleurLoc)
           else createPen(styleT, penWidthC, couleurLoc);
    if trLigne in trace then
      if decalage = 0
         then TraceLigne
         else TraceLigneAvecDecalage;
    if trStat in trace then TraceStat;

    if trTexte in trace then traceTexte;
    if trResidus in trace then traceResidus;
    if trVitesse in trace then traceVecteur(valXder, valYder, mondeVitesse);
    if trAcceleration in trace then traceVecteur(valXder2, valYder2, mondeAcc);
    if courbeExp and borneVisible and (page=pageCourante) then begin
         if //(debutC > 0) or
            not (ImprimanteEnCours or ImageEnCours) then begin
           WindowRT(valX[debutC], valY[debutC], iMondeC, xi, yi);
           traceBorne(xi, yi, bsDebut, couleurLoc, indexModele);
         end;
         if //(finC < pred(pages[page].nmes)) or
            not (ImprimanteEnCours or ImageEnCours) then begin
            WindowRT(valX[finC], valY[finC], iMondeC, xi, yi);
            traceBorne(xi, yi, bsFin, couleurLoc, indexModele);
         end;
    end; // bornes
    if not (OgPolaire in OptionGraphe) and (varY <> nil) then begin
      if avecAxeY and (Monde[iMondeC].TitreAxe.IndexOf(varY.nom) < 0) then
        TraceTitreAxe(varY, iMondeC, index);
      if empilePage and (iMondeC = mondeDebut) and
        (Monde[mondeDebut].TitreAxe.IndexOf(varY.nom) < 0) then
        TraceTitreAxe(varY, iMondeC, index);
      if avecAxeX and (varX <> nil) and
        (monde[mondeX].TitreAxe.IndexOf(varX.nom) < 0) then
        TraceTitreAxe(varX, mondeX, index);
    end;// not polaire
    end;
end;// TraceCourbe

procedure TGrapheReg.TraceFilDeFer;
var
  courbesAtracer: set of byte;
  PremiereCourbe, PremierPoint, DernierPoint : integer;

  procedure TracePoints;
  var
    i, j: integer;
    xi, yi: integer;
    PolyLigne: array[0..5] of Tpoint;
    maxiY, miniY, c: integer;
  begin with courbes[premiereCourbe] do
      if axeYpositif then begin
        maxiY := monde[iMondeC].courbeTop + margeHautBas;
        miniY := monde[iMondeC].courbeBottom - margeHautBas;
      end
      else begin
        maxiY := monde[iMondeC].courbeBottom + margeHautBas;
        miniY := monde[iMondeC].courbeTop - margeHautBas;
      end;
    for i := PremierPoint to dernierPoint do
      if ((i mod pasPoint) = 0) then begin
        j := 0;
        for c := 0 to pred(courbes.Count) do
          if c in CourbesATracer then
            with courbes[c] do begin
              WindowRT(valX[i], valY[i], iMondeC, xi, yi);
              if (xi > 0) and
                 (yi >= miniY) and (yi <= maxiY) then begin
                PolyLigne[j] := Point(xi, yi);
                Inc(j);
              end;
            end; {with courbes}
        canvas.polyLine(slice(PolyLigne,j));
      end;{for i}
  end;// tracePoints

var
  c: integer;
  FilDeFerInterdit: boolean;
  couleurLoc : TColor;
  // TGrapheReg.TraceFilDeFer(DC)
begin
  CourbesAtracer := [];
  premiereCourbe := -1;
  couleurLoc := clBlue;
  FilDeFerInterdit := True;
  for c := 0 to pred(Courbes.Count) do
    with courbes[c] do
      if (trace * [trPoint, trLigne] <> []) and ((indexModele = 0) or
        courbeExp) then begin
        include(CourbesAtracer, c);
        if premiereCourbe < 0 then begin
          premiereCourbe := c;
            premierPoint := DebutC;
            dernierPoint := FinC;
          couleurLoc := couleur;
          if FinC < DebutC then
            exit;
        end
        else if varX <> courbes[premiereCourbe].varX then
          FilDeFerInterdit := False;
      end;
  if FilDeFerInterdit then begin
    FilDeFer := False;
    exit;
  end;
  createPen(psSolid, penWidth, couleurLoc);
  tracePoints;
end;// TraceFilDeFer 

procedure Tdessin.Draw;
const
  marge = 3;
var
  Haut, Larg: integer;
  x, y, h: integer;
  deltaX, deltaY: integer;
  selection : boolean;

  procedure TraceMotifTexte;
  var
    Points: array[0..2] of Tpoint;
    d: integer;

    procedure TraceVert;
    begin with Agraphe.limiteCourbe do begin
        LigneMin := top; // +d ?
        LigneMax := bottom; // -d?
        agraphe.segment(x1i, LigneMin, x1i, LigneMax);
    end end;

    procedure TraceHoriz;
    begin with Agraphe.limiteCourbe do begin
        LigneMin := left + d;
        LigneMax := right - d;
        agraphe.segment(LigneMin, y1i, LigneMax, y1i);
    end end;

    procedure TraceRappel;
    begin with Agraphe.limiteCourbe do begin
        agraphe.segment(x1i, y1i, x1i, bottom);
        agraphe.segment(left, y1i, x1i, y1i);
    end end;

    procedure TraceFleche(x11i,y11i,x22i,y22i : integer);
    begin
        deltaX := x22i - x11i;
        deltaY := y22i - y11i;
        agraphe.segment(x11i, y11i, x22i, y22i);
        d := round(sqrt(sqr(deltaX) + sqr(deltaY)));
        if d < h * 3 then exit;
        deltaX := round(h * deltaX / d);
        deltaY := round(h * deltaY / d);
    // x,y = centre de la base de la flèche
        y := y11i + deltaY;
        x := x11i + deltaX;
        deltax := round(deltax / 3);
        deltay := round(deltay / 3);
        Points[0] := Point(x11i, y11i);
        Points[1] := Point(x - deltaY, y + deltaX);
        Points[2] := Point(x + deltaY, y - deltaX);
    //    CreateSolidBrush(pen.color);
        agraphe.canvas.polygon(Points);
        agraphe.canvas.brush.style := bsClear;
      end;

  begin  // TraceMotifTexte
    with cadre do
      if (bottom > y1i) and (top < y1i) and (left < x1i) and
        (right > x1i) then
        exit;
    d := Agraphe.HautCarac;
    h := h div 2;
    case motifTexte of
      mtNone: ;
      mtFleche: traceFleche(x1i,y1i,x2i,y2i);
      mtDoubleFleche: begin
         traceFleche(x1i,y1i,x2i,y2i);
         traceFleche(x2i,y2i,x1i,y1i);
      end;
      mtCroix: begin
        agraphe.segment(x1i, y1i + d, x1i, y1i - d);
        agraphe.segment(x1i + d, y1i, x1i - d, y1i);
      end;
      mtVert: traceVert;
      mtHoriz: traceHoriz;
      mtLigneRappel: traceRappel;
    end;
  end; // TraceMotifTexte

  procedure TraceMotifLigne;
  var dX,dY : integer;

    procedure TraceFleche(x11i,y11i : integer);
    var Points: array[0..2] of Tpoint;
        dXX,dYY : integer;
    begin
        y := y11i + dY;
        x := x11i + dX;
        dxx := dx div 3;
        dyy := dy div 3;
        Points[0] := Point(x11i, y11i);
        Points[1] := Point(x - dYY, y + dXX);
        Points[2] := Point(x + dYY, y - dXX);
   //     CreateSolidBrush(pen.color);
        agraphe.canvas.polygon(Points);
        agraphe.canvas.brush.style := bsClear;
      end;

  begin
    if motifTexte=mtDoubleFleche then begin
         dX := (x2i - x1i)*8 div 100;
         dY := (y2i - y1i)*8 div 100;
         traceFleche(x1i,y1i);
         dX := -dX;
         dY := -dY;
         traceFleche(x2i,y2i);
    end;
  end; // TraceMotifLigne

  procedure TraceLigneRappel;
  begin
    agraphe.canvas.MoveTo(x1i, y1i);
    with cadre do
      if bottom < y1i then
        agraphe.canvas.LineTo((right + left) div 2, bottom)
      else if top > y1i then
        agraphe.canvas.LineTo((right + left) div 2, top)
      else if left > x1i then
        agraphe.canvas.LineTo(left, (top + bottom) div 2)
      else if right < x1i then
        agraphe.canvas.LineTo(right, (top + bottom) div 2);
  end;

var
  i, z, signeY, long: integer;
  a, b, dX, dY,coeff : double;
  indexX : integer;
  OKp : boolean;
  largeur,oldH : integer;
begin // Tdessin.Draw
  oldH := agraphe.canvas.font.height;
  NumeroPage := pageCourante;
  if (numPage > 0) then
  if isTexte then begin
     if (numPage <= NbrePages) then begin
       NumeroPage := numPage;
       if identification=identNone then begin
          if not pages[NumeroPage].active then exit;
          pen.color := GetCouleurPages(numeroPage);
       end;
     end;
  end // texte
  else begin // ligne
       OKp := numPage <= NbrePages;
       if OKp then if aGraphe.superposePage
          then OKp := pages[NumPage].active
          else OKp := NumPage=pageCourante;
       if not OKp then exit;
       if aGraphe.superposePage then
          pen.color := GetCouleurPages(numPage);
       NumeroPage := numPage;
  end;
  if Agraphe.axeYpositif
     then signeY := -1
     else signeY := +1;
  selection := agraphe.dessinCourant=self;
  if nomX<>'' then begin
     indexX := indexNom(nomX);
     if (indexX<>grandeurInconnue) and
        (grandeurs[indexX].genreG in [Constante,ConstanteGlb,ParamGlb,ParamNormal]) then begin
        x1 := grandeurs[indexX].valeurCourante;
     end
     else nomX := '';
  end;
  if nomY<>'' then begin
     indexX := indexNom(nomY);
     if (indexX<>grandeurInconnue) and
        (grandeurs[indexX].genreG in [Constante,ConstanteGlb,ParamGlb,ParamNormal]) then begin
        y1 := grandeurs[indexX].valeurCourante;
     end
     else nomY := '';
  end;
  if selection and not isTexte
     then if pen.Width<3
         then largeur := 2*pen.width
         else largeur := 3*pen.width div 2
     else largeur := pen.Width;
  agraphe.createPen(pen.style, largeur, pen.color);
  agraphe.canvas.pen.mode := pmCopy;
  if (identification = identCoord) then begin
    for i := 0 to pred(Agraphe.courbes.Count) do
      with Agraphe.courbes[i] do
        if (numCoord = indexNom(varY.nom)) then begin
          if (NumPoint < debutC) or (numPoint > finC) then
            numPoint := (debutC + finC) div 2;
          x1 := valX[numPoint];
          y1 := valY[numPoint];
          iMonde := iMondeC;
          break;
        end;
        motifTexte := MotifIdent;
        hauteur := hauteurIdent;
        avecCadre := avecCadreIdent;
        isOpaque := isOpaqueIdent;
        couleurFond := couleurFondIdent;
  end;
  if isTitre then
    with Agraphe.limiteFenetre do begin // x = position en %
      x1i := round(left + (right - left) * x1);
      y1i := round(top + (top - bottom) * y1);
      x2i := round(left + (right - left) * x2);
      y2i := round(top + (top - bottom) * y2);
    end
  else begin
    Agraphe.WindowRT(x1, y1, iMonde, x1i, y1i);
    Agraphe.WindowRT(x2, y2, iMonde, x2i, y2i);
  end;
  agraphe.setTextAlign(alignement);
  if isTexte then begin // Vérifier texte visible
    with Agraphe.LimiteFenetre do begin
      if x2i > right then
        x2i := (4 * right + left) div 5;
      if x2i < left then
        x2i := (4 * left + right) div 5;
      if y2i > bottom then
        y2i := (4 * bottom + top) div 5;
      if y2i < top then
        y2i := (4 * top + bottom) div 5;
    end;
    TexteLoc.Assign(texte);
    if IsOpaque and not(ImprimanteMono) then begin
       agraphe.canvas.brush.style := bsSolid;
       agraphe.canvas.brush.color := couleurFond;
    end
    else agraphe.canvas.brush.style := bsClear;
    agraphe.canvas.font.Color := pen.color;
    with Agraphe.limiteFenetre do
         h := (right - left + bottom - top) div 2;
    h := round(h * (hauteur / 100.0));
    agraphe.canvas.Font.Height := h;
    if vertical then agraphe.Canvas.font.orientation := 900;
    i := 0;
    while i<texteLoc.Count do begin
       setPourCent(i);
       inc(i);
    end;
    if souligne then
       agraphe.canvas.Font.style := [fsUnderLine];
    cadre.right := x2i;
    cadre.left := x2i;
    i := 0;
    while i < texteLoc.Count do begin
      if texteLoc[i] = ''
         then texteLoc.Delete(i)
         else Inc(i);
    end;
    if centre and not vertical then begin
      Haut := agraphe.canvas.TextHeight('A');
      y2i := y2i - signeY * haut * pred(texteLoc.Count) div 2;
      with agraphe.limiteFenetre do
           if y2i < (top+haut) then y2i := top + haut;
    end;
    cadre.top := y2i;
    cadre.bottom := y2i;
    for i := 0 to pred(texteLoc.Count) do begin
      long := length(texteLoc[i]);
      Haut := agraphe.canvas.TextHeight(texteLoc[i]);
      Larg := agraphe.canvas.TextWidth(texteLoc[i]);
      if centre then
        if vertical then begin
          deltaX := Haut div 2;
          x := x2i - deltaX + 2 * deltaX * i;
          if i = 0 then
            cadre.right := x - marge;
          if i = pred(texteLoc.Count) then
            cadre.left := x + 2 * deltaX + marge;
        end
        else begin
          deltaX := Larg div 2;
          x := x2i - deltaX;
          z := x - marge;
          if cadre.right > z then
            cadre.right := z;
          z := x2i + deltaX + marge;
          if cadre.left < z then
            cadre.left := z;
        end
      else begin
        x := x2i;
        z := x+Larg;
        if cadre.right<z then
           cadre.right := z;
      end;
      if centre then
        if vertical then begin
          deltaY := Larg div 2;
          y := y2i + signeY * deltaY;
          z := y2i - deltaY - marge;
          if cadre.top > z then
            cadre.top := z;
          z := y2i + deltaY + marge;
          if cadre.bottom < z then
            cadre.bottom := z;
        end
        else begin
          deltaY := Haut div 2;
          y := y2i + deltaY * (2 * i - 1) * signeY;
          if i = 0 then
            if Agraphe.axeYpositif
               then cadre.bottom := y - marge
               else cadre.top := y - marge;
          if i = pred(texteLoc.Count) then
            if Agraphe.axeYpositif
               then cadre.top := y - 2 * (deltaY + marge)
               else cadre.bottom := y + 2 * (deltaY + marge);
        end
      else begin
        deltaY := Haut;
        y := y2i+deltaY*i;
        if Agraphe.axeYpositif
           then begin
              z := y+deltaY;
              if cadre.bottom<z then cadre.bottom := z
           end
           else begin
              z := y-deltaY;
              if cadre.top>z then cadre.top := z;
           end;
      end;
 //     if IsOpaque and not(ImprimanteMono) then agraphe.canvas.Rectangle(cadre);
      agraphe.canvas.TextOut(x,y,texteLoc[i]);
    end; // for i
    if (avecCadre and not isOpaque and not selection) then
    agraphe.canvas.Rectangle(cadre);
    if selection then begin
       agraphe.createPen(psSolid, 1, clRed);
       agraphe.canvas.Rectangle(cadre);
    end;
    if avecLigneRappel then TraceLigneRappel;
    TraceMotifTexte;
  end  // texte
  else begin // ligne
    if nomX<>''
       then with Agraphe.LimiteCourbe do begin
            y1i := bottom;
            y2i := (9 * top + bottom) div 10;
            x2i := x1i;
            agraphe.segment(x1i, y1i, x2i, y2i)
       end
       else if nomY<>''
         then with Agraphe.LimiteCourbe do begin
            x1i := left;
            x2i := (9 * left + right) div 10;
            y2i := y1i;
            agraphe.segment(x1i, y1i, x2i, y2i)
         end
         else agraphe.segment(x1i, y1i, x2i, y2i);
       traceMotifLigne;
       if (texteLigne<>tlNone) and
          (sousDessin=nil) then begin
                  sousDessin := Tdessin.Create(agraphe);
                  sousDessin.avecLigneRappel := true;
                  sousDessin.isTexte := true;
                  sousDessin.proprietaire := self;
                  sousDessin.identification := identDroite;
                  if (x1i+x2i)>320 then coeff := 0.9 else coeff := 1.1;
                  sousDessin.x2 := coeff*(x1+x2)/2;
                  if (y1i+y2i)>240 then coeff := 0.9 else coeff := 1.1;
                  sousDessin.y2 := coeff*(y1+y2)/2;
                  Agraphe.dessins.add(sousDessin);
              end;
    if (TexteLigne <> tlNone) and (sousDessin <> nil) then begin
      sousDessin.Texte.Clear;
      Agraphe.MondeRT((x1i + x2i) div 2, (y1i + y2i) div 2, iMonde,
        sousDessin.x1, sousDessin.y1);
      sousDessin.pen.color := pen.color;
      case TexteLigne of
      tlEcartY: if y2i <> y1i then begin
            with Agraphe.monde[mondeY] do
              case Graduation of
                gLog: dY := log10(y2 / y1);
                gInv: dY := 1 / y2 - 1 / y1;
                gLin: dY := y2 - y1;
                gdB: dY  := 20 * log10(y2 / y1);
                else dY := 0;
              end;
            sousDessin.Texte.Add(Agraphe.monde[mondeY].axe.formatNomEtUnite(dY));
        end;
        tlEcartX: if x2i <> x1i then begin
            with Agraphe.monde[mondeX] do
              case Graduation of
                gLog: dX := log10(x2 / x1);
                gInv: dX := 1 / x2 - 1 / x1;
                gLin: dX := x2 - x1;
                else dX := 0;
              end;
            sousDessin.Texte.Add(Agraphe.monde[mondeX].axe.formatNomEtUnite(dX));
        end;
        tlPente: if x2i <> x1i then begin
            Agraphe.setUnitePente(mondeY);
            with Agraphe.monde[mondeY] do
              case Graduation of
                gLog: dY := log10(y2 / y1);
                gInv: dY := 1 / y2 - 1 / y1;
                gLin: dY := y2 - y1;
                gdB: dY  := 20 * log10(y2 / y1);
                else dY := 0;
              end;
            with Agraphe.monde[mondeX] do
              case Graduation of
                gLog: dX := log10(x2 / x1);
                gInv: dX := 1 / x2 - 1 / x1;
                gLin: dX := x2 - x1;
                else
                  dX := 1;
              end;
            sousDessin.Texte.Add(Agraphe.unitePente.formatNomPente(dY / dX));
          end;
        tlEquation: if abs(x2i - x1i) < 2 then
            sousDessin.Texte.Add(Agraphe.monde[mondeX].axe.formatNomEtUnite((x1 + x2) / 2))
          else if abs(y2i - y1i) < 2 then
            sousDessin.Texte.Add(Agraphe.monde[mondeY].axe.formatNomEtUnite((y1 + y2) / 2))
          else begin
            a := (y2 - y1) / (x2 - x1);
            b := y2 - a * x2;
            sousDessin.Texte.Add(Agraphe.monde[mondeY].axe.nom + '=' +
              formatReg(b) + '+' +
              formatReg(a) + '*' + Agraphe.monde[mondeX].axe.nom);
          end;
      end; // case TexteLigne
    end;
  end;
  agraphe.canvas.Font.style  := [];
  agraphe.canvas.Font.orientation := 0;
  agraphe.canvas.brush.color := clWindow;
  agraphe.canvas.font.height := oldH;
end; // Tdessin.Draw

procedure TgrapheReg.TraceIdentPages;
var hauteur : integer;

Procedure TraceMarque(page : codePage);
var prompt : string;
    i,j : integer;
    motif : Tmotif;
    x,y : integer;
    R: TRect;
    premierParam : boolean;
begin
      if reperePage=SPcouleur
        then canvas.pen.color := couleurPages[Page mod NbreCouleur]
        else canvas.pen.color := courbes[0].couleur;
      canvas.font.color := canvas.pen.color;
      if reperePage=SPmotif
        then motif := motifPages[Page mod NbreCouleur]
        else motif := courbes[0].motif;
      if (ChoixIdentPagesDlg=nil) then
          Application.CreateForm(TChoixIdentPagesDlg, ChoixIdentPagesDlg);
      y := (page-1)*hauteur+hautCarac;
      x := LimiteCourbe.right+marge;
      if choixIdentPagesDlg.commentaireCB.checked
         then prompt := Pages[Page].commentaireP+crCR+crLF
         else prompt := '';
      try
      premierParam := true;
      for i := 0 to pred(NbreConst) do
          if choixIdentPagesDlg.ListeConstBox.checked[i] then begin
             if not premierParam then prompt := prompt +' ; ';
             premierParam := false;
             j := indexConst[i];
             prompt := prompt +
                Grandeurs[j].formatNomEtUnite(Pages[Page].ValeurConst[j]);
          end;
      for i := 1 to NbreParam[paramNormal] do
          if choixIdentPagesDlg.ListeConstBox.checked[NbreConst+i-1] then begin
             if not premierParam then prompt := prompt +' ; ';
             premierParam := false;
             prompt := prompt +
                Parametres[paramNormal,i].formatNomEtUnite(Pages[Page].ValeurParam[paramNormal,i]);
          end;
      if prompt='' then prompt := Pages[Page].commentaireP;
      case reperePage of    // espace pour les marques
         SPmotif : prompt := '      '+prompt;
         SPstyle : prompt := '         '+prompt;
      end;
      //SetRect(R, x, y, limiteFenetre.right, y+hauteur);
      InflateRect(R, -1, -1);
      Canvas.MoveTo(x,y);
      Canvas.LineTo(limiteFenetre.right,y);
      case reperePage of
         SPmotif : traceMotif(x+largCarac,y+2*hautCarac div 3,motif);
         SPstyle : begin
           createPen(stylePages[Page mod NbreCouleur],penWidth,canvas.Pen.color);
           y := y+hautCarac div 2;
           segment(x,y,x+3*largCarac,y);
           canvas.pen.style := psSolid;
         end;
      end;
      except // protection ListConstBox
        choixIdentPagesDlg.initParam;
      end;
end; // TraceMarque

var page : integer;
begin
    hauteur := (limiteCourbe.bottom-limiteCourbe.top) div NbrePages;
    for page := 1 to NbrePages do
        if Pages[page].active then traceMarque(page);
end;

procedure Tdessin.DrawLatex;

  procedure TraceMotifTexte;

    procedure TraceVert;
    begin
        y1 := agraphe.monde[mondeY].mini;
        y2 := agraphe.monde[mondeY].maxi;
        // segment(x1, y1, x1, y2);
    end;

    procedure TraceHoriz;
    begin
        x1 := agraphe.monde[mondeX].mini;
        x2 := agraphe.monde[mondeX].maxi;
        // segment(x1, y1, x2, y1);
    end;

    procedure TraceRappel;
    begin with Agraphe.limiteCourbe do begin
 (*  p. 188
    |- (axis cs:'+x1+y1+') node[near start,left] {$\frac{dy}{dx} = -1.58$};
*)
        // segment(x1, y1, x1, bottom);
        // segment(left, y1, x1, y1);
      end;
    end;

  begin  // TraceMotifTexte
  (* p. 183
  \node[pin=135:{$('+x1+y1+')$}] at (axis cs:'+x1+y1+') {};
  p. 188
  \node[coordinate,pin=above:{texte}] at (axis cs:'+x1+y1+') {};
  *)
    case motifTexte of
      mtNone: ;
      mtFleche: ;
      mtDoubleFleche: ;
      mtCroix: begin
        // segment(x1, y1, x1, y1);
        // segment(x1, y1, x1, y1);
      end;
      mtVert: traceVert;
      mtHoriz: traceHoriz;
      mtLigneRappel: traceRappel;
    end;
  end; // TraceMotifTexte

  procedure TraceMotifLigne;
  begin
    if motifTexte=mtDoubleFleche then begin
         // traceFleche(x1i,y1i);
         // traceFleche(x2i,y2i);
(*   pgfplots p. 250
         \pgfplotsextra{%
\pgfpathmoveto{\pgfplotspointaxisxy{'+x1+'}{'+y1+'}}%
\pgfpathlineto{\pgfplotspointaxisxy{'+x2+'}{'+y2+'}}%
\pgfusepath{stroke}%}
*)
    end;
  end; // TraceMotifLigne

  procedure TraceLigneRappel;
  begin
  (*
(*  p. 188
    |- (axis cs:'+x1+y1+') node[near start,left] {$texte$};
    |- (axis cs:'+x1+y1+') node[near start,right] {$texte$};
*)
    with cadre do
      if bottom < y1i then // bottom
      else if top > y1i then // top
      else if left > x1i then // left
      else if right < x1i then // right
  end;

var
  i : integer;
  indexX : integer;
  OKp : boolean;
begin // Tdessin.DrawLatex
  exit;
  NumeroPage := pageCourante;
  if (numPage > 0) then
  if isTexte then begin
     if (numPage <= NbrePages) then begin
       NumeroPage := numPage;
       if identification=identNone then begin
          if not pages[NumeroPage].active then exit;
          pen.color := GetCouleurPages(numeroPage);
       end;
     end;
  end // texte
  else begin
       OKp := numPage <= NbrePages;
       if OKp then if aGraphe.superposePage
          then OKp := pages[NumPage].active
          else OKp := NumPage=pageCourante;
       if not OKp then exit;
       if aGraphe.superposePage then
          pen.color := GetCouleurPages(numPage);
       NumeroPage := numPage;
  end;
  if nomX<>'' then begin
     indexX := indexNom(nomX);
     if (indexX<>grandeurInconnue) and
        (grandeurs[indexX].genreG in [Constante,ConstanteGlb,ParamGlb,ParamNormal]) then
        x1 := grandeurs[indexX].valeurCourante
     else nomX := '';
  end;
  if nomY<>'' then begin
     indexX := indexNom(nomY);
     if (indexX<>grandeurInconnue) and
        (grandeurs[indexX].genreG in [Constante,ConstanteGlb,ParamGlb,ParamNormal]) then
        y1 := grandeurs[indexX].valeurCourante
     else nomY := '';
  end;
  if isTitre then  ; // title
  if isTexte then begin
    TexteLoc.Assign(texte);
// IsOpaque ; couleurFond;
// font.Color := pen.color;
    i := 0;
    while i<texteLoc.Count do begin
       setPourCent(i);
       inc(i);
    end;
    if souligne then
// Font.style := [fsUnderLine];
    for i := 0 to pred(texteLoc.Count) do begin
//     centre ; vertical
// TextOut(x1,y1,texteLoc[i]);
    end;
// avecCadre ; isOpaque
    if avecLigneRappel then TraceLigneRappel;
    TraceMotifTexte;
  end  // texte
  else begin // ligne
    if nomX<>''
       then with Agraphe.LimiteCourbe do begin
            y1 := Agraphe.monde[mondeY].maxi;
            y2 := Agraphe.monde[mondeY].mini;
            // segment(x1, y1, x1, y2)
       end
       else if nomY<>''
         then with Agraphe.LimiteCourbe do begin
            x1 := Agraphe.monde[mondeY].maxi;
            x2 := Agraphe.monde[mondeY].mini;
            // segment(x1, y1, x2, y1)
         end
         else // segment(x1, y1, x2, y2);
       traceMotifLigne;
  end; // ligne
end; // Tdessin.DrawLatex

procedure TGrapheReg.Draw;

  procedure TraceEquivalence(p: codePage);
  const
    maxTexte = 16;
  var
    c, NbreTexteX,nbreTexteY, i: integer;
    xi, yi : integer;
    str: string;
    NumEquX,NumEquY: array[1..MaxTexte] of integer;
    affiche: boolean;
    dx, dy: double;
  begin
    for c := 1 to maxTexte do begin
       numEquX[c] := -1; numEquY[c] := -1;
    end;
    NbreTexteX := 0; NbreTexteY := 0;
    canvas.font.Height := HautCarac;
    with limiteCourbe do begin
       dy := canvas.TextHeight('0')+4;
       dx := canvas.TextWidth('000000');
    end;
    for c := 0 to pred(equivalences[p].Count) do begin
      equivalences[p].items[c].draw;
      affiche := p=pageCourante;
      if affiche then case equivalences[p].items[c].ligneRappel of
         lrX : for i := 1 to NbreTexteX do
           affiche := abs(equivalences[p].items[c].x2i -
              equivalences[p].items[numEquX[i]].x2i) > dx;
         lrY,lrPente : affiche := false;
         lrTangente : for i := 1 to NbreTexteX do
           affiche := abs(equivalences[p].items[c].vei -
              equivalences[p].items[numEquX[i]].vei) > dx;
         else for i := 1 to NbreTexteX do // lrXdeY lrReticule lrEquivalence
           affiche := abs(equivalences[p].items[c].vei -
              equivalences[p].items[numEquX[i]].vei) > dx;
      end;
      if affiche and (NbreTexteX < maxTexte) then begin
        Inc(NbreTexteX);
        numEquX[NbreTexteX] := c;
      end;
      affiche := p=pageCourante;
      if affiche then case equivalences[p].items[c].ligneRappel of
         lrX,lrPente : affiche := false;
         lrY : for i := 1 to NbreTexteY do
            affiche := abs(equivalences[p].items[c].y1i -
                            equivalences[p].items[numEquY[i]].y1i) > dy;
         lrTangente : for i := 1 to NbreTexteY do
           affiche := abs(equivalences[p].items[c].phei -
              equivalences[p].items[numEquY[i]].phei) > dy;
         else for i := 1 to NbreTexteY do // lrXdeY lrReticule lrEquivalence
           affiche := abs(equivalences[p].items[c].pHei -
              equivalences[p].items[numEquY[i]].pHei) > dy;
      end;
      if affiche and (NbreTexteY < maxTexte) then begin
        Inc(NbreTexteY);
        numEquY[NbreTexteY] := c;
      end;
    end;
    canvas.font.style := [fsBold];
    for i := 1 to NbreTexteY do with equivalences[p].items[numEquY[i]] do begin
        canvas.font.Color := colorToRGB(pen.color);
        WindowRT(ve, pHe, mondepH, xi, yi);
        str := '';
        case LigneRappel of
            lrTangente : begin
               setUnitePente(mondepH);
               str := unitePente.formatNomPenteUnite(pente);
            end;
            lrX,lrPente : ;
            lrY : begin
                 if monde[mondepH].axe<>nil
                    then str := monde[mondepH].axe.formatNombre((y2-y1)/ monde[mondepH].exposant) + ' '
                    else str := formatReg((y2-y1)/ monde[mondepH].exposant) + ' ';
                 xi := LimiteCourbe.right - marge;
                 yi := (y1i+y2i) div 2;
            end;
            else begin  // lrXdeY lrReticule lrEquivalence
              if monde[mondepH].axe = nil
                 then str := formatReg(pHe / monde[mondepH].exposant) + ' '
                 else str := monde[mondepH].axe.formatNombre(pHe / monde[mondepH].exposant) + ' ';
              yi := yi + margeCaracY;
              case mondepH of
                 mondeY: xi := limiteCourbe.left + marge;
                 mondeDroit: xi := limiteCourbe.right - marge;
              end;// case monde
              if commentaire <> '' then
                 MyTextOut(xi + largCarac, yi - hautCarac, commentaire);
            end;
        end;// case ligneRappel
        if (xi > limiteCourbe.right div 2)
           then SetTextAlign(TA_right + TA_top)
           else SetTextAlign(TA_left + TA_top);
        if str<>'' then MyTextOutFond(xi, yi, str,FondReticule);
       end;
       SetTextAlign(TA_left + TA_top);
       for i := 1 to NbreTexteX do with equivalences[p].items[numEquX[i]] do begin
          canvas.font.Color := colorToRGB(pen.color);
          str := '';
          case LigneRappel of
            lrX : begin
                 if monde[mondeX].axe=nil
                    then str := formatReg((x2-x1)/ monde[mondeX].exposant) + ' '
                    else str := monde[mondeX].axe.formatNombre((x2-x1)/ monde[mondeX].exposant) + ' ';
                 xi := (x2i+x1i) div 2;
                 yi := limiteCourbe.top;
            end;
            lrY,lrPente,lrTangente : ;
            else begin  // lrXdeY lrReticule lrEquivalence
              if monde[mondeX].axe = nil
                 then str := formatReg(ve / monde[mondeX].exposant) + ' '
                 else str := monde[mondeX].axe.formatNombre(ve / monde[mondeX].exposant) + ' ';
              WindowRT(ve, pHe, mondepH, xi, yi);
              xi := xi + marge;
              yi := basCourbe + margeCaracY;
            end;
         end;// case ligneRappel
         if str<>'' then MyTextOutFond(xi-(canvas.textWidth(str) div 2), yi, str,FondReticule);
      end;
      SetTextAlign(TA_left + TA_top);
      canvas.font.style := [];
  end; // TraceEquivalence

  procedure TraceEquivalenceParam;
  var
    c: integer;
    xi,yi: integer;
    str: string;
  begin
    for c := 0 to pred(equivalences[0].Count) do
      with equivalences[0].items[c] do begin
        draw;
        canvas.font.Color := colorToRGB(pen.color);
        canvas.font.Height := HautCarac;
        WindowRT(ve, pHe, mondepH, xi, yi);
        if (xi > (2 * limiteCourbe.right div 3)) then  ;
        SetTextAlign(TA_right + TA_top);
        if LigneRappel=lrReticule then begin
               if monde[mondepH].axe = nil
                  then str := formatReg(pHe / monde[mondepH].exposant) + ' '
                  else str := monde[mondepH].axe.formatNombre(pHe / monde[mondepH].exposant) + ' ';
               case mondepH of
                  mondeY: MyTextOutFond(limiteCourbe.left +
                       marge, yi + margeCaracY, str,FondReticule);
                  mondeDroit: MyTextOutFond(
                     limiteCourbe.right - succ(
                     monde[mondeDroit].NbreChiffres) * largCarac,
                     yi + margeCaracY, str,FondReticule);
               end;// case monde
               if monde[mondeX].axe = nil
                   then str := formatReg(ve / monde[mondeX].exposant) + ' '
                   else str := monde[mondeX].axe.formatNombre(ve / monde[mondeX].exposant) + ' ';
               myTextOutFond(xi + marge, basCourbe + margeCaracY, str, FondReticule);
               SetTextAlign(TA_left + TA_top);
        end;
      end;
  end; // TraceEquivalenceParam

  procedure AfficheEtiquetteSegment;
  var
    couleurCurseur: Tcolor;

    procedure DebutSegment;

      procedure InitCurseur(c: integer);
      var ecart: integer;
      begin with curseurOsc[c] do begin
          if mondeC = mondeX then exit;
          with courbes[indexCourbe] do begin
            ecart := (finC + debutC) div 3;
            if (Ic <= debutC) or (Ic >= finC) then
              Ic := (c - curseurData1 + 1)* ecart;
            if (c = curseurData2) and (curseurOsc[curseurData1].Ic = curseurOsc[curseurData2].Ic) then
              if curseurOsc[curseurData1].ic > ((finC + debutC) div 2)
                 then curseurOsc[curseurData2].ic := ecart
                 else curseurOsc[curseurData2].ic := 2 * ecart;
            Xr := valX[Ic];
            Yr := valY[Ic];
            windowRT(Xr, Yr, iMondeC, Xc, Yc);
            CCourant := c;
  // Attention au zoom
            if (Xr<monde[mondeX].Mini) or (Xr>monde[mondeX].Maxi) then begin
               Xr:= monde[mondeX].Mini + (monde[mondeX].Maxi-monde[mondeX].Mini)*(c-CurseurData1+1)/3;
               Ic := PointProcheReal(Xr, 0,courbes[indexCourbe], MondeX);
               Yr := valY[Ic];
               windowRT(Xr, Yr, iMondeC, Xc, Yc);
            end;
          end;
      end; end;

    var
      j: integer;
      i: integer;
    begin with canvas do begin
        for j := curseurData1 to curseurData2 do begin
            CurseurOsc[j].mondeC := mondeX;
            CurseurOsc[j].indexCourbe := -1;
        end;
        for i := 0 to pred(courbes.Count) do
          with courbes[i] do
            if page = pageCourante then
              for j := curseurData1 to curseurData2 do
                if (CurseurOsc[j].grandeurCurseur <> nil) and
                   (CurseurOsc[j].indexCourbe = -1) and
                  (CurseurOsc[j].grandeurCurseur = varY) then begin
                  CurseurOsc[j].mondeC := iMondeC;
                  CurseurOsc[j].indexCourbe := i;
                end;
        InitCurseur(curseurData1);
        InitCurseur(curseurData2);
        if CurseurOsc[curseurData1].mondeC <> CurseurOsc[curseurData2].mondeC then
          exclude(optionCurseur, coPente);
        if not(coPente in optionCurseur) then exit;
        setUnitePente(CurseurOsc[curseurData1].mondeC);
    end; end; // DebutSegment

    procedure SetString(i: integer);
    var
      st: string;
      xi, yi: integer;
    begin with curseurOsc[i] do begin
        if mondeC = mondeX then exit;
        traceCurseur(i);
        windowRT(xr, yr, mondeC, xi, yi);
        if coY in OptionCurseur then begin
          if monde[mondeC].graduation = gdB
             then st := GrandeurCurseur.nom + '=' + formatGeneral(
                 20 * log10(yr), precisionMin) + ' dB'
             else st := GrandeurCurseur.formatValeurEtUnite(Yr);
          MyTextOutFond(LimiteCourbe.left + marge,
                        yi + margeCaracY div 2,st,FondReticule);
          valeurCurseur[coY] := yr;
        end;
        if coX in OptionCurseur then begin
          st := monde[mondeX].Axe.formatValeurEtUnite(Xr);
          MyTextOutFond(xi - largCarac * length(st) div 2,
                        BasCourbe + margeCaracY,st,FondReticule);
          valeurCurseur[coX] := xr;
        end;
      end;
    end; // SetString

    procedure StrPente;
    var
      st: string;
      dx, dy: double;
      dxNul: boolean;
      largeur: integer;
      xi, yi, x1, y1, x2, y2: integer;
      posTrait, signe: integer;
    begin
        dxNul := CurseurOsc[curseurData1].Xc = CurseurOsc[curseurData2].Xc;
        dx := 1;
        dy := 1;
        tracePente;
        createPen(psSolid, penWidth, couleurCurseur);
        try
          windowRT((CurseurOsc[curseurData1].xr + CurseurOsc[curseurData2].xr) / 2,
                  (CurseurOsc[curseurData1].yr + CurseurOsc[curseurData2].yr) / 2, curseurOsc[1].mondeC, xi, yi);
          windowRT(CurseurOsc[curseurData2].xr, CurseurOsc[curseurData2].yr, CurseurOsc[curseurData2].mondeC, x2, y2);
          windowRT(CurseurOsc[curseurData1].xr, CurseurOsc[curseurData1].yr, CurseurOsc[curseurData1].mondeC, x1, y1);
          if ((coDeltaY in optionCurseur) or (coPente in optionCurseur)) and
            (CurseurOsc[curseurData1].mondeC = CurseurOsc[curseurData2].mondeC) then begin
            with monde[CurseurOsc[curseurData1].mondeC] do
              case Graduation of
                gLog: begin
                  dY := abs(CurseurOsc[curseurData1].yr / CurseurOsc[curseurData2].yr);
                  st := ' ' + axe.nom + '*' + formatReg(dY);
                  dY := log10(dY);
                end;
                gInv: begin
                  dY := abs(1 / CurseurOsc[curseurData1].yr - 1 / CurseurOsc[curseurData2].yr);
                  st := ' d(' + axe.nom + ')=' + formatReg(dY);
                end;
                gLin: begin
                  dY := abs(CurseurOsc[curseurData1].yr - CurseurOsc[curseurData2].yr);
                  st := axe.FormatValeurEtUnite(dY);
                end;
                gdB: begin
                  dY := 20 * abs(log10(curseurOsc[2].Yr / curseurOsc[1].Yr));
                  st := formatGeneral(dY, precisionMin) + ' dB';
                end;
              end;{case}
            if (coDeltaY in optionCurseur) and
               (CurseurOsc[curseurData1].mondeC = CurseurOsc[curseurData2].mondeC) then begin
              largeur  := length(st) * largCarac div 2;
              posTrait := LimiteCourbe.right - largeur;
              segment(posTrait, y1, posTrait, y2);
              // fléches
              if y1 < y2
                 then signe := 1
                 else signe := -1;
              Segment(posTrait - marge, y1 + 3 * signe * marge, posTrait, y1);
              Segment(posTrait + marge, y1 + 3 * signe * marge, posTrait, y1);
              signe := -signe;
              Segment(posTrait - marge, y2 + 3 * signe * marge, posTrait, y2);
              Segment(posTrait + marge, y2 + 3 * signe * marge, posTrait, y2);
              MyTextOutFond(LimiteCourbe.right - largeur * 2, yi,st,fondReticule);
              valeurCurseur[coDeltaY] := dY;
            end;
          end;
          if (coDeltaX in optionCurseur) or (coPente in optionCurseur) then begin
            with monde[mondeX] do
              case Graduation of
                gLog: begin
                  dX := abs(CurseurOsc[curseurData2].xr / CurseurOsc[curseurData1].xr);
                  st := ' ' + axe.nom + '*' + formatReg(dX);
                  dX := log10(dX); { décade }
                end;
                gInv: begin
                  dX := abs(1 / CurseurOsc[curseurData1].xr - 1 / CurseurOsc[curseurData2].xr);
                  st := formatReg(dX);
                end;
                gLin: begin
                  dX := abs(CurseurOsc[curseurData1].xr - CurseurOsc[curseurData2].xr);
                  st := Axe.FormatValeurEtUnite(dX);
                end;
              end; {case}
            if (coDeltaX in optionCurseur) then begin
              posTrait := limiteCourbe.top + hautCarac div 2;
              segment(x1, posTrait, x2, posTrait);
              // fléche
              if x1 < x2
                 then signe := 1
                 else signe := -1;
              Segment(x1 + signe * marge * 3, posTrait + marge, x1, posTrait);
              Segment(x1 + signe * marge * 3, posTrait - marge, x1, posTrait);
              signe := -signe;
              Segment(x2 + signe * marge * 3, posTrait + marge, x2, posTrait);
              Segment(x2 + signe * marge * 3, posTrait - marge, x2, posTrait);
              MyTextOutFond( xi - length(st) * largCarac div 2,
                             limiteCourbe.top,st,FondReticule);
              valeurCurseur[coDeltaX] := dX;
            end;// deltaX
          end;
          if not dxNul and (coPente in OptionCurseur) then begin
            st := unitePente.formatNomPente(dY / dX);
            largeur := length(st) * largCarac div 2;
            MyTextOutFond(xi - largeur, yi,st,FondReticule);
            valeurCurseur[coPente] := dY / dX;
          end;
        except
        end;
    end;

  begin
    if not(monde[mondeX].defini) then exit;
    if not(monde[mondeY].defini) and
       not(monde[mondeDroit].defini) then exit;
    DebutSegment;
    setTextAlign( TA_top + TA_left);
    if cCourant=0
       then if curseurOsc[curseurData1].indexCourbe >= 0
          then couleurCurseur := courbes[curseurOsc[curseurData1].indexCourbe].couleur
          else couleurCurseur := pColorReticule
       else couleurCurseur := CouleurCurseurs[cCourant];
    canvas.font.Color := couleurCurseur;
    if (([coPente, coDeltaX, coDeltaY] * optionCurseur) <> []) or
         (CurseurOsc[curseurData1].mondeC <> mondeX) then
        strPente;
    setString(curseurData1);
    setString(curseurData2);
  end; // AfficheEtiquetteSegment

    procedure AfficheEtiquetteReticule;

    procedure DebutSegment;
    var
      i: integer;
    begin with canvas, CurseurOsc[1] do begin
        mondeC := mondeY;
        indexReticuleModele := -1;
        for i := 0 to pred(courbes.Count) do
            with courbes[i] do
            if (page=pageCourante) and (indexModele=1) and not courbeExp then
                if indexReticuleModele=-1 then begin
                      indexReticuleModele := i;
                      mondeC := iMondeC;
                      grandeurCurseur := varY;
                   end;
        if indexReticuleModele<0 then exit; // ne devrait pas arriver !
        with courbes[indexReticuleModele] do begin
            if (Ic <= debutC) or (Ic >= finC) then
                Ic := (debutC+finC) div 2;
            Xr := valX[Ic];
            Yr := valY[Ic];
            windowRT(Xr, Yr, mondeC, Xc, Yc);
          end;
    end; end;// DebutSegment

    procedure SetString;
    var
      st: string;
      xi, yi: integer;
    begin with curseurOsc[1] do begin
        traceCurseur(1);
        windowRT(xr, yr, mondeC, xi, yi);
        if monde[mondeC].graduation = gdB
           then st := GrandeurCurseur.nom + '=' + formatGeneral(
              20 * log10(yr), precisionMin) + ' dB'
           else st := GrandeurCurseur.formatValeurEtUnite(Yr);
        MyTextOutFond(LimiteCourbe.left + marge,
                      yi + margeCaracY div 2,st,FondReticule);
        valeurCurseur[coY] := Yr;
        st := monde[mondeX].Axe.formatValeurEtUnite(Xr);
        MyTextOutFond(xi - largCarac * length(st) div 2,
                      BasCourbe + margeCaracY,st,FondReticule);
        valeurCurseur[coX] := Xr;
    end end; // SetString

  begin
    if not (monde[mondeX].defini) or not grapheOK then exit;
    DebutSegment;
    setTextAlign(TA_top + TA_left);
    canvas.font.Color := pColorReticule;
    setString;
  end; // AfficheEtiquetteReticule

var
  c, i: integer;
  p:  codePage;
  SupprDessin,CoordTrouve: boolean;
  nX: string;
  m: indiceMonde;
begin // graphe.draw
  if largeurEcran<64 then largeurEcran := 64;
  axeYpositif := axeYbas;
  with limiteFenetre do begin
    if (right - left) < 50 then exit;
    dimPoint := (bottom - top) * dimPointVGA div 512;
    if dimPoint<1 then dimPoint := 1;
    marge := (bottom-top);
    BorneFenetre.top := top-marge;
    BorneFenetre.bottom := bottom+marge;
    marge := (right-left) div 16;
    BorneFenetre.right := right+marge;
    BorneFenetre.left := left-marge;
  end;
  monde[mondeX].minInt := borneFenetre.left;
  monde[mondeX].maxInt := borneFenetre.right;
  for m := mondeY to high(indiceMonde) do begin
      monde[m].minInt := borneFenetre.top;
      monde[m].maxInt := borneFenetre.bottom;
  end;
  canvas.font.Name := FontName;
  with limiteFenetre do begin
       if imprimanteEnCours or ImageEnCours
          then canvas.Font.Height := (bottom-top) div 20
          else canvas.Font.Height := (right-left) div 60;
       if abs(canvas.Font.Size)<10 then canvas.Font.Size := 10;
       penWidth := (right-left)*penWidthVGA div 1024;
       if penWidth<1 then penWidth := 1;
  end;
  CreatePen(psDot, penWidth, clBlack);
  canvas.brush.style := bsClear;
  canvas.pen.mode := pmCopy;
  LargCarac := canvas.textWidth('A');
  HautCarac := canvas.textHeight('A');
  Marge := largCarac div 2;
  MargeHautBas := hautCarac div 2;
  if axeYpositif
     then margeCaracY := hautCarac + marge
     else margeCaracY := -hautCarac - marge;
  EmpilePage := (OgAnalyseurLogique in optionGraphe) and SuperposePage;
  if (OgOrthonorme in OptionGraphe) and not
    (OgPolaire in OptionGraphe) and (monde[mondeY].axe <> nil) and
    (monde[mondeX].axe <> nil) and
    (monde[mondeY].axe.nomUnite <> monde[mondeX].axe.nomUnite) then
    exclude(OptionGraphe, ogOrthonorme);
  chercheMonde;
  if not grapheOK then exit;
  setRepere;
  UnAxeX := True;
  if (courbes.Count > 0) and (courbes[0].varx <> nil) then begin
    nX := courbes[0].varx.nom;
    for c := 1 to pred(Courbes.Count) do
      with courbes[c] do
        if (varX <> nil) and (varX.nom <> nX) then
          unAxeX := False;
  end;
  if (courbes.Count > 0) then
    drawAxis(graduationZeroX or graduationZeroY);
  indexHisto := 0;
  if withDebutFin then TraceBorneFFT;
  for c := 0 to pred(Courbes.Count) do
      traceCourbe(c);
  c := 0;
  while (c < Dessins.Count) do
    with dessins[c] do
      if isTexte then begin
        SupprDessin := True;
        for i := 0 to pred(texte.Count) do
          if length(texte[i]) > 0 then
            SupprDessin := False;
          if (identification = identCoord) then begin
              CoordTrouve := false;
              for i := 0 to pred(courbes.Count) do begin
                  CoordTrouve := numCoord = indexNom(courbes[i].varY.nom);
                  if CoordTrouve then break;
              end;
              SupprDessin := not CoordTrouve;
          end;
          if SupprDessin and (identification<>identDroite)
             then Dessins.remove(dessins[c])
             else Inc(c);
      end
      else Inc(c);
  for c := pred(Dessins.Count) downto 0 do
    if (dessins[c].identification=identNone) and
      (dessins[c].numPage > NbrePages) then
      dessins.remove(dessins[c]);
  c := 0;
  while c<Dessins.Count do begin
    dessins[c].draw;
    inc(c);
  end;
  if graduationZeroX or graduationZeroY then drawAxis(False);
  if grapheParam
  then traceEquivalenceParam
  else if superposePage
     then for p := 1 to NbrePages do
        traceEquivalence(p)
     else if pageCourante>0 then traceEquivalence(pageCourante);
  if curReticuleDataActif then
      AfficheEtiquetteSegment;
  if curReticuleModeleActif then
      AfficheEtiquetteReticule;
 // if withDebutFin then TraceBorneFFT;
  if FilDeFer then traceFilDeFer;
 setTextAlign(TA_left + TA_top);
  if identificationPages and superposePage
     then traceIdentPages
end; // graphe.draw

procedure TGrapheReg.ResetEquivalence;
var
  p: codePage;
begin
  for p := 0 to MaxPages do
      equivalences[p].Clear;
end;

procedure TGrapheReg.Reset;
begin
  courbes.Clear;
  monde[mondeX].defini := False;
  GrapheOK  := False;
  AutoTick  := True;
  UseDefaut := False;
  borneVisible := true;
end;

destructor TGrapheReg.Destroy;
var
  m: indiceMonde;
  p: codePage;
begin
  courbes.Clear;
  courbes.Free;
  dessins.Clear;
  dessins.Free;
  for p := 0 to MaxPages do begin
    equivalences[p].Clear;
    equivalences[p].Free;
  end;
  equivalenceTampon.Free;
  statusSegment[0].Free;
  statusSegment[1].Free;
  for m := low(indiceMonde) to high(IndiceMonde) do
    monde[m].Free;
  for m := 1 to 2 do
    mondeVecteur[m].Free;
  UnitePente.Free;
  inherited Destroy;
end;

constructor TGrapheReg.Create;
var
  m: indiceMonde;
  i: indiceOrdonnee;
  k: integer;
  p: codePage;
begin
  inherited Create;
  grapheParam := false;
  modif := [gmXY];
  mondeDerivee := mondeY;
  UnitePente := Tunite.Create;
  for m := low(indiceMonde) to high(IndiceMonde) do
    monde[m] := Tmonde.Create;
  monde[mondeX].horizontal := True;
  for m := 1 to 2 do
    mondeVecteur[m] := TmondeVecteur.Create;
  mondeVecteur[2].vitesse := False;
  for i := 1 to maxOrdonnee do
    with Coordonnee[i] do begin
      nomX  := '';
      nomY  := '';
      iMondeC := mondeY;
      Couleur := couleurInit[i];
      StyleT := psSolid;
      motif := motifInit[i];
      trace := [];
  end;
  for k := 1 to maxCurseur do with curseurOsc[k] do begin
      grandeurCurseur := nil;
      indexCourbe := 0;
      mondeC := mondeY;
  end;
  zeroPolaire := 0;
  optionCurseur := [coX, coY, coDeltaX, coDeltaY];
  OptionGraphe := OptionGrapheDefault;
  OptionModele := [OmExtrapole];
  courbes := TlisteCourbe.Create;
  dessins := TlisteDessin.Create;
  for p := 0 to MaxPages do
    equivalences[p] := TlisteEquivalence.Create;
  equivalenceTampon := Tequivalence.Create(0, 0, 0, 0, 0, 0, 0, self);
  statusSegment[0] := TStringList.Create;
  statusSegment[1] := TStringList.Create;
  for k := 1 to 3 do
      curseurOsc[k].Ic := 10 + 10 * k;
  curReticuleDataActif := False;
  curReticuleModeleActif := False;
  FilDeFer := False;
  PasPoint := 1;
  UnAxeX := True;
  BitmapReg := TBitmap.Create;
  reset;
  avecAxeX := True;
  avecAxeY := True;
  AutoTick := True;
  borneVisible := true;
end;

procedure Tcourbe.setStyle(Acouleur: Tcolor; Astyle: TpenStyle; Amotif: Tmotif);
begin
  couleur := ACouleur;
  styleT := Astyle;
  motif := AMotif;
  dimPointC := 0;
  penWidthC := 0;
end;

constructor Tcourbe.Create(AX, AY: vecteur; D, F: integer; VX, VY: tgrandeur);
begin
  valX  := AX;
  valY  := AY;
  incertX := nil;
  incertY := nil;
  DebutC := D;
  FinC  := F;
  Decalage := 0;
  case TraceDefaut of
      tdPoint : Trace := [trPoint];
      tdLigne : Trace := [trLigne];
      tdLissage : Trace := [trLigne,trPoint];
  end;
  varY  := VY;
  varX  := VX;
  texteC := nil;
  couleur := CouleurPages[1];
  styleT := stylePages[1];
  Motif := MotifPages[1];
  IndexModele := 0;
  IndexBande := 0;
  Adetruire := False;
  DetruireIncert := False;
  CourbeExp := False;
  iMondeC := MondeY;
  ModeleParametrique := False;
  PortraitDePhase := False;
  valXder := nil;
  valYder := nil;
  valXder2 := nil;
  valYder2 := nil;
  pointSelect := [];
end;

destructor Tcourbe.Destroy;
begin
  if Adetruire then begin
    valX := nil;
    valY := nil;
    valYder := nil;
  end;
  if DetruireIncert then begin
    incertX := nil;
    incertY := nil;
  end;
  texteC.Free;
  inherited Destroy;
end;

procedure TGrapheReg.AffecteZoomArriere;

  procedure CalcMinMax(newMin,newMax : integer;m: indiceMonde);
// A l'entrée MinInt est l'entier représentant Mini
// A la sortie Mini est le réel correspondant à NewMin
  var
      absMax, absMin:  double;
  begin with monde[m] do begin
      a := a/2;
      b := b+a*(Mini+Maxi)/2;
      Mini := (newMin-b)/a;
      Maxi := (newMax-b)/a;
      AbsMax := abs(maxi);
      AbsMin := abs(mini);
      if AbsMax < AbsMin then AbsMax := AbsMin;
      graduationPiActive := graduationPiActive and
        ((not (AngleEnDegre) and (AbsMax > 0.8) and (AbsMax < 13)) or
        (AngleEnDegre and (AbsMax > 45) and (AbsMax < 720)));
  end end;

var
  m: indiceMonde;
begin
  CalcMinMax(limiteCourbe.left,limiteCourbe.right,mondeX);
  if OgAnalyseurLogique in OptionGraphe then exit;
  for m := mondeY to high(IndiceMonde) do
    if monde[m].defini then
      CalcMinMax(limiteCourbe.bottom,limiteCourbe.top, m);
end;// zoomArriere

procedure TGrapheReg.AffecteZoomAvant(rect: Trect; avecY: boolean);
var
  minX, maxX, minY, maxY: double;

  procedure AffecteZoomLoc(m: indiceMonde);
  begin
    with monde[m], rect do
    begin
      MondeXY(left, top, m, minX, maxY);
      MondeXY(right, bottom, m, maxX, minY);
      VerifMinMaxReal(MinY, MaxY);
      if MinY > Mini then
        Mini := MinY;
      if MaxY < Maxi then
        Maxi := MaxY;
    end;
  end;

var
  m: indiceMonde;
begin
  with monde[mondeX], rect do begin
    MondeXY(left, top, mondeY, minX, maxY);
    MondeXY(right, bottom, mondeY, maxX, minY);
    verifMinMaxReal(MinX, maxX);
    if MinX > Mini then
      Mini := MinX;
    if MaxX < Maxi then
      Maxi := MaxX;
  end;
  if avecY then
    for m := mondeY to high(indiceMonde) do
      if monde[m].defini then begin
        affecteZoomLoc(m);
        break;
      end;
end; // zoomAvant

procedure TGrapheReg.ChercheDroite(x, y, pente: double; m: indiceMonde;
  var x1, y1, x2, y2: integer);
var
  xr1, xr2, dX, dY: double;
begin
  case monde[mondeX].Graduation of
    gLog: pente := x * pente * ln(10);
    gdB: pente  := x * 20 * pente * ln(10);
    gInv: pente := 1 / pente;
  end;
  case monde[m].Graduation of
    gLog: pente := pente / y / ln(10);
    gdB: pente  := 20 * pente / y / ln(10);
    gInv: pente := 1 / pente;
  end;
  RTversXY(x, y, m);
  dX := (monde[mondeX].Maxi - monde[mondeX].Mini) / 6;
  dY := (monde[m].Maxi - monde[m].Mini) / 6;
  if abs(pente) < dY / dX then begin
    xr1 := x - dX;
    xr2 := x + dX;
  end
  else begin
    xr1 := x + dY / pente;
    xr2 := x - dY / pente;
  end;
  if xr1 < monde[mondeX].Mini then
    xr1 := monde[mondeX].Mini;
  if xr2 > monde[mondeX].Maxi then
    xr2 := monde[mondeX].Maxi;
  if xr2 < monde[mondeX].Mini then
    xr2 := monde[mondeX].Mini;
  if xr1 > monde[mondeX].Maxi then
    xr1 := monde[mondeX].Maxi;
  windowXY(xr1, y + pente * (xr1 - x), m, x1, y1);
  windowXY(xr2, y + pente * (xr2 - x), m, x2, y2);
end;

destructor Tdessin.Destroy;
begin
  Texte.Free;
  Pen.Free;
  TexteLoc.Free;
  inherited Destroy;
end;

constructor Tdessin.Create(gr : TgrapheReg);
begin
  inherited Create;
  x1  := 0;
  y1  := 0;
  x2  := 0;
  y2  := 0;
  x1i := 0;
  y1i := 0;
  x2i := 0;
  y2i := 0;
  pen := Tpen.Create;
  pen.color := clBlue;
  vertical := False;
  centre := True;
  IsOpaque := False;
  hauteur := 3;
  isTexte := True;
  repere := nil;
  avecLigneRappel := False;
  motifTexte := mtNone;
  TexteLigne := tlNone;
  souligne := False;
  Texte := TStringList.Create;
  NumPage := 0;
  Identification := identNone;
  CouleurFond := colorToRGB(FondReticule);
  iMonde := MondeY;
  deplacable := True;
 alignement := TA_left + TA_top;
  sousDessin := nil;
  proprietaire := nil;
  TexteLoc := TStringList.Create;
  Agraphe := gr;
end;

procedure TgrapheReg.RectangleGr(Arect: Trect);
begin
  canvas.pen.mode := pmNotXor;
  with Arect do
    canvas.Polygon([point(left, top), point(left, bottom), point(
        right, bottom), point(right, top)]);
  canvas.pen.mode := pmCopy;
end;

procedure TgrapheReg.LineGr(Arect: Trect);
begin
  Canvas.MoveTo(Arect.left, Arect.top);
  Canvas.LineTo(Arect.right, Arect.bottom);
end;

procedure Tdessin.AffectePosition(x, y: integer;
  posDessin: TselectDessin; Shift: TShiftState);

  procedure Affecte1;
  begin
    with Agraphe do
      if isTexte and isTitre then
        with limiteFenetre do begin
          x1 := (x1i - left) / (right - left);
          y1 := (y1i - top) / (top - bottom);
        end
      else
        MondeRT(x1i, y1i, iMonde, x1, y1);
  end;

  procedure Affecte2;
  begin
    with Agraphe do
      if isTexte and isTitre then
        with limiteFenetre do begin
          x2 := (x2i - left) / (right - left);
          y2 := (y2i - top) / (top - bottom);
        end
      else
        MondeRT(x2i, y2i, iMonde, x2, y2);
  end;

var dx, dy: integer;
    i : integer;
    newPoint : integer;
begin with Agraphe do begin
    if not isTexte and
      ((abs(x1i - x2i) + abs(y1i - y2i)) < 3) then begin
      x2i := x1i + 2;
      y2i := y1i + 2;
    end;
    if (posDessin in [sdPoint1, sdHoriz, sdVert]) and
       (identification = identCoord) then begin
          for i := 0 to pred(Agraphe.courbes.Count) do
            with Agraphe.courbes[i] do
              if (numPage = IndexNom(varY.nom)) then begin
                newPoint := pointProche(x, y, i, True, False);
                if newPoint > 0 then numPoint := newPoint;
                break;
              end;
    end;
    case posDessin of
      sdCadre: begin
        x2i := x;
        y2i := y;
        Affecte2;
      end;
      sdPoint2: begin
        if ssShift in Shift then
          if abs(x - x1i) > abs(y - y1i) then
            y := y1i
          else
            x := x1i;
        x2i := x;
        y2i := y;
        Affecte2;
      end;
      sdPoint1, sdHoriz, sdVert: begin
        x1i := x;
        y1i := y;
        Affecte1;
      end;
      sdCentre: begin
        dx  := x - ((x1i + x2i) div 2);
        dy  := y - ((y1i + y2i) div 2);
        x1i := x1i + dx;
        x2i := x2i + dx;
        y1i := y1i + dy;
        y2i := y2i + dy;
        Affecte1;
        Affecte2;
      end;
      sdNone: ;
    end;
end end;

procedure TGrapheReg.AffecteCentreRect(x, y: integer; var r: Trect);
var
  maxi: integer;
begin with r do begin
    maxi := limiteFenetre.right;
    x := x + (right - left) div 2;
    if x > maxi then
      x := maxi;
    maxi := limiteFenetre.left;
    x := x - (right - left);
    if x < maxi then
      x := maxi;
    maxi := limiteFenetre.bottom;
    y := y + (bottom - top) div 2;
    if y > maxi then
      y := maxi;
    maxi := limiteFenetre.top;
    y := y - (bottom - top);
    if y < maxi then
      y := maxi;
    r := rect(x, y, x + right - left, y + bottom - top);
end; end;

function Tdessin.LitOption(grapheC: TgrapheReg): boolean;
begin
  Agraphe := grapheC;
  if isTexte then begin
    if LectureTexteDlg = nil then
      Application.createForm(TLectureTexteDlg, LectureTexteDlg);
    lectureTexteDlg.DessinLoc := self;
    lectureTexteDlg.grapheLoc := Agraphe;
    litOption := lectureTexteDlg.showModal = mrOk;
  end
  else begin
    if OptionLigneDlg = nil then
      Application.createForm(TOptionLigneDlg, OptionLigneDlg);
    OptionLigneDlg.DessinLoc := self;
    OptionLigneDlg.Agraphe := Agraphe;
    litOption := OptionLigneDlg.showModal = mrOk;
  end;
end;

procedure TGrapheReg.SetDessinCourant(x, y: integer);
var
  longueur: integer;

  function distance(i : integer) : integer;
  var
    a, b, c: integer;
  begin with dessins[i] do begin
      A := y1i - y2i;
      B := x2i - x1i;
      C := y2i * x1i - y1i * x2i;
      Longueur := round(sqrt(A * A + B * B));
      if longueur<1 then longueur := 1;
      Result := abs(A * x + B * y + C) div longueur;
  end end;

var
  i: integer;
  multX, multY: integer;
  x0i, y0i: integer; // 32 bits => pas de débordement
  d1, d2, d0, dmin: integer;
begin
  posDessinCourant := sdNone;
  dessinCourant := nil;
  for i := 0 to pred(dessins.Count) do with dessins[i] do begin
      if deplacable then
        if isTexte then begin
          with cadre do
            MultX := (x - left) * (x - right);
          with cadre do
            MultY := (y - top) * (y - bottom);
          if (MultX <= 0) and (MultY <= 0)
          then posDessinCourant := sdCadre
          else case motifTexte of
              mtHoriz: if abs(y - y1i) < 5 then
                  posDessinCourant := sdHoriz;
              mtVert: if abs(x - x1i) < 5 then
                  posDessinCourant := sdVert;
              else if (abs(x - x1i) + abs(y - y1i)) < 10 then
                  posDessinCourant := sdPoint1;
              end
        end // texte
        else // ligne
        if distance(i) < 16 then  begin
          x0i := (x1i + x2i) div 2;
          y0i := (y1i + y2i) div 2;
          d1  := abs(x - x1i) + abs(y - y1i);
          d2  := abs(x - x2i) + abs(y - y2i);
          d0  := abs(x - x0i) + abs(y - y0i);
          dmin := longueur div 3;
          if d1 < dmin then begin
            posDessinCourant := sdPoint1;
            dmin := d1;
          end;
          if d2 < dmin then begin
            posDessinCourant := sdPoint2;
            dmin := d2;
          end;
          if d0 < dmin then
             posDessinCourant := sdCentre;
        end; // ligne distance<16
    if posDessinCourant <> sdNone then begin
        dessinCourant := dessins[i];
        break;
    end;
  end; // for i
end;

procedure TGrapheReg.GetCurseurProche(x, y: integer; Force: boolean);
var i : integer;
begin
    cCourant := 0;
    for i := curseurData1 to curseurData2 do with curseurOsc[i] do
      if (indexCourbe >= 0) and
         (abs(x - xc) + abs(y - yc) < margeCurseur)
            then cCourant := i;
    if Force and (cCourant = 0) then
      if (CurseurOsc[curseurData2].indexCourbe >= 0) and
         ((abs(x - CurseurOsc[curseurData1].Xc) + abs(y - CurseurOsc[curseurData1].yc)) >
           (abs(x - CurseurOsc[curseurData2].Xc) + abs(y - CurseurOsc[curseurData2].yc)))
            then cCourant := curseurData2
            else cCourant := curseurData1;
end;

function TGrapheReg.GetReticuleModeleProche(x, y: integer) : boolean;
begin
    with CurseurOsc[1] do
    result :=  (abs(x - xc) + abs(y - yc) < margeCurseur)
end;

procedure Tmonde.SetDelta(grandAxe: boolean);
// Exposant=puissance de 10 multiple de 3 de manière à avoir entre
// NbreGradMax (10) et N/2.5 (4) graduations sur l'axes.
// ou NbreGradMin (5) et N/2.5 (2) graduations sur l'axes.
// Delta est l'intervalle entre les graduations qui commencent à PremiereGraduation

  procedure setDeltaLongueDuree;
  var
    N: integer;
  begin
    DeltaAxe := (maxi - mini) / 6;
    if DeltaAxe > UnMois then begin
      DeltaAxe := ceil(DeltaAxe / UnMois) * UnMois;
      FormatDuree := 'dd/mmm';
      Arrondi  := UnJour;
    end
    else if DeltaAxe > UnJour then begin
      DeltaAxe := ceil(DeltaAxe / UnJour) * UnJour;
      FormatDuree := 'dd"j"';
      Arrondi  := UnJour;
    end
    else if DeltaAxe > UneHeure then begin
      N := trunc(DeltaAxe / UneHeure);
      case N of
        1, 2, 3, 4: ;
        5: N := 6;
        6: ;
        7: N := 8;
        8: ;
        9, 10, 11: N := 12;
        12: ;
        13, 14, 15, 16, 17: N := 12;
        18, 19, 20, 21, 22, 23, 24: N := 24;
      end;
      DeltaAxe := N * UneHeure;
      FormatDuree := 'dd"j "hh';
      Arrondi  := UneHeure;
    end
    else begin
      if DeltaAxe > QuartdHeure
         then DeltaAxe := ceil(DeltaAxe / QuartdHeure) * QuartdHeure
         else DeltaAxe := ceil(DeltaAxe / UneMinute) * UneMinute;
      FormatDuree := 'hh:mm';
      Arrondi := UneMinute;
    end;
    PointDebut := round(Mini / DeltaAxe) * deltaAxe;
    Exposant := 1;
    NbreChiffres := length(FormatDuree);
  end; // setDeltaLongueDuree

  procedure setDeltaDateTime; // Windows compte en jour
  begin
    DeltaAxe := (Maxi - Mini);
    Arrondi  := 1;
    if deltaAxe > 365 { 1 an } then begin
      FormatDuree := 'dd/mmm';
      deltaAxe := UnMoisWin*ceil(deltaAxe / 6 / UnMoisWin);
      if DeltaAxe>UnAnWin then deltaAxe := UnAnWin*round(deltaAxe/UnAnWin);
      Arrondi  := 1;
    end
    else if deltaAxe > 60 { 2 mois } then begin
      FormatDuree := 'dd/mmm';
      deltaAxe := ceil(deltaAxe / 6);
      if DeltaAxe>10 then deltaAxe := 10*round(deltaAxe/10);
      if DeltaAxe>UnMoisWin then deltaAxe := UnMoisWin*round(deltaAxe/UnMoisWin);
      Arrondi  := 1;
    end
    else if deltaAxe > 6 { 6 jours } then begin
      FormatDuree := 'dd"j" hh';
      deltaAxe := ceil(deltaAxe / 6);
      Arrondi  := UneHeureWin;
    end
    else if deltaAxe > 6 * UneHeureWin { 6 heures } then begin
      deltaAxe := UneHeureWin * ceil(deltaAxe / 6 / UneHeureWin);
      FormatDuree := 'hh:mm';
      Arrondi  := UneMinuteWin;
    end
    else if deltaAxe > UneHeureWin { 1 heure } then begin
      deltaAxe := QuartdHeureWin * ceil(deltaAxe / 6 / QuartdHeureWin);
      FormatDuree := 'hh:mm';
      Arrondi  := UneMinuteWin;
    end
    else begin
      deltaAxe := UneMinuteWin * ceil(deltaAxe / 6 / UneMinuteWin);
      FormatDuree := 'mm:ss';
      Arrondi  := UneSecondeWin;
    end;
    PointDebut := round(Mini / DeltaAxe) * deltaAxe;
    NbreChiffres := length(formatDuree);
    Exposant := 1;
  end; // setDeltaDateTime

  procedure setDeltaDegre;
  var
    largeur: double;
    coeff: integer;
  begin
    largeur := (maxi - mini) / 9; { 9 graduations }
    coeff := 15 * ceil(largeur / 15);
    if (coeff = 75) or (coeff = 105) then
      coeff := 90;
    NbreChiffres := 3;
    DeltaAxe := coeff;
    if orthonorme
       then PointDebut := ceil(miniOrtho / deltaAxe) * deltaAxe
       else PointDebut := ceil(mini / deltaAxe) * deltaAxe;
    Exposant := 1;
  end; // setDeltaDegre

  procedure setDeltaRadian;
  var
    largeur: double;
    coeff, reste: integer;
  begin
    largeur := (maxi - mini) / 9;
    coeff := ceil(largeur * 12 / pi);
    reste := coeff mod 12;
    if (reste = 5) or (reste = 7) then
      coeff := 6 * ceil(largeur * 2 / pi);
    { pi/2 plutôt que 5pi/12 ou 7pi/12 }
    if (reste = 10) or (reste = 11) then
      coeff := 12 * ceil(largeur / pi);
    { pi plutôt que 11pi/12 ou 5pi/6 }
    deltaAxe := pi / 12 * coeff;
    NbreChiffres := 5;
    if orthonorme then
      PointDebut := ceil(miniOrtho / deltaAxe) * deltaAxe
    else
      PointDebut := ceil(mini / deltaAxe) * deltaAxe;
    Exposant := 1;
    GraduationPiActive := True;
  end; // setDeltaRadian

const
  indexMax = 5;
  coeff: array[0..indexMax] of double = (0.1, 0.2, 0.5, 1, 2, 5);
var
  AbsMax, AbsMin, largeur, deltaGrandAxe : double;
  Exponent, N, index, indexGrad: integer;
begin // setDelta
  graduationPiActive := False;
  NbreDecimal := 0;
  Exponent := 0;
  Exposant := 1;
  if Graduation = gInv then
    if maxi > 0
      then AbsMax := abs(1 / maxi)
      else AbsMax := abs(1 / mini)
  else begin
    if orthonorme then begin
      AbsMax := abs(maxiOrtho);
      AbsMin := abs(miniOrtho);
    end
    else begin
      AbsMax := abs(maxi);
      AbsMin := abs(mini);
    end;
    if AbsMax < AbsMin then
      AbsMax := AbsMin;
  end;
  // Nticks := 1;
  try
    if (axe <> nil) and (axe.formatU = fLongueDuree) then begin
      Nticks := 1;
      setDeltaLongueDuree;
      exit;
    end;
    if (axe <> nil) and (axe.formatU in [fDateTime, fDate, fTime]) then begin
      Nticks := 1;
      setDeltaDateTime;
      exit;
    end;
  except
  end;
  indexGrad := 0;
  AutoTick  := AutoTick or (graduation = gLog) or (graduation = gInv);
  if not (AutoTick) then begin
    deltaAxe := abs(deltaAxe);
    N := round((maxi - mini) / deltaAxe);
    AutoTick := (N > 64) or (N < 2);
    if AutoTick then
      afficheErreur(erNbreGradManuel, 0)
    else begin
      Exponent := floor(Log10(deltaAxe));
//      index := 0;
      largeur := (maxi - mini) * dix(-exponent);
      if largeur > NbreGradMaxMan * coeff[indexMax] then begin
//        largeur := largeur / 10;
        Inc(exponent);
      end;
  (*
      while ((largeur / coeff[index]) >= NbreGradMaxMan) and
        (index < indexMax) do
        Inc(index);
   *)
      if NTicks<1 then NTicks := 1;
      if NTicks>5 then NTicks := 5;
     // NTicks := ceil(coeff[index] * dix(exponent) / DeltaAxe);
    end;
  end;
  if AutoTick then begin
    if GraduationPi and (axe <> nil) and
      (axe.nomUnite = 'rad') and
      not polaire and
      (AbsMax > 0.8) and (AbsMax < 100) then begin
      Nticks := 1;
      setDeltaRadian;
      exit;
    end;
    if GraduationPi and (axe <> nil) and
       (axe.nomUnite = '°') and
       not polaire and
       (AbsMax > 45) and (AbsMax < 720) then begin
      Nticks := 1;
      setDeltaDegre;
      exit;
    end;
    if Graduation = gInv then begin
      Exponent := floor(Log10(AbsMax));
      largeur  := (1 / mini - 1 / maxi) * dix(-exponent); // ??
    end
    else if orthonorme then begin
      Exponent := floor(Log10(MaxiOrtho - MiniOrtho));
      largeur  := (maxiOrtho - miniOrtho) * dix(-exponent);
    end
    else begin
      Exponent := floor(Log10(Maxi - Mini));
      largeur  := (maxi - mini) * dix(-exponent);
    end;
    index := 0;
    if grandAxe
       then while ((largeur / coeff[index]) >= NbreGradMax) and (index < indexMax) do
        Inc(index)
       else while ((largeur / coeff[index]) >= NbreGradMin) and (index < indexMax) do
        Inc(index);
    DeltaAxe  := coeff[index] * dix(exponent);
    indexGrad := index mod 3;
  end;// autoTick
  NbreChiffres := ceil(Log10(AbsMax)) - floor(Log10(DeltaAxe));
  NbreDecimal := NbreChiffres-ceil(Log10(AbsMax));
  if NbreDecimal<0 then NbreDecimal := 0;
  if autoTick then begin
    case indexGrad of
      0: Nticks := 5; { 1 -> 0.2 }
      1: Nticks := 4; { 2 -> 0.5 }
      2: Nticks := 5; { 5 -> 1 }
      else Nticks := 1; // pour le compilateur
    end;
    deltaAxe  := deltaAxe / Nticks;
  end;
  if exponent = -1 then exponent := 0;
// nombre <~1 écrit sous forme 0.8 plutôt que 800.10-3
  Index := Exponent div 3;
  if ((Exponent mod 3) < 0) then Dec(index);
//la division euclidienne des informaticiens <> div des maths !
  Exposant := Dix(3 * Index);
  if Graduation = gLog then begin
     NbreChiffres := longNombreAxeLog;
     Exposant := 1;
  end
  else begin
    N := ceil(Log10(AbsMax / Exposant));
    if N > NbreChiffres then NbreChiffres := N;
    if NbreChiffres < 3 then NbreChiffres := 3;
    if NbreChiffres > longNombreAxe then
      NbreChiffres := longNombreAxe;
  end;
  if Polaire then begin
     PointDebut := deltaAxe;
     TickDebut  := 1;
  end
  else begin
    deltaGrandAxe := NTicks*deltaAxe;
    if Graduation = gInv then
      PointDebut := ceil(1 / deltaGrandAxe / maxi) * deltaGrandAxe
    else if Orthonorme
       then PointDebut := floor(miniOrtho / deltaAxe) * deltaAxe
       else PointDebut := floor(mini / deltaAxe) * deltaAxe;
    TickDebut := round(frac(PointDebut / deltaGrandAxe)*NTicks);
  end;
end; // setDelta

function Tmonde.FormatMonde(x: double): string;

  function FormatAxeLog: string;
  var
    unite: coefUnite;
  begin
    x := abs(x);
    unite := nulle;
    while (x >= 1000) and (unite < infini) do begin
      x := x / 1000;
      Inc(unite);
    end; // x<1000 ou infini
    while (x < 1) and (unite > zero) do begin
      x := x * 1000;
      Dec(unite);
    end; // x>=1 ou zero
    case unite of
       infini, zero: Result := '';
       nulle : Result := IntToStr(round(x));
       milli : Result := FloatToStrF(x/1000,ffGeneral,3,3);
       else Result := IntToStr(round(x)) + pointMedian+'10'+puissToStr(puissanceUnite[unite])
    end;
  end;

  function FormatAxeLogNew: string;
  var
    exposant,mantisse : integer;
  begin
    x := abs(x);
    exposant := floor(log10(x));
    mantisse := round(x/power(10,exposant));
    Result := IntToStr(mantisse) + pointMedian+'10'+puissToStr(exposant)
  end;

begin
  if (graduation = gLog)
    then Result := FormatAxeLogNew
    else if axe = nil
       then Result := FloatToStrF(x / exposant, ffGeneral, NbreChiffres, 0)
       else case axe.FormatU of
          fLongueDuree: Result := FormatLongueDureeAxe(x);
          fDateTime, fDate, fTime: Result :=
              FormatDateTime(FormatDuree, Arrondi * round(x / Arrondi));
          else if abs(x / deltaAxe) < 1e-5
             then Result := '0'
             else Result := FloatToStrF(x / exposant, ffGeneral, NbreChiffres, NbreDecimal)
       end; // case
end; // FormatMonde

procedure TtransfertGraphe.AssignEntree(const Agraphe: TgrapheReg);
var
  i: indiceOrdonnee;
  m: indiceMonde;
begin
  for i := 1 to MaxOrdonnee do begin
    nomY[i]  := Agraphe.Coordonnee[i].nomY;
    nomX[i]  := Agraphe.Coordonnee[i].nomX;
    iMonde[i] := Agraphe.Coordonnee[i].iMondeC;
    Trace[i] := Agraphe.Coordonnee[i].Trace;
    Ligne[i] := Agraphe.Coordonnee[i].Ligne;
    Couleur[i] := Agraphe.Coordonnee[i].couleur;
    Style[i] := Agraphe.Coordonnee[i].styleT;
    Motif[i] := Agraphe.Coordonnee[i].Motif;
  end;
  for m := low(IndiceMonde) to high(IndiceMonde) do begin
    Grad[m] := Agraphe.monde[m].Graduation;
    Zero[m] := Agraphe.monde[m].ZeroInclus;
    minidB[m] := Agraphe.monde[m].minidB;
  end;
  zeroPolaire := Agraphe.zeroPolaire;
  OptionGr  := Agraphe.OptionGraphe;
  OptionModele := Agraphe.OptionModele;
  SuperposePage := Agraphe.superposePage;
  FilDeFer  := Agraphe.filDeFer;
  UseDefaut := Agraphe.UseDefaut;
  UseDefautX := Agraphe.UseDefautX;
  AutoTick  := Agraphe.AutoTick;
  PasPoint  := Agraphe.PasPoint;
end;

constructor TtransfertGraphe.Create;
var
  i: indiceOrdonnee;
  m: indiceMonde;
begin
  for i := 1 to MaxOrdonnee do begin
    nomY[i]  := 'Y';
    nomX[i]  := 'X';
    iMonde[i] := mondeY;
    Trace[i] := [trLigne];
    Ligne[i] := liDroite;
    Couleur[i] := couleurInit[i];
    Style[i] := psSolid;
    Motif[i] := motifInit[i];
  end;
  for m := low(IndiceMonde) to high(IndiceMonde) do begin
    Grad[m] := gLin;
    Zero[m] := True;
    minidB[m] := 0;
  end;
  zeroPolaire := 0;
  OptionGr  := [];
  OptionModele := [];
  SuperposePage := False;
  FilDeFer  := False;
  UseDefaut := False;
  UseDefautX := False;
  AutoTick  := True;
  PasPoint  := 1;
end;

procedure TtransfertGraphe.AssignSortie(var Agraphe: TgrapheReg);
var
  i: indiceOrdonnee;
  m: indiceMonde;
begin
  for i := 1 to MaxOrdonnee do begin
    Agraphe.Coordonnee[i].nomY  := nomY[i];
    Agraphe.Coordonnee[i].nomX  := nomX[i];
    Agraphe.Coordonnee[i].iMondeC := iMonde[i];
    Agraphe.Coordonnee[i].Trace := Trace[i];
    Agraphe.Coordonnee[i].Ligne := Ligne[i];
    Agraphe.Coordonnee[i].Motif := Motif[i];
    Agraphe.Coordonnee[i].couleur := couleur[i];
    Agraphe.Coordonnee[i].styleT := Style[i];
  end;
  for m := low(indiceMonde) to high(indiceMonde) do begin
    Agraphe.monde[m].Graduation := Grad[m];
    zero[m] := zero[m] and (Grad[m] = gLin);
    Agraphe.monde[m].ZeroInclus := zero[m];
    Agraphe.monde[m].minidB := minidB[m];
  end;
  Agraphe.zeroPolaire := zeroPolaire;
  Agraphe.OptionGraphe := OptionGr;
  Agraphe.OptionModele := OptionModele;
  Agraphe.SuperposePage := SuperposePage;
  Agraphe.UseDefaut := UseDefaut;
  Agraphe.UseDefautX := UseDefautX;
  Agraphe.AutoTick := AutoTick;
  Agraphe.FilDeFer := FilDeFer;
  Agraphe.PasPoint := PasPoint;
end;

function TgrapheReg.AjouteCourbe(AX, AY: vecteur; Am: indiceMonde;
  Nbre: integer; grandeurX, grandeurY: Tgrandeur; Apage: integer): Tcourbe;
var
  Acourbe: Tcourbe;
begin
  Acourbe := Tcourbe.Create(AX, AY, 0, pred(Nbre), grandeurX, grandeurY);
  Acourbe.page := Apage;
  Acourbe.couleur := GetCouleurPages(Apage);
  Acourbe.styleT := stylePages[Apage mod MaxPages];
  Acourbe.Motif := MotifPages[Apage mod MaxPages];
  Acourbe.iMondeC := Am;
  courbes.Add(Acourbe);
  monde[Am].defini := False;
  monde[mondeX].defini := False;
  monde[Am].axe := grandeurY;
  monde[mondeX].axe := grandeurX;
  Result := Acourbe;
end;

procedure TgrapheReg.TraceCurseur(i: indexCurseur);
var
  xi, yi: integer;
begin with CurseurOsc[i] do begin
    if mondeC = mondeX then exit;
    createPen(PstyleReticule, penWidthReticule, PColorReticule);
    if ReticuleComplet or (i<4) then begin
      xi := Xc;
      yi := Yc;
      canvas.pen.mode := pmNOTXOR;
      segment(limiteCourbe.left + labelYcurseur.Width, Yi, LimiteFenetre.right, Yi);
      if axeYpositif
         then segment(Xi, limiteCourbe.top, Xi, limiteCourbe.bottom)
         else if Xi > labelYcurseur.Width
            then segment(Xi, 0, Xi, BasCourbe)
            else begin
                segment(Xi, 0, Xi, labelYcurseur.Top - PaintBox.top - 2);
                segment(Xi, labelYcurseur.top - PaintBox.top + labelYcurseur.Height +
                      2, Xi, BasCourbe);
            end;
      canvas.pen.mode := pmCOPY;
    end;
    if (i>3) and not ReticuleComplet then begin
      CreateSolidBrush(PColorReticule);
      Yr := courbes[indexCourbe].valY[Ic];
      Xr := courbes[indexCourbe].valX[Ic];
      WindowRT(Xr, Yr, mondeC, xi, yi);
      canvas.Rectangle(Xi - dimBorne, Yi - dimBorne, Xi + dimBorne, Yi + dimBorne);
      canvas.brush.style := bsClear;
    end;
end end; // TraceCurseur

procedure TgrapheReg.TracePente;
var
  xi, yi: integer;
begin
    if (CurseurOsc[curseurData2].indexCourbe <> curseurOsc[curseurData1].indexCourbe) or
       (CurseurOsc[curseurData1].mondeC = mondeX) or
       (CurseurOsc[curseurData2].mondeC = mondeX)
          then exit;
    createPen(PstyleTangente, penWidth, PColorTangente);
    windowRT(CurseurOsc[curseurData1].xr, CurseurOsc[curseurData1].yr, CurseurOsc[curseurData1].mondeC, xi, yi);
    if (xi<0) or
       (yi<0) then exit;
    canvas.MoveTo(Xi, Yi);
    windowRT(CurseurOsc[curseurData2].xr, CurseurOsc[curseurData2].yr, CurseurOsc[curseurData1].mondeC, xi, yi);
    if (xi<0) or
       (yi<0) then exit;
    canvas.LineTo(Xi, Yi);
end;

procedure TgrapheReg.InitReticule(x, y: integer);
var mondeLoc : indiceMonde;
    i : integer;
begin
  with canvas do begin
    if not (monde[mondeX].defini) then exit;
    if monde[mondeY].defini
       then mondeLoc := mondeY
       else if monde[mondeDroit].defini
          then mondeLoc := mondeDroit
          else exit;
    for i := 1 to 3 do with curseurOsc[i] do begin
       xc := x;
       yc := y;
       mondeC := mondeLoc;
       grandeurCurseur := monde[mondeLoc].axe;
    end;
    cCourant := 1;
    traceReticule(1);
  end;
end;

function TgrapheReg.MondeProche(x, y: integer): indiceMonde;
var
  m: indiceMonde;
begin
  Result := mondeY;
  for m := mondeY to high(indiceMonde) do
    with monde[m] do
      if defini and (y > courbeTop) and (y < courbeBottom) then
        Result := m;
end;

function TgrapheReg.PointProche(var x, y: integer; indexCourbe: integer;
  limite, Xseul: boolean): integer;
var
  i, idebut, ifin: integer;
  Xi, Yi, newX, newY: integer;
  distance, distanceMin: integer;
begin
  Result := -1;
  if indexCourbe < 0 then exit;
  newX := x;
  newY := y;
  with courbes[indexCourbe] do begin
    if Limite then begin
      iDebut := DebutC;
      iFin := FinC;
    end
    else begin
      iDebut := 0;
      iFin := pred(pages[pageCourante].nmes);
    end;
    distanceMin := 64;
    for i := idebut to ifin do begin
      windowRT(valX[i], valY[i], iMondeC, Xi, Yi);
      if Xseul then
        distance := abs(X - Xi)
      else
        distance := abs(X - Xi) + abs(Y - Yi);
      if distance < distanceMin then begin
        distanceMin := distance;
        Result := i;
        newX := Xi;
        newY := Yi;
      end;
    end;
    if Result >= 0 then begin
      x := newX;
      y := newY;
    end;
  end;
end;

function TgrapheReg.PointProcheModele(var x, y: integer): integer;
var
  i: integer;
  Xi, Yi, newX, newY: integer;
  distance, distanceMin: integer;
begin
  Result := -1;
  newX := x;
  newY := y;
  with courbes[indexReticuleModele] do begin
    distanceMin := 64;
    for i := debutC to finC do begin
      windowRT(valX[i], valY[i], iMondeC, Xi, Yi);
      distance := abs(X - Xi) + abs(Y - Yi);
      if distance < distanceMin then begin
         distanceMin := distance;
         Result := i;
         newX := Xi;
         newY := Yi;
      end; // distance
    end; // for i
  if Result >= 0 then begin
      x := newX;
      y := newY;
  end
  else begin // X seul
      for i := debutC to finC do begin
      windowRT(valX[i], valY[i], iMondeC, Xi, Yi);
      distance := abs(X - Xi);
      if distance < distanceMin then begin
        distanceMin := distance;
        Result := i;
        newX := Xi;
        newY := Yi;
      end;
    end;
    if Result >= 0 then begin
      x := newX;
      y := newY;
     end;
  end; // for i
  end; // with
end;

function TgrapheReg.PointProcheModeleX(var x, y: integer): integer;
var
  i: integer;
  Xi, Yi, newX, newY: integer;
  distance, distanceMin: integer;
begin
  Result := -1;
  newX := x;
  newY := y;
  with courbes[indexReticuleModele] do begin
    distanceMin := abs(X-curseurOsc[1].Xc);
    for i := debutC to finC do begin
      windowRT(valX[i], valY[i], iMondeC, Xi, Yi);
      distance := abs(X - Xi);
      if distance < distanceMin then begin
         distanceMin := distance;
         Result := i;
         newX := Xi;
         newY := Yi;
      end; // distance
    end; // for i
  if Result >= 0 then begin
      x := newX;
      y := newY;
  end
  end; // with
end;

function TgrapheReg.PointProcheModeleY(var x,y: integer): integer;
var
  i: integer;
  Xi, Yi, newX, newY: integer;
  distance, distanceMin: integer;
begin
  Result := -1;
  newX := x;
  newY := y;
  with courbes[indexReticuleModele] do begin
    distanceMin := abs(Y - curseurOsc[1].Yc);
    for i := debutC to finC do begin
      windowRT(valX[i], valY[i], iMondeC, Xi, Yi);
      distance := abs(Y - Yi);
      if distance < distanceMin then begin
         distanceMin := distance;
         Result := i;
         newX := Xi;
         newY := Yi;
      end; // distance
    end; // for i
  if Result >= 0 then begin
      x := newX;
      y := newY;
  end
  end; // with
end;

function TgrapheReg.CourbeProche(var x, y: integer): integer;
var
  i: integer;
  Xi, Yi: integer;
  distance, distanceMin: integer;
  indexCourbe: integer;
begin
  Result := -1;
  distanceMin := 64;
  for indexCourbe := 0 to pred(courbes.Count) do
    with courbes[indexCourbe] do
      if page = pageCourante then begin
        for i := debutC to finC do begin
          windowRT(valX[i], valY[i], iMondeC, Xi, Yi);
          distance := abs(X - Xi) + abs(Y - Yi);
          if distance < distanceMin then begin
            distanceMin := distance;
            Result := indexCourbe;
          end;
        end;
      end;
end;

function TgrapheReg.CourbeModeleProche(var x, y: integer): integer;
var
  i: integer;
  Xi, Yi: integer;
  distance, distanceMin: integer;
  indexCourbe: integer;
begin
  Result := -1;
  distanceMin := 64;
  for indexCourbe := 0 to pred(courbes.Count) do
    with courbes[indexCourbe] do
      if (indexModele=1) and (page = pageCourante) and not courbeExp then begin
        for i := debutC to finC do begin
          windowRT(valX[i], valY[i], iMondeC, Xi, Yi);
          distance := abs(X - Xi) + abs(Y - Yi);
          if distance < distanceMin then begin
            distanceMin := distance;
            Result := indexCourbe;
          end;
        end;
      end;
end;

function TgrapheReg.SupprReticule(x, y: integer): boolean;
var
  i, ii:  integer;
  Xi, Yi: integer;
  distance, distanceMin: integer;
begin with equivalences[pageCourante] do begin
    Result := True;
    ii := -1;
    distanceMin := 16;
    for i := 0 to pred(Count) do begin
      with items[i] do
        windowRT(Ve, pHe, MondeY, Xi, Yi);
      distance := abs(X - Xi) + abs(Y - Yi);
      if distance < distanceMin then begin
        distanceMin := distance;
        ii := i;
      end;
    end;
    if ii >= 0 then
      remove(items[ii]);
end; end;

procedure TgrapheReg.SetSegmentInt(x, y: integer);
var
  zz, newCourbe: integer;
begin
    if cCourant=0 then exit;
    if not(monde[mondeX].defini) then exit;
    if not(monde[mondeY].defini) and
       not(monde[mondeDroit].defini)
           then exit;
    with CurseurOsc[cCourant] do begin
      TraceCurseur(cCourant); // efface
      TracePente; // efface
      zz := PointProche(x, y, indexCourbe, True, False);
      if zz < 0 then begin
        newCourbe := courbeProche(x, y);
        if newCourbe >= 0 then begin
          zz := PointProche(x, y, newCourbe, True, False);
          if zz >= 0 then begin
            indexCourbe := newCourbe;
            mondeC := courbes[newCourbe].iMondeC;
            grandeurCurseur := courbes[newCourbe].varY;
          end;
        end;
      end;
      if zz >= 0 then begin
        Ic := zz;
        Xc := x;
        Yc := y;
      end;
      TraceCurseur(cCourant); // retrace
      TracePente;
end end; // setSegmentInt

procedure TgrapheReg.SetReticuleModele(x, y: integer;selonX,selonY : boolean);
var
  zz: integer;
begin with CurseurOsc[1] do
    if (monde[mondeX].defini) and
       (monde[mondeC].defini) then begin
      TraceCurseur(1); // efface
      if selonX
         then if selonY
            then zz := PointProcheModele(x, y)
            else zz := PointProcheModeleX(x, y)
         else zz := PointProcheModeleY(x, y);
      if zz >= 0 then begin
        Ic := zz;
        Xc := x;
        Yc := y;
      end;
      TraceCurseur(1); // retrace
    end;
end; // setReticuleModele

procedure TgrapheReg.SetSegmentReal(i: integer);
begin
   if cCourant=0 then exit;
   if not(monde[mondeX].defini) then exit;
   if not(monde[mondeY].defini) and
      not(monde[mondeDroit].defini) then exit;
  with CurseurOsc[cCourant] do begin
      TraceCurseur(cCourant); // efface
      TracePente; //  efface
      Ic := i;
      with courbes[indexCourbe] do
        windowRT(valX[Ic], valY[Ic], iMondeC, Xc, Yc);
      TraceCurseur(cCourant); // retrace
      TracePente;
    end;
end; // setSegmentReal

procedure TgrapheReg.SetStatusReticuleData(var Agrid: TstringGrid);
var
  AutreCurseur: integer;

  procedure SetString(i: integer);
  var
    st: string;
  begin with curseurOsc[i] do begin
      if mondeC = mondeX then exit;
      statusSegment[0].Add('n°');
      statusSegment[1].Add(IntToStr(Ic));
      if coY in OptionCurseur then begin
        if monde[mondeC].graduation = gdB then
          st := formatGeneral(20 * log10(yr), precisionMin) + ' dB'
        else
          st := GrandeurCurseur.formatValeurEtUnite(Yr);
        statusSegment[0].Add(GrandeurCurseur.nom);
        statusSegment[1].Add(st);
      end;
      if coX in OptionCurseur then begin
        st := monde[mondeX].Axe.formatValeurEtUnite(Xr);
        statusSegment[0].Add(monde[mondeX].Axe.nom);
        statusSegment[1].Add(st);
      end;
  end; end; // SetString

  procedure SetVecteur(i: integer);
  var
    v, vx, vy: double;
  begin
    if curseurOsc[i].mondeC = mondeX then exit;
    with curseurOsc[i], courbes[indexCourbe] do begin
      if trVitesse in Trace then begin
        vx := ValXder[ic];
        vy := ValYder[ic];
        v  := sqrt(vx * vx + vy * vy);
        statusSegment[0].Add('v');
        statusSegment[1].Add(FormatReg(v));
      end;
      if trAcceleration in Trace then begin
        vx := ValXder2[ic];
        vy := ValYder2[ic];
        v  := sqrt(vx * vx + vy * vy);
        statusSegment[0].Add('a');
        statusSegment[1].Add(FormatReg(v));
      end;
    end;
  end; // SetVecteur

  procedure StrPente;
  var
    tit,st: string;
    dx, dy: double;
    dxNul: boolean;
  begin with curseurOsc[cCourant] do begin
      dxNul := curseurOsc[autreCurseur].xc = Xc;
      dX := 0;dY := 0;// pour le compilateur
      try
        with monde[CurseurOsc[curseurData1].mondeC] do begin
          tit := DeltaMaj + axe.nom;
          st := '';
          case Graduation of
            gLog: begin
              dY := curseurOsc[autreCurseur].yr / Yr;
              tit := axe.nom + '*';
              st :=  formatReg(dY);
              dY := log10(dY);
            end;
            gInv: begin
              dY := 1 / curseurOsc[autreCurseur].yr - 1 / Yr;
              st := formatReg(dY);
            end;
            gLin: begin
              dY := Yr - curseurOsc[autreCurseur].yr;
              st := axe.FormatValeurEtUnite(dY);
            end;
            gdB: begin
              dY := 20 * log10(Yr/curseurOsc[autreCurseur].yr);
              st := formatGeneral(dY, precisionMin) + ' dB';
            end;
            else dxNul := true;
          end;
        end;
        if (coDeltaY in optionCurseur) and
           (CurseurOsc[curseurData1].mondeC = CurseurOsc[curseurData2].mondeC) then begin
          statusSegment[1].Add(st);
          statusSegment[0].Add(tit);
        end;
        with monde[mondeX] do begin
          tit := DeltaMaj + axe.nom;
          st := '';
          case Graduation of
            gLog: begin
              dX := Xr / curseurOsc[autreCurseur].xr;
              tit := axe.nom + '*';
              st := formatReg(dX);
              dX := log10(dX);// décade
            end;
            gInv: begin
              dX := 1 / curseurOsc[autreCurseur].xr - 1 / Xr;
              st := formatReg(dX);
            end;
            gLin: begin
              dX := Xr - curseurOsc[autreCurseur].xr;
              st := Axe.FormatValeurEtUnite(dX);
            end;
            else dxNul := true;
          end;
        end;
        if (coDeltaX in optionCurseur) then begin
          statusSegment[1].Add(st);
          statusSegment[0].Add(tit);
        end;
        if not dxNul and (coPente in OptionCurseur) then begin
          st := unitePente.formatNomPente(dY / dX);
          statusSegment[1].Add(st);
          statusSegment[0].Add('');
        end;
      except
      end;
    end;
  end;

var
  i,j: integer;
begin
    if cCourant=4 then AutreCurseur := 3 else autreCurseur := 4;
    StatusSegment[0].Clear;
    StatusSegment[1].Clear;
    if not (monde[mondeX].defini) or
       not (monde[curseurOsc[cCourant].mondeC].defini) then
      exit;
    setString(curseurData1);
    setString(curseurData2);
    if (cCourant = 0) then exit;
    if (([coPente, coDeltaX, coDeltaY] * optionCurseur) <> []) or
      (curseurOsc[autreCurseur].mondeC <> mondeX) then
      strPente;
    setVecteur(curseurData1);
    setVecteur(curseurData2);
    if StatusSegment[0].Count > Agrid.RowCount then begin
        Agrid.rowCount := StatusSegment[0].Count;
        Agrid.Height := Agrid.rowCount*Agrid.DefaultRowHeight+4;
    end;
    for i := 0 to pred(StatusSegment[1].Count) do
        Agrid.Cells[1,i] := StatusSegment[1].strings[i];
    for i := 0 to pred(StatusSegment[0].Count) do
        Agrid.Cells[0,i] := StatusSegment[0].strings[i];
    for i := StatusSegment[0].Count to pred(Agrid.RowCount) do
        for j := 0 to Agrid.ColCount - 1 do
            Agrid.Cells[i,j] := '';
    for i := StatusSegment[1].Count to pred(Agrid.RowCount) do
        for j := 0 to Agrid.ColCount - 1 do
            Agrid.Cells[i,j] := '';
end; // SetStatusReticuleData

function TlisteDessin.Add(Item: Tdessin): integer;
begin
  Result := inherited Add(Item);
end;

function TlisteDessin.Get(Index: integer): Tdessin;
begin
  Result := Tdessin(inherited Get(Index));
end;

procedure TlisteDessin.Put(Index: integer; Item: Tdessin);
begin
  inherited Put(Index, Item);
end;

function TlisteDessin.Remove(Item: Tdessin): integer;
begin
  Result := inherited Remove(Item);
  Item.Free;
end;

procedure TlisteDessin.Clear;
var
  i: integer;
begin
  for i := 0 to pred(Count) do
    items[i].Free;
  inherited Clear;
end;

procedure TlisteCourbe.Clear;
var
  i: integer;
begin
  for i := 0 to pred(Count) do
    items[i].Free;
  inherited Clear;
end;

procedure TlisteEquivalence.Clear;
var
  i: integer;
begin
  for i := 0 to pred(Count) do
    items[i].Free;
  inherited Clear;
end;

function TlisteEquivalence.Remove(Item: Tequivalence): integer;
begin
  Result := inherited Remove(Item);
  Item.Free;
end;

function TlisteCourbe.Add(Item: Tcourbe): integer;
begin
  Result := inherited Add(Item);
end;

function TlisteCourbe.Get(Index: integer): Tcourbe;
begin
  Result := Tcourbe(inherited Get(Index));
end;

procedure TlisteCourbe.Put(Index: integer; Item: Tcourbe);
begin
  inherited Put(Index, Item);
end;

function TlisteCourbe.Remove(Item: Tcourbe): integer;
begin
  Result := inherited Remove(Item);
  Item.Free;
end;

function TlisteEquivalence.Add(Item: Tequivalence): integer;
begin
  Result := inherited Add(Item);
end;

function TlisteEquivalence.Get(Index: integer): Tequivalence;
begin
  Result := Tequivalence(inherited Get(Index));
end;

procedure TlisteEquivalence.Put(Index: integer; Item: Tequivalence);
begin
  inherited Put(Index, Item);
end;

function TgrapheReg.GetBorne(x, y: integer; var Borne: Tborne): boolean;
var
  xi, yi: integer;
  i: integer;

  procedure Affecte(st: TstyleBorne; indexM: codeIntervalle);
  begin
    GetBorne := True;
    with Borne do begin
      indexCourbe := i;
      xb := xi;
      yb := yi;
      IndexModele := indexM;
      Style := st;
    end;
  end;

begin // GetBorne
  Borne.style := bsAucune;
  Result := False;
  if (NbreModele = 0) or
     not BorneVisible then exit;
  for i := 0 to pred(courbes.Count) do
    with courbes[i] do
      if courbeExp and
         (page = pageCourante) and
         (FinC > DebutC) then begin
        windowRT(valX[debutC], valY[debutC], iMondeC, xi, yi);
        if (abs(xi - x) + abs(yi - y)) < 6 then begin
          Affecte(bsDebut, indexModele);
          break;
        end;
        if abs(xi - x) < 4 then begin
          Affecte(bsDebutVert, indexModele);
          break;
        end;
        windowRT(valX[finC], valY[finC], iMondeC, xi, yi);
        if (abs(xi - x) + abs(yi - y)) < 6 then begin
          Affecte(bsFin, indexModele);
          break;
        end;
        if abs(xi - x) < 4 then begin
          Affecte(bsFinVert, indexModele);
          break;
        end;
      end;
end; // GetBorne

function TgrapheReg.isAxe(x, y: integer): integer;
begin
  Result := -1;
end; // isAxe

procedure TgrapheReg.setBorne(x, y: integer; var borne: Tborne);

  function LigneProche: integer;
  var
    i: integer;
    Xi, Yi, newX, newY: integer;
    distance, distanceMin: integer;
  begin
    Result := -1;
    newX := x;
    newY := y;
    with courbes[borne.indexCourbe] do begin
      distanceMin := 32;
      for i := 0 to pred(pages[pageCourante].nmes) do begin
        windowRT(valX[i], valY[i], iMondeC, Xi, Yi);
        distance := abs(X - Xi);
        if distance < distanceMin then begin
          distanceMin := distance;
          Result := i;
          newX := Xi;
          newY := Yi;
        end;
      end;
      if Result > 0 then begin
        X := newX;
        Y := newY;
      end;
    end;
  end;

var
  i: integer;
begin with borne do begin
    TraceBorne(xb, yb, style, courbes[indexCourbe].couleur, indexModele);
    // efface
    if style in [bsDebutVert, bsFinVert]
       then i := LigneProche
       else i := PointProche(x, y, indexCourbe, False, False);
    if i >= 0 then begin
      case style of
        bsDebut, bsDebutVert: courbes[indexCourbe].debutC := i;
        bsFin, bsFinVert: courbes[indexCourbe].finC := i;
      end;
      xb := x;
      yb := y;
      for i := courbes[indexCourbe].debutC to courbes[indexCourbe].finC do
          pages[courbes[indexCourbe].page].PointActif[i] := true;
    end;
    TraceBorne(xb, yb, style, courbes[indexCourbe].couleur, indexModele);
    // retrace
end end; // setBorne

procedure TgrapheReg.EncadreTitre(Active: boolean);
begin
  if canvas = nil then exit; { ??? }
  with canvas do begin
    if active
       then pen.color := clActiveCaption
       else pen.color := clWindow;
    pen.mode := pmCopy;
    pen.Width := 3;
    with limiteFenetre do begin
      moveTo(left, top);
      lineTo(right, top);
    end;
    pen.Width := 1;
  end;
end;

function TgrapheReg.PointProcheReal(x, y: double; Acourbe: Tcourbe;m: indiceMonde): integer;
var
  distanceMin, delta: double;

  function PointProcheX: integer;
  var
    i: integer;
  begin with Acourbe, monde[mondeX] do begin
      Result := -1;
      case Graduation of
        gLog: distanceMin := power(10,maxi - 0.5);
        gInv: distanceMin := (1 / mini - 1 / maxi) / 4;
        gLin: distanceMin := (maxi - mini) / 4;
      end;
      for i := DebutC to FinC do begin
        delta := abs(valX[i] - x);
        if delta < distanceMin then begin
          distanceMin := delta;
          Result := i;
        end;
      end;
    end;
  end;

  function PointProcheY: integer;
  var
    i: integer;
  begin with Acourbe, monde[mondeY] do begin
      Result := -1;
      case Graduation of
        gLog: distanceMin := power(10,maxi - 0.5);
        gdB: distanceMin  := power(10,maxi / 20 - 0.5) - power(10,mini / 20 - 0.5);
        gInv: distanceMin := (1 / mini - 1 / maxi) / 4;
        gLin: distanceMin := (maxi - mini) / 4;
      end;
      for i := DebutC to FinC do begin
        delta := abs(valY[i] - y);
        if delta < distanceMin then begin
          distanceMin := delta;
          Result := i;
        end;
      end;
    end;
  end;

begin
  case m of
    mondeX: Result := pointProcheX;
    mondeY: Result := pointProcheY;
    else
      Result := -1;
  end;
end;

procedure TgrapheReg.TraceMotif(xi, yi: integer; motif: Tmotif);
var
  PolyLigne: array[1..5] of Tpoint;
  sauveWidth : integer;
  sauveStyle : TpenStyle;
begin
  sauveWidth := canvas.pen.width;
  sauveStyle := canvas.pen.style;
  CreatePen(psSolid, penWidth, canvas.pen.color);
  case Motif of
    mCroix: begin
      segment(xi - dimPoint, yi, xi + dimPoint, yi);
      segment(xi, yi - dimPoint, xi, yi + dimPoint);
    end;
    mCroixDiag: begin
      segment(xi - dimPoint, yi - dimPoint, xi + dimPoint, yi + dimPoint);
      segment(xi + dimPoint, yi - dimPoint, xi - dimPoint, yi + dimPoint);
    end;
    mCercle, mCerclePlein: canvas.ellipse(xi - dimPoint, yi - dimPoint,
        xi + dimPoint + 1, yi + dimPoint + 1);
    mCarre, mCarrePlein: canvas.rectangle(xi - dimPoint, yi - dimPoint,
        xi + dimPoint, yi + dimPoint);
    mLosange: begin
      PolyLigne[1] := Point(xi - dimPoint, yi);
      PolyLigne[2] := Point(xi, yi - dimPoint);
      PolyLigne[3] := Point(xi + dimPoint, yi);
      PolyLigne[4] := Point(xi, yi + dimPoint);
      PolyLigne[5] := Point(xi - dimPoint, yi);
      canvas.PolyLine(PolyLigne);
    end;
  end;
  createPen(sauveStyle, sauveWidth, canvas.pen.color);
end;

const
  NbreLigneDessin = 14;

function Tdessin.NbreData: integer;
begin
  NbreData := NbreLigneDessin + texte.Count;
end;

procedure Tdessin.Store;
var
  i: integer;
begin
  writeln(fichier,x1);
  writeln(fichier,y1);
  writeln(fichier,x2);
  writeln(fichier,y2);
  writeln(fichier, hauteur);{5}
  writeln(fichier, numPage, ' ', iMonde,
    ' ', Ord(identification),
    ' ', Ord(TexteLigne),
    ' ', '1',
    ' ', couleurFond);{6}
  writeln(fichier, pen.color, ' ', Ord(pen.style), ' ', pen.Width,
    ' ', Ord(isTitre), ' ', Ord(isOpaque));{7}
  writeln(fichier, Ord(Souligne));{8}
  writeln(fichier, Ord(vertical));{9}
  writeln(fichier, Ord(avecCadre));{10}
  writeln(fichier, Ord(centre)); {11}
  writeln(fichier, Ord(isTexte));{12}
  writeln(fichier, Ord(avecLigneRappel));
  writeln(fichier, Ord(MotifTexte));{14}
  for i := 0 to pred(texte.Count) do
      ecritChaineRW3(Texte[i]);
end;

(*
procedure Tdessin.StoreXML;

function writeInteger(Nom : string;valeur : integer) : IXMLNOde;
var newNode : IXMLNode;
begin
    newNode := Anode.AddChild(Nom);
    newNode.nodeValue := intToStr(valeur);
    result := NewNode;
end;

function writeFloat(Nom : string;valeur : double) : IXMLNode;
var newNode : IXMLNode;
begin
    newNode := Anode.AddChild(Nom);
    newNode.nodeValue := FloatToStr(valeur);
    result := newNode;
end;

function writeString(Nom,valeur : string) : IXMLNODE;
var newNode : IXMLNode;
begin
    newNode := Anode.AddChild(Nom);
    newNode.nodeValue := valeur;
    result := newNode;
end;

function writeBool(Nom : string;valeur : boolean) : IXMLNode;
var newNode : IXMLNode;
begin
    newNode := Anode.AddChild(Nom);
    newNode.nodeValue := ord(valeur);
    result := newNode;
end;

begin
  writeFloat('',x1);
  writeFloat('',y1);
  writeFloat('',x2);
  writeFloat('',y2);
  writeInteger('',hauteur);{5}
  writeInteger('',numPage);
  writeInteger('',iMonde);
  writeInteger('',ord(identification));
  writeInteger('',ord(TexteLigne));
  writeInteger('',couleurFond);{6}
  writeInteger('',pen.color);
  writeInteger('',ord(pen.style));
  writeInteger('',pen.Width);
  writeBool('',isTitre);
  writeBool('',isOpaque);
  writeBool('',Souligne);
  writeBool('',vertical);
  writeBool('',avecCadre);
  writeBool('',centre);
  writeBool('',isTexte);
  writeBool('',avecLigneRappel);
  writeInteger('',Ord(MotifTexte));
  Texte.Delimiter := crCR;
  writeString('',Texte.delimitedText);
end;

procedure Tdessin.LoadXML(ANode : IXMLNode);

Function LitBool : boolean;
var s : string;
begin
     s := ANode.NodeValue;
     result := s<>'0';
end;

Function LitInteger : integer;
begin
     try
     result := StrToInt(ANode.text);
     except
        result := 0;
     end;
end;

Function LitFloat : double;
var s : string;
begin
     s := ANode.NodeValue;
     result := StrToFloatWin(s);
end;

begin
 if ANode.NodeName='x1' then begin
   x1 := litFloat;
   end;
    if ANode.NodeName='y1' then begin
   y1 := litFloat;
   end;
    if ANode.NodeName='x2' then begin
   x2 := litFloat;
    end;
    if ANode.NodeName='y2' then begin
   y2 := litFloat;
    end;
    if ANode.NodeName='H' then begin
   hauteur := litInteger;
    end;
    if ANode.NodeName='Page' then begin
   numPage := litInteger;
    end;
  if ANode.NodeName='Monde' then begin
     iMonde := litInteger;
  end;
  if ANode.NodeName='' then begin
    identification := Tidentification(litInteger);
  end;
  if ANode.NodeName='Ident' then begin
    TexteLigne := TtexteLigne(litInteger);
  end;
  if  ANode.NodeName='Fond' then begin
     couleurFond := litInteger;
  end;
  if ANode.NodeName='Couleur' then begin
     pen.color := litInteger;
  end;
  if ANode.NodeName='' then begin
    pen.style := TpenStyle(litInteger);
  end;
  if ANode.NodeName='' then begin
    pen.Width := litInteger;
  end;
  if ANode.NodeName='Titre' then begin
    isTitre := litBool;
  end;
  if ANode.NodeName='Opaque' then begin
    isOpaque := litBool;
  end;
  if ANode.NodeName='Souigne' then begin
  Souligne := litBool;
  end;
  if ANode.NodeName='Vertical' then begin
  vertical := litBool;
  end;
  if ANode.NodeName='Cadre' then begin
  avecCadre := litBool;
  end;
  if ANode.NodeName='Centre' then begin
  centre  := litBool;
  end;
  if ANode.NodeName='isTexte' then begin
  isTexte := litBool;
  end;
  if ANode.NodeName='Rappel' then begin
  avecLigneRappel := litBool;
  end;
  if ANode.NodeName='Motif' then begin
  MotifTexte := TmotifTexte(litInteger);
  end;
  if ANode.NodeName='Texte' then begin
  //  Texte.add(ligneWin);
  end;
  if identification = identCoord then
     pen.color := couleurInit[(numCoord mod MaxOrdonnee) + 1];
end;
*)

procedure Tdessin.Load;
var
  i, imax: integer;
  z:  integer;
begin
  imax := NbreLigneWin(ligneWin) - NbreLigneDessin;
  readln(fichier, x1); {1}
  readln(fichier, y1); {2}
  readln(fichier, x2); {3}
  readln(fichier, y2); {4}
  readln(fichier, hauteur);{5}
  Read(fichier, numPage); // début 6
  if not eoln(fichier) then
    Read(fichier, iMonde);
  if not eoln(fichier) then begin
    Read(fichier, z);
    identification := Tidentification(z);
  end;
  if not eoln(fichier) then begin
    Read(fichier, z);
    TexteLigne := TtexteLigne(z);
  end;
  if not eoln(fichier) then
     Read(fichier, z);
  if not eoln(fichier) then
     Read(fichier, couleurFond);
  readln(fichier); // fin 6
  Read(fichier, z); // debut 7
  pen.color := z;
  if not eoln(fichier) then begin
    Read(fichier, z);
    pen.style := TpenStyle(z);
    Read(fichier, z);
    pen.Width := z;
  end;
  if not eoln(fichier) then begin
    Read(fichier, z);
    isTitre := z <> 0;
  end;
  if not eoln(fichier) then begin
    Read(fichier, z);
    isOpaque := z <> 0;
  end;
  readln(fichier);{fin 7}
  Souligne := litBooleanWin;{8}
  vertical := litBooleanWin;{9}
  avecCadre := litBooleanWin;{10}
  centre  := litBooleanWin;{11}
  isTexte := litBooleanWin;{12}
  avecLigneRappel := litBooleanWin; {13}
  readln(fichier, z); {14}
  MotifTexte := TmotifTexte(z);
  for i := 1 to imax do begin {15 sqq}
    litLigneWin;
    Texte.add(ligneWin);
  end;
  if identification = identCoord then
     pen.color := couleurInit[(numCoord mod MaxOrdonnee) + 1];
end;

procedure TgrapheReg.majNomGr(index: codeGrandeur);
var
  i: indiceMonde;
begin
  for i := 1 to maxOrdonnee do
    with coordonnee[i] do begin
      if codeY = index then
        nomY := grandeurs[index].nom;
      if codeX = index then
        nomX := grandeurs[index].nom;
    end;
end;


procedure Tmonde.Assign(Amonde: Tmonde);
begin
  mini := Amonde.mini;
  maxi := Amonde.maxi;
  MinidB := Amonde.minidB;
  Graduation := Amonde.graduation;
  zeroInclus := Amonde.zeroInclus;
  Axe  := Amonde.Axe;
  deltaAxe := Amonde.deltaAxe;
  exposant := Amonde.exposant;
  pointDebut := Amonde.pointDebut;
end;

function TgrapheReg.AjusteMonde: boolean;

  procedure AjusteMondeCartesien;

    procedure AjusteMondeLoc(amonde: indiceMonde; valeur: vecteur; Nbre: integer);
    var
      min, max: double;
    begin
      GetMinMax(valeur, Nbre, min, max);
      case monde[amonde].graduation of
        gLin: begin
          if monde[amonde].mini > min then begin
            monde[amonde].Mini := Min;
            Result := True;
          end;
          if monde[amonde].maxi < max then begin
            monde[amonde].Maxi := Max;
            Result := True;
          end;
        end;// linéaire
        gLog: begin
          try
            min := log10(min);
            if monde[amonde].mini > min then begin
              monde[amonde].Mini := Min;
              Result := True;
            end;
          except
          end;
          try
            max := log10(max);
            if monde[amonde].maxi < max then
            begin
              monde[amonde].Maxi := Max;
              Result := True;
            end;
          except
          end;
        end;{log}
        gInv: ;
        gdB: begin
          try
            min := log10(min) * 20;
            if monde[amonde].mini > min then begin
              monde[amonde].Mini := Min;
              Result := True;
            end;
          except
          end;
          try
            max := log10(max) * 20;
            if monde[amonde].maxi < max then begin
              monde[amonde].Maxi := Max;
              Result := True;
            end;
          except
          end;
        end;{décibel}
      end; // case graduation
    end;

  var
    j: integer;
  begin
    for j := 0 to pred(Courbes.Count) do
      with courbes[j] do begin
        AjusteMondeLoc(imondeC, valY, finC - debutC + 1);
        AjusteMondeLoc(mondeX, valX, finC - debutC + 1);
      end;{j}
  end; // AjusteMondeCartesien

  procedure AjusteMondePolaire;
  var
    j, i: integer;
    rayon, angle, x, y: double;
  begin
    for j := 0 to pred(Courbes.Count) do
      with courbes[j] do begin
        for i := debutC to finC do begin
          angle := valX[i];
          rayon := valY[i]-zeroPolaire;
          angleEnRadian(angle);
          x := rayon * cos(angle);
          with monde[mondeX] do
            if x > Maxi then begin
              Maxi := x;
              Result := True;
            end
            else if x < Mini then begin
              Mini := x;
              Result := True;
            end;
          y := rayon * sin(angle);
          with monde[iMondeC] do
            if y > Maxi then begin
              Maxi := y;
              Result := True;
            end
            else if y < Mini then begin
              Mini := y;
              Result := True;
            end;
        end;{i}
      end;{j}
  end; // AjusteMondePolaire

var
  margeMonde: double;
  j: indiceMonde;
begin
  Result := False;
  if OgPolaire in OptionGraphe
     then AjusteMondePolaire
     else AjusteMondeCartesien;
  if Result then
    for j := MondeY to High(indiceMonde) do begin
      MargeMonde := (monde[j].Maxi - monde[j].Mini) / 64;
      if monde[j].Mini * (monde[j].Mini - MargeMonde) > 0 then
        monde[j].Mini := monde[j].Mini - MargeMonde;
      if monde[j].Maxi * (monde[j].Maxi + MargeMonde) > 0 then
        monde[j].Maxi := monde[j].Maxi + MargeMonde;
    end;
end; // AjusteMonde

procedure TgrapheReg.traceBorne(xi, yi: integer;
  style: TstyleBorne; couleur: Tcolor; indexM: integer);
var
  y1,dB: integer;
  Points:  array[0..2] of Tpoint;
label fin;
begin
  if not avecBorne or (indexM = 0) or
    ((xi = limiteCourbe.left) and (style = bsDebutVert)) or
    (xi<0) or (yi<0) then exit;
  createSolidBrush(couleur);
  createPen(psSolid, 1, couleur);
  canvas.pen.mode := pmNotXor;
  dB := dimBorne;
  if style in [bsFin, bsFinVert] then dB := -dimBorne;
  if not (ImprimanteEnCours or ImageEnCours) then begin
    // symbole de borne=triangle
    Points[0] := Point(xi - dB, yi + dB);
    Points[1] := Point(xi - dB, yi - dB);
    Points[2] := Point(xi + dB, yi);
    canvas.Polygon(Points); // triangle 3 points
    if style = bsAucune then goto fin;
  end;
  if fonctionTheorique[indexM].variableEgalAbscisse and
    (fonctionTheorique[indexM].indexX = indexTri) then begin
    y1 := (LimiteCourbe.top + LimiteCourbe.bottom) div 2;
    Points[0] := Point(xi - 2*dB, y1 + dB);
    Points[1] := Point(xi - 2*dB, y1 - dB);
    Points[2] := Point(xi, y1);
    canvas.Polygon(Points); // triangle 3 points
    createPen(psDash, 3, couleur);
    segment(xi, LimiteCourbe.top, xi, LimiteCourbe.bottom);
  end;
  fin:
  canvas.pen.mode := pmCopy;
  canvas.brush.style := bsClear;
end; // traceBorne

procedure TGrapheReg.TraceReticule(index: indexCurseur);
begin with CurseurOsc[index], paintBox.Canvas do begin
    if not monde[mondeX].defini or
       not monde[mondeC].defini then exit;
    mondeRT(xc,yc, mondeC, Xr, Yr);
  //  createPen(PstyleReticule, penWidthReticule, PColorReticule);
    Pen.mode := pmNOTXOR;
    moveTo(0, Yc);
    lineTo(PaintBox.Width, Yc);
    moveTo(Xc, 0);
    lineTo(Xc, PaintBox.Height);
    Pen.mode := pmCopy;
end; end;

procedure TGrapheReg.TraceEcart;
var
  x0, y0, x1, y1, x2, y2 : integer;
begin with paintBox.Canvas do begin
    if not monde[mondeX].defini or
       not monde[curseurOsc[1].mondeC].defini then exit;
//    createPen(PstyleReticule, penWidthReticule, PColorReticule);
    x1 := curseurOsc[2].Xc;
    y1 := curseurOsc[2].Yc;
    x2 := curseurOsc[1].Xc;
    y2 := curseurOsc[1].Yc;
    x0 := limiteFenetre.left+1;
    y0 := limiteFenetre.Top+1;
    Pen.mode  := pmNOTXOR;
    Pen.color := clHighLight;
    if y1<>y2 then begin
       moveTo(x0, y1);
       lineTo(x0, y2);
    end;
    if x1<>x2 then begin
       moveTo(x1, y0);
       lineTo(x2, y0);
    end;
    Pen.mode := pmCopy;
end; end;

procedure TgrapheReg.SetUnitePente(iMonde: indiceMonde);
begin
  UnitePente.UniteDerivee(monde[mondeX].axe, monde[iMonde].axe,
    monde[mondeX].graduation, monde[iMonde].graduation);
  UnitePente.CoeffAff := monde[mondeX].exposant / monde[iMonde].exposant;
end;

procedure TgrapheReg.TraceBorneFFT;
var
  XdebutInt, XfinInt, zut: integer;
begin
  with pages[pageCourante] do begin
    if (debutFFT = 0) and (finFFT > nmes - 8) then
      exit;
    windowXY(valeurVar[0, finFFT], 0, mondeY, XfinInt, zut);
    windowXY(valeurVar[0, debutFFT], 0, mondeY, XdebutInt, zut);
  end;
  if XdebutInt < LimiteCourbe.left then
     XdebutInt := LimiteCourbe.left;
  if XfinInt >= LimiteCourbe.right then
     XfinInt := pred(LimiteCourbe.right);
  if (XfinInt-XdebutInt)<(3*LimiteCourbe.right div 4)
     then begin
        canvas.pen.Width := 3*penWidth;
        canvas.Pen.Color := clGray;
        canvas.Brush.color := clCream;
        canvas.Brush.style := bsSolid;
        canvas.Rectangle(XdebutInt,LimiteCourbe.bottom-marge,XfinInt,LimiteFenetre.top+marge);
     end
     else begin
        createPen(psSolid, 3*penWidth, GetCouleurPages(1));
        segment(XdebutInt, LimiteCourbe.bottom, XdebutInt, LimiteCourbe.top);
        segment(XfinInt, LimiteCourbe.bottom, XfinInt, LimiteCourbe.top);
     end;
  canvas.Brush.style := bsClear;
  canvas.Pen.Width := 1;
end;

procedure TgrapheReg.setCourbeDerivee;
var
  i: integer;
begin
  if ligneRappelCourante in [lrEquivalence, lrTangente] then begin
    equivalenceCourante := equivalenceTampon;
    equivalenceCourante.ligneRappel := lrTangente;
  end
  else EquivalenceCourante := nil;
  indexCourbeEquivalence := -1;
  mondeDerivee := mondeY;
  for i := 0 to pred(courbes.Count) do
    with courbes[i] do
      if (page = pageCourante) and (iMondeC = mondeY) then begin
        indexCourbeEquivalence := i;
        break;
      end;
  resetCourbeDerivee;
end;

procedure TgrapheReg.resetCourbeDerivee;
var
  i: integer;
  codeDerivee: codeGrandeur;
  nomDer: string;
  varYloc: tGrandeur;
  sauveIndex: integer;
begin
  if indexCourbeEquivalence < 0 then exit;
  mondeDerivee := courbes[indexCourbeEquivalence].iMondeC;
  if courbes[indexCourbeEquivalence].courbeExp then
    { recherche courbe modélisée ou lissée correspondante }
    for i := 0 to pred(courbes.Count) do
      with courbes[i] do
        if (page=pageCourante) and (valYder <> nil) and
           (varY=courbes[indexCourbeEquivalence].varY) then begin
          if (indexModele=1) and not (courbeExp) then begin
            indexCourbeEquivalence := i;
            equivalenceTampon.modelisee := True;
            break;
          end;
          if (indexModele = indexSpline) or
             (indexModele = indexSinc) then begin
             indexCourbeEquivalence := i;
             equivalenceTampon.modelisee := True;
             break;
          end;
        end;
  varYloc := monde[mondeDerivee].axe;
  if courbes[indexCourbeEquivalence].varY <> nil then begin
     varYloc := courbes[indexCourbeEquivalence].varY;
     equivalenceTampon.vary := varYloc;
  end;
  codeDerivee := indexDerivee(varYloc, monde[mondeX].axe, False, False);
  if codeDerivee = grandeurInconnue then begin
    nomDer := 'der' + varYloc.nom;
    i := 0;
    while not nomCorrect(nomDer, grandeurInconnue) do begin
      nomDer := 'der' + varYloc.nom + IntToStr(i);
      Inc(i);
    end;
    Fvaleurs.memo.Lines.add(nomDer + '=d(' +
      monde[mondeDerivee].axe.nom + ')/d(' + monde[mondeX].axe.nom + ')');
    ajoutGrandeurNonModele := True;
    sauveIndex := indexCourbeEquivalence;
    Fvaleurs.MajBtnClick(nil); // RàZ indexCourbeEquivalence
    Application.ProcessMessages;
    indexCourbeEquivalence := sauveIndex;
  end;
  UnitePente.UniteDerivee(monde[mondeX].axe, varYloc, gLin, gLin);
  if uniteSIGlb
     then coeffSIpente := dix(monde[mondeX].axe.puissance - varYloc.puissance)
     else coeffSIpente := 1;
end;

procedure TgrapheReg.chercheMinMax;
begin
  if OgPolaire in OptionGraphe then begin
    miniMod := 0;
    if angleEnDegre
       then maxiMod := 360
       else maxiMod := 2 * pi;
  end
  else with monde[mondeX] do
      case Graduation of
        gLog: begin
          maxiMod := power(10,Maxi);
          miniMod := power(10,Mini);
        end;
        gInv: begin
          maxiMod := 1 / Mini;
          miniMod := 1 / Maxi;
        end;
        else begin
          maxiMod := Maxi;
          miniMod := Mini;
        end;
      end;{case}
end; // chercheMinMax

function TgrapheReg.isAxeX(x, y: integer;var adroite : boolean): boolean;
begin
  Result := (y > limiteCourbe.bottom) and
    (y < limiteFenetre.bottom) and
    (x > limiteCourbe.left) and
    (x < (limiteFenetre.right - 8 * largCarac));
  adroite := x>((limiteCourbe.left+limiteCourbe.right) div 2);
end;

function TgrapheReg.isAxeY(x, y: integer;var enHaut : boolean): boolean;
begin
  Result := (x > limiteFenetre.left) and
    (x < (limiteCourbe.left)) and
    (y > limiteCourbe.top) and
    (y < limiteCourbe.bottom);
  enHaut := y<((limiteCourbe.top+limiteCourbe.bottom) div 2);
end;

function TgrapheReg.BasCourbe: integer;
begin
  if axeYpositif
     then Result := limiteCourbe.top
     else Result := limiteCourbe.bottom;
end;

function TgrapheReg.HautCourbe: integer;
begin
  if axeYpositif
     then Result := limiteCourbe.bottom
     else Result := limiteCourbe.top;
end;

Procedure TTransfertGraphe.Ecrit(numero : integer);
var i : integer;
    codeY,Nbre : integer;
begin
    nbre := 0;
    for i := 1 to maxOrdonnee do begin
        codeY := indexNom(nomY[i]);
        if (codeY<>grandeurInconnue) and
           (grandeurs[codeY].genreG=variable)
           then begin
        codeY := indexNom(nomX[i]);
        if (codeY<>grandeurInconnue) and
           (grandeurs[codeY].genreG=variable)
           then inc(Nbre);
           end;
   end;
   if nbre=0 then exit;
   writeln(fichier,symbReg2,'1 DEBUT PAGE'+intTostr(numero));
   writeln(fichier,numero);
   writeln(fichier,symbReg2,Nbre,' LIGNE');
   for i := 1 to Nbre do
       writeln(fichier,ord(ligne[i]));
   writeln(fichier,symbReg2,Nbre,' COULEUR');
   for i := 1 to Nbre do
       writeln(fichier,couleur[i]);
   writeln(fichier,symbReg2,Nbre,' MOTIF');
   for i := 1 to Nbre do
       writeln(fichier,ord(motif[i]));
   writeln(fichier,symbReg2,Nbre,' X');
   for i := 1 to Nbre do
       ecritChaineRW3(nomX[i]);
   writeln(fichier,symbReg2,Nbre,' Y');
   for i := 1 to Nbre do
       ecritChaineRW3(nomY[i]);
   writeln(fichier,symbReg2,Nbre,' TRACE');
   for i := 1 to Nbre do
       writeln(fichier,word(trace[i]));
   writeln(fichier,symbReg2,Nbre,' MONDE');
   for i := 1 to Nbre do
       writeln(fichier,ord(iMonde[i]));
   writeln(fichier,symbReg2,succ(mondeDroit-mondeX),' GRADUATION');
   for i := mondeX to MondeDroit do
       writeln(fichier,ord(grad[i]));
   writeln(fichier,symbReg2,succ(mondeDroit-mondeX),' ZERO');
   for i := mondeX to MondeDroit do
       writeln(fichier,ord(zero[i]));
   writeln(fichier,symbReg2,'13 OPTIONS');
   writeln(fichier,byte(optionGr));{1}
   writeln(fichier,byte(optionModele));{2}
   writeln(fichier,ord(traceDefaut));{3}
   writeln(fichier,OrdreLissage);{4}
   writeln(fichier,DimPointVGA);{5}
   writeln(fichier,ord(UseDefaut));{6}
   writeln(fichier,ord(UseDefautX));{7}
   writeln(fichier,ord(SuperposePage));{8}
   writeln(fichier,ord(avecEllipse));{9}
   writeln(fichier,-1);{10}
   writeln(fichier,ord(FilDeFer));{11}
   writeln(fichier,ord(false));{12}
   writeln(fichier,pasPoint);{13}
   writeln(fichier,symbReg2,'1 FIN');
   writeln(fichier,numero);
end;

Procedure TTransfertGraphe.Lit;
var imax : integer;
    i : integer;
    choix : integer;
    zWord : word;
    zByte : byte;
    zInt : integer;
    lectureOK,termine : boolean;
begin // lit
   termine := false;
   while (Length(LigneWin)>0) and (ligneWin[1]=symbReg2) and not termine do begin
      choix := 0;
      imax := NbreLigneWin(ligneWin);
      if pos('X',ligneWin)<>0 then choix := 1;
      if pos('Y',ligneWin)<>0 then choix := 2;
      if pos('MONDE',ligneWin)<>0 then choix := 3;
      if pos('GRADUATION',ligneWin)<>0 then choix := 4;
      if pos('OPTIONS',ligneWin)<>0 then choix := 5;
      if pos('ZERO',ligneWin)<>0 then choix := 6;
      if pos('TRACE',ligneWin)<>0 then choix := 13;
      if pos('COULEUR',ligneWin)<>0 then choix := 16;
      if pos('LIGNE',ligneWin)<>0 then choix := 17;
      if pos('MOTIF',ligneWin)<>0 then choix := 18;
      if pos('INVERSE',ligneWin)<>0 then choix := 23;
      if pos('FIN PAGE',ligneWin)<>0 then choix := 24;
      lectureOK := true;
      case choix of
           0 : for i := 1 to imax do readln(fichier);
           1 : for i := 1 to imax do begin
                litLigneWin;
                nomX[i] := LigneWin;
           end;
           2 : for i := 1 to imax do begin
                litLigneWin;
                nomY[i] := LigneWin;
           end;
           3 : for i := 1 to imax do begin
                litLigneWin;
                if ligneWin[1]=symbReg2
                   then begin
                       lectureOK := false;
                       break;
                   end
                   else begin
                       zByte := StrToInt(ligneWin);
                       iMonde[i] := indiceMonde(zByte);
                   end
           end;
           4 : for i := 1 to imax do begin
                litLigneWin;
                if ligneWin[1]=symbReg2
                   then begin
                       lectureOK := false;
                       break;
                   end
                   else begin
                       zByte := StrToInt(ligneWin);
                       grad[pred(i)] := Tgraduation(zByte);
                   end;    
           end;
           5 : begin
             litLigneWin; {1}
             optionGr := TsetOptionGraphe(byte(strToInt(ligneWin)));
             litLigneWin; {2}
             optionModele := TsetOptionModele(byte(strToInt(ligneWin)));
             litLigneWin; {3}
             try
             TraceDefaut := TtraceDefaut(strToInt(ligneWin));
             except
             TraceDefaut := tdPoint;
             end;
             litLigneWin; {4}
             OrdreLissage := strToInt(ligneWin);
             litLigneWin; {5}
             DimPointVGA := strToInt(ligneWin);
             useDefaut := litBooleanWin; {6}
             useDefautX := litBooleanWin; {7}
             superposePage := litBooleanWin; {8}
             avecEllipse := litBooleanWin; {9}
             litLigneWin;{10}
             FilDeFer := litBooleanWin; {11}
             litBooleanWin; {12}
             readln(fichier,PasPoint); {13 }
             for i := 14 to imax do readln(fichier);
           end;
           6 : for i := 1 to imax do
                zero[pred(i)] := litBooleanWin;
           13 : for i := 1 to imax do begin
                 readln(fichier,zWord);
                 trace[i] := TsetTrace(zWord);
           end;
           16 : for i := 1 to imax do
               readln(fichier,couleur[i]);
           17 : for i := 1 to imax do begin
               readln(fichier,zByte);
               ligne[i] := Tligne(zByte);
           end;
           18 : for i := 1 to imax do begin
               readln(fichier,zByte);
               motif[i] := Tmotif(zByte);
           end;
           23 : for i := 1 to imax do
                litBooleanWin;
           24 : begin
                litLigneWin;
                termine := true;
           end;
       end; { case }
       if lectureOK then litLigneWin;
    end;
end; // lit

function TgrapheReg.WindowX(xr: double): integer;
begin
  case monde[mondeX].graduation of
    gLog: xr := Log10(xr);
    gInv: xr := 1 / xr;
  end;
  Result := monde[mondeX].affecteEntier(xr);
end;

procedure TgrapheReg.CreateSolidBrush(couleur : Tcolor);
begin
     canvas.Brush.Style := bsSolid;
     canvas.Brush.Color := couleur;
end;

procedure TgrapheReg.MyTextOut(x,y : integer;S : string);
begin
     if Canvas.font.orientation=900 then begin
     // normal TA_TOP
        if (alignement and TA_BOTTOM)= TA_BOTTOM
           then x := x - canvas.textHeight(S)
            else if (alignement and TA_BASELINE)= TA_BASELINE
               then x := x - (canvas.textHeight(S) div 2);
     // normal TA_LEFT
        if (alignement and TA_CENTER)= TA_CENTER
            then y := y - (canvas.textWidth(S) div 2)
            else if (alignement and TA_RIGHT)= TA_RIGHT
                 then y := y - canvas.textWidth(S);
     end
     else begin
         if (alignement and TA_BOTTOM)= TA_BOTTOM
            then if axeYpositif
               then y := y + canvas.textHeight(S)
               else y := y - canvas.textHeight(S)
            else if (alignement and TA_BASELINE)= TA_BASELINE then
               if axeYpositif
                  then y := y + (canvas.textHeight(S) div 2)
                  else y := y - (canvas.textHeight(S) div 2);
         if (alignement and TA_CENTER)= TA_CENTER
             then x := x - (canvas.textWidth(S) div 2)
             else if (alignement and TA_RIGHT)= TA_RIGHT
                 then x := x - canvas.textWidth(S);
     end;
     canvas.TextOut(x,y,S);
end;

procedure TgrapheReg.MyTextOutFond(x,y : integer;S : string;couleur : Tcolor);
begin
     if ImprimanteMono then begin
        myTextOut(x,y,S);
        exit;
     end;
     canvas.Brush.Color := couleur;
     MyTextOut(x,y,S);
     canvas.Brush.Color := clWindow;
end;

procedure TgrapheReg.SetTextAlign(aligne : integer);
begin
   Alignement := aligne
// TA_BASELINE The reference point will be on the base line of the text.
// TA_BOTTOM The reference point will be on the bottom edge of the bounding rectangle.
// TA_TOP The reference point will be on the top edge of the bounding rectangle.
// TA_CENTER The reference point will be aligned horizontally with the center of the bounding rectangle.
// TA_LEFT The reference point will be on the left edge of the bounding rectangle.
// TA_RIGHT The reference point will be on the right edge of the bounding rectangle.
end;

procedure TgrapheReg.createHatchBrush(aCode : TbrushStyle;couleur : Tcolor);
begin
     canvas.Brush.Style := aCode;
     canvas.Brush.Color := couleur;
end;

procedure TgrapheReg.CreatePen(aStyle : TpenStyle;aWidth  : integer;couleur : Tcolor);
begin
     if imprimanteMono
        then canvas.pen.Color := clBlack
        else canvas.pen.Color := couleur;
     canvas.pen.Style := astyle;
     canvas.pen.Width := awidth;
end;

procedure TgrapheReg.changeEchelleX(xnew, xold: integer;ismaxi : boolean);
begin with monde[mondeX] do begin
    if ismaxi then begin
       xnew := xnew - limiteCourbe.left;
       if xnew < 16 then exit;
       xold := xold - limiteCourbe.left;
       A := A * xnew / xold;
       Maxi := Mini + (limiteCourbe.right - limiteCourbe.left) / A;
       B := limiteCourbe.left - Mini * A;
    end
    else begin
       xnew := limiteCourbe.right-xnew;
       if xnew < 16 then exit;
       xold := limiteCourbe.right - xold;
       A := A * xnew / xold;
       Mini := Maxi - (limiteCourbe.right - limiteCourbe.left) / A;
       B := limiteCourbe.left - Mini * A;
    end;
    useDefautX := true;
    MaxDefaut := Maxi;
    MinDefaut := Mini;
end end;

procedure TgrapheReg.changeEchelleY(ynew, yold: integer;isMaxi : boolean);
begin with monde[mondeY] do begin
    if ismaxi then begin
       ynew := limiteCourbe.bottom - ynew;
       if ynew < 16 then exit;
       yold := limiteCourbe.bottom - yold;
       A := A * ynew / yold;
       Maxi := Mini + (limiteCourbe.top - limiteCourbe.bottom) / A;
       B := limiteCourbe.bottom - Maxi * A;
    end
    else begin
       ynew := ynew - limiteCourbe.top;
       if ynew < 16 then exit;
       yold := yold - limiteCourbe.top;
       A := A * ynew / yold;
       Mini := Maxi + (limiteCourbe.bottom - limiteCourbe.top) / A;
       B := limiteCourbe.bottom - Maxi * A;
    end;
    useDefaut := true;
    MaxDefaut := Maxi;
    MinDefaut := Mini;
end end;

procedure Tcourbe.Calcul;
var
  indexZero1, indexZero2,debut: integer;

  procedure CherchePeriode;
  var
    signe: double;
    iMax:  integer;
  begin
    indexZero1 := debut;
    signe := valY[debut] - valeurMoy;
    if signe = 0 then signe := -1;
    iMax := debutC + (finC - debut) div 2;
    while (indexZero1 < iMax) and
          ((valY[indexZero1] - valeurMoy) * signe > 0) do
         Inc(indexZero1);
    // Premier passage par "zéro" (vers le bas par ex.)
    signe := valY[indexZero1] - valeurMoy;
    if signe = 0 then signe := -1;
    indexZero2 := indexZero1 + 8;
    while (indexZero2 < (finC - 8)) and
          ((valY[indexZero2] - valeurMoy) * signe >= 0) do
      Inc(indexZero2);
    // On repasse par "zéro" dans le sens opposé
    indexZero2 := indexZero2 + 7;
    while (indexZero2 > finC) and
          ((valY[indexZero2] - valeurMoy) * signe <= 0) do
      Inc(indexZero2);
    // On repasse par "zéro" dans le même sens
    if indexZero2 < finC then
       periode := valX[indexZero2] - valX[indexZero1];
  end;

var
  i, N : integer;
  sommeEff, sommeMoy, zz: double;
begin
  debut := debutC;
  while (debut<finC) and isNan(valY[debut]) do inc(debut);
  N := succ(finC - debut);
  valeurEff := Nan;
  periode := Nan;
  valeurMax := valY[debut];
  valeurMin := valY[debut];
  for i := succ(debut) to finC do begin
    zz := valY[i];
    if zz > valeurMax then
      valeurMax := zz
    else if zz < valeurMin then
      valeurMin := zz;
  end;
  valeurMoy := (valeurMax + valeurMin) / 2;
  if N < 16 then
    exit;
  CherchePeriode;
  if indexZero2 >= finC then
    exit;
  SommeEff := 0;
  SommeMoy := 0;
  N := succ(indexZero2 - indexZero1);
  for i := indexZero1 to indexZero2 do begin
    zz := valY[i];
    SommeEff := SommeEff + sqr(zz);
    SommeMoy := SommeMoy + zz;
  end;
  valeurEff := sqrt(SommeEff / N);
  valeurMoy := sommeMoy / N;
end; // Tcourbe.Calcul

procedure TgrapheReg.VersLatex(NomFichier : string;suffixe : char);

Function FloatToStrLatex(const valeur : double) : string;
var posSeparator : integer;
begin
   if isNan(valeur) then result := ''
   else begin
   result := FloatToStrF(valeur,ffGeneral,4,2);
   if FormatSettings.decimalSeparator<>'.' then begin
      posSeparator := pos(FormatSettings.decimalSeparator,result);
      if posSeparator>0 then result[posSeparator] := '.';
   end;
   end;
end;
 var NomFichierDebut : String;

 // title p. 177
 // unit p. 226
 // barre d'erreur p. 160

 procedure TraceMonde(m : indiceMonde);
 const NbreMax = 128;
 var FichierAsc : textFile;
     exposantX,exposantY : double;

 procedure Extrait(index : integer); // enlever les points inutiles
    var
      idebut: integer;
      deltaX : double;

      procedure Nettoie;
      var
        ifin: integer;
        maxi, mini, maxiX, miniX: double;
        xdebut,xcourant, ycourant: double;
      begin with courbes[index] do begin
        xdebut := valX[idebut];
        yCourant := valY[idebut];
        maxi := ycourant;
        mini := ycourant;
        maxiX := xdebut;
        miniX := xdebut;
        iFin := iDebut + 1;
        repeat
          yCourant := valY[iFin];
          xCourant := valX[iFin];
          if yCourant > maxi then begin
            maxi := ycourant;
            maxiX := xCourant;
          end;
          if yCourant < mini then begin
            mini := ycourant;
            miniX := xCourant;
          end;
          Inc(iFin);
        until (iFin>finC) or ((xCourant-xDebut)>deltaX); // verticale différente
        writeln(fichierAsc,FloatToStrLatex(valX[iDebut]/exposantX)+crTab+
                           FloatToStrLatex(valY[iDebut]/exposantY));
// on garde début
        if (mini-valY[iDebut])*(mini-valY[iFin])>0 then
            writeln(fichierAsc,FloatToStrLatex(miniX/exposantX)+crTab+
                               FloatToStrLatex(mini/exposantY));
// on garde mini si en dehors de debut fin
        if (maxi-valY[iDebut])*(maxi-valY[iFin])>0 then
            writeln(fichierAsc,FloatToStrLatex(maxiX/exposantX)+crTab+
                               FloatToStrLatex(maxi/exposantY));
        writeln(fichierAsc,FloatToStrLatex(valX[iFin]/exposantX)+crTab+
                           FloatToStrLatex(valY[iFin]/exposantY));
// on garde fin
        idebut := iFin+1; // prochain debut
      end end;

    begin
      deltaX := 2*(monde[mondeX].maxi-monde[mondeX].mini)/NbreMax;
      idebut := courbes[index].debutC;
      repeat
           Nettoie
      until idebut >= courbes[index].finC;
    end; // extrait

 var i,j,i1 : integer;
     Nx,Ny : string;
     NomFichierCourant : string;
     prefixe : string;
     lignePlot : string;
 begin
      prefixe := '';
      i1 := 0;
      for i := 0 to Courbes.Count - 1 do
          if courbes[i].iMondeC=m then begin
              i1 := i;
              break;
          end;
      if monde[mondeX].graduation=gLog
         then if monde[m].graduation=gLog
              then prefixe := 'loglog'
              else prefixe := 'semilogx'
         else if monde[m].graduation=gLog
              then prefixe := 'semilogy';
// else if (OgPolaire in options) then prefixe := 'polar';
// en cours d'installation dans pgfPlots
      ajouteLigneLatex('\begin{'+prefixe+'axis}[');
      ajouteLigneLatex('height=8cm,width=12cm');
      exposantX := monde[mondeX].exposant;
      exposantY := monde[m].exposant;
// TODO texte p. 183 188
// TODO ligne p 280
// title p 177
// barre d'erreur p 160
// unité p. 226
//     ajouteLigneLatex('xscale=0.7, yscale=0.2');
//     ajouteLigneLatex(',scale only axis');
      if m=mondeY then if (ogZeroGradue in OptionGraphe)
         then ajouteLigneLatex(',axis x line=center,axis y line=center')
         else ajouteLigneLatex(',axis x line=bottom,axis y line=left');
      if m=mondeDroit then
         ajouteLigneLatex(',axis x line=none,axis y line=right');
      with monde[mondeX] do
           ajouteLigneLatex(',xmin='+FloatToStrLatex(mini/exposantX)+
                            ',xmax='+FloatToStrLatex(maxi/exposantX));
      with monde[m] do
           ajouteLigneLatex(',ymin='+FloatToStrLatex(mini/exposantY)+
                            ',ymax='+FloatToStrLatex(maxi/exposantY));
      if OgQuadrillage in OptionGraphe then ajouteLigneLatex(',grid=major');
      if m=mondeY then begin
         Nx := courbes[i1].varX.titreAff;
         Nx := Nx+courbes[i1].varX.UniteAxe(exposantX);
      end;
      Ny := courbes[i1].varY.titreAff;
      with monde[m] do
        if graduation=gdB
           then Ny := Ny+' (dB)'
           else Ny := Ny+courbes[i1].varY.UniteAxe(exposantY);
      ajouteLigneLatex(',title={'+translateNomTexte(pages[courbes[i1].page].titrePage)+'}');
      if m=mondeY then ajouteLigneLatex(',xlabel={'+translateNomTexte(Nx)+'}');
      ajouteLigneLatex(',ylabel={'+translateNomTexte(Ny)+'}');
      ajouteLigneLatex(']');
// enlever blanc et carac spéciaux
      for i := 0 to Courbes.Count - 1 do with courbes[i] do if iMondeC=m
          then begin
          NomFichierCourant := NomFichierDebut+intToStr(i)+'.txt';
          AssignFile(fichierAsc,NomFichierCourant);
          Rewrite(fichierAsc);
//          writeln(fichierAsc,'#'+pages[page].TitrePage);
//          writeln(fichierAsc,varX.nom+crTab+varY.nom);
          if (finC-debutC)>NbreMax
          then extrait(i)
          else if (trPoint in trace) and (motif=mIncert)
          then begin for j := debutC to finC do
              writeln(fichierAsc,FloatToStrLatex(valX[j]/exposantX)+crTab+
                                 FloatToStrLatex(valY[j]/exposantY)+crTab+
                                 FloatToStrLatex(incertX[j]/exposantX)+crTab+
                                 FloatToStrLatex(incertY[j]/exposantY))
                                 // incertitude non définie ?
          end
          else begin for j := debutC to finC do
              writeln(fichierAsc,FloatToStrLatex(valX[j]/exposantX)+crTab+
                                 FloatToStrLatex(valY[j]/exposantY));
          end;
          closeFile(FichierAsc);
          lignePlot := '\addplot[draw='+couleurLatex(couleur);
          if (trLigne in trace)
             then if (trPoint in trace)
                then
                else lignePlot := lignePLot+',mark=none,smooth'
             else lignePlot := lignePlot+',only marks';
          if trPoint in trace then begin
               case motif of
                    mCroix, mCroixDiag, mCercle, mCarre, mLosange,
                    mCerclePlein, mCarrePlein,mPixel : begin
                       lignePlot := lignePlot+',mark='+MotifLatex[motif];
                       if motif in [mcerclePlein,mcarrePlein] then
                          lignePlot := lignePlot+',mark options={fill='+couleurLatex(couleur)+'}';
                       if motif=mPixel then
                          lignePlot := lignePlot+',mark options={xscale=1,yscale=1}';
                     end;
                     mIncert : lignePlot := lignePlot+',error bars/.cd'+
                              ',y dir=both, y explicit'+
                              ',x dir=both, x explicit'+
                              ',error mark=none';
                     mEchantillon : lignePlot := lignePlot+',ycomb,mark=*';
                     mHisto : lignePlot := lignePlot+',ybar,bar width=5pt,ybar interval=0';
                     mSpectre : lignePlot := lignePlot + ',ycomb';
                     else lignePlot := lignePlot+',mark=*';
                end; // case
             end; // trPoint
          if (trPoint in trace) and (motif=mIncert)
             then lignePlot := lignePlot+'] table[x error index=2,y error index=3]'
             else lignePLot := lignePlot+'] file';
          lignePlot := lignePlot+' {'+ExtractFileName(NomFichierCourant)+'};';
          ajouteLigneLatex(lignePlot);
      end;
      ajouteLigneLatex('\end{'+prefixe+'axis}');
 end;

 var avecAxeDroit : boolean;
     i : integer;
  begin
      if not grapheOK then exit;
      NomFichierDebut := ChangeFileExt(ExtractFileName(NomFichier),'');
      trimAscii127(NomFichierDebut);
      NomFichierDebut := ExtractFilePath(NomFichier)+NomFichierDebut+suffixe;
{$IFDEF Debug}
 //  ajouteLigneLatex('\documentclass{article}');
 //  ajouteLigneLatex('\usepackage{tikz}');
  // ajouteLigneLatex('\begin{document}');
{$ENDIF}
      ajouteLigneLatex('');
      ajouteLigneLatex('\begin{tikzpicture}');
//      if (OgAnalyseurLogique in optionGraphe) then ;
(*
\usetikzlibrary{calc}

  \begin{axis}[name=plot1,height=3cm,width=3cm]
    \addplot coordinates {(0,0) (1,1) (2,2)};
  \end{axis}
  \begin{axis}[name=plot2,at={($(plot1.south)-(0,1cm)$)},anchor=north,height=3cm,width=3cm]
    \addplot coordinates {(0,2) (1,1) (2,0)};
  \end{axis}
  \begin{axis}[name=plot3,at={($(plot2.south)-(0,1cm)$)},anchor=north,height=3cm,width=3cm]
    \addplot coordinates {(0,2) (1,1) (2,1)};
  \end{axis}
  \begin{axis}[name=plot4,at={($(plot3.south)-(0,1cm)$)},anchor=north,height=3cm,width=3cm]
    \addplot coordinates {(0,2) (1,1) (1,0)};
  \end{axis}

*)
      avecAxeDroit := false;
      for i := 0 to Courbes.Count - 1 do
          with courbes[i] do if iMondeC=mondeDroit then begin
               avecAxeDroit := true;
               break;
          end;
      traceMonde(mondeY);
      if avecAxeDroit then traceMonde(mondeDroit);
      for i := 0 to pred(Dessins.Count) do
          dessins[i].drawLatex;
  //    for i := 0 to pred(equivalences[pageCourante].Count) do
         // equivalences[pageCourante].items[i].drawLatex(self,latexStr);
      ajouteLigneLatex('\end{tikzpicture}');
   {$IFDEF Debug}
 //     ajouteLigneLatex('\end{document}');
   {$ENDIF}
      ajouteLigneLatex('');
  end;// versLatex

procedure TGrapheReg.VersJPEG(NomFichier: string);
var
  AbitMap: TbitMap;
  Ajpeg: TjpegImage;
  sauveLimite: Trect;
  sauveCanvas : Tcanvas;
begin
  sauveLimite := limiteCourbe; // écran
  AbitMap := TbitMap.Create;
  AbitMap.Height := YmaxBitmap;
  AbitMap.Width := XmaxBitmap;
  AbitMap.pixelFormat := pf24bit;
  with limiteFenetre do begin
       top := 0;
       left := 0;
       bottom := YmaxBitmap;
       right := xmaxBitmap;
  end;
  sauveCanvas := canvas;
  imageEnCours := True;
  canvas := AbitMap.Canvas;
  Draw;
  imageEnCours := False;
  Ajpeg := TjpegImage.Create;
  Ajpeg.Assign(Abitmap);
  if NomFichier = ''
     then ClipBoard.Assign(Ajpeg)
     else Ajpeg.saveToFile(NomFichier);
  Ajpeg.Free;
  AbitMap.Free;
  LimiteCourbe := sauveLimite;
  canvas := sauveCanvas;
end;

procedure TGrapheReg.VersBitmap(NomFichier : string);
var
  AbitMap: TbitMap;
  sauveLimite: Trect;
  sauveCanvas : Tcanvas;
begin
  sauveLimite := limiteCourbe; // écran
  AbitMap := TbitMap.Create;
  AbitMap.Height := YmaxBitmap;
  AbitMap.Width := XmaxBitmap;
  AbitMap.pixelFormat := pf24bit;
  limiteFenetre := rect(0, 0, XmaxBitmap, YmaxBitmap);
  sauveCanvas := canvas;
  imageEnCours := True;
  canvas := AbitMap.Canvas;
  Draw;
  imageEnCours := False;
  if NomFichier = ''
     then ClipBoard.Assign(ABitmap)
     else ABitmap.saveToFile(NomFichier);
  AbitMap.Free;
  LimiteCourbe := sauveLimite;
  canvas := sauveCanvas;
end;

Procedure TgrapheReg.SetRepere;
var Dessin : Tdessin;
    i,cY : integer;
    y : double;
begin
   i := 0;
   while (i<dessins.count) do begin
       if (dessins[i].repere<>nil)
           then Dessins.remove(dessins[i])
           else inc(i);
   end;
   if pageCourante=0 then exit;
   with pages[pageCourante] do begin
   if coordonnee[1].codeX<>0 then exit;  // temps en abscisse obligatoire ?
   cY := coordonnee[1].codeY;
   if (cY=grandeurInconnue) or
      (grandeurs[cY].genreG<>variable) then exit;
   y := monde[coordonnee[1].iMondeC].maxi;
   for i := 0 to pred(ListeRepere.count) do begin
      Dessin := Tdessin.create(self);
      with Dessin, ListeRepere[i] do begin
        y1 := y;
        y2 := y;
        x1 := valeur;
        x2 := valeur;
        pen.style := TpenStyle(PstyleRepere);
        isTexte := true;
        IsOpaque := true;
        MotifTexte := mtVert;
        vertical := false;
        texte.add(text);
        pen.color := PColorRepere;
        repere := ListeRepere[i];
        numPage := pageCourante;
     end;
     dessins.add(Dessin);
   end;
end end;

procedure TDessin.SetPourCent(var i: integer);
  var
    posPourCent: integer;
    LigneTexte:  string;

    procedure SetLoc(Agrandeur: Tgrandeur; valeur: double; Nbre: integer);
    begin
      Delete(LigneTexte, posPourCent, Nbre);
      Insert(Agrandeur.formatNomEtUnite(valeur), LigneTexte, posPourCent);
    end;

    procedure SetParam(i,nbre : integer);
    begin
      Delete(LigneTexte, posPourCent, Nbre);
      Insert(pages[numeroPage].paramEtPrec(i,true), LigneTexte, posPourCent);
    end;

    procedure PosCode(code: string; numero: integer);
    begin
      code := '%' + code;
      if numero >= 0 then
         code := code + intToStr(numero);
      posPourCent := pos(code, AnsiUpperCase(LigneTexte));
    end;

  var
   j: integer;
   posEt: integer;
   LignePrec: string;
  begin
    LigneTexte := texteLoc[i];
    posCode('X', -1);
    if posPourCent > 0 then
      SetLoc(Agraphe.monde[mondeX].Axe, x1, 2);
    posCode('Y', -1);
    if posPourCent > 0 then
      SetLoc(Agraphe.monde[iMonde].Axe, y1, 2);
    posCode('S', -1);
    if posPourCent > 0 then begin
      Delete(LigneTexte, posPourCent, 3);
      Insert(pages[NumeroPage].commentaireP, LigneTexte, posPourCent);
    end;
    for j := 1 to NbreParam[ParamNormal] do begin
      posCode('P', j);
      if posPourCent > 0 then
        SetParam(j,3);
    end;
    for j := 1 to NbreModele do with FonctionTheorique[j] do begin
      posCode('M', j);
      if posPourCent > 0 then begin
         Delete(LigneTexte, posPourCent, 3);
         if ModeleNumerique
         then Insert(expressionNumerique, LigneTexte, posPourCent)
         else begin
         Insert(grandeurs[indexY].nom+'('+grandeurs[indexX].nom+')='+expression, LigneTexte, posPourCent);
         // cdot nécessite Arial Unicode
         // mais les lettres grecques sont atroces
         repeat
               posEt := pos('*',ligneTexte);
             //  if posEt>0 then ligneTexte[posEt] := pointMedian;
         until posEt=0;
         repeat
               posEt := pos('+',ligneTexte);
               if posEt>0 then  ligneTexte[posEt] := '@';
         until posEt=0;
         repeat
               posEt := pos('@',ligneTexte);
               if posEt>0 then begin
                  delete(ligneTexte,posET,1);
                  insert(' + ',ligneTexte,posEt);
               end;
         until posEt=0;
         end;
         if affichagePrecision
            then with pages[pageCourante] do LignePrec := stSigmaY+
                        addrY.formatNomEtUnite(sigmaY)
            else LignePrec := '';
      end;
    end;
    for j := 1 to NbreModeleGlb do with FonctionTheoriqueGlb[j] do begin
      posCode('M', j+decalageModeleGlb);
      if posPourCent > 0 then begin
         Delete(LigneTexte, posPourCent, 3);
         if ModeleNumerique
         then Insert(expressionNumerique, LigneTexte, posPourCent)
         else begin
         Insert(grandeurs[indexY].nom+'('+grandeurs[indexX].nom+')='+expression, LigneTexte, posPourCent);
         // cdot nécessite Arial Unicode
         // mais les lettres grecques sont atroces
         repeat
               posEt := pos('*',ligneTexte);
              // if posEt>0 then ligneTexte[posEt] := pointMedian;
         until posEt=0;
         repeat
               posEt := pos('+',ligneTexte);
               if posEt>0 then  ligneTexte[posEt] := '@';
         until posEt=0;
         repeat
               posEt := pos('@',ligneTexte);
               if posEt>0 then begin
                  delete(ligneTexte,posET,1);
                  insert(' + ',ligneTexte,posEt);
               end;
         until posEt=0;
         end;
      end;
    end;
    for j := 0 to pred(NbreConst) do begin
      posCode('C', j + 1);
      if posPourCent > 0 then
        setLoc(
          Grandeurs[indexConst[j]],
          Pages[NumeroPage].ValeurConst[indexConst[j]], 3);
    end;
    posPourCent := pos('%',LigneTexte);
    if (posPourCent > 0) and
       charInSet(LigneTexte[posPourCent+1],['P','M','C']) then Delete(LigneTexte, posPourCent, 3);
    texteLoc[i] := LigneTexte;
    if lignePrec<>'' then begin
       inc(i);
       TexteLoc.insert(i,lignePrec)
    end;
end; // SetPourCent

procedure TGrapheReg.VersFichier(NomFichier : string);
var extension : string;
begin
try
     extension := AnsiUpperCase(extractFileExt(nomFichier));
     if extension='' then begin
         nomFichier := ChangeFileExt(nomFichier,'.WMF');
         extension := '.WMF';
     end;
     if extension='.BMP'
             then versBitmap(nomFichier)
                 else if (extension='.JPG')
                     then VersJpeg(nomFichier)
  except
  end;
end;

procedure TgrapheReg.VersPressePapier(grapheCB : TgrapheClipBoard);
begin
     case grapheCB of
          gcBitmap : VersBitmap('');
          gcJpeg : VersJpeg('');
     end;
end;

procedure TgrapheReg.getIndexSonagramme(t,f : double;var it,ifreq : integer;var freq : double);
var tmax,tmin : double;
    iMax,k : integer;
begin with courbes[0] do begin
      iMax := NbreSonagramme*nbreSonagrammeAff;
      tmax := valX[imax];
      tmin := valX[0];
      it := round(imax*t/(tmax-tmin)/NbreSonagrammeAff);
      iMax := round(FreqMaxSonagramme/pasFreqSonagramme);
      tmax := valY[iMax];
      tmin := 0;
      ifreq := round(imax*f/(tmax-tmin));
      // pointe sur son[it,ifreq]
      tmax := son[it,ifreq];
      freq := valY[ifreq];
      for k := 1 to 3 do begin
          if (iFreq+k<imax) and (son[it,ifreq+k]>tmax) then begin
             tmax :=  son[it,ifreq+k];
             freq := valY[ifreq+k];
          end;
          if (iFreq-k>0) and (son[it,ifreq-k]>tmax) then begin
             tmax :=  son[it,ifreq-k];
             freq := valY[ifreq-k];
          end;
      end;
end end;

{$I equival.pas}
{$I axes.pas}

end.
