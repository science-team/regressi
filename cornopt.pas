{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit cornopt;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, Spin, ExtCtrls, regUtil, compile, aideKey;

type
  TCornishOptDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    HelpBtn: TBitBtn;
    ClasseGroupe: TRadioGroup;
    NombreSpin: TSpinEdit;
    LabelNombre: TLabel;
    EditCibleK: TEdit;
    CibleCB: TCheckBox;
    NomGroupe: TGroupBox;
    ConcentrationListe: TListBox;
    GroupBox1: TGroupBox;
    int2SigmaCB: TCheckBox;
    int3SigmaCB: TCheckBox;
    t95CB: TCheckBox;
    t99CB: TCheckBox;
    GroupBox2: TGroupBox;
    VitesseListe: TListBox;
    MoyenneCB: TCheckBox;
    MedianeCB: TCheckBox;
    EditCibleV: TEdit;
    LabelCibleK: TLabel;
    LabelCibleV: TLabel;
    procedure ClasseGroupeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure Modif(Sender: TObject);
  private
     procedure setLabel;
  public
     modifCoord : boolean;
     procedure SetVitesseConcentration(V,C : string);
  end;

var
  CornishOptDlg: TCornishOptDlg;

implementation

  {$R *.lfm}

Procedure TCornishOptDlg.setLabel;
begin
     EditCibleK.visible := cibleCB.checked;
     LabelCibleK.visible := cibleCB.checked;
     EditCibleV.visible := cibleCB.checked;
     LabelCibleV.visible := cibleCB.checked;
     LabelNombre.visible := false;
     NombreSpin.visible := false;
     case ClasseGroupe.ItemIndex of
          0 : ;
          1 : begin
             NombreSpin.visible := true;
             LabelNombre.visible := true;
          end;
          2 : begin
          end;
     end;
end;

procedure TCornishOptDlg.ClasseGroupeClick(Sender: TObject);
begin
     SetLabel
end;

procedure TCornishOptDlg.FormActivate(Sender: TObject);
begin
     inherited;
     modifCoord := false;
     setLabel;
end;

procedure TCornishOptDlg.SetVitesseConcentration(V,C : string);
var i : integer;
begin
     ConcentrationListe.Items.Clear;
     VitesseListe.Items.Clear;
     for i := 0 to pred(NbreGrandeurs) do with grandeurs[i] do
         if genreG=variable then begin
             ConcentrationListe.Items.add(nom);
             if nom=C then ConcentrationListe.itemIndex := i;
             VitesseListe.Items.add(nom);
             if nom=V then VitesseListe.itemIndex := i;
         end;
     if ConcentrationListe.itemIndex<0
        then ConcentrationListe.itemIndex := 0;
     if VitesseListe.itemIndex<0
        then VitesseListe.itemIndex := 1;
end;

procedure TCornishOptDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(HELP_MethodedeCornishBowden)
end;

procedure TCornishOptDlg.Modif(Sender: TObject);
begin
     ModifCoord := true
end;

end.
