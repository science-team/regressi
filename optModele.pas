{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit optModele;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons, colorbox,
  StdCtrls, Spin, ExtCtrls, SysUtils, Dialogs, ComCtrls,
  regutil, compile, graphker, options, aideKey;

type
  TOptionModeleDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    HelpBtn: TBitBtn;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Chi2CB: TCheckBox;
    LevenbergCB: TCheckBox;
    RecuitCB: TCheckBox;
    TabSheet1: TTabSheet;
    UnitParamCB: TCheckBox;
    AjusteOrigineCB: TCheckBox;
    GroupBox1: TGroupBox;
    AfficheCorrel: TCheckBox;
    LabelCorrel: TLabel;
    ChiffreCorrelSE: TSpinEdit;
    EchelleModeleCB: TCheckBox;
    ExtrapoleModeleCB: TCheckBox;
    IncertitudeCB: TCheckBox;
    VisuAjusteCB: TCheckBox;
    BandeConfianceCB: TCheckBox;
    AffichePvaleur: TCheckBox;
    OptionsBtn: TSpeedButton;
    ModelePagesIndependantesCB: TCheckBox;
    ModeleManuelCB: TCheckBox;
    TabSheet3: TTabSheet;
    Label5: TLabel;
    ColorBox1: TColorBox;
    Label12: TLabel;
    ColorBox8: TColorBox;
    Label6: TLabel;
    ColorBox7: TColorBox;
    Label13: TLabel;
    ColorBox6: TColorBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    ColorBox2: TColorBox;
    ColorBox3: TColorBox;
    ColorBox4: TColorBox;
    ColorBox5: TColorBox;
    IncertitudeHelpBtn: TSpeedButton;
    IncertParamRG: TRadioGroup;
    ModelePourCentCB: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure AfficheCorrelClick(Sender: TObject);
    procedure OptionsBtnClick(Sender: TObject);
    procedure ColorBox1Change(Sender: TObject);
    procedure IncertitudeHelpBtnClick(Sender: TObject);
  private
  public
    DlgGraphique : TgrapheReg;
  end;

var
  OptionModeleDlg: TOptionModeleDlg;

implementation

uses optcolordlg, AideIncertitudeU;

  {$R *.lfm}

procedure TOptionModeleDlg.FormActivate(Sender: TObject);
var i,j : integer;
begin
     inherited;
     EchelleModeleCB.checked := OmEchelle in DlgGraphique.optionModele;
     ExtrapoleModeleCB.checked := OmExtrapole in DlgGraphique.optionModele;
     IncertitudeCB.checked := avecEllipse;
     ajusteOrigineCB.Checked := ajusteOrigine;
     AfficheCorrel.checked := withCoeffCorrel;
     AffichePvaleur.checked := withPvaleur;
     BandeConfianceCB.checked := withBandeConfiance;
     ChiffreCorrelSE.value := PrecisionCorrel;
     LevenbergCB.checked := LevenbergMarquardt;
     UnitParamCB.checked := ChercheUniteParam;
     VisuAjusteCB.checked := VisualisationAjustement;     
     RecuitCB.checked := Recuit;
     chi2CB.checked := avecChi2 and chi2CB.visible;
     optionsBtn.Visible := optionsPermises;
     modelePourCentCB.Checked := modelePourCent;
     modeleManuelCB.checked := avecModeleManuel;
     incertParamRG.itemIndex := ord(AffIncertParam);
     with TabSheet3 do
        for i := 0 to pred(controlCount) do
           if controls[i] is TcolorBox then begin
              j := TcolorBox(controls[i]).tag;
              TcolorBox(controls[i]).selected := couleurModele[j];
           end;
end;

procedure TOptionModeleDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(HELP_OptionsdeModelisation)
end;

procedure TOptionModeleDlg.IncertitudeHelpBtnClick(Sender: TObject);
begin
 if AideIncertitudeForm=nil then
     Application.CreateForm(TAideIncertitudeForm, AideIncertitudeForm);
  AideIncertitudeForm.pageControl1.activePage := tabsheet2;
  AideIncertitudeForm.show;
end;

procedure TOptionModeleDlg.OKBtnClick(Sender: TObject);
begin
    if EchelleModeleCB.checked
        then include(DlgGraphique.OptionModele,OmEchelle)
        else exclude(DlgGraphique.OptionModele,OmEchelle);
    if ExtrapoleModeleCB.checked
        then include(DlgGraphique.OptionModele,OmExtrapole)
        else exclude(DlgGraphique.OptionModele,OmExtrapole);
    avecEllipse := IncertitudeCB.checked;
    VisualisationAjustement := VisuAjusteCB.checked;
    withCoeffCorrel := AfficheCorrel.checked;
    withPvaleur := AffichePvaleur.checked;
    withBandeConfiance := BandeConfianceCB.checked;
    PrecisionCorrel := ChiffreCorrelSE.value;
    avecChi2 := chi2CB.checked;
    if avecChi2 then avecEllipse := true;
    LevenbergMarquardt := LevenbergCB.checked;
    Recuit := RecuitCB.checked;
    ChercheUniteParam := UnitparamCB.checked;
    modelePourCent := modelePourCentCB.checked;
    ajusteOrigine := ajusteOrigineCB.Checked;
    avecModeleManuel := modeleManuelCB.checked;
    AffIncertParam := TAffIncertParam(incertParamRG.itemIndex);
end;

procedure TOptionModeleDlg.AfficheCorrelClick(Sender: TObject);
begin
  inherited;
  ChiffreCorrelSE.visible := AfficheCorrel.Checked;
  LabelCorrel.visible := AfficheCorrel.Checked;
end;

procedure TOptionModeleDlg.OptionsBtnClick(Sender: TObject);
begin
   OptionCouleurDlg := TOptionCouleurDlg.create(self);
   OptionCouleurDlg.DlgGraphique := nil;
   OptionCouleurDlg.ShowModal;
   OptionCouleurDlg.free;
end;

procedure TOptionModeleDlg.ColorBox1Change(Sender: TObject);
begin
     with sender as TcolorBox do
        CouleurModele[tag] := selected
end;

end.
