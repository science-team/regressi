{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit varkeyb;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, Grids, sysUtils,
  constreg, regutil, maths, uniteker, graphker, compile,
  valeurs, graphvar, aideKey;

type
  TNewClavierDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    HelpBtn: TBitBtn;
    GroupBox1: TGroupBox;
    GridVariab: TStringGrid;
    ConstGB: TGroupBox;
    GridConst: TStringGrid;
    MemoGB: TGroupBox;
    MemoClavier: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    IncrementAutoCB: TCheckBox;
    Label3: TLabel;
    TriCB: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GridVariabKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure HelpBtnClick(Sender: TObject);
    procedure IncrementAutoCBClick(Sender: TObject);
  private
  public
    grapheMinMax : boolean;
  end;

var
  NewClavierDlg: TNewClavierDlg;

implementation

uses Regmain;

  {$R *.lfm}

procedure TNewClavierDlg.FormCreate(Sender: TObject);
var i,Acol,Arow : integer;
begin
     GridConst.cells[0,0] := stSymbole;
     GridConst.cells[1,0] := stUnite;
     GridConst.cells[2,0] := stSignif;
     with GridVariab do begin
         colWidths[0] := 60;
         colWidths[1] := 60;
         colWidths[2] := 150;
         colWidths[3] := 100;
         colWidths[4] := 100;
         cells[0,0] := stSymbole;
         cells[2,0] := stSignif;
         cells[1,0] := stUnite;
         cells[3,0] := stMinimum;
         cells[4,0] := stMaximum;
         for i := 1 to 4 do begin
             cells[3,i] := '0';
             cells[4,i] := ''; // ou '1' ?
         end;
         for Acol := 0 to pred(colCount) do
             for Arow := 0 to pred(rowCount) do
                 cells[col,row] := '';
     end;
     with gridConst do begin
         colWidths[0] := 60;
         colWidths[1] := 60;
         colWidths[2] := 150;
         cells[0,0] := stSymbole;
         cells[1,0] := stUnite;
         cells[2,0] := stSignif;
         for Acol := 0 to pred(colCount) do
             for Arow := 0 to pred(rowCount) do
               cells[col,row] := '';
        col := 0;
        row := 1;
     end;
end;

procedure TNewClavierDlg.OKBtnClick(Sender: TObject);
var indexMondeMax : integer;

Function Ajoute(G : TstringGrid;i : integer) : boolean;
var j : integer;
    u : Tunite;
    uniteOK : boolean;
    v : codeGrandeur;
    newNom : String;
    indexMonde : integer;
begin
Result := false;
with G do if cells[0,i]<>''
       then begin
            NewNom := cells[0,i];
            trimComplet(NewNom);
            if NomCorrect(newNom,grandeurInconnue)
               then begin
                   U := Tunite.create;
                   U.nomUnite := cells[1,i];
                   UniteOK := U.correct;
                   if pos('°',U.nomUnite)>0 then AngleEnDegre := true;
                   if pos('rad',U.nomUnite)>0 then AngleEnDegre := false;
                   U.free;
                   if not UniteOK and not OKReg(OkUniteInconnue,HELP_Unites) then begin
                       row := i;
                       col := 1;
                       resetEnTete;
                       exit;
                   end;
                   if G=GridVariab
                      then with FgrapheVariab.Graphes[1] do begin
                          indexMonde := indexMondeMax;
                          for j := 2 to pred(i) do
                              if cells[1,i]=cells[1,j] then
                                   indexMonde := Coordonnee[pred(j)].iMondeC;
                          with monde[indexMonde] do begin
                            Graduation := gLin;                          
                            try
                              setMinMaxDefaut(getFloat(cells[3,i]),
                                              getFloat(cells[4,i]));
                            except
                              if i<=2 then grapheMinMax := false;
                            end;
                         end;
                         if i=1
                            then for j := 1 to 4 do
                                 Coordonnee[j].nomX := NewNom
                            else begin
                                 Coordonnee[pred(i)].nomY := NewNom;
                                 Coordonnee[pred(i)].iMondeC := indexMonde;
                            end;
                         if indexMonde=indexMondeMax then inc(indexMondeMax);
                         v := ajouteExperimentale(newNom,variable);
                         grandeurs[v].fonct.expression := cells[2,i];
                         monde[indexMonde].axe := grandeurs[v];
                      end
                      else v := ajouteExperimentale(newNom,constante);
                   grandeurs[v].NomUnite := cells[1,i];
                   if '°'=Grandeurs[v].nomUnite then AngleEnDegre := true;
                   if 'rad'=Grandeurs[v].nomUnite then AngleEnDegre := false;
               end
               else begin
                   resetEnTete;
                   afficheErreur(codeErreurC,0);
                   row := i;
                   col := 0;
                   exit;
               end;
        end;
    Result := true;
end;

var i : integer;
begin
     if not FregressiMain.verifSauve then begin
        modalResult := mrCancel;
        exit;
     end;
     grapheMinMax := true;
     FichierTrie := not TriCB.checked;
     NomFichierData := '';
     ModeAcquisition := AcqClavier;
     indexMondeMax := mondeX;
     for i := 1 to 4 do if not Ajoute(gridVariab,i) then begin
         modalResult := mrNone;
         gridVariab.setFocus;
         exit;
     end;
     for i := 1 to 2 do if not Ajoute(gridConst,i) then begin
         modalResult := mrNone;
         gridConst.setFocus;
         exit;
     end;
     for i := 0 to pred(MemoClavier.Lines.Count) do
         if MemoClavier.lines[i]<>'' then
            Fvaleurs.Memo.Lines.Add(''''+MemoClavier.lines[i]);
     modalResult := mrOK;            
end;

procedure TNewClavierDlg.FormActivate(Sender: TObject);
begin
     inherited;
     gridVariab.col := 0;
     gridVariab.row := 1;
     gridConst.col := 0;
     gridConst.row := 1;
     TriCB.Checked := not DataTrieGlb;
     MemoClavier.clear;
     GridVariab.SetFocus;
end;

procedure TNewClavierDlg.GridVariabKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    (* if key=vk_return then  with (sender as TstringGrid) do
        if col<4
           then col := col+1
           else if (row+1)<rowCount then begin
                row := row+1;
                col := 0;
           end  *)
end;

procedure TNewClavierDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(HELP_EntreedeDonneesauClavier)
end;

procedure TNewClavierDlg.IncrementAutoCBClick(Sender: TObject);
begin
    autoIncrementation := IncrementAutoCB.checked
end;

end.
